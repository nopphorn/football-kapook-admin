	<script language="JavaScript">
	
	var stack_bar_top = {"dir1": "down", "dir2": "right", "push": "top", "spacing1": 0, "spacing2": 0};
	
	function show_stack_bar_top(type,title,text) {
		var opts = {
		
			title: "Over Here",
			text: "Check me out. I'm in a different stack.",
			addclass: "stack-bar-top",
			cornerclass: "",
			width: "100%",
			stack: stack_bar_top,mouse_reset: false
			
		};
		switch (type) {
		case 'error':
			opts.type 	= 	"error";
			break;
		case 'info':
			opts.type 	=	"info";
			break;
		case 'success':
			opts.type 	= 	"success";
			break;
		}
		
		opts.title 		=	title;
		opts.text 		= 	text;
		
		new PNotify(opts);
	}
	
	$(document).ready(function () {
		$(".submit_new_round").click(function(e){
			$('#myModal').modal('hide');
		});
			
		<?php if($_REQUEST['success_create_comp']==1){ ?>
		show_stack_bar_top('success','สำเร็จ','ระบบได้สร้างการแข่งขันใหม่แล้ว');
		<?php } ?>
			
		<?php if($_REQUEST['error_create_comp']==1){ ?>
		show_stack_bar_top('error','ไม่สำเร็จ','ระบบไม่สามารถสร้างการแข่งขันใหม่ได้');
		<?php } ?>
			
		<?php if($_REQUEST['success_delete_comp']==1){ ?>
		show_stack_bar_top('success','สำเร็จ','ระบบได้ลบการแข่งขันแล้ว');
		<?php } ?>
			
		<?php if($_REQUEST['error_delete_comp']==1){ ?>
		show_stack_bar_top('error','ไม่สำเร็จ','ระบบไม่สามารถลบการแข่งขันได้');
		<?php } ?>
			
		<?php if($_REQUEST['success_update_comp']==1){ ?>
		show_stack_bar_top('success','สำเร็จ','ระบบได้ปรับปรุงการแข่งขันแล้ว');
		<?php } ?>
			
		<?php if($_REQUEST['error_update_comp']==1){ ?>
		show_stack_bar_top('error','ไม่สำเร็จ','ระบบไม่สามารถปรับปรุงการแข่งขันได้');
		<?php } ?>
		
		<?php if($_REQUEST['success_refresh_table']==1){ ?>
		show_stack_bar_top('success','สำเร็จ','ระบบได้ refresh ตารางคะแนนแล้ว');
		<?php } ?>
			
	});
	
	$('body').on('hidden.bs.modal', '.modal', function () {
		$(this).removeData('bs.modal');
	});
	
	</script>
</body>
</html>