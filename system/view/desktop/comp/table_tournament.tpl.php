<style>
.btn-primary:hover{
  color:#337ab7
}
</style>

<?php

if($this->dataRound) {
	$round_id	=	$this->dataRound['id'];
}else{
	$round_id	=	-1;
}

function final_print($compID,$tableID,$roundID,$index_round,$tmpIdTeam,$finalMatch,$thirdMatch = null,$index_final_match = 0,$index_third_match = 1){ ?>
	<td class="round_column r_2 final">
		<div class="mtch_container">
			<div class="match_unit">
				<div class="m_segment m_top <?php
					if($finalMatch['TeamWinner'] == 1) { echo 'winner'; }
					else { echo 'loser'; }
					?>" data-team-id="<?php
					if($finalMatch['Team1KPID'] > 0) { echo $finalMatch['Team1KPID']; }
					else{ echo $tmpIdTeam; $tmpIdTeam--; }
				?>">
					<span>
						<a href="<?php echo $finalMatch['Team1URL']; ?>">
							<?php if(strlen($finalMatch['Team1Logo'])){ ?>
							<img src="<?php echo $finalMatch['Team1Logo']; ?>" style="height: 25px;"/>
							<?php }else{ ?>
							<img src="http://football.kapook.com/uploads/logo/default.png" style="height: 25px;"/>
							<?php } ?>
							<span><?php echo $finalMatch['Team1']; ?></span>
						</a>
						<strong><?php echo $finalMatch['Team1FTScore']; ?></strong>
					</span>
				</div>
				<div class="m_segment m_botm <?php
					if($finalMatch['TeamWinner'] == 2) { echo 'winner'; }
					else { echo 'loser'; }
					?>" data-team-id="<?php
					if($finalMatch['Team2KPID'] > 0) { echo $finalMatch['Team2KPID']; }
					else{ echo $tmpIdTeam; $tmpIdTeam--; }
				?>">
					<span>
						<a href="<?php echo $finalMatch['Team2URL']; ?>">
							<?php if(strlen($finalMatch['Team2Logo'])){ ?>
							<img src="<?php echo $finalMatch['Team2Logo']; ?>" style="height: 25px;"/>
							<?php }else{ ?>
							<img src="http://football.kapook.com/uploads/logo/default.png" style="height: 25px;"/>
							<?php } ?>
							<span><?php echo $finalMatch['Team2']; ?></span>
						</a>
						<strong><?php echo $finalMatch['Team2FTScore']; ?></strong>
					</span>
				</div>
				<div class="m_dtls">
					<?php if($roundID > 0){ ?>
					<span><a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/table_round_tournament_match/<?php echo $compID; ?>/<?php echo $tableID; ?>/<?php echo $roundID; ?>/<?php echo $index_round; ?>/<?php echo $index_final_match; ?>" type="button" class="btn btn-primary glyphicon glyphicon-pencil" role="button" ></a></span>
					<?php }else{ ?>
					<span><a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/table_tournament_match/<?php echo $compID; ?>/<?php echo $tableID; ?>/<?php echo $index_round; ?>/<?php echo $index_final_match; ?>" type="button" class="btn btn-primary glyphicon glyphicon-pencil" role="button" ></a></span>
					<?php } ?>
				</div>
			</div>
		</div>
		<?php if($thirdMatch!=null){ ?>
		<div class="mtch_container third_position">
			<div class="match_unit">
				<div class="m_dtls">
					<?php if($roundID > 0){ ?>
					<span><a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/table_round_tournament_match/<?php echo $compID; ?>/<?php echo $tableID; ?>/<?php echo $roundID; ?>/<?php echo $index_round; ?>/<?php echo $index_third_match; ?>" type="button" class="btn btn-primary glyphicon glyphicon-pencil" role="button" ></a></span>
					<?php }else{ ?>
					<span><a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/table_tournament_match/<?php echo $compID; ?>/<?php echo $tableID; ?>/<?php echo $index_round; ?>/<?php echo $index_third_match; ?>" type="button" class="btn btn-primary glyphicon glyphicon-pencil" role="button" ></a></span>
					<?php } ?>
				</div>
				<div class="m_segment m_top <?php
					if($thirdMatch['TeamWinner'] == 1) { echo 'winner'; }
					else { echo 'loser'; }
					?>" data-team-id="<?php
					if($thirdMatch['Team1KPID'] > 0) { echo $thirdMatch['Team1KPID']; }
					else{ echo $tmpIdTeam; $tmpIdTeam--; }
				?>">
					<span>
						<a href="<?php echo $thirdMatch['Team1URL']; ?>">
							<?php if(strlen($thirdMatch['Team1Logo'])){ ?>
							<img src="<?php echo $thirdMatch['Team1Logo']; ?>" style="height: 25px;"/>
							<?php }else{ ?>
							<img src="http://football.kapook.com/uploads/logo/default.png" style="height: 25px;"/>
							<?php } ?>
							<span><?php echo $thirdMatch['Team1']; ?></span>
						</a>
						<strong><?php echo $thirdMatch['Team1FTScore']; ?></strong>
					</span>
				</div>
				<div class="m_segment m_botm <?php
					if($thirdMatch['TeamWinner'] == 2) { echo 'winner'; }
					else { echo 'loser'; }
					?>" data-team-id="<?php
					if($thirdMatch['Team2KPID'] > 0) { echo $thirdMatch['Team2KPID']; }
					else{ echo $tmpIdTeam; $tmpIdTeam--; }
				?>">
					<span>
						<a href="<?php echo $thirdMatch['Team2URL']; ?>">
							<?php if(strlen($thirdMatch['Team2Logo'])){ ?>
							<img src="<?php echo $thirdMatch['Team2Logo']; ?>" style="height: 25px;"/>
							<?php }else{ ?>
							<img src="http://football.kapook.com/uploads/logo/default.png" style="height: 25px;"/>
							<?php } ?>
							<span><?php echo $thirdMatch['Team2']; ?></span>
						</a>
						<strong><?php echo $thirdMatch['Team2FTScore']; ?></strong>
					</span>
				</div>
			</div>
		</div>
		<?php } ?>
	</td>
<?php
	return $tmpIdTeam;
}

function round_print($dataRound,$class_round,$size_match,$offset_match,$compID,$tableID,$roundID,$index_round,$tmpIdTeam){ ?>
	<td class="round_column <?php echo $class_round; ?>">
		<?php for( $i=$offset_match ; $i<($offset_match+$size_match) ; $i++) {
			$tmpMatch = $dataRound[$i];
			?>
			<div class="mtch_container">
				<div class="match_unit">
					<div class="m_segment m_top <?php
						if($tmpMatch['TeamWinner'] == 1) { echo 'winner'; }
						else { echo 'loser'; }
						?>" data-team-id="<?php
						if($tmpMatch['Team1KPID'] > 0) { echo $tmpMatch['Team1KPID']; }
						else{ echo $tmpIdTeam; $tmpIdTeam--; }
					?>">
						<span>
							<a href="<?php echo $tmpMatch['Team1URL']; ?>">
								<?php if(strlen($tmpMatch['Team1Logo'])){ ?>
								<img src="<?php echo $tmpMatch['Team1Logo']; ?>" style="height: 25px;"/>
								<?php }else{ ?>
								<img src="http://football.kapook.com/uploads/logo/default.png" style="height: 25px;"/>
								<?php } ?>
								<span><?php echo $tmpMatch['Team1']; ?></span>
							</a>
							<strong><?php echo $tmpMatch['Team1FTScore']; ?></strong>
						</span>
					</div>
					<div class="m_segment m_botm <?php
						if($tmpMatch['TeamWinner'] == 2) { echo 'winner'; }
						else { echo 'loser'; }
						?>" data-team-id="<?php
						if($tmpMatch['Team2KPID'] > 0) { echo $tmpMatch['Team2KPID']; }
						else{ echo $tmpIdTeam; $tmpIdTeam--; }
					?>">
						<span>
							<a href="<?php echo $tmpMatch['Team2URL']; ?>">
								<?php if(strlen($tmpMatch['Team2Logo'])){ ?>
								<img src="<?php echo $tmpMatch['Team2Logo']; ?>" style="height: 25px;"/>
								<?php }else{ ?>
								<img src="http://football.kapook.com/uploads/logo/default.png" style="height: 25px;"/>
								<?php } ?>
								<span><?php echo $tmpMatch['Team2']; ?></span>
							</a>
							<strong><?php echo $tmpMatch['Team2FTScore']; ?></strong>
						</span>
					</div>
					<div class="m_dtls">
					<?php if($roundID > 0){ ?>
						<span><a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/table_round_tournament_match/<?php echo $compID; ?>/<?php echo $tableID; ?>/<?php echo $roundID; ?>/<?php echo $index_round; ?>/<?php echo $i; ?>" type="button" class="btn btn-primary glyphicon glyphicon-pencil" role="button" ></a></span>
					<?php }else{ ?>
						<span><a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/table_tournament_match/<?php echo $compID; ?>/<?php echo $tableID; ?>/<?php echo $index_round; ?>/<?php echo $i; ?>" type="button" class="btn btn-primary glyphicon glyphicon-pencil" role="button" ></a></span>
					<?php } ?>
					</div>
				</div>
			</div>
		<?php } ?>
	</td>
<?php
	return $tmpIdTeam;
} ?>

<div class="container">
	<form action="<?php echo BASE_HREF; ?>api/adminfootball/comp/<?php
		if($this->dataRound){
			if(isset($this->dataTable)) {
				echo 'update_table_round_tournament';
			}else{
				echo 'create_table_round_tournament';
			}
		}else{
			if(isset($this->dataTable)) {
				echo 'update_table_tournament';
			}else{
				echo 'create_table_tournament';
			}
		}
	?>" method="POST">
		<input type="hidden" name="comp_id" value="<?php echo $this->dataComp['id']; ?>">
		<?php if(isset($this->dataTable)) {
			?><input type="hidden" name="id" value="<?php echo $this->dataTable['id']; ?>">
		<?php } ?>
		<?php if($this->dataRound) { ?>
			<input type="hidden" name="round_id" value="<?php echo $this->dataRound['id']; ?>">
		<?php }else{ ?>
			<input type="hidden" name="round_id" value="-1">
		<?php } ?>
		<div class="form-group">
			<label >Name Table</label>
			<input type="text" class="form-control" id="name" name="name" value="<?php if(isset($this->dataTable)) { echo $this->dataTable['name']; }?>" placeholder="ชื่อตารางคะแนน">
		</div>
		<?php if(!isset($this->dataTable)){ ?>
			<label >จำนวนทีมที่แข่งขัน</label>
			<div class="caption">
				<input type="radio" name="SizeTeam" value="4" > 4 ทีม
				<input type="radio" name="SizeTeam" value="8" > 8 ทีม
				<input type="radio" name="SizeTeam" value="16" checked > 16 ทีม
			</div>
			
			<label >มีนัดชิงที่ 3 ไหม?</label>
			<div class="caption">
				<input type="radio" name="isThird" value="0" checked> ไม่มี
				<input type="radio" name="isThird" value="1" > มี
			</div>
		<?php } ?>
		<div class="row form-group"><center>
			<div class="col-sm-6 col-md-6"><button type="submit" class="btn btn-primary btn-lg btn-block"><?php if(isset($this->dataTable)) { ?>บันทึกตารางคะแนน<?php }else{ ?>สร้างตารางคะแนนใหม่<?php } ?></button></div>
			<div class="col-sm-6 col-md-6"><a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/table/<?php echo $this->dataComp['id']; ?>" class="btn btn-default btn-lg btn-block">กลับไปหน้ารวมตารางคะแนน</a></div>
		</center></div>
		<?php if(isset($this->dataTable)){ ?>
			<div class="brackets_container">
				<table>
					<!--rounds container-->
					<thead><tr>
						<?php if(count($this->dataTable['RoundData'])>=4){ ?>
						<th>
							<span>รอบ 16 ทีมสุดท้าย</span>
						</th>
						<?php }
						if(count($this->dataTable['RoundData'])>=3){ ?>
						<th>
							<span>รอบก่อนรองชนะเลิศ</span>
						</th>
						<?php }
						if(count($this->dataTable['RoundData'])>=2){ ?>
						<th>
							<span>รอบรองชนะเลิศ</span>
						</th>
						<?php }
						if(count($this->dataTable['RoundData'])>=1){ ?>
						<th>
							<span>รอบชิงชนะเลิศ</span>
						</th>
						<?php }
						if(count($this->dataTable['RoundData'])>=2){ ?>
						<th>
							<span>รอบรองชนะเลิศ</span>
						</th>
						<?php }
						if(count($this->dataTable['RoundData'])>=3){ ?>
						<th>
							<span>รอบก่อนรองชนะเลิศ</span>
						</th>
						<?php }
						if(count($this->dataTable['RoundData'])>=4){ ?>
						<th>
							<span>รอบ 16 ทีมสุดท้าย</span>
						</th>
						<?php } ?>
					</tr></thead>
					<!--matches container-->
					<?php
						$index_round 	= 	0;
						$tmpIdTeam		=	-2;
					?>
					<tbody><tr id="playground">
						<!-- Round of 16 -->
						<?php if(count($this->dataTable['RoundData'])>=4){
							$tmpRound = $this->dataTable['RoundData'][$index_round];
							$tmpIdTeam = round_print($tmpRound,"r_16",4,0,$this->dataComp['id'],$this->dataTable['id'],$round_id,$index_round,$tmpIdTeam);
							$index_round++;
						} ?>
						<!-- Round of 8 -->
						<?php if(count($this->dataTable['RoundData'])>=3){
							$tmpRound = $this->dataTable['RoundData'][$index_round];
							$tmpIdTeam = round_print($tmpRound,"r_8",2,0,$this->dataComp['id'],$this->dataTable['id'],$round_id,$index_round,$tmpIdTeam);
							$index_round++;
						} ?>
						<!-- Round of 4 -->
						<?php if(count($this->dataTable['RoundData'])>=2){
							$tmpRound = $this->dataTable['RoundData'][$index_round];
							$tmpIdTeam = round_print($tmpRound,"r_4",1,0,$this->dataComp['id'],$this->dataTable['id'],$round_id,$index_round,$tmpIdTeam);
							$index_round++;
						} ?>
						<!-- Final & 3rd -->
						<?php if(count($this->dataTable['RoundData'])>=1){
							$tmpRound = $this->dataTable['RoundData'][$index_round];
							if(isset($tmpRound[1])){
								$tmpIdTeam = final_print($this->dataComp['id'],$this->dataTable['id'],$round_id,$index_round,$tmpIdTeam,$tmpRound[0],$tmpRound[1]);
							}else{
								$tmpIdTeam = final_print($this->dataComp['id'],$this->dataTable['id'],$round_id,$index_round,$tmpIdTeam,$tmpRound[0]);
							}
							$index_round--;
						} ?>
						<!-- Round of 4 -->
						<?php if(count($this->dataTable['RoundData'])>=2){
							$tmpRound = $this->dataTable['RoundData'][$index_round];
							$tmpIdTeam = round_print($tmpRound,"r_4 reversed",1,1,$this->dataComp['id'],$this->dataTable['id'],$round_id,$index_round,$tmpIdTeam);
							$index_round--;
						} ?>
						<!-- Round of 8 -->
						<?php if(count($this->dataTable['RoundData'])>=3){
							$tmpRound = $this->dataTable['RoundData'][$index_round];
							$tmpIdTeam = round_print($tmpRound,"r_8 reversed",2,2,$this->dataComp['id'],$this->dataTable['id'],$round_id,$index_round,$tmpIdTeam);
							$index_round--;
						} ?>
						<!-- Round of 16 -->
						<?php if(count($this->dataTable['RoundData'])>=4){
							$tmpRound = $this->dataTable['RoundData'][$index_round];
							$tmpIdTeam = round_print($tmpRound,"r_16 reversed",4,4,$this->dataComp['id'],$this->dataTable['id'],$round_id,$index_round,$tmpIdTeam);
							//$index_round--;
						} ?>
					</tr></tbody>
				</table>
			</div>
		<?php } ?>
	</form>
</div>