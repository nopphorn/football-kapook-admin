<div class="container" style="padding-bottom: 60px;">
	<form role="form" action="<?php echo BASE_HREF; ?>api/adminfootball/comp/update_news" method="POST">
		<input type="hidden" name="id" value="<?php echo $this->dataComp['id']; ?>">
		
		<div class="form-group">
			<label for="kapookfootball_News_Country" class="control-label">ข่าวของประเทศที่ต้องการ (kapookfootball_News_Country)</label>
			<div>
				<label class="checkbox-inline">
					<input type="checkbox" id="nation1" name="kapookfootball_News_Country[]" value="1" <? if(in_array('1', $this->dataComp['kapookfootball_News_Country'])){ echo 'checked'; } ?> > อังกฤษ
				</label>
				<label class="checkbox-inline">
					<input type="checkbox" id="nation2" name="kapookfootball_News_Country[]" value="2" <? if(in_array('2', $this->dataComp['kapookfootball_News_Country'])){ echo 'checked'; } ?> > อิตาลี
				</label>
				<label class="checkbox-inline">
					<input type="checkbox" id="nation3" name="kapookfootball_News_Country[]" value="3" <? if(in_array('3', $this->dataComp['kapookfootball_News_Country'])){ echo 'checked'; } ?> > สเปน
				</label>
				<label class="checkbox-inline">
					<input type="checkbox" id="nation4" name="kapookfootball_News_Country[]" value="4" <? if(in_array('4', $this->dataComp['kapookfootball_News_Country'])){ echo 'checked'; } ?> > เยอรมันนี
				</label>
				<label class="checkbox-inline">
					<input type="checkbox" id="nation10" name="kapookfootball_News_Country[]" value="10" <? if(in_array('10', $this->dataComp['kapookfootball_News_Country'])){ echo 'checked'; } ?> > ฝรั่งเศส
				</label>
				<label class="checkbox-inline">
					<input type="checkbox" id="nation8" name="kapookfootball_News_Country[]" value="8" <? if(in_array('8', $this->dataComp['kapookfootball_News_Country'])){ echo 'checked'; } ?> > ไทย
				</label>
				<label class="checkbox-inline">
					<input type="checkbox" id="nation6" name="kapookfootball_News_Country[]" value="6" <? if(in_array('6', $this->dataComp['kapookfootball_News_Country'])){ echo 'checked'; } ?> > แชมเปี้ยนลีค
				</label>
				<label class="checkbox-inline">
					<input type="checkbox" id="nation12" name="kapookfootball_News_Country[]" value="12" <? if(in_array('12', $this->dataComp['kapookfootball_News_Country'])){ echo 'checked'; } ?> > แชมเปี้ยนชิพ
				</label>
				<label class="checkbox-inline">
					<input type="checkbox" id="nation11" name="kapookfootball_News_Country[]" value="11" <? if(in_array('11', $this->dataComp['kapookfootball_News_Country'])){ echo 'checked'; } ?> > ยูฟ่าคัพ
				</label>
				<label class="checkbox-inline">
					<input type="checkbox" id="nation13" name="kapookfootball_News_Country[]" value="13" <? if(in_array('13', $this->dataComp['kapookfootball_News_Country'])){ echo 'checked'; } ?> > ซื้อขายนักเตะ
				</label>
			</div>
		</div>
		
		
		<div class="form-group">
			<label for="kapookfootball_News_CountryOtherKeyword">คีย์เวิร์ดเฉพาะกิจ (1)</label>
			<input type="text" class="form-control" name="kapookfootball_News_CountryOtherKeyword" value="<?php echo implode ( ',' , $this->dataComp['kapookfootball_News_CountryOtherKeyword'] ); ?>" data-role="tagsinput">
		</div>
		<div class="form-group">
			<label for="kapookfootball_News_CountryOtherKeyword2">คีย์เวิร์ดเฉพาะกิจ (2)</label>
			<input type="text" class="form-control" name="kapookfootball_News_CountryOtherKeyword2" value="<?php echo implode ( ',' , $this->dataComp['kapookfootball_News_CountryOtherKeyword2'] ); ?>" data-role="tagsinput">
		</div>
		<div class="form-group">
			<label for="kapookfootball_News_CountryOtherKeyword3">คีย์เวิร์ดเฉพาะกิจ (3) [Team]</label>
			<input type="text" id="keyword3" class="form-control" name="kapookfootball_News_CountryOtherKeyword3" data-role="tagsinput">
		</div>
		
		<div class="row form-group"><center>
			<div class="col-sm-6 col-md-6"><button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button></div>
			<div class="col-sm-6 col-md-6"><a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/" role="button" class="btn btn-default btn-lg btn-block">Back</a></div>
		</center></div>
		
	</form>
	
<script>
	var teamnames = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		prefetch: '<?php echo BASE_HREF; ?>api/adminfootball/team/getList?type_output=1&type=json'
	});
	
	teamnames.initialize();

	$('#keyword3').tagsinput({
		itemValue: 'value',
        itemText: 'name',
		typeaheadjs: {
			name: 'teamnames',
			displayKey: 'name',
			source: teamnames.ttAdapter()
		}
	});
	
	$( document ).ready(function() {
		$.ajax({
			url: "<?php echo BASE_HREF; ?>api/adminfootball/comp/get_listTeamKeyword/<?php echo $this->dataComp['id']; ?>?type=json",
			dataType: "json",
			success: function(json_team){
				$.each(json_team, function(index, value){
					$('#keyword3').tagsinput('add', { "value": value.value , "name": value.name})
				});
			}
        })
	});
	
</script>
</div>