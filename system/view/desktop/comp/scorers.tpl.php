<div class="container">
	<div class="row form-group">
		<div class="col-sm-3 col-md-3"><a href="<?php echo BASE_HREF; ?>api/adminfootball/player/info" class="btn btn-success btn-lg btn-block" target="_blank">สร้างผู้เล่นใหม่</a></div>
		<div class="col-sm-3 col-md-3"><a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/scorers_select/<?php echo $this->dataComp['id']; ?>" class="btn btn-success btn-lg btn-block">เพิ่มผู้เล่นในตารางดาวซัลโว</a></div>
	</div>
	
	<table class="table table-bordered table-striped">
		<colgroup>
			<col class="col-xs-1">
			<col class="col-xs-7">
			<col class="col-xs-1">
			<col class="col-xs-1">
			<col class="col-xs-1">
			<col class="col-xs-1">
			<col class="col-xs-1">
		</colgroup>
		<thead>
			<tr>
				<th>รูปภาพ</th>
				<th>ชื่อผู้เล่น</th>
				<th>ทีม</th>
				<th>ประตู</th>
				<th>แก้ไข</th>
				<th>ลบ</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($this->listScorer as $tmpData) { ?>
			<tr>
				<td>
				<?php if(strlen($tmpData['dataPlayer']['img'])) { ?>
				<img src="<?php echo str_replace("football.kapook.com", "202.183.165.189", $tmpData['dataPlayer']['img']); ?>" style="width: 100%;"></td>
				<?php }else{ ?>
				<img src="http://football.kapook.com/uploads/scorer/noface.jpg" style="width: 100%;"></td>
				<?php } ?>
				<td style="vertical-align: middle;"><?php echo $tmpData['dataPlayer']['name']; ?></td>
				
				<?php
					if(!empty($tmpData['dataTeam']['NameTH'])){
						$strTeam	=	$tmpData['dataTeam']['NameTH'];
					}else if(!empty($tmpData['dataTeam']['NameTHShort'])){
						$strTeam	=	$tmpData['dataTeam']['NameTHShort'];
					}else{
						$strTeam	=	$tmpData['dataTeam']['NameEN'];
					}
				?>
				
				<td style="vertical-align: middle;"><img src="<?php echo $tmpData['dataTeam']['Logo']; ?>" title="<?php echo $strTeam; ?>" style="width: 100%;"></td>
				<td style="vertical-align: middle;">
					<form action="<?php echo BASE_HREF; ?>api/adminfootball/comp/update_scorer_goal">
						<input type="hidden" name="id" value="<?php echo $tmpData['id']; ?>">
						<input type="hidden" name="comp_id" value="<?php echo $this->dataComp['id']; ?>">
						<input type="number" name="goal" class="form-control" style="width: 100%;height: inherit;font-size: 26pt;text-align: right;" value="<?php echo $tmpData['goal']; ?>">
						<button type="submit" class="btn btn-primary" style="width: 100%;">บันทึก</button>
					</form>
				</td>
				<td style="vertical-align: middle;">
					<a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/scorers_select/<?php echo $this->dataComp['id']; ?>/<?php echo $tmpData['id']; ?>" type="button" class="btn btn-default">แก้ไขดาวซัลโว</a>
					<a href="<?php echo BASE_HREF; ?>api/adminfootball/player/info/<?php echo $tmpData['dataPlayer']['id']; ?>" type="button" class="btn btn-default" target="_blank">แก้ไขข้อมูลผู้เล่น</a>
				</td>
				<td style="vertical-align: middle;"><a onclick="return confirm('แน่ใจแล้วนะ ว่าจะลบคนนี้ออกจากตารางดาวซัลโว?')" href="<?php echo BASE_HREF; ?>api/adminfootball/comp/delete_scorer/<?php echo $tmpData['id']; ?>?comp_id=<?php echo $this->dataComp['id']; ?>" type="button" class="btn btn-danger">ลบ</a></td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
	
</div>