<div class="container" ng-app="CompApp" ng-controller="CompController">
	<h2>ลีกที่นำตารางการแข่งขันมาใช้</h2><hr style="margin-top: 0px;margin-bottom: 10px;">
	<form action="<?php echo BASE_HREF; ?>api/adminfootball/comp/update_listleagueformatch" method="POST">
	<?php if($this->dataRound) { ?>
		<input type="hidden" name="round_id" value="<?php echo $this->dataRound['id']; ?>">
	<?php } ?>
	<table class="table table-bordered table-striped">
		<colgroup>
			<col class="col-xs-1">
			<col class="col-xs-4">
			<col class="col-xs-4">
			<col class="col-xs-3">
			<col class="col-xs-1">
		</colgroup>
		<thead>
			<tr>
				<th>League ID</th>
				<th>League NameTH</th>
				<th>League NameEN</th>
				<th>League Description</th>
				<th>Add?</th>
			</tr>
		</thead>
		<input type="hidden" name="comp_id" value="<?php echo $this->dataComp['id']; ?>">
		<tbody>
			<tr ng-repeat="tmpData in listUserLeague">
				<input type="hidden" name="league[]" value="{{tmpData.id}}">
				<td style="vertical-align: middle;">{{tmpData.id}}</td>
				<td style="vertical-align: middle;">{{tmpData.NameTH}}</td>
				<td style="vertical-align: middle;">{{tmpData.NameEN}}</td>
				<td style="vertical-align: middle;">{{tmpData.Description}}</td>
				<td><button type="button" class="btn btn-danger" ng-click="delete($index)">ลบ</button></td>
			</tr>
		</tbody>
    </table>
	
	<div style="margin-bottom: 80px;">
		<button type="submit" class="btn btn-primary btn-block">บันทึก</button>
	</div>
	</form>

	<h2>เลือกลีกที่ต้องการ</h2><hr style="margin-top: 0px;margin-bottom: 10px;">
	<div class="row form-group"><center>
		<div class="col-sm-6 col-md-6">
			<label>โซนที่ต้องการ</label>
			<select class="form-control" ng-model="zoneSelect" ng-change="getListLeague()">
				<option value="0">กรุณาเลือกลีก</option>
				<?php foreach( $this->dataZone as $tmpZone ){ ?>
				<option value="<?php echo $tmpZone['id']; ?>"><?php echo $tmpZone['NameEN']; ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="col-sm-6 col-md-6">
			<label>ปีที่ต้องการ</label>
			<select class="form-control" ng-model="yearSelect" ng-change="getListLeague()">
				<option value="0">ทุกปี</option>
				<option value="2013">2013/2014</option>
				<option value="2014">2014/2015</option>
				<option value="2015">2015/2016</option>
				<option value="2016">2016/2017</option>
				<option value="2017">2017/2018</option>
			</select>
		</div>
	</center></div>
	<table class="table table-bordered table-striped" ng-if="zoneSelect==0">
		<colgroup>
			<col class="col-xs-12">
		</colgroup>
		<thead>
			<tr>
				<th><center>กรุณาเลือกลีก</center></th>
			</tr>
		</thead>
	</table>
	<table class="table table-bordered table-striped" ng-if="zoneSelect!=0">
		<colgroup>
			<col class="col-xs-1">
			<col class="col-xs-4">
			<col class="col-xs-4">
			<col class="col-xs-3">
			<col class="col-xs-1">
		</colgroup>
		<thead>
			<tr>
				<th>League ID</th>
				<th>League NameTH</th>
				<th>League NameEN</th>
				<th>League Description</th>
				<th>Add?</th>
			</tr>
		</thead>
		<tbody>
			<tr ng-repeat="tmpData in listLeague" ng-style="tmpData.style">
				<td style="vertical-align: middle;">{{tmpData.id}}</td>
				<td style="vertical-align: middle;">{{tmpData.NameTH}}</td>
				<td style="vertical-align: middle;">{{tmpData.NameEN}}</td>
				<td style="vertical-align: middle;">{{tmpData.Description}}</td>
				<td><button type="button" class="btn btn-success" ng-click="add($index)">เพิ่ม</button></td>
			</tr>
		</tbody>
	</table>
</div>

<script>				
	
	(function(angular) {
		var myApp = angular.module('CompApp', []);

		myApp.controller('CompController', ['$scope','$http', function($scope,$http) {
		
			$scope.zoneSelect = 0;
			$scope.yearSelect = 0;
			
			$scope.listLeague = [];
			$scope.listUserLeague = [];
			
			$scope.getListLeague = function(){
				if($scope.zoneSelect==0){
					$scope.listLeague = [];
				}else{
					$http.post('http://football.kapook.com/api/adminfootball/league/getjson/' + $scope.zoneSelect + '/' + $scope.yearSelect).success(function(data, status, headers, config) {
						$scope.listLeague			=	data;
						for( var i in $scope.listLeague ){
							$scope.listLeague[i].style				=		{};
							for( var j in $scope.listUserLeague ){
								if( $scope.listLeague[i].id == $scope.listUserLeague[j].id ){
									$scope.listLeague[i].style		=		{'background-color' : 'darksalmon'};
									break;
								}
							}
						}
					}).
					error(function(data, status, headers, config) {
						// called asynchronously if an error occurs
						// or server returns response with an error status.
					});
				}
			};
			
			$scope.delete = function(index){
				for( var i in $scope.listLeague ){
					if( $scope.listLeague[i].id == $scope.listUserLeague[index].id ){
						$scope.listLeague[i].style				=		{};
						break;
					}
				}
				$scope.listUserLeague.splice(index, 1);
			};
			
			$scope.add = function(index){
				for( var i in $scope.listUserLeague ){
					if( $scope.listUserLeague[i].id == $scope.listLeague[index].id ){
						return;
					}
				}
				$scope.listLeague[index].style		=		{'background-color' : 'darksalmon'};
				$scope.listUserLeague.push($scope.listLeague[index]);
			};
			<?php if($this->dataRound){ ?>
			$http.post('http://football.kapook.com/api/adminfootball/comp/get_listleagueformatch/<?php echo $this->dataComp['id']; ?>/<?php echo $this->dataRound['id']; ?>?type=json').success(function(data, status, headers, config) {
				$scope.listUserLeague			=	data;
			}).
			error(function(data, status, headers, config) {
				// called asynchronously if an error occurs
				// or server returns response with an error status.
			});
			<?php }else{ ?>
			$http.post('http://football.kapook.com/api/adminfootball/comp/get_listleagueformatch/<?php echo $this->dataComp['id']; ?>?type=json').success(function(data, status, headers, config) {
				$scope.listUserLeague			=	data;
			}).
			error(function(data, status, headers, config) {
				// called asynchronously if an error occurs
				// or server returns response with an error status.
			});
			<?php } ?>
			
		}]);

	})(window.angular);
			
	</script>