<?php
	$list_TV = array(
	
		0 => array(
			'nameType' 	=> 	'TV Analog',
			'listTV'	=>	array(
				'tv3' 	=> 	'<img src="http://202.183.165.189/uploads/tvlogo/tv3.png" height="20">',
				'tv5'	=>	'<img src="http://202.183.165.189/uploads/tvlogo/tv5.png" height="20">',
				'tv7'	=>	'<img src="http://202.183.165.189/uploads/tvlogo/tv7.png" height="20">',
				'tv9'	=>	'<img src="http://202.183.165.189/uploads/tvlogo/tv9.png" height="20">',
				'tv11'	=>	'<img src="http://202.183.165.189/uploads/tvlogo/tv11.png" height="20">',
				'tpbs'	=>	'<img src="http://202.183.165.189/uploads/tvlogo/tpbs.png" height="20">'
			)
		),

		1 => array(
			'nameType' 	=> 	'TV Digital',
			'listTV'	=>	array(
				'tv5hd'			=> '<img src="http://202.183.165.189/uploads/tvlogo/tv5hd.png" height="20">',
				'tv11hd'		=> '<img src="http://202.183.165.189/uploads/tvlogo/tv11hd.png" height="20">',
				'tpbshd'		=> '<img src="http://202.183.165.189/uploads/tvlogo/tpbshd.png" height="20">',
				'tv3family'		=> '<img src="http://202.183.165.189/uploads/tvlogo/tv3family.png" height="20">',
				'mcotfamily'	=> '<img src="http://202.183.165.189/uploads/tvlogo/mcotfamily.png" height="20">',
				'tnn'			=> '<img src="http://202.183.165.189/uploads/tvlogo/tnn.png" height="20">',
				'newtv'			=> '<img src="http://202.183.165.189/uploads/tvlogo/newtv.png" height="20">',
				'springnews'	=> '<img src="http://202.183.165.189/uploads/tvlogo/springnews.png" height="20">',
				'nation'		=> '<img src="http://202.183.165.189/uploads/tvlogo/nation.png" height="20">',
				'workpointtv' 	=> '<img src="http://202.183.165.189/uploads/tvlogo/workpointtv.png" height="20">',
				'true4u' 		=> '<img src="http://202.183.165.189/uploads/tvlogo/true4u.png" height="20">',
				'gmmchannel' 	=> '<img src="http://202.183.165.189/uploads/tvlogo/gmmchannel.png" height="20">',
				'now' 			=> '<img src="http://202.183.165.189/uploads/tvlogo/now.png" height="20">',
				'ch8' 			=> '<img src="http://202.183.165.189/uploads/tvlogo/ch8.png" height="20">',
				'tv3sd' 		=> '<img src="http://202.183.165.189/uploads/tvlogo/tv3sd.png" height="20">',
				'mono' 			=> '<img src="http://202.183.165.189/uploads/tvlogo/mono.png" height="20">',
				'mcothd' 		=> '<img src="http://202.183.165.189/uploads/tvlogo/mcothd.png" height="20">',
				'one' 			=> '<img src="http://202.183.165.189/uploads/tvlogo/one.png" height="20">',
				'thairath' 		=> '<img src="http://202.183.165.189/uploads/tvlogo/thairath.png" height="20">',
				'tv3hd' 		=> '<img src="http://202.183.165.189/uploads/tvlogo/tv3hd.png" height="20">',
				'amarin' 		=> '<img src="http://202.183.165.189/uploads/tvlogo/amarin.png" height="20">',
				'pptv' 			=> '<img src="http://202.183.165.189/uploads/tvlogo/pptv.png" height="20">'
			)
		),
		
		2 => array(
			'nameType' 		=> 	'True Vision',						
			'listTV'		=>	array(
				'tshd1'		=>	'<img src="http://202.183.165.189/uploads/tvlogo/tshd1.png" height="20">',
				'tshd2' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/tshd2.png" height="20">',
				'tshd3' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/tshd3.png" height="20">',
				'tshd4' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/tshd4.png" height="20">',
				'ts1' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/ts1.png" height="20">',
				'ts2' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/ts2.png" height="20">',
				'ts3' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/ts3.png" height="20">',
				'ts4' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/ts4.png" height="20">',
				'ts5' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/ts5.png" height="20">',
				'ts6' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/ts6.png" height="20">',
				'ts7' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/ts7.png" height="20">',
				'tstennis'	=>	'<img src="http://202.183.165.189/uploads/tvlogo/tstennis.png" height="20">',
				'hayha'		=>	'<img src="http://202.183.165.189/uploads/tvlogo/hayha.png" height="20">(352)',
				'thaithai'	=>	'<img src="http://202.183.165.189/uploads/tvlogo/thaithai.png" height="20"> (358)',
				'true' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/true.png" height="20">'
			)
		),
		
		3 => array(
			'nameType' 	=> 	'CTH',						
			'listTV'	=>	array(
				'cthstd1'	=>	'<img src="http://202.183.165.189/uploads/tvlogo/cthstd1.png" height="20">',
				'cthstd2' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/cthstd2.png" height="20">',
				'cthstd3' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/cthstd3.png" height="20">',
				'cthstd4' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/cthstd4.png" height="20">',
				'cthstd5' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/cthstd5.png" height="20">',
				'cthstd6' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/cthstd6.png" height="20">',
				'cthstdx'	=>	'<img src="http://202.183.165.189/uploads/tvlogo/cthstdx.png" height="20">'
			)
		),
		
		4 => array(
			'nameType' 	=> 	'GMM',						
			'listTV'	=>	array(
				'gmmclubchannel' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/gmmclubchannel.png" height="20">',
				'gmmfootballplus' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/gmmfootballplus.png" height="20">',
				'gmmfootballmax' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/gmmfootballmax.png" height="20">',
				'gmmfootballeuro' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/gmmfootballeuro.png" height="20">',
				'gmmfootballextrahd'	=>	'<img src="http://202.183.165.189/uploads/tvlogo/gmmfootballextrahd.png" height="20">',
				'gmmsportextreme'		=>	'<img src="http://202.183.165.189/uploads/tvlogo/gmmsportextreme.png" height="20">',
				'gmmsportplus'			=>	'<img src="http://202.183.165.189/uploads/tvlogo/gmmsportplus.png" height="20">',
				'gmmsportextra'			=>	'<img src="http://202.183.165.189/uploads/tvlogo/gmmsportplus.png" height="20">',
				'gmmsportone'			=>	'<img src="http://202.183.165.189/uploads/tvlogo/gmmsportplus.png" height="20">'
			)
		),
		
		5 => array(
			'nameType' 	=> 	'Siamsport',						
			'listTV'	=>	array(
				'siamsportnew'		=>	'Siamsport News',
				'siamsportfootball' =>	'<img src="http://202.183.165.189/uploads/tvlogo/siamsportfootball.png" height="20">',
				'siamsportlive' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/siamsportlive.png" height="20">'
			)
		),
		
		6 => array(
			'nameType' 	=> 	'RS',						
			'listTV'	=>	array(
				'sunchannel'		=>	'<img src="http://202.183.165.189/uploads/tvlogo/sunchannel.png" height="20">',
				'worldcupchannel' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/worldcupchannel.png" height="20">'
			)
		),
		
		7 => array(
			'nameType' 	=> 	'Bein Sport',						
			'listTV'	=>	array(
				'bein'		=>	'<img src="http://202.183.165.189/uploads/tvlogo/bein.png" height="20">',
				'bein1' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/bein1.png" height="20">',
				'bein2' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/bein2.png" height="20">'
			)
		),
		
		8 => array(
			'nameType' 	=> 	'Fox Sport',						
			'listTV'	=>	array(
				'foxsport' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/foxsport.png" height="20">',
				'foxsport2' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/foxsport2.png" height="20">',
				'foxsport3' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/foxsport3.png" height="20">',
				'foxsportplay' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/foxsportplay.png" height="20">'
			)
		),
		
		9 => array(
			'nameType' 	=> 	'อื่นๆ',						
			'listTV'	=>	array(
				'starsport' 			=>	'<img src="http://202.183.165.189/uploads/tvlogo/starsport.png" height="20">',
				'astrosupersport1' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/astrosupersport1.png" height="20">',
				'astrosupersport2' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/astrosupersport2.png" height="20">',
				'astrosupersporthd3' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/astrosupersporthd3.png" height="20">',
				'asn' 					=>	'<img src="http://202.183.165.189/uploads/tvlogo/asn.png" height="20">',
				'asn2' 					=>	'<img src="http://202.183.165.189/uploads/tvlogo/asn2.png" height="20">',
				'bugaboo' 				=>	'<img src="http://202.183.165.189/uploads/tvlogo/bugaboo.png" height="20">'
			)
		)
	);
?>

<div class="container">
	<form action="<?php echo BASE_HREF; ?>api/adminfootball/comp/<?php
		if($this->dataRound){
			if(isset($this->dataMatch)) {
				echo 'update_round_dummyMatch';
			}else{
				echo 'create_round_dummyMatch';
			}
		}else{
			if(isset($this->dataMatch)) {
				echo 'update_dummyMatch';
			}else{
				echo 'create_dummyMatch';
			}
		}
	?>" method="POST">
	<input type="hidden" name="comp_id" value="<?php echo $this->dataComp['id']; ?>">
	<?php if(isset($this->dataMatch)) {
		?><input type="hidden" name="id" value="<?php echo $this->dataMatch['id']; ?>">
	<?php } ?>
	<?php if($this->dataRound) { ?>
		<input type="hidden" name="round_id" value="<?php echo $this->dataRound['id']; ?>">
	<?php } ?>
	<table class="table table-bordered table-striped">
		<thead>
			<tr>
				<th class="col-xs-6" colspan="3"><div style="font-size: x-large;float: left;">Team1</div><button class="btn btn-default" type="button" style="float: right;" onclick="window.open('<?php echo BASE_HREF; ?>api/adminfootball/comp/get_listTeamForMatchDummy/1','','height=400,width=400');">ค้นหาทีม <span class="glyphicon glyphicon-search"></span></button></th>
				<th class="col-xs-6" colspan="3"><div style="font-size: x-large;float: left;">Team2</div><button class="btn btn-default" type="button" style="float: right;" onclick="window.open('<?php echo BASE_HREF; ?>api/adminfootball/comp/get_listTeamForMatchDummy/2','','height=400,width=400');">ค้นหาทีม <span class="glyphicon glyphicon-search"></span></button></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td style="width: 10%;">Team Name</td>
				<td ><input type="text" class="form-control" placeholder="Team 1 Name" id="Team1Name" name="Team1" value="<?php if(isset($this->dataMatch)) { echo $this->dataMatch['Team1']; } ?>"></td>
				<td class="col-xs-1" rowspan="3"><input type="number" name="Team1FTScore" class="form-control" style="width: 109px;height: 100%;font-size: 43pt;text-align: right;" value="<?php if(isset($this->dataMatch)) { echo $this->dataMatch['Team1FTScore']; } ?>"></td>
				<td class="col-xs-1" rowspan="3"><input type="number" name="Team2FTScore" class="form-control" style="width: 109px;height: 100%;font-size: 43pt;" value="<?php if(isset($this->dataMatch)) { echo $this->dataMatch['Team2FTScore']; } ?>"></td>
				<td style="width: 10%;">Team Name</td>
				<td ><input type="text" class="form-control" placeholder="Team 2 Name" id="Team2Name" name="Team2" value="<?php if(isset($this->dataMatch)) { echo $this->dataMatch['Team2']; } ?>"></td>
			</tr>
			<tr>
				<td>Path logo</td>
				<td><input type="text" class="form-control" placeholder="Team 1 Path Logo" id="Team1Logo" name="Team1Logo" value="<?php if(isset($this->dataMatch)) { echo $this->dataMatch['Team1Logo']; } ?>"></td>
				<td>Path logo</td>
				<td><input type="text" class="form-control" placeholder="Team 2 Path Logo" id="Team2Logo" name="Team2Logo" value="<?php if(isset($this->dataMatch)) { echo $this->dataMatch['Team2Logo']; } ?>"></td>
			</tr>
			<tr>
				<td>URL Team</td>
				<td><input type="text" class="form-control" placeholder="Team 1 URL" id="Team1URL" name="Team1URL" value="<?php if(isset($this->dataMatch)) { echo $this->dataMatch['Team1URL']; } ?>"></td>
				<td>URL Team</td>
				<td><input type="text" class="form-control" placeholder="Team 2 URL" id="Team2URL" name="Team2URL" value="<?php if(isset($this->dataMatch)) { echo $this->dataMatch['Team2URL']; } ?>"></td>
			</tr>
		</tbody>
    </table>
	<div class="form-group row">
	<div class="col-xs-6">
		<label for="MatchDateTime">Match Time</label>
		<input type="text" class="form-control" id="MatchDateTime" name="MatchDateTime" placeholder="เวลาการแข่งขัน" value="<?php
			if(isset($this->dataMatch)) {
				echo $this->dataMatch['MatchDateTime'];
			}else{
				echo date('Y-m-d H:i:s');
			} ?>">
	</div>
	<div class="col-xs-6">
		<label for="MatchStatus">Status Match</label>
		<select class="form-control" name="MatchStatus" id="MatchStatus">
			<option value="Sched" <?php
				if(isset($this->dataMatch))
				{
					if($this->dataMatch['MatchStatus']=='Sched'){
						echo 'selected';
					}
				}
			?>>Sched</option>
			<option value="1 HF" <?php
				if(isset($this->dataMatch))
				{
					if($this->dataMatch['MatchStatus']=='1 HF'){
						echo 'selected';
					}
				}
			?>>1 HF</option>
			<option value="H/T" <?php
				if(isset($this->dataMatch))
				{
					if($this->dataMatch['MatchStatus']=='H/T'){
						echo 'selected';
					}
				}
			?>>H/T</option>
			<option value="2 HF" <?php
				if(isset($this->dataMatch))
				{
					if($this->dataMatch['MatchStatus']=='2 HF'){
						echo 'selected';
					}
				}
			?>>2 HF</option>
			<option value="E/T" <?php
				if(isset($this->dataMatch))
				{
					if($this->dataMatch['MatchStatus']=='E/T'){
						echo 'selected';
					}
				}
			?>>E/T</option>
			<option value="Pen" <?php
				if(isset($this->dataMatch))
				{
					if($this->dataMatch['MatchStatus']=='Pen'){
						echo 'selected';
					}
				}
			?>>Pen</option>
			<option value="Fin" <?php
				if(isset($this->dataMatch))
				{
					if($this->dataMatch['MatchStatus']=='Fin'){
						echo 'selected';
					}
				}
			?>>Fin</option>
			<option value="Int" <?php
				if(isset($this->dataMatch))
				{
					if($this->dataMatch['MatchStatus']=='Int'){
						echo 'selected';
					}
				}
			?>>Int</option>
			<option value="Abd" <?php
				if(isset($this->dataMatch))
				{
					if($this->dataMatch['MatchStatus']=='Abd'){
						echo 'selected';
					}
				}
			?>>Abd</option>
			<option value="Post" <?php
				if(isset($this->dataMatch))
				{
					if($this->dataMatch['MatchStatus']=='Post'){
						echo 'selected';
					}
				}
			?>>Post</option>
			<option value="Canc" <?php
				if(isset($this->dataMatch))
				{
					if($this->dataMatch['MatchStatus']=='Canc'){
						echo 'selected';
					}
				}
			?>>Canc</option>
		</select>
	</div>
	</div>
	<div class="form-group">
		<center><label>ช่องการถ่ายทอดสด</label></center>
		<table class="table table-bordered">
			<?php
			for( $j=0,$sizeTV=count($list_TV) ; $j< $sizeTV ; $j++ )
			{
				?><tr>
					<td colspan="5"><b><?php echo $list_TV[$j]['nameType']; ?></b></td>
				</tr>
				<?php
				echo '<tr>';
				$countTV = 5;
				foreach ($list_TV[$j]['listTV'] as $k => $v) {
					if (in_array($k, $this->dataMatch['TVLiveList']))
						$check = 'CHECKED';
					else
						$check = '';
					echo '<td><input name="TVLiveList[]" type="checkbox" value="' . $k . '" ' . $check . '>' . $v . '</td>';
										
					$countTV--;
					if($countTV <= 0){
						$countTV = 5;
						echo '</tr><tr>';
					}
				}
				if($countTV!=5)
					echo '<td colspan="'. $countTV .'"></td>';
					echo '</tr>';
			}
			?>
		</table>
	</div>
	<div class="row form-group"><center>
		<div class="col-sm-6 col-md-6"><button type="submit" class="btn btn-primary btn-lg btn-block"><?php if(isset($this->dataMatch)) { ?>บันทึกการแข่งขัน<?php }else{ ?>สร้างการแข่งขันใหม่<?php } ?></button></div>
		<?php if($this->dataRound) { ?>
		<div class="col-sm-6 col-md-6"><a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/program_round/<?php echo $this->dataComp['id']; ?>/<?php echo $this->dataRound['id']; ?>" class="btn btn-default btn-lg btn-block">กลับไปหน้าตารางการแข่งขัน</a></div>
		<?php }else{ ?>
		<div class="col-sm-6 col-md-6"><a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/program/<?php echo $this->dataComp['id']; ?>" class="btn btn-default btn-lg btn-block">กลับไปหน้าตารางการแข่งขัน</a></div>
		<?php } ?>
	</center></div>
	</form>
</div>

<script>				
	$(document).ready(function(){
		$('#MatchDateTime').datetimepicker({
			lang:'en',
			format:'Y-m-d H:i:s'
		});
	});
</script>