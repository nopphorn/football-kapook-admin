<div class="container" style="padding-bottom: 60px;">
	<form role="form" action="<?php echo BASE_HREF; ?>api/adminfootball/comp/update" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="id" value="<?php echo $this->dataComp['id']; ?>">
		<div class="form-group">
			<label for="nameTH">ชื่อเต็ม(ไทย)</label>
			<input type="text" class="form-control" name="nameTH" placeholder="ชื่อการแข่งขันแบบยาว" value="<?php echo $this->dataComp['nameTH']; ?>">
		</div>
		<div class="form-group">
			<label for="nameTHShort">ชื่อย่อ(ไทย)</label>
			<input type="text" class="form-control" name="nameTHShort" placeholder="ชื่อการแข่งขันแบบสั้น" value="<?php echo $this->dataComp['nameTHShort']; ?>">
		</div>
		<div class="form-group">
			<label for="nameURL">ชื่อurl</label>
			<input type="text" class="form-control" name="nameURL" placeholder="ชื่อการแข่งขันบน URL" value="<?php echo $this->dataComp['nameURL']; ?>">
		</div>
		<div class="form-group">
			<label for="nameURLRootRoute">Root Full Home page URL(ถ้าไม่ใส่จะเป็น "http://football.kapook.com/tournament/[ชื่อurl]")</label>
			<input type="text" class="form-control" name="nameURLRootRoute" placeholder="ชื่อการแข่งขันบน URL" value="<?php echo $this->dataComp['nameURLRootRoute']; ?>">
		</div>
		<div class="form-group">
			<label for="SEOtitle">SEO Title</label>
			<input type="text" class="form-control" name="SEOTitle" placeholder="SEO Title" value="<?php echo $this->dataComp['SEOTitle']; ?>">
		</div>
		<div class="form-group">
			<label for="SEOdescription">SEO Description</label>
			<input type="text" class="form-control" name="SEOdescription" placeholder="SEO Description" value="<?php echo $this->dataComp['SEOdescription']; ?>">
		</div>
		<div class="form-group">
			<label for="SEOkeywords">SEO Keywords</label>
			<input type="text" class="form-control" name="SEOkeywords" placeholder="SEO Keywords" value="<?php echo $this->dataComp['SEOkeywords']; ?>">
		</div>
		<div class="form-group">
			<label for="FBtitle">FB OG Title</label>
			<input type="text" class="form-control" name="FBtitle" placeholder="FB OG Title" value="<?php echo $this->dataComp['FBtitle']; ?>">
		</div>
		<div class="form-group">
			<label for="FBdescription">FB OG Description</label>
			<input type="text" class="form-control" name="FBdescription" placeholder="FB OG Description" value="<?php echo $this->dataComp['FBdescription']; ?>">
		</div>
		<div class="form-group">
			<label>FB OG Image</label>
			
			<div class="row">
				<div class="col-sm-4 col-md-4">
					<div class="thumbnail">
						<?php if(strlen($this->dataComp['FBimg'])){ ?>
						<img src="<?php echo str_replace("football.kapook.com", "202.183.165.189", $this->dataComp['FBimg']); ?>">
						<?php }else{ ?>
						<img src="<?php echo BASE_HREF; ?>/api/adminfootball/assets/img/noimage.png">
						<?php } ?>
						<div class="caption">
							<input type="radio" name="FBimgSelect" value="1" <?php if(!strlen($this->dataComp['FBimg'])){ ?> disabled <?php }else{ ?> checked <?php } ?>> ใช้รูปปัจจุบัน
						</div>
					</div>
				</div>
				<div class="col-sm-8 col-md-8">
					<div class="thumbnail">
						<div class="caption">
							<input type="radio" name="FBimgSelect" value="2"> อัพโหลดรูปใหม่
							<input type="file" name="FBimg" id="FBimg" disabled>
						</div>
					</div>
				</div>
				<div class="col-sm-8 col-md-8">
					<div class="thumbnail">
						<div class="caption">
							<input type="radio" name="FBimgSelect" value="3" <?php if(!strlen($this->dataComp['FBimg'])){ ?> checked <?php } ?>> ไม่ใช้รูปภาพ
						</div>
					</div>
				</div>
			</div>
			
		</div>
		<div class="row form-group"><center>
			<div class="col-sm-6 col-md-6"><button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button></div>
			<div class="col-sm-6 col-md-6"><a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/" role="button" class="btn btn-default btn-lg btn-block">Back</a></div>
		</center></div>
	</form>
</div>

	<script language="JavaScript">
		$('input[type=radio][name=FBimgSelect]').change(function() {
			if (this.value == '2') {
				$( "#FBimg" ).prop( "disabled", false );
			}
			else {
				$( "#FBimg" ).prop( "disabled", true );
			}
		});
	</script>