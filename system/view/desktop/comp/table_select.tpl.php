<div class="container" ng-app="CompApp" ng-controller="CompController">
	<form action="<?php echo BASE_HREF; ?>api/adminfootball/comp/<?php
		if($this->dataRound){
			if(isset($this->dataTable)) {
				echo 'update_table_round_select';
			}else{
				echo 'create_table_round_select';
			}
		}else{
			if(isset($this->dataTable)) {
				echo 'update_table_select';
			}else{
				echo 'create_table_select';
			}
		}
	?>" method="POST">
	<input type="hidden" name="comp_id" value="<?php echo $this->dataComp['id']; ?>">
	<?php if(isset($this->dataTable)) {
		?><input type="hidden" name="id" value="<?php echo $this->dataTable['id']; ?>">
	<?php } ?>
	<?php if($this->dataRound) { ?>
		<input type="hidden" name="round_id" value="<?php echo $this->dataRound['id']; ?>">
	<?php }else{ ?>
		<input type="hidden" name="round_id" value="-1">
	<?php } ?>
	<div class="form-group">
		<label >Name Table</label>
		<input type="text" class="form-control" id="name" name="name" value="<?php if(isset($this->dataTable)) { echo $this->dataTable['name']; }?>" placeholder="ชื่อตารางคะแนน">
	</div>
	<h2>ลีกที่นำมาใช้</h2><hr style="margin-top: 0px;margin-bottom: 10px;">
	<table class="table table-bordered table-striped">
		<colgroup>
			<col class="col-xs-1">
			<col class="col-xs-4">
			<col class="col-xs-4">
			<col class="col-xs-2">
			<col class="col-xs-1">
		</colgroup>
		<thead>
			<tr>
				<th>League ID</th>
				<th>League NameTH</th>
				<th>League NameEN</th>
				<th>League Description</th>
				<th>ลบออก</th>
			</tr>
		</thead>
		<tbody>
			<tr ng-repeat="tmpData in listUserLeague">
				<input type="hidden" name="league_list[]" value="{{tmpData.id}}">
				<td style="vertical-align: middle;">{{tmpData.id}}</td>
				<td style="vertical-align: middle;">{{tmpData.NameTH}}</td>
				<td style="vertical-align: middle;">{{tmpData.NameEN}}</td>
				<td style="vertical-align: middle;">{{tmpData.Description}}</td>
				<td><button type="button" class="btn btn-danger" ng-click="delete_league($index)">ลบ</button></td>
			</tr>
		</tbody>
    </table>
	
	<h2>การแข่งขันที่นำมาใช้</h2><hr style="margin-top: 0px;margin-bottom: 10px;">
	<table class="table table-bordered table-striped">
		<colgroup>
			<col class="col-xs-1">
			<col class="col-xs-2">
			<col class="col-xs-1">
			<col class="col-xs-2">
			<col class="col-xs-2">
			<col class="col-xs-2">
			<col class="col-xs-1">
		</colgroup>
		<thead>
			<tr>
				<th>Match ID</th>
				<th>ทีม 1</th>
				<th>ผลการแข่งขัน</th>
				<th>ทีม 2</th>
				<th>วันและเวลาการแข่งขัน</th>
				<th>XSMatch ID</th>
				<th>ลบออก</th>
			</tr>
		</thead>
		<tbody>
			<tr ng-repeat="tmpData in listUserMatch">
				<input type="hidden" name="match_list[{{$index}}]" value="{{tmpData.id}}">
				<td style="vertical-align: middle;">{{tmpData.id}}</td>
				<td style="vertical-align: middle;">{{tmpData.Team1}}</td>
				<td style="vertical-align: middle;">{{tmpData.FTScore}}</td>
				<td style="vertical-align: middle;">{{tmpData.Team2}}</td>
				<td style="vertical-align: middle;">{{tmpData.MatchDateTime}}</td>
				<td style="vertical-align: middle;">{{tmpData.XSMatchID}}</td>
				<td><button type="button" class="btn btn-danger" ng-click="delete_match($index)">ลบ</button></td>
			</tr>
		</tbody>
    </table>
	
	<h2>ทีมที่นำมาใช้</h2><hr style="margin-top: 0px;margin-bottom: 10px;">
	<table class="table table-bordered table-striped">
		<colgroup>
			<col class="col-xs-1">
			<col class="col-xs-2">
			<col class="col-xs-2">
			<col class="col-xs-1">
			<col class="col-xs-2">
			<col class="col-xs-2">
			<col class="col-xs-2">
			<col class="col-xs-1">
		</colgroup>
		<thead>
			<tr>
				<th>Team ID</th>
				<th>Team NameEN</th>
				<th>Team NameTH</th>
				<th>League ID</th>
				<th>League NameEN</th>
				<th>League NameTH</th>
				<th>League Description</th>
				<th>ลบออก</th>
			</tr>
		</thead>
		<tbody>
			<tr ng-repeat="tmpData in listUserTeam">
				<input type="hidden" name="team_list[{{$index}}]" value="{{tmpData.team_id}}">
				<input type="hidden" name="team_league_list[{{$index}}]" value="{{tmpData.league_id}}">
				<td style="vertical-align: middle;">{{tmpData.team_id}}</td>
				<td style="vertical-align: middle;">{{tmpData.TeamEN}}</td>
				<td style="vertical-align: middle;">{{tmpData.TeamTH}}</td>
				<td style="vertical-align: middle;">{{tmpData.league_id}}</td>
				<td style="vertical-align: middle;">{{tmpData.LeagueEN}}</td>
				<td style="vertical-align: middle;">{{tmpData.LeagueTH}}</td>
				<td style="vertical-align: middle;">{{tmpData.LeagueDescription}}</td>
				<td><button type="button" class="btn btn-danger" ng-click="delete_team($index)">ลบ</button></td>
			</tr>
		</tbody>
    </table>

	<div style="margin-bottom: 80px;">
		<button type="submit" class="btn btn-primary btn-block">บันทึก</button>
	</div>
	</form>
	
	<h2>เลือกข้อมูลที่ต้องการ</h2><hr style="margin-top: 0px;margin-bottom: 10px;">
	<div role="tabpanel">

		<!-- Nav tabs -->
		<ul class="nav nav-tabs" role="tablist">
			<li role="presentation" class="active"><a href="#league" aria-controls="league" role="tab" data-toggle="tab">League</a></li>
			<li role="presentation"><a href="#match" aria-controls="match" role="tab" data-toggle="tab">Match</a></li>
			<li role="presentation"><a href="#team" aria-controls="team" role="tab" data-toggle="tab">Team</a></li>
		</ul>
		<!-- Tab panes -->
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="league">
				<div class="row form-group"><center>
					<div class="col-sm-6 col-md-6">
						<label>โซนที่ต้องการ</label>
						<select class="form-control" ng-model="zoneSelect" ng-change="getListLeague()">
							<option value="0">ทุกโซน</option>
							<?php foreach( $this->dataZone as $tmpZone ){ ?>
							<option value="<?php echo $tmpZone['id']; ?>"><?php echo $tmpZone['NameEN']; ?></option>
							<?php } ?>
						</select>
					</div>
					<div class="col-sm-6 col-md-6">
						<label>ปีที่ต้องการ</label>
						<select class="form-control" ng-model="yearSelect" ng-change="getListLeague()">
							<option value="0">ทุกปี</option>
							<option value="2013">2013/2014</option>
							<option value="2014">2014/2015</option>
							<option value="2015">2015/2016</option>
							<option value="2016">2016/2017</option>
							<option value="2017">2017/2018</option>
						</select>
					</div>
				</center></div>
				<table class="table table-bordered table-striped" ng-if="zoneSelect==0">
					<colgroup>
						<col class="col-xs-12">
					</colgroup>
					<thead>
						<tr>
							<th><center>กรุณาเลือกลีก</center></th>
						</tr>
					</thead>
				</table>
				<table class="table table-bordered table-striped" ng-if="zoneSelect!=0">
					<colgroup>
						<col class="col-xs-1">
						<col class="col-xs-4">
						<col class="col-xs-4">
						<col class="col-xs-2">
						<col class="col-xs-1">
					</colgroup>
					<thead>
						<tr>
							<th>League ID</th>
							<th>League NameTH</th>
							<th>League NameEN</th>
							<th>League Description</th>
							<th>Add?</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="tmpData in listLeague" ng-style="tmpData.style">
							<td style="vertical-align: middle;">{{tmpData.id}}</td>
							<td style="vertical-align: middle;">{{tmpData.NameTH}}</td>
							<td style="vertical-align: middle;">{{tmpData.NameEN}}</td>
							<td style="vertical-align: middle;">{{tmpData.Description}}</td>
							<td><button type="button" class="btn btn-success" ng-click="add_league($index)">เพิ่ม</button></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div role="tabpanel" class="tab-pane" id="match">
				<div class="row form-group"><center>
					<div class="col-sm-4 col-md-4">
						<label>โซนที่ต้องการค้นหา</label>
						<select class="form-control" ng-model="zoneSelect_match" ng-change="getListMatch(true)">
							<option value="0">ทุกโซน</option>
							<?php foreach( $this->dataZone as $tmpZone ){ ?>
							<option value="<?php echo $tmpZone['id']; ?>"><?php echo $tmpZone['NameEN']; ?></option>
							<?php } ?>
						</select>
					</div>
					<div class="col-sm-6 col-md-6">
						<label>ลีกที่ต้องการค้นหา</label>
						<select class="form-control" ng-model="leagueSelect_match" ng-change="getListMatch(false)">
							<option value="0">ทุกลีก</option>
							<option ng-repeat="tmpData in listLeagueMatch" value="{{tmpData.id}}">{{tmpData.NameTH}}</option>
						</select>
					</div>
					<div class="col-sm-6 col-md-6">
						<label>วันที่เริ่มการค้นหา</label>
						<input type="text" class="form-control MatchDate" placeholder="วันที่แข่งขัน" ng-model="dateFromSelect_match" ng-change="getListMatch(false)">
					</div>
					<div class="col-sm-6 col-md-6">
						<label>วันที่สิ้นสุดการค้นหา</label>
						<input type="text" class="form-control MatchDate" placeholder="วันที่แข่งขัน" ng-model="dateToSelect_match" ng-change="getListMatch(false)">
					</div>
				</center></div>
				<table class="table table-bordered table-striped">
					<colgroup>
						<col class="col-xs-1">
						<col class="col-xs-2">
						<col class="col-xs-1">
						<col class="col-xs-2">
						<col class="col-xs-2">
						<col class="col-xs-2">
						<col class="col-xs-1">
					</colgroup>
					<thead>
						<tr>
							<th>Match ID</th>
							<th>ทีม 1</th>
							<th>ผลการแข่งขัน</th>
							<th>ทีม 2</th>
							<th>วันและเวลาการแข่งขัน</th>
							<th>XSMatch ID</th>
							<th>Add?</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="tmpData in listMatch" ng-style="tmpData.style">
							<td style="vertical-align: middle;">{{tmpData.id}}</td>
							<td style="vertical-align: middle;">{{tmpData.Team1}}</td>
							<td style="vertical-align: middle;">{{tmpData.FTScore}}</td>
							<td style="vertical-align: middle;">{{tmpData.Team2}}</td>
							<td style="vertical-align: middle;">{{tmpData.MatchDateTime}}</td>
							<td style="vertical-align: middle;">{{tmpData.XSMatchID}}</td>
							<td><button type="button" class="btn btn-success" ng-click="add_match($index)">เพิ่ม</button></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div role="tabpanel" class="tab-pane" id="team">
				<div class="row form-group"><center>
					<div class="col-sm-3 col-md-3">
						<label>โซนที่ต้องการ</label>
						<select class="form-control" ng-model="zoneSelect_team" ng-change="getListTeam(true)">
							<option value="0">กรุณาเลือกโซน</option>
							<?php foreach( $this->dataZone as $tmpZone ){ ?>
							<option value="<?php echo $tmpZone['id']; ?>"><?php echo $tmpZone['NameEN']; ?></option>
							<?php } ?>
						</select>
					</div>
					<div class="col-sm-6 col-md-6">
						<label>การแข่งขันที่ต้องการ</label>
						<select class="form-control" ng-model="leagueSelect_team" ng-change="getListTeam(false)">
							<option value="0">กรุณาเลือกลีก</option>
							<option ng-repeat="tmpData in listLeagueTeam" value="{{tmpData.id}}" ng-selected="leagueSelect_team == tmpData.id">{{tmpData.NameTH}}</option>
						</select>
					</div>
					<div class="col-sm-3 col-md-3">
						<label>ปีที่ต้องการ</label>
						<select class="form-control" ng-model="yearSelect_team" ng-change="getListTeam(true)">
							<option value="0">ทุกปี</option>
							<option value="2013">2013/2014</option>
							<option value="2014">2014/2015</option>
							<option value="2015">2015/2016</option>
							<option value="2016">2016/2017</option>
							<option value="2017">2017/2018</option>
						</select>
					</div>
				</center></div>
				<table class="table table-bordered table-striped" ng-if="leagueSelect_team==0">
					<colgroup>
						<col class="col-xs-12">
					</colgroup>
					<thead>
						<tr>
							<th><center>กรุณาเลือกโซนและลีก</center></th>
						</tr>
					</thead>
				</table>
				<table class="table table-bordered table-striped" ng-if="leagueSelect_team!=0">
					<colgroup>
						<col class="col-xs-1">
						<col class="col-xs-5">
						<col class="col-xs-5">
						<col class="col-xs-1">
					</colgroup>
					<thead>
						<tr>
							<th>Team ID</th>
							<th>Team NameEN</th>
							<th>Team NameTH</th>
							<th>Add?</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="tmpData in listTeam" ng-style="tmpData.style">
							<td style="vertical-align: middle;">{{tmpData.id}}</td>
							<td style="vertical-align: middle;">{{tmpData.NameEN}}</td>
							<td style="vertical-align: middle;">{{tmpData.NameTH}}</td>
							<td><button type="button" class="btn btn-success" ng-click="add_team($index)">เพิ่ม</button></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<script>				
	
	(function(angular) {
		var myApp = angular.module('CompApp', []);

		myApp.controller('CompController', ['$scope','$http', function($scope,$http) {
		
			$scope.zoneSelect = 0;
			$scope.yearSelect = 0;
			
			$scope.zoneSelect_match = 0;
			$scope.leagueSelect_match = 0;
			$scope.dateFromSelect_match = "<?php echo date('Y-m-d',strtotime('-1 days')); ?>";
			$scope.dateToSelect_match = "<?php echo date('Y-m-d'); ?>";
			
			$scope.zoneSelect_team = 0;
			$scope.yearSelect_team = 0;
			$scope.leagueSelect_team = 0;
			
			$scope.leagueNameEN = '';
			$scope.leagueNameTH = '';
			$scope.leagueDescription = '';
			
			
			$scope.listLeagueMatch = [];
			$scope.listLeagueTeam = [];
			
			$scope.listLeague = [];
			$scope.listMatch = [];
			$scope.listTeam = [];
			
			$scope.listUserLeague = [];
			$scope.listUserMatch = [];
			$scope.listUserTeam = [];
			
			$scope.getListLeague = function(){
				if($scope.zoneSelect==0){
					$scope.listLeague = [];
				}else{
					$http.post('http://football.kapook.com/api/adminfootball/league/getjson/' + $scope.zoneSelect + '/' + $scope.yearSelect).success(function(data, status, headers, config) {
						$scope.listLeague			=	data;
						for( var i in $scope.listLeague ){
							$scope.listLeague[i].style				=		{};
							for( var j in $scope.listUserLeague ){
								if( $scope.listLeague[i].id == $scope.listUserLeague[j].id ){
									$scope.listLeague[i].style		=		{'background-color' : 'darksalmon'};
									break;
								}
							}
						}
					}).
					error(function(data, status, headers, config) {
						// called asynchronously if an error occurs
						// or server returns response with an error status.
					});
				}
			};
			
			$scope.getListMatch = function(isZone){
				var url = 'http://football.kapook.com/api/adminfootball/livescore/getjson?status=1&dateFrom=' + $scope.dateFromSelect_match + '&dateTo=' + $scope.dateToSelect_match;
				
				if($scope.zoneSelect_match > 0){
					url = url + '&zoneID=' + $scope.zoneSelect_match;
					if(isZone){
						$scope.leagueSelect_match = 0;
						$http.post('http://football.kapook.com/api/adminfootball/league/getjson/' + $scope.zoneSelect_match + '/0').success(function(data, status, headers, config) {
							$scope.listLeagueMatch			=	data;
						}).
						error(function(data, status, headers, config) {
							// called asynchronously if an error occurs
							// or server returns response with an error status.
						});
					}
				}else{
					$scope.leagueSelect_match = 0;
					$scope.listLeagueMatch = [];
				}
				
				if($scope.leagueSelect_match > 0){
					url = url + '&leagueID=' + $scope.leagueSelect_match;
				}
				
				$http.post(url).success(function(data, status, headers, config) {
					$scope.listMatch			=	data;
					for( var i in $scope.listMatch ){
						$scope.listMatch[i].style				=		{};
						for( var j in $scope.listUserMatch ){
							if( $scope.listMatch[i].id == $scope.listUserMatch[j].id ){
								$scope.listMatch[i].style		=		{'background-color' : 'darksalmon'};
								break;
							}
						}
					}
				}).
				error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
				});
			};
			
			$scope.getListTeam = function(isZone){

				if( $scope.zoneSelect_team > 0){
					if(isZone){
						$http.post('http://football.kapook.com/api/adminfootball/league/getjson/' + $scope.zoneSelect_team + '/' + $scope.yearSelect_team).success(function(data, status, headers, config) {
							$scope.listLeagueTeam			=	data;
							var tmp_leagueSelect_team		=	$scope.leagueSelect_team;
							$scope.leagueSelect_team		=	0;
							for( var i in $scope.listLeagueTeam ){
								if( $scope.listLeagueTeam[i].id == tmp_leagueSelect_team ){
									$scope.leagueSelect_team	=	tmp_leagueSelect_team;
									break;
								}
							}
						}).
						error(function(data, status, headers, config) {
							// called asynchronously if an error occurs
							// or server returns response with an error status.
						});
					}
				}else{
					$scope.leagueSelect_team = 0;
					$scope.listLeagueTeam = [];
				}
				
				if( $scope.leagueSelect_team > 0 ){
				
					for( var i in $scope.listLeagueTeam ){
						if( $scope.listLeagueTeam[i].id == $scope.leagueSelect_team ){
							$scope.leagueNameEN			=	$scope.listLeagueTeam[i].NameEN;
							$scope.leagueNameTH			=	$scope.listLeagueTeam[i].NameTH;
							$scope.leagueDescription	=	$scope.listLeagueTeam[i].Description;
							break;
						}
					}
				
					var url = 'http://football.kapook.com/api/adminfootball/team/getByLeague/' + $scope.leagueSelect_team + '?type=json';
					
					$http.post(url).success(function(data, status, headers, config) {
						$scope.listTeam			=	data;
						for( var i in $scope.listTeam ){
							$scope.listTeam[i].style				=		{};
							for( var j in $scope.listUserTeam ){
								if( 
									( $scope.listTeam[i].id == $scope.listUserTeam[j].team_id ) &&
									( $scope.leagueSelect_team == $scope.listUserTeam[j].league_id )
								){
									$scope.listTeam[i].style		=		{'background-color' : 'darksalmon'};
									break;
								}
							}
						}
					}).
					error(function(data, status, headers, config) {
						// called asynchronously if an error occurs
						// or server returns response with an error status.
					});
				}
			};
			
			$scope.delete_league = function(index){
				for( var i in $scope.listLeague ){
					if( $scope.listLeague[i].id == $scope.listUserLeague[index].id ){
						$scope.listLeague[i].style				=		{};
						break;
					}
				}
				$scope.listUserLeague.splice(index, 1);
			};
			
			$scope.delete_match = function(index){
				for( var i in $scope.listMatch ){
					if( $scope.listMatch[i].id == $scope.listUserMatch[index].id ){
						$scope.listMatch[i].style				=		{};
						break;
					}
				}
				$scope.listUserMatch.splice(index, 1);
			};
			
			$scope.delete_team = function(index){
				for( var i in $scope.listTeam ){
					if(
						( $scope.listTeam[i].id == $scope.listUserTeam[index].team_id ) &&
						( $scope.leagueSelect_team == $scope.listUserTeam[index].league_id )
					)
					{
						$scope.listTeam[i].style				=		{};
						break;
					}
				}
				$scope.listUserTeam.splice(index, 1);
			};
			
			$scope.add_league = function(index){
				for( var i in $scope.listUserLeague ){
					if( $scope.listUserLeague[i].id == $scope.listLeague[index].id ){
						return;
					}
				}
				$scope.listLeague[index].style		=		{'background-color' : 'darksalmon'};
				$scope.listUserLeague.push($scope.listLeague[index]);
			};
			
			$scope.add_match = function(index){
				for( var i in $scope.listUserMatch ){
					if( $scope.listUserMatch[i].id == $scope.listMatch[index].id ){
						return;
					}
				}
				$scope.listMatch[index].style		=		{'background-color' : 'darksalmon'};
				$scope.listUserMatch.push($scope.listMatch[index]);
			};
			
			$scope.add_team = function(index){
				for( var i in $scope.listUserTeam ){
					if(
						( $scope.listUserTeam[i].team_id == $scope.listTeam[index].id ) &&
						( $scope.listUserTeam[i].league_id == $scope.leagueSelect_team )
					){
						return;
					}
				}
				$scope.listTeam[index].style		=		{'background-color' : 'darksalmon'};
				
				var tmpArrayData					=		[];
				tmpArrayData["team_id"]				=		$scope.listTeam[index].id;
				tmpArrayData["TeamEN"]				=		$scope.listTeam[index].NameEN;
				tmpArrayData["TeamTH"]				=		$scope.listTeam[index].NameTH;
				tmpArrayData["league_id"]			=		$scope.leagueSelect_team;
				tmpArrayData["LeagueEN"]			=		$scope.leagueNameEN;
				tmpArrayData["LeagueTH"]			=		$scope.leagueNameTH;
				tmpArrayData["LeagueDescription"]	=		$scope.leagueDescription;
			
				$scope.listUserTeam.push(tmpArrayData);
			};
			
			<?php if(isset($this->dataTable)){ ?>
			
			$http.post('http://football.kapook.com/api/adminfootball/comp/get_listleagueandmatchfortable/<?php echo $this->dataTable['id']; ?>?type=json').success(function(data, status, headers, config) {
				$scope.listUserLeague			=	data.league_list;
				$scope.listUserMatch			=	data.match_list;
				$scope.listUserTeam				=	data.team_list;
			}).
			error(function(data, status, headers, config) {
				// called asynchronously if an error occurs
				// or server returns response with an error status.
			});
			
			<?php } ?>

			$http.post('http://football.kapook.com/api/adminfootball/livescore/getjson?status=1&dateFrom=' + $scope.dateFromSelect_match + '&dateTo=' + $scope.dateToSelect_match).success(function(data, status, headers, config) {
				$scope.listMatch			=	data;
				for( var i in $scope.listMatch ){
					$scope.listMatch[i].style				=		{};
					for( var j in $scope.listUserMatch ){
						if( $scope.listMatch[i].id == $scope.listUserMatch[j].id ){
							$scope.listMatch[i].style		=		{'background-color' : 'darksalmon'};
							break;
						}
					}
				}
			}).
			error(function(data, status, headers, config) {
				// called asynchronously if an error occurs
				// or server returns response with an error status.
			});
			
		}]);

	})(window.angular);
					
	$(document).ready(function(){
		$('.MatchDate').datetimepicker({
			lang:'en',
			timepicker:false,
			format:'Y-m-d'
		});
	});
			
	</script>