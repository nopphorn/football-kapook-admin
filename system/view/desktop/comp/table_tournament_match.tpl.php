<?php
	$round_index	= 	$this->round_index;
	$match_index	=	$this->match_index;
	$dataMatch		=	$this->dataTable['RoundData'][$round_index][$match_index];
?>
<div class="container" ng-app="CompApp" ng-controller="CompController">
	<form action="<?php echo BASE_HREF; ?>api/adminfootball/comp/update_table_tournament_match" method="POST">
		<!-- comp_id -->
		<input type="hidden" name="comp_id" value="<?php echo $this->dataComp['id']; ?>">
		<!-- table_id -->
		<?php if(isset($this->dataTable)) {
			?><input type="hidden" name="id" value="<?php echo $this->dataTable['id']; ?>">
		<?php } ?>
		<!-- round_id -->
		<?php if($this->dataRound) { ?>
			<input type="hidden" name="round_id" value="<?php echo $this->dataRound['id']; ?>">
		<?php }else{ ?>
			<input type="hidden" name="round_id" value="-1">
		<?php } ?>
		<!-- round_index & match_index -->
		<input type="hidden" name="round_index" value="<?php echo $round_index; ?>">
		<input type="hidden" name="match_index" value="<?php echo $match_index; ?>">
		
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th class="col-xs-6" colspan="3"><div style="font-size: x-large;float: left;">Team1</div><button class="btn btn-default" type="button" style="float: right;" onclick="window.open('<?php echo BASE_HREF; ?>api/adminfootball/comp/get_listTeamForMatchDummy/1','','height=400,width=400');">ค้นหาทีม <span class="glyphicon glyphicon-search"></span></button></th>
					<th class="col-xs-6" colspan="3"><div style="font-size: x-large;float: left;">Team2</div><button class="btn btn-default" type="button" style="float: right;" onclick="window.open('<?php echo BASE_HREF; ?>api/adminfootball/comp/get_listTeamForMatchDummy/2','','height=400,width=400');">ค้นหาทีม <span class="glyphicon glyphicon-search"></span></button></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td style="width: 10%;">Team Name</td>
					<td ><input type="text" class="form-control" placeholder="Team 1 Name" id="Team1Name" name="Team1" value="<?php echo $dataMatch['Team1']; ?>"></td>
					<td class="col-xs-1" rowspan="4"><input type="number" name="Team1FTScore" class="form-control" style="width: 109px;height: 100%;font-size: 43pt;text-align: right;" value="<?php echo $dataMatch['Team1FTScore']; ?>"></td>
					<td class="col-xs-1" rowspan="4"><input type="number" name="Team2FTScore" class="form-control" style="width: 109px;height: 100%;font-size: 43pt;" value="<?php echo $dataMatch['Team2FTScore']; ?>"></td>
					<td style="width: 10%;">Team Name</td>
					<td ><input type="text" class="form-control" placeholder="Team 2 Name" id="Team2Name" name="Team2" value="<?php echo $dataMatch['Team2']; ?>"></td>
				</tr>
				<tr>
					<td>Team KP ID</td>
					<td><input type="text" class="form-control" placeholder="Team 1 Kapook ID" id="Team1KPID" name="Team1KPID" value="<?php echo $dataMatch['Team1KPID']; ?>"></td>
					<td>Team KP ID</td>
					<td><input type="text" class="form-control" placeholder="Team 2 Kapook ID" id="Team2KPID" name="Team2KPID" value="<?php echo $dataMatch['Team2KPID']; ?>"></td>
				</tr>
				<tr>
					<td>Path logo</td>
					<td><input type="text" class="form-control" placeholder="Team 1 Path Logo" id="Team1Logo" name="Team1Logo" value="<?php echo $dataMatch['Team1Logo']; ?>"></td>
					<td>Path logo</td>
					<td><input type="text" class="form-control" placeholder="Team 2 Path Logo" id="Team2Logo" name="Team2Logo" value="<?php echo $dataMatch['Team2Logo']; ?>"></td>
				</tr>
				<tr>
					<td>URL Team</td>
					<td><input type="text" class="form-control" placeholder="Team 1 URL" id="Team1URL" name="Team1URL" value="<?php echo $dataMatch['Team1URL']; ?>"></td>
					<td>URL Team</td>
					<td><input type="text" class="form-control" placeholder="Team 2 URL" id="Team2URL" name="Team2URL" value="<?php echo $dataMatch['Team2URL']; ?>"></td>
				</tr>
			</tbody>
		</table>
		
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th class="col-xs-12" colspan="3"><div style="font-size: x-large;"><center>เลือกทีมที่ชนะ</center></div></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="col-xs-4"><input type="radio" name="TeamWinner" id="TeamWinner_1" value="1"> ทีมที่ 1</td>
					<td class="col-xs-4"><input type="radio" name="TeamWinner" id="TeamWinner_0" value="0"> ไม่เลือก/เสมอ</td>
					<td class="col-xs-4"><input type="radio" name="TeamWinner" id="TeamWinner_2" value="2"> ทีมที่ 2</td>
				</tr>
			</tbody>
		</table>
		<table class="table table-bordered table-striped" ng-repeat="tmpData in listMatch">
			<thead>
				<tr>
					<th class="col-xs-12" colspan="6">
						<div style="font-size: x-large;float: left;">นัดที่ {{$index+1}}</div>
						
						<button class="btn btn-danger" type="button" style="float: right;" ng-click="remove_match($index)">
							ลบการแข่งขัน
						</button>
						
					</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="col-xs-6" colspan="3"><div style="font-size: x-large;float: left;">Team1</div><button class="btn btn-default" type="button" style="float: right;" ng-click="select_team($index,1)">ค้นหาทีม <span class="glyphicon glyphicon-search"></span></button></td>
					<td class="col-xs-6" colspan="3"><div style="font-size: x-large;float: left;">Team2</div><button class="btn btn-default" type="button" style="float: right;" ng-click="select_team($index,2)">ค้นหาทีม <span class="glyphicon glyphicon-search"></span></button></td>
				</tr>
				
				<tr>
					<td style="width: 10%;">Team Name</td>
					<td ><input type="text" class="form-control" placeholder="Team 1 Name" id="Team1Name_{{$index}}" name="Team1_arr[{{$index}}]" ng-model="tmpData.Team1" ></td>
					<td class="col-xs-1" rowspan="3"><input type="number" name="Team1FTScore_arr[{{$index}}]" class="form-control" style="width: 109px;height: 100%;font-size: 43pt;text-align: right;" ng-model="tmpData.Team1FTScore"></td>
					<td class="col-xs-1" rowspan="3"><input type="number" name="Team2FTScore_arr[{{$index}}]" class="form-control" style="width: 109px;height: 100%;font-size: 43pt;" ng-model="tmpData.Team2FTScore"></td>
					<td style="width: 10%;">Team Name</td>
					<td ><input type="text" class="form-control" placeholder="Team 2 Name" id="Team2Name_{{$index}}" name="Team2_arr[{{$index}}]" ng-model="tmpData.Team2"></td>
				</tr>
				
				<tr>
					<td>Path logo</td>
					<td><input type="text" class="form-control" placeholder="Team 1 Path Logo" id="Team1Logo_{{$index}}" name="Team1Logo_arr[{{$index}}]" ng-model="tmpData.Team1Logo"></td>
					<td>Path logo</td>
					<td><input type="text" class="form-control" placeholder="Team 2 Path Logo" id="Team2Logo_{{$index}}" name="Team2Logo_arr[{{$index}}]" ng-model="tmpData.Team2Logo"></td>
				</tr>
				
				<tr>
					<td>URL Team</td>
					<td><input type="text" class="form-control" placeholder="Team 1 URL" id="Team1URL_{{$index}}" name="Team1URL_arr[{{$index}}]" ng-model="tmpData.Team1URL" ></td>
					<td>URL Team</td>
					<td><input type="text" class="form-control" placeholder="Team 2 URL" id="Team2URL_{{$index}}" name="Team2URL_arr[{{$index}}]" ng-model="tmpData.Team2URL" ></td>
				</tr>
				
				<tr>
					<td><b>Match Time</b></td>
					<td colspan="2"><input type="text" class="form-control" name="MatchDateTime_arr[{{$index}}]" placeholder="เวลาการแข่งขัน" ng-model="tmpData.MatchDateTime" datepicker /></td>
					<td><b>Match Status</b></td>
					<td colspan="2">
						<select class="form-control" name="MatchStatus_arr[{{$index}}]" ng-model="tmpData.MatchStatus">
							<option value="Sched">Sched</option>
							<option value="1 HF">1 HF</option>
							<option value="H/T">H/T</option>
							<option value="2 HF">2 HF</option>
							<option value="E/T">E/T</option>
							<option value="Pen">Pen</option>
							<option value="Fin">Fin</option>
							<option value="Int">Int</option>
							<option value="Abd">Abd</option>
							<option value="Post">Post</option>
							<option value="Canc">Canc</option>
						</select>
					</td>
				</tr>
				
				<tr>
					<td><b>Match URL</b></td>
					<td colspan="5"><input type="text" class="form-control" name="MatchURL_arr[{{$index}}]" placeholder="URL การแข่งขัน" ng-model="tmpData.MatchURL" /></td>
				</tr>
				
			</tbody>
		</table>
		<div class="row form-group" ng-if="listMatch.length < 2"><center>
			<div class="col-sm-12 col-md-12"><button type="button" ng-click="add_match()" class="btn btn-success btn-lg btn-block"><span class="glyphicon glyphicon-plus-sign"></span> เพิ่มคู่การแข่งขัน</button></div>
		</center></div>
		<div class="row form-group"><center>
			<div class="col-sm-6 col-md-6"><button type="submit" class="btn btn-primary btn-lg btn-block">บันทึกการแข่งขัน</button></div>
			<?php if($this->dataRound) { ?>
			<div class="col-sm-6 col-md-6"><a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/table_tournament/<?php echo $this->dataComp['id']; ?>/<?php echo $this->dataRound['id']; ?>" class="btn btn-default btn-lg btn-block">กลับไปหน้าตารางการแข่งขัน</a></div>
			<?php }else{ ?>
			<div class="col-sm-6 col-md-6"><a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/table_tournament/<?php echo $this->dataComp['id']; ?>/<?php echo $this->dataComp['id']; ?>" class="btn btn-default btn-lg btn-block">กลับไปหน้าตารางการแข่งขัน</a></div>
			<?php } ?>
		</center></div>
	</form>
</div>

<script>				
	$(document).ready(function(){
		$('#TeamWinner_<?php echo $dataMatch['TeamWinner']; ?>').prop('checked',true);
	});
	
	(function(angular) {
		var myApp = angular.module('CompApp', []);
		var date = new Date();
		if(date.getMonth()<9){
			var month = "0" + (date.getMonth()+1)
		}else{
			var month = (date.getMonth()+1)
		}
		
		if(date.getDate()<10){
			var date_num = "0" + date.getDate()
		}else{
			var date_num = date.getDate()
		}
		
		if(date.getHours()<10){
			var hour = "0" + date.getHours()
		}else{
			var hour = date.getHours()
		}
		
		if(date.getMinutes()<10){
			var minutes = "0" + date.getMinutes()
		}else{
			var minutes = date.getMinutes()
		}
		
		if(date.getSeconds()<10){
			var seconds = "0" + date.getSeconds()
		}else{
			var seconds = date.getSeconds()
		}
		
		var today = date.getFullYear() + "-" + month + "-" + date_num + " " + hour + ":" + minutes + ":" + seconds;

		myApp.directive("datepicker", function () {
			return {
				link: function (scope, elem, attrs) {
					elem.datetimepicker({
						lang:'en',
						format:'Y-m-d H:i:s'
					});
				}
			}
		});

		myApp.controller('CompController', ['$scope','$http', function($scope,$http) {
			$scope.listMatch = [];
			
			/*$scope.popup_team = function(index){
				window.open('<?php echo BASE_HREF; ?>api/adminfootball/comp/get_listTeamForMatchDummy/' + index,'','height=400,width=400');
			};*/
			
			$scope.add_match = function(){
				$scope.listMatch.push(
					{
						Team1 : '',
						Team2 : '',
						Team1FTScore : 0,
						Team2FTScore : 0,
						Team1Logo : '',
						Team2Logo : '',
						Team1URL : '',
						Team2URL : '',
						MatchDateTime : today,
						MatchStatus : "Sched",
						MatchURL : ""
					}
				);
			};
			
			$scope.remove_match = function(index){
				$scope.listMatch.splice(index, 1);
			};
			
			$scope.select_team = function(index,side){
				window.open('<?php echo BASE_HREF; ?>api/adminfootball/comp/get_listTeamForMatchDummy/' + side + '/' + index,'','height=400,width=400');
			};
			
			<?php foreach($dataMatch['MatchData'] as $tmpMatch){ ?>
				$scope.listMatch.push(
					{
						Team1 : '<?php echo $tmpMatch['Team1']; ?>',
						Team2 : '<?php echo $tmpMatch['Team2']; ?>',
						Team1FTScore : <?php echo $tmpMatch['Team1FTScore']; ?>,
						Team2FTScore : <?php echo $tmpMatch['Team2FTScore']; ?>,
						Team1Logo : '<?php echo $tmpMatch['Team1Logo']; ?>',
						Team2Logo : '<?php echo $tmpMatch['Team2Logo']; ?>',
						Team1URL : '<?php echo $tmpMatch['Team1URL']; ?>',
						Team2URL : '<?php echo $tmpMatch['Team2URL']; ?>',
						MatchDateTime : '<?php echo $tmpMatch['MatchDateTime']; ?>',
						MatchStatus : '<?php echo $tmpMatch['MatchStatus']; ?>',
						MatchURL : '<?php echo $tmpMatch['MatchURL']; ?>'
					}
				);
			<?php } ?>
		}]);
	})(window.angular);
	
</script>