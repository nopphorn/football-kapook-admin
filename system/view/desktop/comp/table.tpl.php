<div class="container">
	<?php if(($this->site_page == "table_round")&&($this->dataRoundTable)){ ?>
	<div class="row form-group"><center>
		<div class="col-sm-6 col-md-4"><a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/table_round_select/<?php echo $this->dataComp['id']; ?>/<?php echo $this->dataRoundTable['id']; ?>" class="btn btn-success btn-lg btn-block">สร้างตารางคะแนนจากลีกและการแข่งชัน</a></div>
		<div class="col-sm-6 col-md-4"><a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/table_round_dummy/<?php echo $this->dataComp['id']; ?>/<?php echo $this->dataRoundTable['id']; ?>" class="btn btn-success btn-lg btn-block">สร้างตารางคะแนนจำลอง(Dammy Table)</a></div>
		<div class="col-sm-6 col-md-4"><a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/table_round_tournament/<?php echo $this->dataComp['id']; ?>/<?php echo $this->dataRoundTable['id']; ?>" class="btn btn-success btn-lg btn-block">สร้างแบบทัวร์นาเม้น(Tournament)</a></div>
	</center></div>
	<?php }else if($this->site_page == "table"){ ?>
	<div class="row form-group"><center>
		<div class="col-sm-6 col-md-4"><a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/table_select/<?php echo $this->dataComp['id']; ?>" class="btn btn-success btn-lg btn-block">สร้างตารางคะแนนจากลีกและการแข่งชัน</a></div>
		<div class="col-sm-6 col-md-4"><a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/table_dummy/<?php echo $this->dataComp['id']; ?>" class="btn btn-success btn-lg btn-block">สร้างตารางคะแนนจำลอง(Dammy Table)</a></div>
		<div class="col-sm-6 col-md-4"><a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/table_tournament/<?php echo $this->dataComp['id']; ?>" class="btn btn-success btn-lg btn-block">สร้างแบบทัวร์นาเม้น(Tournament)</a></div>
	</center></div>
	<?php } ?>
	
	<?php if($this->site_page == "table") { ?>
	
	<a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/table_round/<?php echo $this->dataComp['id']; ?>/" class="btn btn-success glyphicon glyphicon-plus" type="button" role="button" style="float: right;">สร้างรอบการแข่งขัน</a>
	<h2>รอบการแข่งขันสำหรับโปรแกรมการแข่งขัน</h2>
	<hr style="margin-top: 0px;margin-bottom: 10px;">
	<table class="table table-bordered table-striped">
		<colgroup>
			<col class="col-xs-1">
			<col class="col-xs-6">
			<col class="col-xs-2">
			<col class="col-xs-2">
			<col class="col-xs-1">
			<col class="col-xs-1">
		</colgroup>
		<thead>
			<tr>
				<th>Round ID</th>
				<th>Round Name</th>
				<th>TabShow</th>
				<th>Order</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$max = count($this->listRound);
			foreach( $this->listRound as $index => $tmpRound ) { ?>
			<tr>
				<td style="vertical-align: middle;"><?php echo $tmpRound['id']; ?></td>
				<td style="vertical-align: middle;"><?php echo $tmpRound['name']; ?></td>
				<td style="vertical-align: middle;">
				<?php if( intval($this->dataComp['TabTable']) == intval($tmpRound['id']) ) { ?>
					<button class="btn btn-success" disabled="disabled" style="float: right;"><span class="glyphicon glyphicon-ok">  เลือกแท็บนี้</span></button>
				<?php }else{ ?>
					<a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/set_tabtable/<?php echo $this->dataComp['id']; ?>/<?php echo $tmpRound['id']; ?>" type="button" class="btn btn-default" role="button" style="float: right;">เลือกแสดงแท็บนี้</a>
				<?php }?></td>
				<td style="vertical-align: middle;">
					<?php if(($index < $max-1) && ($max > 1)) { ?>
						<a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/order_down_roundtable/<?php echo $this->dataComp['id']; ?>/<?php echo $tmpRound['id']; ?>" type="button" class="btn btn-default" role="button" style="float: right;"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a>
					<?php } ?>
					
					<?php if($index>0) { ?>
						<a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/order_up_roundtable/<?php echo $this->dataComp['id']; ?>/<?php echo $tmpRound['id']; ?>" type="button" class="btn btn-default" role="button" style="float: right;"><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span></a>
					<?php } ?></td>
				<td style="vertical-align: middle;">
					<a style="float: right;margin-right: 10px;" href="<?php echo BASE_HREF; ?>api/adminfootball/comp/table_round/<?php echo $this->dataComp['id']; ?>/<?php echo $tmpRound['id']; ?>" type="button" class="btn btn-primary" role="button" ><span class="glyphicon glyphicon-pencil" aria-hidden="true" ></span></a>
				</td>
				<td><a onclick="return confirm('แน่ใจแล้วนะ ว่าจะลบรอบนี้?')" href="<?php echo BASE_HREF; ?>api/adminfootball/comp/delete_roundtable/<?php echo $tmpRound['id']; ?>?comp_id=<?php echo $this->dataComp['id']; ?>" type="button" class="btn btn-danger">ลบ</a></td>
			</tr>
			<?php } ?>
			<tr>
				<td style="vertical-align: middle;" colspan="2"></td>
				<td style="vertical-align: middle;">
				<?php if( intval($this->dataComp['TabTable']) == -1 ) { ?>
					<button class="btn btn-success" disabled="disabled" style="float: right;"><span class="glyphicon glyphicon-ok">  เลือกแสดงแบบดั้งเดิม</span></button>
				<?php }else{ ?>
					<a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/set_tabtable/<?php echo $this->dataComp['id']; ?>" type="button" class="btn btn-default" role="button" style="float: right;">แสดงแบบดั้งเดิม</a>
				<?php }?></td>
				<td style="vertical-align: middle;" colspan="3"></td>
			</tr>
		</tbody>
    </table>

	<?php } else if($this->site_page == "table_round") { ?>
		<label>Name Round</label>
		<?php if($this->dataRoundTable) { ?>
		<form action="<?php echo BASE_HREF; ?>api/adminfootball/comp/update_roundtable" method="POST">
			<input type="hidden" name="id" value="<?php echo $this->dataRoundTable['id']; ?>">
		<?php }else{ ?>
		<form action="<?php echo BASE_HREF; ?>api/adminfootball/comp/create_roundtable" method="POST">
		<?php } ?>
			<input type="hidden" name="comp_id" value="<?php echo $this->dataComp['id']; ?>">
			<div class="form-group row">
				<div class="col-sm-10 col-md-10">
					<input type="text" class="form-control" id="name" name="name" value="<?php echo $this->dataRoundTable['name']; ?>" placeholder="ชื่อรอบการแข่งขัน">
				</div>
				<?php if($this->dataRoundTable) { ?>
				<div class="col-sm-2 col-md-2"><input type="submit" class="btn btn-success" value="เปลี่ยนชื่อรอบ"></div>
				<?php }else{ ?>
				<div class="col-sm-2 col-md-2"><input type="submit" class="btn btn-success" value="สร้างรอบใหม่"></div>	
				<?php } ?>
			</div>
		</form>
	<?php } ?>
	
	<?php if( (($this->site_page == "table_round")&&($this->dataRoundTable)) || ($this->site_page == "table") ){ ?>
	
	<h2>ตารางคะแนน</h2><hr style="margin-top: 0px;margin-bottom: 10px;">
	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	  
		<?php
		$index = 0;
		$max = count($this->listTable);
		foreach($this->listTable as $tmpData){ 
			
		?>
		
		<div class="panel panel-default">
			
			<div class="panel-heading" role="tab" id="heading_<?php echo $tmpData['id'];?>">
				<h4 class="panel-title" style="padding-bottom: 10px;">
					<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo $tmpData['id'];?>" aria-expanded="false" aria-controls="collapse_<?php echo $tmpData['id'];?>" style="vertical-align: -5px;">
						<?php echo $tmpData['name']; ?>
					</a>
					
					<?php if($this->site_page == "table_round"){ ?>
					<a onclick="return confirm('แน่ใจแล้วนะ ว่าจะลบการแข่งขันนี้?')" href="<?php echo BASE_HREF; ?>api/adminfootball/comp/delete_table/<?php echo $tmpData['id'];?>?comp_id=<?php echo $this->dataComp['id']; ?>&round_id=<?php echo $this->dataRoundTable['id']; ?>" type="button" class="btn btn-danger" role="button" style="float: right;"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>
					<?php }else{ ?>
					<a onclick="return confirm('แน่ใจแล้วนะ ว่าจะลบการแข่งขันนี้?')" href="<?php echo BASE_HREF; ?>api/adminfootball/comp/delete_table/<?php echo $tmpData['id'];?>?comp_id=<?php echo $this->dataComp['id']; ?>" type="button" class="btn btn-danger" role="button" style="float: right;"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>
					<?php } ?>
					
					<?php if($this->site_page == "table_round"){ ?>
						<?php if($tmpData['type']==1){ ?>
						<a style="float: right;margin-right: 10px;" href="<?php echo BASE_HREF; ?>api/adminfootball/comp/table_round_select/<?php echo $this->dataComp['id']; ?>/<?php echo $this->dataRoundTable['id']; ?>/<?php echo $tmpData['id']; ?>" type="button" class="btn btn-primary" role="button" ><span class="glyphicon glyphicon-pencil" aria-hidden="true" ></span></a>
						<?php }else if($tmpData['type']==2){ ?>
						<a style="float: right;margin-right: 10px;" href="<?php echo BASE_HREF; ?>api/adminfootball/comp/table_round_dummy/<?php echo $this->dataComp['id']; ?>/<?php echo $this->dataRoundTable['id']; ?>/<?php echo $tmpData['id']; ?>" type="button" class="btn btn-primary" role="button" ><span class="glyphicon glyphicon-pencil" aria-hidden="true" ></span></a>
						<?php }else if($tmpData['type']==3){ ?>
						<a style="float: right;margin-right: 10px;" href="<?php echo BASE_HREF; ?>api/adminfootball/comp/table_round_tournament/<?php echo $this->dataComp['id']; ?>/<?php echo $this->dataRoundTable['id']; ?>/<?php echo $tmpData['id']; ?>" type="button" class="btn btn-primary" role="button" ><span class="glyphicon glyphicon-pencil" aria-hidden="true" ></span></a>
						<?php } ?>
					<?php }else{ ?>
						<?php if($tmpData['type']==1){ ?>
						<a style="float: right;margin-right: 10px;" href="<?php echo BASE_HREF; ?>api/adminfootball/comp/table_select/<?php echo $this->dataComp['id']; ?>/<?php echo $tmpData['id']; ?>" type="button" class="btn btn-primary" role="button" ><span class="glyphicon glyphicon-pencil" aria-hidden="true" ></span></a>
						<?php }else if($tmpData['type']==2){ ?>
						<a style="float: right;margin-right: 10px;" href="<?php echo BASE_HREF; ?>api/adminfootball/comp/table_dummy/<?php echo $this->dataComp['id']; ?>/<?php echo $tmpData['id']; ?>" type="button" class="btn btn-primary" role="button" ><span class="glyphicon glyphicon-pencil" aria-hidden="true" ></span></a>
						<?php }else if($tmpData['type']==3){ ?>
						<a style="float: right;margin-right: 10px;" href="<?php echo BASE_HREF; ?>api/adminfootball/comp/table_tournament/<?php echo $this->dataComp['id']; ?>/<?php echo $tmpData['id']; ?>" type="button" class="btn btn-primary" role="button" ><span class="glyphicon glyphicon-pencil" aria-hidden="true" ></span></a>
						<?php } ?>
					<?php } ?>
					
					<?php if(($this->site_page == "table_round")&&($this->dataRoundTable)){ ?>
					
						<?php if(($index < $max-1) && ($max > 1)) { ?>
						<a style="float: right;margin-right: 10px;" href="<?php echo BASE_HREF; ?>api/adminfootball/comp/order_up_tableinround/<?php echo $this->dataComp['id']; ?>/<?php echo $this->dataRoundTable['id']; ?>/<?php echo $tmpData['id']; ?>" type="button" class="btn btn-default" role="button" style="float: right;"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a>
						<?php } ?>
						
						<?php if($index>0) { ?>
						<a style="float: right;margin-right: 10px;" href="<?php echo BASE_HREF; ?>api/adminfootball/comp/order_up_tableinround/<?php echo $this->dataComp['id']; ?>/<?php echo $this->dataRoundTable['id']; ?>/<?php echo $tmpData['id']; ?>" type="button" class="btn btn-default" role="button" style="float: right;"><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span></a>
						<?php } ?>
					
					<?php }else if($this->site_page == "table"){ ?>
					
						<?php if(($index < $max-1) && ($max > 1)) { ?>
						<a style="float: right;margin-right: 10px;" href="<?php echo BASE_HREF; ?>api/adminfootball/comp/order_down_table/<?php echo $this->dataComp['id']; ?>/<?php echo $tmpData['id']; ?>" type="button" class="btn btn-default" role="button" style="float: right;"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a>
						<?php } ?>
						
						<?php if($index>0) { ?>
						<a style="float: right;margin-right: 10px;" href="<?php echo BASE_HREF; ?>api/adminfootball/comp/order_up_table/<?php echo $this->dataComp['id']; ?>/<?php echo $tmpData['id']; ?>" type="button" class="btn btn-default" role="button" style="float: right;"><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span></a>
						<?php } ?>
					
					<?php } ?>
					
				</h4>
			</div>
			
			<? if(($tmpData['type']==1)||($tmpData['type']==2)){ ?>
			<div id="collapse_<?php echo $tmpData['id'];?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?php echo $tmpData['id'];?>">
				<table class="table table-bordered table-striped">
					<colgroup>
						<col class="col-xs-1">
						<col class="col-xs-3">
						<col class="col-xs-1">
						<col class="col-xs-1">
						<col class="col-xs-1">
						<col class="col-xs-1">
						<col class="col-xs-1">
						<col class="col-xs-1">
						<col class="col-xs-1">
						<col class="col-xs-1">
					</colgroup>
					<thead>
						<tr>
							<th>Logo</th>
							<th>Team</th>
							<th>แข่ง</th>
							<th>ชนะ</th>
							<th>แพ้</th>
							<th>เสมอ</th>
							<th>ได้</th>
							<th>เสีย</th>
							<th>ผลต่าง</th>
							<th>คะแนน</th>
						</tr>
					</thead>
					<tbody><?php
						foreach( $tmpData['TeamData'] as $tmpTeam ){ ?>
						<tr>
							<td><img src="<?php echo $tmpTeam['logo']; ?>" width="24" height="24" alt=""></td>
							<td><?php echo $tmpTeam['name']; ?></td>
							<td><?php echo intval($tmpTeam['win']+$tmpTeam['lose']+$tmpTeam['draw']); ?></td>
							<td><?php echo intval($tmpTeam['win']); ?></td>
							<td><?php echo intval($tmpTeam['lose']); ?></td>
							<td><?php echo intval($tmpTeam['draw']); ?></td>
							<td><?php echo intval($tmpTeam['goal_for']); ?></td>
							<td><?php echo intval($tmpTeam['goal_against']); ?></td>
							<td><?php echo intval($tmpTeam['goal_dif']); ?></td>
							<td><?php echo intval($tmpTeam['point']); ?></td>
						</tr><?php
						} ?>
				  </tbody>
				</table>
			</div>
			<?php }else if($tmpData['type']==3){ ?>
			<div id="collapse_<?php echo $tmpData['id'];?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?php echo $tmpData['id'];?>">
				
			</div>
			<?php } ?>
		</div>
		<?php
			$index++;
		}
		?>
	</div>
	<?php } ?>
</div>