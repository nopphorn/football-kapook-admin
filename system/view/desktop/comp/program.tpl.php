<div class="container">
	<?php if(($this->site_page == "program_round")&&($this->dataRoundMatch)){ ?>
	<div class="row form-group"><center>
		<div class="col-sm-6 col-md-6"><a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/program_round_league/<?php echo $this->dataComp['id']; ?>/<?php echo $this->dataRoundMatch['id']; ?>" class="btn btn-success btn-lg btn-block">เพิ่ม/ลด ลีก</a></div>
		<div class="col-sm-6 col-md-6"><a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/program_round_dummy/<?php echo $this->dataComp['id']; ?>/<?php echo $this->dataRoundMatch['id']; ?>" class="btn btn-success btn-lg btn-block">เพิ่มการแข่งขันจำลอง(Dammy Match)</a></div>
	</center></div>
	<?php }else if($this->site_page == "program"){ ?>
	<div class="row form-group"><center>
		<div class="col-sm-6 col-md-6"><a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/program_league/<?php echo $this->dataComp['id']; ?>" class="btn btn-success btn-lg btn-block">เพิ่ม/ลด ลีก</a></div>
		<div class="col-sm-6 col-md-6"><a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/program_dummy/<?php echo $this->dataComp['id']; ?>" class="btn btn-success btn-lg btn-block">เพิ่มการแข่งขันจำลอง(Dammy Match)</a></div>
	</center></div>
	<?php } ?>
	
	<?php if($this->site_page == "program") { ?>
	
	<a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/program_round/<?php echo $this->dataComp['id']; ?>/" class="btn btn-success glyphicon glyphicon-plus" type="button" role="button" style="float: right;">สร้างรอบการแข่งขัน</a>
	<h2>รอบการแข่งขันสำหรับโปรแกรมการแข่งขัน</h2>
	<hr style="margin-top: 0px;margin-bottom: 10px;">
	<table class="table table-bordered table-striped">
		<colgroup>
			<col class="col-xs-1">
			<col class="col-xs-6">
			<col class="col-xs-2">
			<col class="col-xs-2">
			<col class="col-xs-1">
			<col class="col-xs-1">
		</colgroup>
		<thead>
			<tr>
				<th>Round ID</th>
				<th>Round Name</th>
				<th>TabShow</th>
				<th>Order</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$max = count($this->listRound);
			foreach( $this->listRound as $index => $tmpRound ) { ?>
			<tr>
				<td style="vertical-align: middle;"><?php echo $tmpRound['id']; ?></td>
				<td style="vertical-align: middle;"><?php echo $tmpRound['name']; ?></td>
				<td style="vertical-align: middle;">
				<?php if( intval($this->dataComp['TabProgram']) == intval($tmpRound['id']) ) { ?>
					<button class="btn btn-success" disabled="disabled" style="float: right;"><span class="glyphicon glyphicon-ok">  เลือกแท็บนี้</span></button>
				<?php }else{ ?>
					<a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/set_tabprogram/<?php echo $this->dataComp['id']; ?>/<?php echo $tmpRound['id']; ?>" type="button" class="btn btn-default" role="button" style="float: right;">เลือกแสดงแท็บนี้</a>
				<?php }?></td>
				<td style="vertical-align: middle;">
					<?php if(($index < $max-1) && ($max > 1)) { ?>
						<a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/order_down_roundmatch/<?php echo $this->dataComp['id']; ?>/<?php echo $tmpRound['id']; ?>" type="button" class="btn btn-default" role="button" style="float: right;"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a>
					<?php } ?>
					
					<?php if($index>0) { ?>
						<a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/order_up_roundmatch/<?php echo $this->dataComp['id']; ?>/<?php echo $tmpRound['id']; ?>" type="button" class="btn btn-default" role="button" style="float: right;"><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span></a>
					<?php } ?></td>
				<td style="vertical-align: middle;">
					<a style="float: right;margin-right: 10px;" href="<?php echo BASE_HREF; ?>api/adminfootball/comp/program_round/<?php echo $this->dataComp['id']; ?>/<?php echo $tmpRound['id']; ?>" type="button" class="btn btn-primary" role="button" ><span class="glyphicon glyphicon-pencil" aria-hidden="true" ></span></a>
				</td>
				<td><a onclick="return confirm('แน่ใจแล้วนะ ว่าจะลบรอบนี้?')" href="<?php echo BASE_HREF; ?>api/adminfootball/comp/delete_roundmatch/<?php echo $tmpRound['id']; ?>?comp_id=<?php echo $this->dataComp['id']; ?>" type="button" class="btn btn-danger">ลบ</a></td>
			</tr>
			<?php } ?>
			<tr>
				<td style="vertical-align: middle;" colspan="2"></td>
				<td style="vertical-align: middle;">
				<?php if( intval($this->dataComp['TabProgram']) == -1 ) { ?>
					<button class="btn btn-success" disabled="disabled" style="float: right;"><span class="glyphicon glyphicon-ok">  เลือกแสดงแบบดั้งเดิม</span></button>
				<?php }else{ ?>
					<a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/set_tabprogram/<?php echo $this->dataComp['id']; ?>" type="button" class="btn btn-default" role="button" style="float: right;">แสดงแบบดั้งเดิม</a>
				<?php }?></td>
				<td style="vertical-align: middle;" colspan="3"></td>
			</tr>
		</tbody>
    </table>
	
	<?php } else if($this->site_page == "program_round") { ?>
		<label>Name Round</label>
		<?php if($this->dataRoundMatch) { ?>
		<form action="<?php echo BASE_HREF; ?>api/adminfootball/comp/update_roundmatch" method="POST">
			<input type="hidden" name="id" value="<?php echo $this->dataRoundMatch['id']; ?>">
		<?php }else{ ?>
		<form action="<?php echo BASE_HREF; ?>api/adminfootball/comp/create_roundmatch" method="POST">
		<?php } ?>
			<input type="hidden" name="comp_id" value="<?php echo $this->dataComp['id']; ?>">
			<div class="form-group row">
				<div class="col-sm-10 col-md-10">
					<input type="text" class="form-control" id="name" name="name" value="<?php echo $this->dataRoundMatch['name']; ?>" placeholder="ชื่อรอบการแข่งขัน">
				</div>
				<?php if($this->dataRoundMatch) { ?>
				<div class="col-sm-2 col-md-2"><input type="submit" class="btn btn-success" value="เปลี่ยนชื่อรอบ"></div>
				<?php }else{ ?>
				<div class="col-sm-2 col-md-2"><input type="submit" class="btn btn-success" value="สร้างรอบใหม่"></div>	
				<?php } ?>
			</div>
		</form>
	<?php } ?>
	
	
	<?php if( (($this->site_page == "program_round")&&($this->dataRoundMatch)) || ($this->site_page == "program") ){ ?>
	
	<h2>ลีกที่นำตารางการแข่งขันมาใช้</h2><hr style="margin-top: 0px;margin-bottom: 10px;">
	<table class="table table-bordered table-striped">
		<colgroup>
			<col class="col-xs-1">
			<col class="col-xs-4">
			<col class="col-xs-4">
			<col class="col-xs-3">
		</colgroup>
		<thead>
			<tr>
				<th>League ID</th>
				<th>League NameTH</th>
				<th>League NameEN</th>
				<th>League Description</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach( $this->listLeague as $tmpLeague ) { ?>
			<tr>
				<td style="vertical-align: middle;"><?php echo $tmpLeague['id']; ?></td>
				<td style="vertical-align: middle;"><?php echo $tmpLeague['NameTH']; ?></td>
				<td style="vertical-align: middle;"><?php echo $tmpLeague['NameEN']; ?></td>
				<td style="vertical-align: middle;"><?php echo $tmpLeague['Description']; ?></td>
			</tr>
			<?php } ?>
		</tbody>
    </table>
	
	<h2>การแข่งขันทั้งหมด</h2><hr style="margin-top: 0px;margin-bottom: 10px;">
	<table class="table table-bordered table-striped">
		<colgroup>
			<col class="col-xs-1">
			<col class="col-xs-2">
			<col class="col-xs-1">
			<col class="col-xs-2">
			<col class="col-xs-2">
			<col class="col-xs-1">
			<col class="col-xs-1">
			<col class="col-xs-1">
		</colgroup>
		<thead>
			<tr>
				<th>Match ID</th>
				<th>ทีม 1</th>
				<th>ผลการแข่งขัน</th>
				<th>ทีม 2</th>
				<th>วันและเวลาการแข่งขัน</th>
				<th>XSMatch ID</th>
				<th>แก้ไข</th>
				<th>ลบออก</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach( $this->listMatch as $tmpMatch ) { ?>
			<tr>
				<td style="vertical-align: middle;"><?php echo $tmpMatch['id']; ?></td>
				<td style="vertical-align: middle;"><?php echo $tmpMatch['Team1']; ?></td>
				<td style="vertical-align: middle;"><?php echo $tmpMatch['FTScore']; ?></td>
				<td style="vertical-align: middle;"><?php echo $tmpMatch['Team2']; ?></td>
				<td style="vertical-align: middle;"><?php echo $tmpMatch['MatchDateTime']; ?></td>
				<td style="vertical-align: middle;"><?php echo $tmpMatch['XSMatchID']; ?></td>
				<?php if(isset($tmpMatch['dummy_id'])) { ?>
					<?php if($this->site_page == "program_round"){ ?>
					<td><a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/program_round_dummy/<?php echo $this->dataComp['id']; ?>/<?php echo $this->dataRoundMatch['id']; ?>/<?php echo $tmpMatch['dummy_id']; ?>" target="_blank" type="button" class="btn btn-primary">แก้ไข</a></td>
					<?php }else{ ?>
					<td><a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/program_dummy/<?php echo $this->dataComp['id']; ?>/<?php echo $tmpMatch['dummy_id']; ?>" target="_blank" type="button" class="btn btn-primary">แก้ไข</a></td>
					<?php } ?>
					<td>
					<?php if($this->site_page == "program_round"){ ?>
					<a onclick="return confirm('แน่ใจแล้วนะ ว่าจะลบนัดนี้?')" href="<?php echo BASE_HREF; ?>api/adminfootball/comp/delete_dummyMatch/<?php echo $tmpMatch['dummy_id'];?>?comp_id=<?php echo $this->dataComp['id']; ?>&round_id=<?php echo $this->dataRoundMatch['id']; ?>" type="button" class="btn btn-danger">ลบ</a></td>
					<?php }else{ ?>
					<a onclick="return confirm('แน่ใจแล้วนะ ว่าจะลบนัดนี้?')" href="<?php echo BASE_HREF; ?>api/adminfootball/comp/delete_dummyMatch/<?php echo $tmpMatch['dummy_id'];?>?comp_id=<?php echo $this->dataComp['id']; ?>" type="button" class="btn btn-danger">ลบ</a></td>
					<?php } ?>
				<?php }else{ ?>
					<td><a href="<?php echo BASE_HREF; ?>api/manage_match_detail.php?id=<?php echo $tmpMatch['id']; ?>" target="_blank" type="button" class="btn btn-primary">แก้ไข</a></td>
					<td></td>
				<?php } ?>
			</tr>
			<?php } ?>
		</tbody>
    </table>
	<?php } ?>
	
</div>