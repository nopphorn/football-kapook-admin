<div class="container" ng-app="CompApp" ng-controller="CompController">
	<form action="<?php echo BASE_HREF; ?>api/adminfootball/comp/<?php
		if($this->dataRound){
			if(isset($this->dataTable)) {
				echo 'update_table_round_dummy';
			}else{
				echo 'create_table_round_dummy';
			}
		}else{
			if(isset($this->dataTable)) {
				echo 'update_table_dummy';
			}else{
				echo 'create_table_dummy';
			}
		}
	?>" method="POST">
	<input type="hidden" name="comp_id" value="<?php echo $this->dataComp['id']; ?>">
	<?php if(isset($this->dataTable)) {
		?><input type="hidden" name="id" value="<?php echo $this->dataTable['id']; ?>">
	<?php } ?>
	<?php if($this->dataRound) { ?>
		<input type="hidden" name="round_id" value="<?php echo $this->dataRound['id']; ?>">
	<?php }else{ ?>
		<input type="hidden" name="round_id" value="-1">
	<?php } ?>
	<div class="form-group">
		<label >Name Table</label>
		<input type="text" class="form-control" id="name" name="name" value="<?php if(isset($this->dataTable)) { echo $this->dataTable['name']; }?>" placeholder="ชื่อตารางคะแนน">
	</div>
	
	<div class="form-group">
		<label >รูปแบบการเรียงคะแนน</label>
		<label class="radio-inline">
			<input type="radio" name="type_order" value="1" <?php if(intval($this->dataTable['type_order'])==1){ echo 'checked'; } ?>> เรียงด้วยคะแนน
		</label>
		<label class="radio-inline">
			<input type="radio" name="type_order" value="2" <?php if(intval($this->dataTable['type_order'])==2){ echo 'checked'; } ?>> เรียงตามลำดับด้านล่าง
		</label>
	</div>
	
	<table class="table table-bordered table-striped" ng-repeat="tmpData in listUserTeam">
		<thead>
			<tr>
				<th class="col-xs-12" colspan="6">
					<div style="font-size: x-large;float: left;">Team {{$index+1}}</div>
					<button class="btn btn-default" type="button" style="float: right;" ng-click="popup_team($index+1)">
						ค้นหาทีม<span class="glyphicon glyphicon-search"></span>
					</button>
					
					<button ng-if="$index > 1" class="btn btn-danger" type="button" style="float: right;" ng-click="remove_team($index)">
						ลบทีม
					</button>
					
				</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="col-xs-1" >Team Name</td>
				<td class="col-xs-5" ><input type="text" class="form-control" placeholder="ชื่อทีม" id="Team{{$index+1}}Name" name="TeamData[{{$index}}][name]" value="{{tmpData.name}}"></td>
				<td class="col-xs-2" >Win</td>
				<td class="col-xs-1" ><input type="number" class="form-control" name="TeamData[{{$index}}][win]" value="{{tmpData.win}}"></td>
				<td class="col-xs-2" >Goal For</td>
				<td class="col-xs-1" ><input type="number" class="form-control" name="TeamData[{{$index}}][goal_for]" value="{{tmpData.goal_for}}"></td>
			</tr>
			<tr>
				<td>Path logo</td>
				<td><input type="text" class="form-control" placeholder="พาร์ทรูปโลโก้" id="Team{{$index+1}}Logo" name="TeamData[{{$index}}][logo]" value="{{tmpData.logo}}"></td>
				<td>Draw</td>
				<td><input type="number" class="form-control" placeholder="Team 1 Name" name="TeamData[{{$index}}][draw]" value="{{tmpData.draw}}"></td>
				<td>Goal Against</td>
				<td><input type="number" class="form-control" placeholder="Team 1 Name" name="TeamData[{{$index}}][goal_against]" value="{{tmpData.goal_against}}"></td>
			</tr>
			<tr>
				<td>URL Team</td>
				<td><input type="text" class="form-control" placeholder="URLทีม" id="Team{{$index+1}}URL" name="TeamData[{{$index}}][url]" value="{{tmpData.url}}"></td>
				<td>Lose</td>
				<td><input type="number" class="form-control" placeholder="Team 1 Name" name="TeamData[{{$index}}][lose]" value="{{tmpData.lose}}"></td>
				<td>บวกลบคะแนนพิเศษ(กรณีโดนตัดแต้มต่างๆ)</td>
				<td><input type="number" class="form-control" placeholder="Team 1 Name" name="TeamData[{{$index}}][sp_point]" value="{{tmpData.sp_point}}"></td>
			</tr>
		</tbody>
    </table>
	<div class="row form-group"><center>
		<div class="col-sm-12 col-md-12"><button type="button" ng-click="add_team()" class="btn btn-success btn-lg btn-block"><span class="glyphicon glyphicon-plus-sign"></span> เพิ่มทีม</button></div>
	</center></div>
	<div class="row form-group"><center>
		<div class="col-sm-6 col-md-6"><button type="submit" class="btn btn-primary btn-lg btn-block"><?php if(isset($this->dataTable)) { ?>บันทึกตารางคะแนน<?php }else{ ?>สร้างตารางคะแนนใหม่<?php } ?></button></div>
		<div class="col-sm-6 col-md-6"><a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/table/<?php echo $this->dataComp['id']; ?>" class="btn btn-default btn-lg btn-block">กลับไปหน้ารวมตารางคะแนน</a></div>
	</center></div>
	</form>
</div>

<script>
	
	(function(angular) {
		var myApp = angular.module('CompApp', []);

		myApp.controller('CompController', ['$scope','$http', function($scope,$http) {

			$scope.popup_team = function(index){
				window.open('<?php echo BASE_HREF; ?>api/adminfootball/comp/get_listTeamForMatchDummy/' + index,'','height=400,width=400');
			};
			
			$scope.add_team = function(){
				$scope.listUserTeam.push(
					{
						name : '',
						logo : '',
						url : '',
						win : 0,
						draw : 0,
						lose : 0,
						goal_for : 0,
						goal_against : 0,
						sp_point : 0
					}
				);
			};
			
			$scope.remove_team = function(index){
				$scope.listUserTeam.splice(index, 1);
			};
			
			<?php if(isset($this->dataTable)){ ?>
			
			$http.post('http://football.kapook.com/api/adminfootball/comp/get_listteamfortable/<?php echo $this->dataTable['id']; ?>?type=json').success(function(data, status, headers, config) {
				$scope.listUserTeam			=	data;
			}).
			error(function(data, status, headers, config) {
				// called asynchronously if an error occurs
				// or server returns response with an error status.
			});
			
			<?php }else{ ?>
			$scope.listUserTeam = [
			{
				name : '',
				logo : '',
				url : '',
				win : 0,
				draw : 0,
				lose : 0,
				goal_for : 0,
				goal_against : 0,
				sp_point : 0
			},
			{
				name : '',
				logo : '',
				url : '',
				win : 0,
				draw : 0,
				lose : 0,
				goal_for : 0,
				goal_against : 0,
				sp_point : 0
			}];
			<?php } ?>
			
		}]);

	})(window.angular);
	
</script>