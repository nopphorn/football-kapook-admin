<div class="container">
	<div style="padding-bottom: 40px;">
		<button type="button" class="btn btn-success" style="float: right;" data-toggle="modal" data-target="#myModal">
			<span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> สร้างการแข่งขัน
		</button>
	</div>
	
	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	  
		<?php foreach($this->arrComp as $tmpData){ ?>
		
		<div class="panel panel-default">
			
			<div class="panel-heading" role="tab" id="heading_<?php echo $tmpData['id'];?>">
				<h4 class="panel-title" style="padding-bottom: 10px;">
					<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo $tmpData['id'];?>" aria-expanded="false" aria-controls="collapse_<?php echo $tmpData['id'];?>" style="vertical-align: -5px;">
						<?php echo $tmpData['nameTH'];?>
					</a>
					<a onclick="return confirm('แน่ใจแล้วนะ ว่าจะลบการแข่งขันนี้?')" href="<?php echo BASE_HREF; ?>api/adminfootball/comp/delete/<?php echo $tmpData['id'];?>" type="button" class="btn btn-danger" role="button" style="float: right;"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
					<a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/refresh_table/<?php echo $tmpData['id'];?>" type="button" class="btn btn-success" role="button" style="float: right;margin-right: 10px;">Refresh Table</a>
				</h4>
			</div>
			
			<div id="collapse_<?php echo $tmpData['id'];?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?php echo $tmpData['id'];?>">
				<ul class="list-group">
					<a class="list-group-item" href="<?php echo BASE_HREF; ?>api/adminfootball/comp/info/<?php echo $tmpData['id'];?>">ข้อมูลเบื้องต้น</a>
					<a class="list-group-item" href="<?php echo BASE_HREF; ?>api/adminfootball/comp/news/<?php echo $tmpData['id'];?>">ข่าว</a>
					<a class="list-group-item" href="<?php echo BASE_HREF; ?>api/adminfootball/comp/scorers/<?php echo $tmpData['id'];?>">ดาวซัลโว</a>
					<a class="list-group-item" href="<?php echo BASE_HREF; ?>api/adminfootball/comp/table/<?php echo $tmpData['id'];?>">ตารางคะแนน</a>
					<a class="list-group-item" href="<?php echo BASE_HREF; ?>api/adminfootball/comp/program/<?php echo $tmpData['id'];?>">ตารางการแข่งขัน</a>
				</ul>
			</div>
			
		</div>
		
		<?php } ?>
		
	</div>
	
	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">สร้างการแข่งขันใหม่</h4>
				</div>
				<form role="form" action="<?php echo BASE_HREF; ?>api/adminfootball/comp/create" method="POST">
				<div class="modal-body">
					<div class="form-group">
						<label for="nameTH">ชื่อเต็ม(ไทย)</label>
						<input type="text" class="form-control" name="nameTH" placeholder="ชื่อการแข่งขันแบบยาว">
					</div>
					<div class="form-group">
						<label for="nameTHShort">ชื่อย่อ(ไทย)</label>
						<input type="text" class="form-control" name="nameTHShort" placeholder="ชื่อการแข่งขันแบบสั้น">
					</div>
					<div class="form-group">
						<label for="nameURL">ชื่อurl</label>
						<input type="text" class="form-control" name="nameURL" placeholder="ชื่อการแข่งขันบน URL">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-default submit_new_round">Submit</button>
				</div>
				</form>
			</div>
		</div>
	</div>
	
</div>