<style type="text/css">
	.sortorder:after {
		content: '\25b2';
	}
	.sortorder.reverse:after {
		content: '\25bc';
	}
	.sort_click {
		cursor:pointer
	}
	.table>tbody>tr>td {
		vertical-align: middle;
		padding: 4px;
	}
	.league_select {
		height: 20px;
		padding: 0px;
		font-size: xx-small;
		width: inherit;
	}
	.league-button {
		font-size: xx-small;
		height: 20px;
		padding-top: 3px;
	}
</style>

<div class="container" ng-app="CompApp" ng-controller="CompController" style="width: 95%">
	<form action="<?php echo BASE_HREF; ?>api/adminfootball/league/update" method="POST">
		<div class="row form-group"><center>
			<div class="col-sm-6 col-md-6">
				<label>โซนที่ต้องการ</label>
				<select class="form-control" ng-model="zoneSelect" ng-change="getListLeague()">
					<option value="0">กรุณาเลือกโซน</option>
					<?php foreach( $this->dataZone as $tmpZone ){ ?>
					<option value="<?php echo $tmpZone['id']; ?>"><?php echo $tmpZone['NameEN']; ?></option>
					<?php } ?>
				</select>
			</div>
			<div class="col-sm-6 col-md-6">
				<label>ปีที่ต้องการ</label>
				<select class="form-control" ng-model="yearSelect" ng-change="getListLeague()">
					<option value="0">ทุกปี</option>
					<option value="2013">2013/2014</option>
					<option value="2014">2014/2015</option>
					<option value="2015">2015/2016</option>
					<option value="2016">2016/2017</option>
					<option value="2017">2017/2018</option>
				</select>
			</div>
		</center></div>
		<div class="row form-group">
			<div class="col-sm-3 col-md-3"><button class="btn btn-success btn-lg btn-block" type="submit">บันทึกรวม</button></div>
		</div>
		<table class="table table-bordered table-striped" ng-if="zoneSelect==0">
			<colgroup>
				<col class="col-xs-12">
			</colgroup>
			<thead>
				<tr>
					<th><center>กรุณาเลือกลีก</center></th>
				</tr>
			</thead>
		</table>
		<table class="table table-bordered table-striped" style="font-size: x-small;" ng-if="zoneSelect!=0">
			<thead>
				<tr>
					<td style="width: 1%;"><span class="sort_click" ng-click="order('Priority')">Priority</span><span class="sortorder" ng-show="predicate == 'Priority'" ng-class="{reverse:reverse}"></span></td>
					<td><span class="sort_click" ng-click="order('id')">ID</span><span class="sortorder" ng-show="predicate == 'id'" ng-class="{reverse:reverse}"></span></td>
					<td><span class="sort_click" ng-click="order('KPZoneID')">zoneID</span><span class="sortorder" ng-show="predicate == 'KPZoneID'" ng-class="{reverse:reverse}"></span></td>
					<td><span class="sort_click" ng-click="order('NameEN')">league nameEN</span><span class="sortorder" ng-show="predicate == 'NameEN'" ng-class="{reverse:reverse}"></span></td>
					<td><span class="sort_click" ng-click="order('NameTH')">league nameTH</span><span class="sortorder" ng-show="predicate == 'NameTH'" ng-class="{reverse:reverse}"></span></td>
					<td><span class="sort_click" ng-click="order('NameTHShort')">league nameTH Short</span><span class="sortorder" ng-show="predicate == 'NameTHShort'" ng-class="{reverse:reverse}"></span></td>
					<td><span class="sort_click" ng-click="order('Description')">Description</span><span class="sortorder" ng-show="predicate == 'Description'" ng-class="{reverse:reverse}"></span></td>
					<td><span class="sort_click" ng-click="order('Year')">Year</span><span class="sortorder" ng-show="predicate == 'Year'" ng-class="{reverse:reverse}"></span></td>
					<td><span class="sort_click" ng-click="order('Status')">status</span><span class="sortorder" ng-show="predicate == 'Status'" ng-class="{reverse:reverse}"></span></td>
					<td><span class="sort_click" ng-click="order('AutoStatus')">auto-status</span><span class="sortorder" ng-show="predicate == 'AutoStatus'" ng-class="{reverse:reverse}"></span></td>
					<td><b>บันทึก?</b></td>
					<td><b>ดู/แก้ไขทีมในลีก</b></td>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="tmpData in listLeague | orderBy:predicate:reverse " style="background-color : #{{bgColor[tmpData.Status]}};">
					<td><input name="Priority[{{tmpData.id}}]" type="number" value="{{tmpData.Priority}}" ng-model="tmpData.Priority" style="width: 100%;"></td>
					<td>{{tmpData.id}}</td>
					<td>{{tmpData.KPZoneID}}</td>
					<td>{{tmpData.NameEN}}</td>
					<td><input name="NameTH[{{tmpData.id}}]" type="text" value="{{tmpData.NameTH}}" ng-model="tmpData.NameTH" style="width: 100%;"></td>
					<td><input name="NameTHShort[{{tmpData.id}}]"type="text" value="{{tmpData.NameTHShort}}" ng-model="tmpData.NameTHShort" style="width: 100%;"></td>
					<td>{{tmpData.Description}}</td>
					<td>{{tmpData.Year}}</td>
					<td>
						<select class="form-control league_select" name="Status[{{tmpData.id}}]" ng-model="tmpData.Status">
							<option value="0" style="background-color: #FFCC8F">NO</option>
							<option value="1" style="background-color: #BDE6C1">YES</option>
						</select>
					</td>
					<td>
						<select class="form-control league_select" name="AutoStatus[{{tmpData.id}}]" ng-model="tmpData.AutoStatus">
							<option value="0" style="background-color: #FFCC8F">NO</option>
							<option value="1" style="background-color: #BDE6C1">YES</option>
						</select>
					</td>
					<td><button type="button" class="btn btn-success league-button" ng-click="updateLeague(tmpData.id)" >บันทึก</button></td>
					<td></td>
				</tr>
			</tbody>
		</table>
	</form>
</div>

<form id="formzone" action="<?php echo BASE_HREF; ?>api/adminfootball/league/update" method="POST">
	<input type="hidden" name="id" id="id" value="">
	<input type="hidden" name="Priority" id="Priority" value="">
	<input type="hidden" name="NameTH" id="NameTH" value="">
	<input type="hidden" name="NameTHShort" id="NameTHShort" value="">
	<input type="hidden" name="Status" id="Status" value="">
	<input type="hidden" name="AutoStatus" id="AutoStatus" value="">
</form>

<script>
	
	(function(angular) {
		var myApp = angular.module('CompApp', []);

		myApp.controller('CompController', ['$scope','$http', function($scope,$http) {
			
			$scope.listLeague 	= 	[];
			$scope.zoneSelect 	=	0;
			$scope.yearSelect 	=	0;
			
			$scope.reverse 		=	false;
			$scope.predicate 	=	"NameEN";
			
			$scope.bgColor		=	[];
			
			$scope.bgColor[0]	=	"FFCC8F";
			$scope.bgColor[1]	=	"BDE6C1";
			
			$scope.getListLeague = function(){
				if($scope.zoneSelect==0){
					$scope.listLeague = [];
				}else{
					$http.post('http://football.kapook.com/api/adminfootball/league/getjson/' + $scope.zoneSelect + '/' + $scope.yearSelect).success(function(data, status, headers, config) {
						$scope.listLeague			=		data;
					}).
					error(function(data, status, headers, config) {
						// called asynchronously if an error occurs
						// or server returns response with an error status.
					});
				}
			};
			
			$scope.updateLeague 	=	function(league_id){
				for( var i in $scope.listLeague ){
					if( $scope.listLeague[i].id == league_id ){
						
						document.getElementById("id").value				=	$scope.listLeague[i].id;
						document.getElementById("Priority").value		=	$scope.listLeague[i].Priority;
						document.getElementById("NameTH").value			=	$scope.listLeague[i].NameTH;
						document.getElementById("NameTHShort").value	=	$scope.listLeague[i].NameTHShort;
						document.getElementById("Status").value			=	$scope.listLeague[i].Status;
						document.getElementById("AutoStatus").value		=	$scope.listLeague[i].AutoStatus;
						
						document.getElementById("formzone").submit();
						
						//console.log($scope.listLeague[i]);
						
						break;
						
					}
				}
			};
			
			$scope.order = function(predicate) {
				$scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
				$scope.predicate = predicate;
			};
			
		}]);

	})(window.angular);
			
</script>