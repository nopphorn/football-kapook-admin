<style type="text/css">
	.sortorder:after {
		content: '\25b2';
	}
	.sortorder.reverse:after {
		content: '\25bc';
	}
	.sort_click {
		cursor:pointer
	}
</style>

<div class="container" ng-app="CompApp" ng-controller="CompController">
	<form action="<?php echo BASE_HREF; ?>api/adminfootball/zone/update" method="POST">
		<div class="row form-group">
			<div class="col-sm-3 col-md-3"><button class="btn btn-success btn-lg btn-block" type="submit">บันทึกรวม</button></div>
		</div>
		<table class="table table-bordered table-striped">
			<colgroup>
				<col class="col-xs-1">
				<col class="col-xs-4">
				<col class="col-xs-4">
				<col class="col-xs-1">
				<col class="col-xs-1">
				<col class="col-xs-1">
			</colgroup>
			<thead>
				<tr>
					<th><span class="sort_click" ng-click="order('id')">ID</span><span class="sortorder" ng-show="predicate == 'id'" ng-class="{reverse:reverse}"></span> </th>
					<th><span class="sort_click" ng-click="order('NameEN')">Zone Name[EN]</span> <span class="sortorder" ng-show="predicate == 'NameEN'" ng-class="{reverse:reverse}"></span></th>
					<th><span class="sort_click" ng-click="order('NameTH')">Zone Name[TH]</span> <span class="sortorder" ng-show="predicate == 'NameTH'" ng-class="{reverse:reverse}"></span></th>
					<th><span class="sort_click" ng-click="order('Status')">Status?</span><span class="sortorder" ng-show="predicate == 'Status'" ng-class="{reverse:reverse}"></span></th>
					<th><span class="sort_click" ng-click="order('AutoStatus')">Auto-Status?</span><span class="sortorder" ng-show="predicate == 'AutoStatus'" ng-class="{reverse:reverse}"></span></th>
					<th>บันทึก</th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="tmpData in listZone | orderBy:predicate:reverse " style="background-color : #{{bgColor[tmpData.Status]}};">
					<td>{{tmpData.id}}</td>
					<td>{{tmpData.NameEN}}</td>
					<td><input name="NameTH[{{tmpData.id}}]" type="text" value="{{tmpData.NameTH}}"></td>
					<td>
						<select class="form-control" name="Status[{{tmpData.id}}]" ng-model="tmpData.Status">
							<option value="0" style="background-color: #FFCC8F">NO</option>
							<option value="1" style="background-color: #BDE6C1">YES</option>
						</select>
					</td>
					<td>
						<select class="form-control" name="AutoStatus[{{tmpData.id}}]" ng-model="tmpData.AutoStatus">
							<option value="0">NO</option>
							<option value="1">YES</option>
						</select>
					</td>
					<td style="vertical-align: middle;"><button type="button" class="btn btn-success" ng-click="updateZone(tmpData.id)">บันทึกข้อมูล</button></td>
				</tr>
			</tbody>
		</table>
		<div class="row form-group">
			<div class="col-sm-3 col-md-3"><button class="btn btn-success btn-lg btn-block" type="submit">บันทึกรวม</button></div>
		</div>
	</form>
</div>

<form id="formzone" action="<?php echo BASE_HREF; ?>api/adminfootball/zone/update" method="POST">
	<input type="hidden" name="id" id="id" value="">
	<input type="hidden" name="NameTH" id="NameTH" value="">
	<input type="hidden" name="Status" id="Status" value="">
	<input type="hidden" name="AutoStatus" id="AutoStatus" value="">
</form>

<script>
	
	(function(angular) {
		var myApp = angular.module('CompApp', []);

		myApp.controller('CompController', ['$scope','$http', function($scope,$http) {
			
			$scope.listZone = [];
			
			$scope.reverse 		=	false;
			$scope.predicate 	=	"NameEN";
			
			$scope.bgColor		=	[];
			
			$scope.bgColor[0]	=	"FFCC8F";	
			$scope.bgColor[1]	=	"BDE6C1";	
			
			
			$http.post('<?php echo BASE_HREF; ?>api/adminfootball/zone/get_zone?type=json').success(function(data, status, headers, config) {
				$scope.listZone			=		data;
			}).
			error(function(data, status, headers, config) {
				// called asynchronously if an error occurs
				// or server returns response with an error status.
			});
			
			$scope.updateZone 	=	function(zone_id){
				for( var i in $scope.listZone ){
					if( $scope.listZone[i].id == zone_id ){
						
						document.getElementById("id").value			=	$scope.listZone[i].id;
						document.getElementById("NameTH").value		=	$scope.listZone[i].NameTH;
						document.getElementById("Status").value		=	$scope.listZone[i].Status;
						document.getElementById("AutoStatus").value	=	$scope.listZone[i].AutoStatus;
						
						document.getElementById("formzone").submit();
						
						console.log($scope.listZone[i].Status);
						break;
						
					}
				}
			};
			
			$scope.order = function(predicate) {
				$scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
				$scope.predicate = predicate;
			};
			
		}]);

	})(window.angular);
			
</script>