<div class="container">
	<a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/" role="button" class="btn btn-default btn-lg btn-block">จัดการรายการแข่งขัน(Competition)</a>
	<a href="<?php echo BASE_HREF; ?>api/adminfootball/player/" role="button" class="btn btn-default btn-lg btn-block">จัดการผู้เล่น(Player)</a>
	<a href="<?php echo BASE_HREF; ?>api/adminfootball/zone/" role="button" class="btn btn-default btn-lg btn-block">จัดการโซน(Zone)</a>
	<a href="<?php echo BASE_HREF; ?>api/adminfootball/league/" role="button" class="btn btn-default btn-lg btn-block">จัดการลีก(League)</a>
	<a href="<?php echo BASE_HREF; ?>api/adminfootball/team/" role="button" class="btn btn-default btn-lg btn-block">จัดการทีม(Team)</a>
	<a href="<?php echo BASE_HREF; ?>api/adminfootball/match/" role="button" class="btn btn-default btn-lg btn-block">จัดการการแข่งขัน(Match)</a>
	<a href="<?php echo BASE_HREF; ?>api/adminfootball/market/" role="button" class="btn btn-default btn-lg btn-block">จัดการตลาดซื้อขายนักเตะ(Market Player)</a>
</div>