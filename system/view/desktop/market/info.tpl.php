<div class="container" style="padding-bottom: 60px;">

	<?php if($this->dataMarket){ ?>
	<form role="form" action="<?php echo BASE_HREF; ?>api/adminfootball/market/update" method="POST" enctype="multipart/form-data">
		<input class="form-control" type="hidden" name="id" value="<?php echo $this->dataMarket['id']; ?>">
	<?php }else{ ?>
	<form role="form" action="<?php echo BASE_HREF; ?>api/adminfootball/market/create" method="POST" enctype="multipart/form-data">
	<?php } ?>
		<div class="form-group">
			<label>Player Data</label>
			
			<div class="row">
				<div class="col-sm-4 col-md-4">
					<div class="thumbnail">
						<?php if(strlen($this->dataMarket['PlayerData']['img'])){ ?>
						<img id="PlayerImg" src="<?php echo str_replace("football.kapook.com", "202.183.165.189", $this->dataMarket['PlayerData']['img']); ?>">
						<?php }else{ ?>
						<img id="PlayerImg" src="http://football.kapook.com/uploads/scorer/noface.jpg">
						<?php } ?>
					</div>
				</div>
				
				<div class="col-sm-8 col-md-8">
					<div class="panel panel-default ">
						<div class="panel-heading">
							<h3 class="panel-title">ผู้เล่น</h3>
						</div>
						<div class="panel-body">
							<?php if($this->dataMarket) { ?>
							<input class="form-control" type="hidden" name="PlayerID" id="player_id" value="<?php echo $this->dataMarket['PlayerData']['id']; ?>">
							<div style="padding-top: 5px;padding-left: 5px;float: left;" id="PlayerName"><?php echo $this->dataMarket['PlayerData']['name']; ?></div>
							<?php }else{ ?>
							<input class="form-control" type="hidden" name="PlayerID" id="player_id" value="0">
							<div style="padding-top: 5px;padding-left: 5px;float: left;" id="PlayerName">N/A</div>
							<?php } ?>
							<button class="btn btn-default" type="button" style="float: right;" onclick="window.open('<?php echo BASE_HREF; ?>api/adminfootball/player/get_listPlayerForScorer','','height=400,width=400');">ค้นหาผู้เล่น <span class="glyphicon glyphicon-search"></span></button>
						</div>
					</div>
				</div>
				
				<div class="col-sm-4 col-md-4">
					<div class="panel panel-danger ">
						<div class="panel-heading">
							<h3 class="panel-title">ทีมที่ออกไป</h3>
						</div>
						<div class="panel-body">
							<?php if($this->dataMarket['TeamOutData']) { ?>
							<input class="form-control" type="hidden" name="FromTeamKPID" id="FromTeamKPID" value="<?php echo $this->dataMarket['TeamOutData']['id']; ?>">
							<img id="TeamOutLogo" style="width: 30px;float: left;" src="<?php echo $this->dataMarket['TeamOutData']['Logo']; ?>">
							
							<?php if(!empty($this->dataMarket['TeamOutData']['NameTH'])){
								$strTeam	=	$this->dataMarket['TeamOutData']['NameTH'];
							}else if(!empty($this->dataMarket['TeamOutData']['NameTHShort'])){
								$strTeam	=	$this->dataMarket['TeamOutData']['NameTHShort'];
							}else{
								$strTeam	=	$this->dataMarket['TeamOutData']['NameEN'];
							} ?>
							<div style="padding-top: 5px;padding-left: 5px;float: left;" id="TeamOutName"><?php echo $strTeam; ?></div>
							
							<?php }else{ ?>
							<input class="form-control" type="hidden" name="FromTeamKPID" id="FromTeamKPID" value="0">
							<img id="TeamOutLogo" style="width: 30px;float: left;" src="http://football.kapook.com/uploads/logo/default.png">
							<div style="padding-top: 5px;padding-left: 5px;float: left;" id="TeamOutName">N/A</div>
							<?php } ?>
							
							<button class="btn btn-default" type="button" style="float: right;" onclick="window.open('<?php echo BASE_HREF; ?>api/adminfootball/market/get_listTeamForOut','','height=400,width=400');">ค้นหาทีม <span class="glyphicon glyphicon-search"></span></button>
							
						</div>
					</div>
				</div>
				
				<div class="col-sm-4 col-md-4">
					<div class="panel panel-success ">
						<div class="panel-heading">
							<h3 class="panel-title">ทีมที่เข้าไป</h3>
						</div>
						<div class="panel-body">
							<?php if($this->dataMarket['TeamInData']) { ?>
							<input class="form-control" type="hidden" name="ToTeamKPID" id="ToTeamKPID" value="<?php echo $this->dataMarket['TeamInData']['id']; ?>">
							<img id="TeamInLogo" style="width: 30px;float: left;" src="<?php echo $this->dataMarket['TeamInData']['Logo']; ?>">
							
							<?php if(!empty($this->dataMarket['TeamInData']['NameTH'])){
								$strTeam	=	$this->dataMarket['TeamInData']['NameTH'];
							}else if(!empty($this->dataMarket['TeamInData']['NameTHShort'])){
								$strTeam	=	$this->dataMarket['TeamInData']['NameTHShort'];
							}else{
								$strTeam	=	$this->dataMarket['TeamInData']['NameEN'];
							} ?>
							<div style="padding-top: 5px;padding-left: 5px;float: left;" id="TeamInName"><?php echo $strTeam; ?></div>
							
							<?php }else{ ?>
							<input class="form-control" type="hidden" name="ToTeamKPID" id="ToTeamKPID" value="0">
							<img id="TeamInLogo" style="width: 30px;float: left;" src="http://football.kapook.com/uploads/logo/default.png">
							<div style="padding-top: 5px;padding-left: 5px;float: left;" id="TeamInName">N/A</div>
							<?php } ?>
							
							<button class="btn btn-default" type="button" style="float: right;" onclick="window.open('<?php echo BASE_HREF; ?>api/adminfootball/market/get_listTeamForIn','','height=400,width=400');">ค้นหาทีม <span class="glyphicon glyphicon-search"></span></button>
							
						</div>
					</div>
				</div>
				
				<div class="col-sm-4 col-md-4">
					<div class="panel panel-default ">
						<div class="panel-heading">
							<h3 class="panel-title">จำนวนตัวเลข</h3>
						</div>
						<div class="panel-body"><input class="form-control" name="Price" value="<?php  echo $this->dataMarket['Price']; ?>"></div>
					</div>
				</div>
				
				<div class="col-sm-4 col-md-4">
					<div class="panel panel-default ">
						<div class="panel-heading">
							<h3 class="panel-title">จำนวนหน่วย/บอกสถานะพิเศษ</h3>
						</div>
						<div class="panel-body"><input class="form-control" name="PriceText" value="<?php echo $this->dataMarket['PriceText']; ?>"></div>
					</div>
				</div>
				
				<div class="col-sm-8 col-md-8">
					<div class="panel panel-default ">
						<div class="panel-heading">
							<h3 class="panel-title">ลิงค์ข่าว</h3>
						</div>
						<div class="panel-body"><input class="form-control" name="NewsURL" value="<?php echo $this->dataMarket['NewsURL']; ?>"></div>
					</div>
				</div>
				
				<div class="col-sm-4 col-md-4">
					<div class="panel panel-default ">
						<div class="panel-heading">
							<h3 class="panel-title">ปี-เดือน-วัน ซื้อขาย</h3>
						</div>
						<div class="panel-body"><input type="text" class="form-control" id="Date" name="Date" placeholder="ปี-เดือน-วัน ซื้อขายนักเตะ" value="<?php
							if(isset($this->dataMarket['Date'])) {
								echo $this->dataMarket['Date'];
							}else{
								echo date('Y-m-d');
							} ?>">
						</div>
					</div>
				</div>

			</div>
		</div>
		<div class="row form-group"><center>
			<div class="col-sm-6 col-md-6"><button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button></div>
			<div class="col-sm-6 col-md-6"><a href="<?php echo BASE_HREF; ?>api/adminfootball/market/" role="button" class="btn btn-default btn-lg btn-block">Back</a></div>
		</center></div>
	</form>
</div>

<script>				
	$(document).ready(function(){
		$('#Date').datetimepicker({
			lang:'en',
			format:'Y-m-d',
			timepicker:false
		});
	});
</script>