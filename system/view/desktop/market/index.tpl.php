<?php
function nav_page($current_page,$max_page,$keyword = ''){
	
	$slot = 5;
	$number_page = array();
	$number_page[] = $current_page;
	$slot--;
	$i = 1;
	$end_head = false;
	$end_tail = false;
	while(1){
		
		if(($current_page - $i)>=1){
			array_unshift($number_page,$current_page-$i);
			$slot--;
		}else{
			$end_head = true;
		}
		
		if($slot == 0){
			break;
		}
		
		if(($current_page + $i)<=$max_page){
			$number_page[] = $current_page+$i;
			$slot--;
		}else{
			$end_tail = true;
		}
		
		if($slot == 0){
			break;
		}
		
		if($end_head && $end_tail){
			break;
		}
		$i++;
	}

	echo '<nav><ul class="pagination pagination-lg">';
	// Previous
	if($current_page > 1){
		echo '<li><a href="' . BASE_HREF . 'api/adminfootball/market/?page=' . ($current_page-1);
		if(strlen($keyword)>=1){
			echo '&keyword=' . $keyword;
		}
		echo '" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
	}else{
		echo '<li class="disabled"><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
	}
	// Page Number
	foreach($number_page as $number){
		if($number == $current_page){
			echo '<li class="active"><a href="#">';
		}else{
			echo '<li><a href="' . BASE_HREF . 'api/adminfootball/market/?page=' . $number;
			if(strlen($keyword)>=1){
				echo '&keyword=' . $keyword;
			}
			echo '">';
		}
		echo $number . '</a></li>';
	}
	// Next
	if($current_page < $max_page){
		echo '<li><a href="' . BASE_HREF . 'api/adminfootball/market/?page=' . ($current_page+1);
		if(strlen($keyword)>=1){
			echo '&keyword=' . $keyword;
		}
		echo '" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
	}else{
		echo '<li class="disabled"><a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
	}
	echo '</ul></nav>';
}
?>
<div class="container">
	<div class="row form-group">
		<div class="col-sm-3 col-md-3"><a href="<?php echo BASE_HREF; ?>api/adminfootball/market/info" class="btn btn-success btn-block">สร้างการซื้อขายใหม่</a></div>
	</div>
	<hr style="margin-top: 0px;margin-bottom: -10px;">
	<center>
	<?php
		nav_page($this->data_market['page'],$this->data_market['page_all']);
	?>
	</center>
	<table class="table table-bordered table-striped" style="margin-bottom: 0px;">
		<colgroup>
			<col class="col-xs-1">
			<col class="col-xs-3">
			<col class="col-xs-1">
			<col class="col-xs-1">
			<col class="col-xs-1">
			<col class="col-xs-1">
			<col class="col-xs-1">
			<col class="col-xs-2">
			<col class="col-xs-1">
		</colgroup>
		<thead>
			<tr>
				<th>รูปภาพ</th>
				<th>ชื่อผู้เล่น</th>
				<th>ออก</th>
				<th>เข้า</th>
				<th>ราคา / หน่วย</th>
				<th>ปี-เดือน-วัน ซื้อขาย</th>
				<th>ลิงค์ข่าว</th>
				<th>แก้ไข</th>
				<th>ลบ</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($this->data_market["list"] as $tmpData) { /*var_dump($tmpData);*/ ?>
			<tr>
				<!--Player Image-->
				<td>
				<?php if(strlen($tmpData['PlayerData']['img'])) { ?>
				<img src="<?php echo str_replace("football.kapook.com", "202.183.165.189", $tmpData['PlayerData']['img']); ?>" style="width: 100%;"></td>
				<?php }else{ ?>
				<img src="http://football.kapook.com/uploads/scorer/noface.jpg" style="width: 100%;"></td>
				<?php } ?>
				<!--Player Name-->
				<td style="vertical-align: middle;"><?php echo $tmpData['PlayerData']['name']; ?></td>
				<!--Team Out-->
				<?php
					if(!empty($tmpData['TeamOutData']['NameTH'])){
						$strTeam	=	$tmpData['TeamOutData']['NameTH'];
					}else if(!empty($tmpData['TeamOutData']['NameTHShort'])){
						$strTeam	=	$tmpData['TeamOutData']['NameTHShort'];
					}else{
						$strTeam	=	$tmpData['TeamOutData']['NameEN'];
					}
				?>
				<td style="vertical-align: middle;"><img src="<?php echo $tmpData['TeamOutData']['Logo']; ?>" title="<?php echo $strTeam; ?>" style="width: 100%;"></td>
				<!--Team In-->
				<?php
					if(!empty($tmpData['TeamInData']['NameTH'])){
						$strTeam	=	$tmpData['TeamInData']['NameTH'];
					}else if(!empty($tmpData['TeamInData']['NameTHShort'])){
						$strTeam	=	$tmpData['TeamInData']['NameTHShort'];
					}else{
						$strTeam	=	$tmpData['TeamInData']['NameEN'];
					}
				?>
				<td style="vertical-align: middle;"><img src="<?php echo $tmpData['TeamInData']['Logo']; ?>" title="<?php echo $strTeam; ?>" style="width: 100%;"></td>
				<td><?php echo $tmpData['Price'] . " " . $tmpData['PriceText']; ?></td>
				<td><?php echo $tmpData['Date']; ?></td>
				<td><?php if(strlen($tmpData['NewsURL'])) { ?>
					<a class="btn btn-info" href="<?php echo $tmpData['NewsURL']; ?>" role="button" target="_blank">Link</a>
				<?php } ?></td>
				<td style="vertical-align: middle;">
					<a href="<?php echo BASE_HREF; ?>api/adminfootball/market/info/<?php echo $tmpData['id']; ?>" type="button" class="btn btn-default">แก้ไขการซื้อขาย</a>
					<a href="<?php echo BASE_HREF; ?>api/adminfootball/player/info/<?php echo $tmpData['PlayerData']['id']; ?>" type="button" class="btn btn-default">แก้ไขข้อมูลผู้เล่น</a>
				</td>
				<td style="vertical-align: middle;"><a onclick="return confirm('แน่ใจแล้วนะ ว่าจะลบคนนี้ออกจากตารางดาวซัลโว?')" href="<?php echo BASE_HREF; ?>api/adminfootball/market/delete/<?php echo $tmpData['id']; ?>" type="button" class="btn btn-danger">ลบ</a></td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
	<center>
	<?php
		nav_page($this->data_market['page'],$this->data_market['page_all']);
	?>
	</center>
	
</div>