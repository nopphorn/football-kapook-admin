<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>ระบบการจัดการฟุตบอลกระปุก 1.0</title>

	<!-- CSS -->
	<link href="<?php echo BASE_HREF; ?>api/adminfootball/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo BASE_HREF; ?>api/adminfootball/assets/css/pnotify.custom.min.css" rel="stylesheet">
	<link href="<?php echo BASE_HREF; ?>api/adminfootball/assets/datetimepicker/jquery.datetimepicker.css" rel="stylesheet">
	<link href="<?php echo BASE_HREF; ?>api/adminfootball/assets/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">
	<link href="<?php echo BASE_HREF; ?>api/adminfootball/assets/css/typeahead.css" rel="stylesheet">
	<link href="<?php echo BASE_HREF; ?>api/adminfootball/assets/Bracketz/css/style.css" rel="stylesheet">
	<link href="<?php echo BASE_HREF; ?>api/adminfootball/assets/summernote/summernote.css" rel="stylesheet">
	<link href="<?php echo BASE_HREF; ?>api/adminfootball/assets/kartik-v-bootstrap-fileinput/css/fileinput.css" rel="stylesheet">
		
	<!-- JS -->
	<script src="<?php echo BASE_HREF; ?>api/adminfootball/assets/js/jquery-1.11.0.min.js"></script>
	<script src="<?php echo BASE_HREF; ?>api/adminfootball/assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?php echo BASE_HREF; ?>api/adminfootball/assets/js/pnotify.custom.min.js"></script>
	<script src="<?php echo BASE_HREF; ?>api/adminfootball/assets/js/angular.min.js"></script>
	<script src="<?php echo BASE_HREF; ?>api/adminfootball/assets/datetimepicker/jquery.datetimepicker.js"></script>
	<script src="<?php echo BASE_HREF; ?>api/adminfootball/assets/js/typeahead.bundle.js"></script>
	<script src="<?php echo BASE_HREF; ?>api/adminfootball/assets/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>
	<script src="<?php echo BASE_HREF; ?>api/adminfootball/assets/Bracketz/js/main.js"></script>
	<script src="<?php echo BASE_HREF; ?>api/adminfootball/assets/summernote/summernote.js"></script>
	<script src="<?php echo BASE_HREF; ?>api/adminfootball/assets/kartik-v-bootstrap-fileinput/js/plugins/canvas-to-blob.min.js"></script>
	<script src="<?php echo BASE_HREF; ?>api/adminfootball/assets/kartik-v-bootstrap-fileinput/js/plugins/sortable.min.js"></script>
	<script src="<?php echo BASE_HREF; ?>api/adminfootball/assets/kartik-v-bootstrap-fileinput/js/plugins/purify.min.js"></script>
	<script src="<?php echo BASE_HREF; ?>api/adminfootball/assets/kartik-v-bootstrap-fileinput/js/fileinput.js"></script>
	<script src="<?php echo BASE_HREF; ?>api/adminfootball/assets/js/checklist-model.js"></script>
	<script src="<?php echo BASE_HREF; ?>api/adminfootball/assets/js/angular-sanitize.min.js"></script>

</head>
<body>

<?php 
	$site_id	=	$this->site_id;
?>

<nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="position: static;">
	<div class="container">
		<ul class="nav navbar-nav">	
			<li <?php if( $site_id == 1 ){ ?> class="active" <?php } ?>><a href="<?php echo BASE_HREF; ?>api/adminfootball/">หน้าแรก</a></li>
			<li <?php if( $site_id == 2 ){ ?> class="active" <?php } ?>><a href="<?php echo BASE_HREF; ?>api/adminfootball/comp/">จัดการรายการแข่งขัน</a></li>
			<li <?php if( $site_id == 3 ){ ?> class="active" <?php } ?>><a href="<?php echo BASE_HREF; ?>api/adminfootball/player/">จัดการผู้เล่น</a></li>
			<li <?php if( $site_id == 4 ){ ?> class="active" <?php } ?>><a href="<?php echo BASE_HREF; ?>api/adminfootball/zone/">จัดการโซน</a></li>
			<li <?php if( $site_id == 5 ){ ?> class="active" <?php } ?>><a href="<?php echo BASE_HREF; ?>api/adminfootball/league/">จัดการลีก</a></li>
			<li <?php if( $site_id == 6 ){ ?> class="active" <?php } ?>><a href="<?php echo BASE_HREF; ?>api/adminfootball/team/">จัดการทีม</a></li>
			<li <?php if( $site_id == 7 ){ ?> class="active" <?php } ?>><a href="<?php echo BASE_HREF; ?>api/adminfootball/match/">จัดการการแข่งขัน</a></li>
			<li <?php if( $site_id == 8 ){ ?> class="active" <?php } ?>><a href="<?php echo BASE_HREF; ?>api/adminfootball/market/">จัดการตลาดซื้อขายนักเตะ</a></li>
		</ul>
	</div>
</nav>

<div class="container">
	<ol class="breadcrumb"><?php echo $this->breadcrum; ?></ol>
</div>