<div class="container" style="padding-bottom: 60px;">
	<form role="form" action="<?php echo BASE_HREF; ?>api/adminfootball/team/update_detail/<?php echo $this->dataTeam['id']; ?>" method="POST" enctype="multipart/form-data">
		<div class="form-group">
			<label>Team Data</label>
			
			<div class="row">
				<div class="col-sm-4 col-md-4">
					<div class="thumbnail">
						<img src="<?php echo str_replace("football.kapook.com", "202.183.165.189", $this->dataTeam['Logo']); ?>">
						<div class="caption"><input type="file" name="UploadLogo"></div>
					</div>
				</div>
				
				<div class="col-sm-4 col-md-4">
					<div class="panel panel-default ">
						<div class="panel-heading">
							<h3 class="panel-title">Team ID</h3>
						</div>
						<div class="panel-body"><?php echo $this->dataTeam['id']; ?></div>
					</div>
				</div>
				
				<div class="col-sm-4 col-md-4">
					<div class="panel panel-default ">
						<div class="panel-heading">
							<h3 class="panel-title">Team NameEN</h3>
						</div>
						<div class="panel-body"><?php echo $this->dataTeam['NameEN']; ?></div>
					</div>
				</div>
				
				<div class="col-sm-4 col-md-4">
					<div class="panel panel-default ">
						<div class="panel-heading">
							<h3 class="panel-title">Team NameTH</h3>
						</div>
						<div class="panel-body"><input class="form-control" type="text" name="NameTH" value="<?php echo $this->dataTeam['NameTH']; ?>"></div>
					</div>
				</div>
				
				<div class="col-sm-4 col-md-4">
					<div class="panel panel-default ">
						<div class="panel-heading">
							<h3 class="panel-title">Team NameTHShort</h3>
						</div>
						<div class="panel-body"><input class="form-control" type="text" name="NameTHShort" value="<?php echo $this->dataTeam['NameTHShort']; ?>"></div>
					</div>
				</div>
				
				<div class="col-sm-8 col-md-8">
					<div class="panel panel-default ">
						<div class="panel-heading">
							<h3 class="panel-title">Status</h3>
						</div>
						<div class="panel-body">
							<select class="form-control" name="Status">
								<option value="0" <?php if($this->dataTeam['Status']==0){ echo 'selected'; } ?>>NO</option>
								<option value="1" <?php if($this->dataTeam['Status']==1){ echo 'selected'; } ?>>YES</option>
							</select>
						</div>
					</div>
				</div>
				
				<div class="col-sm-12 col-md-12">
					<div class="panel panel-default ">
						<div class="panel-heading">
							<h3 class="panel-title">Info</h3>
						</div>
						<div class="panel-body">
							<textarea name="Info" class="form-control" rows="3"><?php echo $this->dataTeam['Info']; ?></textarea >
						</div>
					</div>
				</div>
				
				<div class="col-sm-12 col-md-12">
					<div class="panel panel-default ">
						<div class="panel-heading">
							<h3 class="panel-title">EmbedCode</h3>
						</div>
						<div class="panel-body">
							<textarea name="EmbedCode" class="form-control" rows="3"><?php echo $this->dataTeam['EmbedCode']; ?></textarea >
						</div>
					</div>
				</div>
				
			</div>
		</div>
		<div class="row form-group"><center>
			<div class="col-sm-6 col-md-6"><button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button></div>
			<div class="col-sm-6 col-md-6"><a href="<?php echo BASE_HREF; ?>api/adminfootball/player/" role="button" class="btn btn-default btn-lg btn-block">Back</a></div>
		</center></div>
	</form>
</div>