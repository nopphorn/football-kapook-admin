<style type="text/css">
	.sortorder:after {
		content: '\25b2';
	}
	.sortorder.reverse:after {
		content: '\25bc';
	}
	.sort_click {
		cursor:pointer
	}
</style>

<div class="container" ng-app="CompApp" ng-controller="CompController">
	<div class="row form-group"><center>
		<div class="col-sm-3 col-md-3">
			<label>โซนที่ต้องการ</label>
			<select class="form-control" ng-model="zoneSelect_team" ng-change="getListTeam(true)">
				<option value="0">กรุณาเลือกโซน</option>
				<?php foreach( $this->dataZone as $tmpZone ){ ?>
				<option value="<?php echo $tmpZone['id']; ?>"><?php echo $tmpZone['NameEN']; ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="col-sm-6 col-md-6">
			<label>การแข่งขันที่ต้องการ</label>
			<select class="form-control" ng-options="tmpData.id as tmpData.NameTH for tmpData in listLeagueTeam" ng-model="leagueSelect_team" ng-change="getListTeam(false)">
				<option value="">กรุณาเลือกลีก</option>
			</select>
		</div>
		<div class="col-sm-3 col-md-3">
			<label>ปีที่ต้องการ</label>
			<select class="form-control" ng-model="yearSelect_team" ng-change="getListTeam(true)">
				<option value="0">ทุกปี</option>
				<option value="2013">2013/2014</option>
				<option value="2014">2014/2015</option>
				<option value="2015">2015/2016</option>
				<option value="2016">2016/2017</option>
				<option value="2017">2017/2018</option>
			</select>
		</div>
	</center></div>
	<table class="table table-bordered table-striped" ng-if="leagueSelect_team==0">
		<colgroup>
			<col class="col-xs-12">
		</colgroup>
		<thead>
			<tr>
				<th><center>กรุณาเลือกโซนและลีก</center></th>
			</tr>
		</thead>
	</table>
	
	<table class="table table-bordered table-striped" ng-if="leagueSelect_team!=0">
		<colgroup>
			<col class="col-xs-1">
			<col class="col-xs-1">
			<col class="col-xs-2">
			<col class="col-xs-2">
			<col class="col-xs-2">
			<col class="col-xs-1">
			<col class="col-xs-1">
			<col class="col-xs-1">
			<col class="col-xs-1">
		</colgroup>
		<thead>
			<tr>
				<th>Team ID</th>
				<th><center>Logo</center></th>
				<th>Team NameEN</th>
				<th>Team NameTH</th>
				<th>Team NameTHShort</th>
				<th>Status</th>
				<th>บันทึกด่วน</th>
				<th>แก้ไขโดยละเอียด</th>
				<th>ไปหน้าทีม</th>
			</tr>
		</thead>
		<tbody>
			<tr ng-repeat="tmpData in listTeam" ng-style="tmpData.style" style="background-color : #{{bgColor[tmpData.Status]}};">
				<td style="vertical-align: middle;">{{tmpData.id}}</td>
				<td style="vertical-align: middle;"><center><img src="{{tmpData.Logo}}" style="width: 45%;"></center></td>
				<td style="vertical-align: middle;">{{tmpData.NameEN}}</td>
				<td style="vertical-align: middle;"><input class="form-control" ng-model="tmpData.NameTH"></td>
				<td style="vertical-align: middle;"><input class="form-control" ng-model="tmpData.NameTHShort"></td>
				<td>
					<select class="form-control" name="Status[{{tmpData.id}}]" ng-model="tmpData.Status">
						<option value="0" style="background-color: #FFCC8F">NO</option>
						<option value="1" style="background-color: #BDE6C1">YES</option>
					</select>
				</td>
				<td><button type="button" class="btn btn-success" ng-click="submit_team($index)">บันทึกด่วน</button></td>
				<td><a href="<?php echo BASE_HREF; ?>api/adminfootball/team/detail/{{tmpData.id}}" type="button" class="btn btn-info" target="_blank">แก้ไข</a></td>
				<td><a href="{{tmpData.url}}" type="button" class="btn btn-primary" target="_blank">ไปหน้าทีม</a></td>
			</tr>
		</tbody>
	</table>
</div>
<script>
	
	(function(angular) {
		var myApp = angular.module('CompApp', []);

		myApp.controller('CompController', ['$scope','$http', function($scope,$http) {
			
			$scope.listTeam = [];
			$scope.zoneSelect_team = 0;
			$scope.yearSelect_team = 0;
			$scope.leagueSelect_team = 0;
			
			$scope.bgColor		=	[];
			$scope.bgColor[0]	=	"FFCC8F";
			$scope.bgColor[1]	=	"BDE6C1";
			
			$scope.getListTeam = function(isZone){

				if(( $scope.zoneSelect_team > 0) && (isZone) ){
					$http.post('http://football.kapook.com/api/adminfootball/league/getjson/' + $scope.zoneSelect_team + '/' + $scope.yearSelect_team).success(function(data, status, headers, config) {
						$scope.listLeagueTeam			=	data;
						var isHas = false;
						for( var i in $scope.listLeagueTeam ){
							if( $scope.listLeagueTeam[i].id == $scope.leagueSelect_team ){
								isHas = true;
								break;
							}
						}
						if(!isHas){
							$scope.leagueSelect_team		=	0;
						}
						
						if( $scope.leagueSelect_team > 0 ){
							$scope.getListTeam_phase_2();
						}else{
							$scope.listTeam = [];
						}
					}).
					error(function(data, status, headers, config) {
						// called asynchronously if an error occurs
						// or server returns response with an error status.
					});
				}else{
					if(isZone){
						$scope.listLeagueTeam 			= 	[];
						$scope.leagueSelect_team		=	0;
					}
					
					if( $scope.leagueSelect_team > 0 ){
						$scope.getListTeam_phase_2();
					}else{
						$scope.listTeam = [];
					}
				}
			};
			
			$scope.getListTeam_phase_2 = function(){
				var url = 'http://football.kapook.com/api/adminfootball/team/getByLeague/' + $scope.leagueSelect_team + '?type=json';
					
				$http.post(url).success(function(data, status, headers, config) {
					$scope.listTeam			=	data;
				}).
				error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
				});
			}
			
			$scope.submit_team	= function(index){
				
				var data = jQuery.extend(true, {}, $scope.listTeam[index]);

				$http({
					method: 'POST',
					url: "<?php echo BASE_HREF; ?>api/adminfootball/team/update/" + data.id,
					data: $.param(data),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).
				success(function(data, status, headers, config) {
					if(data.is_success == 1){
						show_stack_bar_top('success','สำเร็จ','อัพเดทข้อมูลทีมแล้ว');
					}else{
						show_stack_bar_top('danger','ผิดพลาด','ไม่สามารถอัพเดทข้อมูลทีมได้');
					}
				}).
				error(function(data, status, headers, config) {
					show_stack_bar_top('success','ผิดพลาด','ไม่สามารถอัพเดทข้อมูลทีมได้');
				});
			}
			
		}]);

	})(window.angular);
			
</script>