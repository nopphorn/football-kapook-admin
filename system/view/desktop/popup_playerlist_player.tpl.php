<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>ระบบการจัดการฟุตบอลกระปุก 1.0</title>

		<!-- CSS -->
		<link href="<?php echo BASE_HREF; ?>api/adminfootball/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
			
		<!-- JS -->
		<script src="<?php echo BASE_HREF; ?>api/adminfootball/assets/js/jquery-1.11.0.min.js"></script>
		<script src="<?php echo BASE_HREF; ?>api/adminfootball/assets/bootstrap/js/bootstrap.min.js"></script>
		
	</head>
	<body>
		<form action="<?php echo BASE_HREF; ?>api/adminfootball/player/get_listPlayerForScorer" method="GET" style="z-index: 1;width: 100%;">
			<input type="text" class="form-control" name="keyword" placeholder="ค้นหาทีม" value="<?php echo $this->keyword; ?>" style="width: 80%;float:left;">
			<input class="btn btn-default" type="submit" value="Submit" style="width: 20%;">
		</form>
		<?php if(strlen($this->keyword)>0) { ?>
		<div class="list-group" id="listTeam" style="padding-top: -15px;margin-bottom: 0px;">
			<?php
			foreach($this->listPlayer as $tmpPlayer) {
				if(strlen($tmpPlayer['img'])){
					$pathPicture 			= 		str_replace("football.kapook.com", "202.183.165.189", $tmpPlayer['img']);
				}else{
					$pathPicture 			= 		'http://football.kapook.com/uploads/scorer/noface.jpg';
				}
			?>
			<a href="#" class="list-group-item" onclick="selectPlayer('<?php
				echo $tmpPlayer['name'];
			?>','<?php
				echo $pathPicture;
			?>','<?php
				echo $tmpPlayer['id'];
			?>')"><img src="<?php echo $pathPicture; ?>" height="50"> <?php echo $tmpPlayer['name']; ?></a>
			<?php
			}
			?>
		</div>
		<?php }else { ?>
		<div><h3>พิมพ์คำที่ต้องการเพื่อค้นหาผู้เล่น</h3></div>
		<?php } ?>
	</body>
	
	<script language="JavaScript">
		function selectPlayer(name,picture,id){
			var doc = window.opener.document;
			
			PlayerName = doc.getElementById("PlayerName");
			PlayerName.innerHTML = name;
			
			PlayerImg = doc.getElementById("PlayerImg");
			PlayerImg.setAttribute("src", picture);
			
			playerID = doc.getElementById("player_id");
			playerID.value = id;

			window.close();
		}
	</script>
	
</html>
