<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>ระบบการจัดการฟุตบอลกระปุก 1.0</title>

		<!-- CSS -->
		<link href="<?php echo BASE_HREF; ?>api/adminfootball/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
			
		<!-- JS -->
		<script src="<?php echo BASE_HREF; ?>api/adminfootball/assets/js/jquery-1.11.0.min.js"></script>
		<script src="<?php echo BASE_HREF; ?>api/adminfootball/assets/bootstrap/js/bootstrap.min.js"></script>
		
	</head>
	<body>
		<form action="<?php echo BASE_HREF; ?>api/adminfootball/comp/get_listTeamForMatch/<?php echo $this->side; ?>/<?php if($this->index_match >=0){ echo $this->index_match; } ?>" method="GET" style="z-index: 1;width: 100%;">
			<input type="text" class="form-control" name="keyword" placeholder="ค้นหาทีม" value="<?php echo $this->keyword; ?>" style="width: 80%;float:left;">
			<input class="btn btn-default" type="submit" value="Submit" style="width: 20%;">
		</form>
		<?php if(strlen($this->keyword)>0) { ?>
		<div class="list-group" id="listTeam" style="padding-top: -15px;margin-bottom: 0px;">
			<?php
			foreach($this->listTeam as $tmpTeam) {
				$Logo 						= 		str_replace(' ','-',$tmpTeam['NameEN']).'.png';
				$Logo_MC					=		$this->mem_lib->get('Football2014-Team-Logo-' . $Logo);
				if($Logo_MC==true){
					$pathlogo 				= 		'http://football.kapook.com/uploads/logo/' . $Logo;
				}else{
					$pathlogo 				= 		'http://football.kapook.com/uploads/logo/default.png';
				}
				var_dump();
			?>
			<a href="#" class="list-group-item" onclick="selectTeam('<?php
				if(!empty($tmpTeam['NameTH'])){
					$strTeam	=	$tmpTeam['NameTH'];
				}else if(!empty($tmpTeam['NameTHShort'])){
					$strTeam	=	$tmpTeam['NameTHShort'];
				}else{
					$strTeam	=	$tmpTeam['NameEN'];
				}
				echo $strTeam;
			?>','<?php
				echo $pathlogo;
			?>','<?php
				echo 'http://football.kapook.com/team-' . strtolower(str_replace(" ","-",$tmpTeam['NameEN']));
			?>',<?php
				echo $tmpTeam['id'];
			?>,'<?php
				echo $tmpTeam['NameEN'];
			?>')"><img src="<?php echo $pathlogo; ?>" height="30"> <?php echo $strTeam; ?></a>
			<?php
			}
			?>
		</div>
		<?php }else { ?>
		<div><h3>พิมพ์คำที่ต้องการเพื่อค้นหาทีม</h3></div>
		<?php } ?>
	</body>
	
	<script language="JavaScript">
		function selectTeam(name,logo,urlTeam,teamID,nameEN){
			var doc = window.opener.document;
			
			if($("#Team<?php echo $this->side; ?>Name",opener.document).length > 0){
				TeamName = doc.getElementById("Team<?php echo $this->side; ?>Name");
				TeamName.innerHTML = name;
			}
				
			if($("#Team<?php echo $this->side; ?>Logo",opener.document).length > 0){
				TeamLogo = doc.getElementById("Team<?php echo $this->side; ?>Logo");
				TeamLogo.setAttribute("src", logo);
			}
				
			if($("#Team<?php echo $this->side; ?>KPID",opener.document).length > 0){
				TeamKPID = doc.getElementById("Team<?php echo $this->side; ?>KPID");
				TeamKPID.value = teamID;
			}
			
			if($("#Team<?php echo $this->side; ?>",opener.document).length > 0){
				TeamNameEN = doc.getElementById("Team<?php echo $this->side; ?>");
				TeamNameEN.value = nameEN;
			}

			window.close();
		}
	</script>
	
</html>
