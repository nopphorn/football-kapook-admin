<?php
	$listOdd = array(
		'0.25' => 'เสมอควบครึ่ง',
		'0.5' => 'ครึ่งลูก',
		'0.75' => 'ครึ่งควบลูก',
		'1' => 'หนึ่งลูก',
		'1.25' => 'ลูกควบลูกครึ่ง',
		'1.5' => 'ลูกครึ่ง',
		'1.75' => 'ลูกครึ่งควบสองลูก',
		'2' => 'สองลูก',
		'2.25' => 'สองลูกควบสองลูกครึ่ง',
		'2.5' => 'สองลูกครึ่ง',
		'2.75' => 'สองลูกครึ่งควบสามลูก',
		'3' => 'สามลูก',
		'3.25' => 'สามลูกควบสามลูกครึ่ง',
		'3.5' => 'สามลูกครึ่ง',
		'3.75' => 'สามลูกครึ่งควบสี่ลูก',
		'4' => 'สี่ลูก',
		'4.25' => 'สี่ลูกควบสี่ลูกครึ่ง',
		'4.5' => 'สี่ลูกครึ่ง',
		'4.75' => 'สี่ลูกครึ่งควบห้าลูก',
		'5' => 'ห้าลูก',
		'5.25' => 'ห้าลูกควบห้าลูกครึ่ง',
		'5.5' => 'ห้าลูกครึ่ง',
		'5.75' => 'ห้าลูกครึ่งควบหกลูก',
		'6' => 'หกลูก',
		'6.25' => 'หกลูกควบหกลูกครึ่ง',
		'6.5' => 'หกลูกครึ่ง',
		'6.75' => 'หกลูกครึ่งควบเจ็ดลูก',
		'7' => 'เจ็ดลูก',
		'7.25' => 'เจ็ดลูกควบเจ็ดลูกครึ่ง',
		'7.5' => 'เจ็ดลูกครึ่ง',
		'7.75' => 'เจ็ดลูกครึ่งควบแปดลูก',
		'8' => 'แปดลูก',
		'8.25' => 'แปดลูกควบแปดลูกครึ่ง',
		'8.5' => 'แปดลูกครึ่ง',
		'8.75' => 'แปดลูกครึ่งควบเก้าลูก',
		'9' => 'เก้าลูก',
		'9.25' => 'เก้าลูกควบเก้าลูกครึ่ง',
		'9.5' => 'เก้าลูกครึ่ง',
		'9.75' => 'เก้าลูกครึ่งควบสิบลูก',
	);
?>

<style type="text/css">
	.sortorder:after {
		content: '\25b2';
	}
	.sortorder.reverse:after {
		content: '\25bc';
	}
	.sort_click {
		cursor:pointer
	}
	
	.show-grid [class^=col-] {
		padding-top: 10px;
		padding-bottom: 10px;
		border: 1px solid #ddd;
		border: 1px solid rgba(86,61,124,.2);
	}
	
	.leaguename {
		overflow: hidden;
		word-break: break-all;
		text-overflow: ellipsis;
		white-space: nowrap;
	}
	
	.panel-heading-league {
		padding-bottom: 35px;
		padding-left: 0px;
		padding-right: 0px;
	}
	.sortorder:after {
		content: '\25b2';
	}
	.sortorder.reverse:after {
		content: '\25bc';
	}
	
</style>

<div class="container" ng-app="CompApp" ng-controller="CompController">
	<div class="panel panel-primary">
		<div class="panel-heading"><h3 class="panel-title">เงื่อนไขในการค้นหา</h3></div>
		<table class="table">
			<tbody>
				<tr><th style="background: #fbeaea;font-size: x-large;"><b><center>โซน/ลีก/ปี</center></b></th></tr>
				<tr><th>
					<div class="row form-group"><center>
						<div class="col-sm-4 col-md-4">
							<label>โซนที่ต้องการค้นหา</label>
								<select class="form-control" ng-model="zoneSelect_match" ng-change="getListMatch(true,true)">
								<option value="0">ทุกโซน</option>
								<?php foreach( $this->dataZone as $tmpZone ){ ?>
								<option value="<?php echo $tmpZone['id']; ?>"><?php echo $tmpZone['NameEN']; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col-sm-4 col-md-4">
							<label>ลีกที่ต้องการค้นหา</label>
							<select class="form-control" ng-options="tmpData.id as tmpData.NameTH for tmpData in listLeagueMatch" ng-model="leagueSelect_match" ng-change="getListMatch(false,true)">
								<option value="">ทุกลีก</option>
							</select>
							<!--<select class="form-control" ng-model="leagueSelect_match" ng-change="getListMatch(false,true)">
								<option value="0">ทุกลีก</option>
								<option ng-repeat="tmpData in listLeagueMatch" value="{{tmpData.id}}">{{tmpData.NameTH}}</option>
							</select>-->
						</div>
						<div class="col-sm-4 col-md-4">
							<label>ปีที่ต้องการค้นหา</label>
							<select class="form-control" ng-model="yearSelect_match" ng-change="getListMatch(true,true)">
								<option value="0">ทุกปี</option>
								<option value="2013">2013/2014</option>
								<option value="2014">2014/2015</option>
								<option value="2015">2015/2016</option>
								<option value="2016">2016/2017</option>
								<option value="2017">2017/2018</option>
							</select>
						</div>
					</center></div>
				</th></tr>
				<tr><th style="background: #fbeaea;font-size: x-large;"><b><center>MatchDateTime</center></b></th></tr>
				<tr><th>
					<div class="row form-group">
						<div class="col-sm-3 col-md-3">
							<label class="radio-inline">
								<input type="radio" name="MatchTimeSearch" ng-model="matchTimeSearch" ng-value="1" ng-change="getListMatch(false,true)"> ไม่กำหนดเวลา
							</label>
						</div>
						<div class="col-sm-3 col-md-3">
							<label class="radio-inline">
								<input type="radio" name="MatchTimeSearch" ng-model="matchTimeSearch" ng-value="2" ng-change="getListMatch(false,true)">ก่อน...
							</label>
						</div>
						<div class="col-sm-3 col-md-3">
							<label class="radio-inline">
								<input type="radio" name="MatchTimeSearch" ng-model="matchTimeSearch" ng-value="3" ng-change="getListMatch(false,true)">หลัง...
							</label>
						</div>
						<div class="col-sm-3 col-md-3">
							<label class="radio-inline">
								<input type="radio" name="MatchTimeSearch" ng-model="matchTimeSearch" ng-value="4" ng-change="getListMatch(false,true)">ระหว่าง...
							</label>
						</div>
						<center>
							<div class="col-sm-6 col-md-6">
								<label>วันที่เริ่มการค้นหา</label>
								<input type="text" class="form-control MatchDate" placeholder="วันที่แข่งขัน" ng-model="dateFromSelect_match" ng-change="getListMatch(false,true)" ng-disabled="isFromDate">
							</div>
							<div class="col-sm-6 col-md-6">
								<label>วันที่สิ้นสุดการค้นหา</label>
								<input type="text" class="form-control MatchDate" placeholder="วันที่แข่งขัน" ng-model="dateToSelect_match" ng-change="getListMatch(false,true)" ng-disabled="isToDate">
							</div>
						</center>
					</div>
				</th></tr>
				<tr><th style="background: #fbeaea;font-size: x-large;"><b><center>อื่นๆ</center></b></th></tr>
				<tr><th>
					<div class="row form-group"><center>
						<div class="col-sm-2 col-md-2">
							<label>สถานะ</label>
							<select class="form-control" ng-model="statusSelect_match" ng-change="getListMatch(false,true)">
								<option value="-1">ทุกสถานะ</option>
								<option value="0">ปิด</option>
								<option value="1">เปิด</option>
							</select>
						</div>
						<div class="col-sm-2 col-md-2">
							<label>MatchStatus</label>
							<select class="form-control" ng-model="MatchStatus" ng-change="getListMatch(false,true)">
								<option value="">ทั้งหมด</option>
								<option value="Sched">Sched</option>
								<option value="1 HF">1 HF</option>
								<option value="H/T">H/T</option>
								<option value="2 HF">2 HF</option>
								<option value="E/T">E/T</option>
								<option value="Pen">Pen</option>
								<option value="Fin">Fin</option>
								<option value="Int">Int</option>
								<option value="Abd">Abd</option>
								<option value="Post">Post</option>
								<option value="Canc">Canc</option>
							</select>
						</div>
						<div class="col-sm-5 col-md-5">
							<label>Sort by</label><br>
							<button class="btn btn-default" type="button" ng-click="order('XSMatchID')">XSMatchID<span class="sortorder" ng-show="predicate == 'XSMatchID'" ng-class="{reverse:reverse}"></span></button>
							<button class="btn btn-default" type="button" ng-click="order('KPMatchID')">KPMatchID<span class="sortorder" ng-show="predicate == 'KPMatchID'" ng-class="{reverse:reverse}"></span></button>
							<button class="btn btn-default" type="button" ng-click="order('MatchDateTime')">MatchDateTime<span class="sortorder" ng-show="predicate == 'MatchDateTime'" ng-class="{reverse:reverse}"></span></button>
						</div>
					</center></div>
				</th></tr>
				<tr><th style="background: #fbeaea;font-size: x-large;"><b><center>สร้างการค้นหาลัด จากเงื่อนไข ณ ปัจจุบัน</center></b></th></tr>
				<tr><th>
					<div class="col-sm-12 col-md-12" style="padding-left: 0px;padding-right: 0px;">
						<div class="input-group">
							<input type="text" ng-model="new_serach" class="form-control" placeholder="ชื่อการค้นหาลัดใหม่">
							<span class="input-group-btn">
								<button class="btn btn-success" type="button" ng-click="add_search()">สร้าง</button>
							</span>
						</div>
					</div>
				</th></tr>
				<tr><th style="background: #fbeaea;font-size: x-large;"><b><center>การค้นหาลัดที่บันทึกไว้</center></b></th></tr>
				<tr><th>
					<div ng-repeat="tmpData in listSearch" class="btn-group" role="group" aria-label="..." style="margin-right: 10px;">
						<button type="button" class="btn btn-default" ng-click="set_search($index)">{{tmpData.name}}</button>
						<button type="button" class="btn btn-danger" ng-click="delete_search($index)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
					</div>
				</th></tr>
			</tbody>
		</table>
	</div>
	
	<nav><ul class="pager">
		<li class="previous" ng-class="{disabled : isnotprevious}"><a href="#" class="service" ng-click="page_previous()"><span aria-hidden="true">&larr;</span> Older</a></li>
		<li class="next" ng-class="{disabled : isnotnext}"><a href="#" class="service" ng-click="page_next()">Newer <span aria-hidden="true">&rarr;</span></a></li>
	</ul></nav>
	
	<div ng-repeat="tmpData in listMatch" class="panel {{bgColor[tmpData.Status]}}" >
		<div class="panel-heading panel-heading-league">
			<div class="col-md-8 col-sm-7 col-xs-6 leaguename">
				<b>{{tmpData.XSLeagueCountry}}</b> : {{tmpData.XSLeagueName}} - {{tmpData.XSLeagueDesc}}
			</div>
			<div class="col-md-4 col-sm-5 col-xs-6">
				<a style="float: right;margin-top: -4px;" ng-href="{{tmpData.url}}" type="button" target="_blank" class="btn btn-primary">ไปยังหน้าแมตช์</a>
				<a style="float: right;margin-top: -4px;margin-right: 10px;" ng-href="<?php echo BASE_HREF; ?>api/adminfootball/match/detail/{{tmpData.id}}" type="button" target="_blank" class="btn btn-info">แก้ไขโดยละเอียด</a>
			</div>
		</div>
		<div class="panel-body" style="background-color: {{bgColorFill[tmpData.Status]}};">
			<div class="row"><div class="col-md-12 col-sm-12 col-xs-12">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td class="col-xs-3" rowspan="3"><center><img id="Team1Logo" style="width: 125px;" src="{{tmpData.Team1Logo}}"><br>{{tmpData.Team1Show}}</center></td>
						<td class="col-xs-2" ><input type="number" class="form-control" ng-model="tmpData.Team1FTScore" ng-disabled="!tmpData.isEdit"></td>
						<td class="col-xs-2" ><center>Full-Time</center></td>
						<td class="col-xs-2" ><input type="number" class="form-control" ng-model="tmpData.Team2FTScore" ng-disabled="!tmpData.isEdit"></td>
						<td class="col-xs-3" rowspan="3"><center><img id="Team2Logo" style="width: 125px;" src="{{tmpData.Team2Logo}}"><br>{{tmpData.Team2Show}}</center></td>
					</tr>
					<tr style="background-color: #f9f9f9;">
						<td><input type="number" class="form-control" ng-model="tmpData.Team1ETScore" ng-show="tmpData.isET" ng-disabled="!tmpData.isET || !tmpData.isEdit"></td>
						<td><center><button class="btn" ng-class="{'btn-success' : tmpData.isET,'btn-danger' : !tmpData.isET,'disabled' : !tmpData.isEdit}" type="button" id="ETButton" ng-click="toggleET($index)">Extra Time</button></center></td>
						<td><input type="number" class="form-control" ng-model="tmpData.Team2ETScore" ng-show="tmpData.isET" ng-disabled="!tmpData.isET || !tmpData.isEdit"></td>
					</tr>
					<tr>
						<td><input type="number" class="form-control" ng-model="tmpData.Team1PNScore" ng-show="tmpData.isPN" ng-disabled="!tmpData.isPN || !tmpData.isEdit"></td>
						<td><center><button class="btn" ng-class="{'btn-success' : tmpData.isPN,'btn-danger' : !tmpData.isPN,'disabled' : !tmpData.isEdit}" type="button" id="PNButton" ng-click="togglePN($index)">PK</button></center></td>
						<td><input type="number" class="form-control" ng-model="tmpData.Team2PNScore" ng-show="tmpData.isPN" ng-disabled="!tmpData.isPN || !tmpData.isEdit"></td>
					</tr>
				</tbody>
			</table>
			</div></div>
			<div class="row">
				<div class="col-md-2 col-sm-2 col-xs-6">
					<label>XSMatchID</label>
					<p class="lead">{{tmpData.XSMatchID}}</p>
				</div>
				<div class="col-md-2 col-sm-2 col-xs-6">
					<label>KPMatchID</label>
					<p class="lead">{{tmpData.id}}</p>
				</div>
				<div class="col-md-2 col-sm-2 col-xs-6">
					<label>MatchDateTime</label>
					<p class="lead">{{tmpData.MatchDateTime}}</p>
				</div>
				<div class="col-md-2 col-sm-2 col-xs-6">
					<label>สถานะ</label>
					<select class="form-control" ng-model="tmpData.Status">
						<option value="0">ปิด</option>
						<option value="1">เปิด</option>
					</select>
				</div>
				<div class="col-md-2 col-sm-2 col-xs-6">
					<label>MatchStatus</label>
					<select class="form-control" ng-model="tmpData.MatchStatus" ng-disabled="!tmpData.isEdit">
						<option value="Sched">Sched</option>
						<option value="1 HF">1 HF</option>
						<option value="H/T">H/T</option>
						<option value="2 HF">2 HF</option>
						<option value="E/T">E/T</option>
						<option value="Pen">Pen</option>
						<option value="Fin">Fin</option>
						<option value="Int">Int</option>
						<option value="Abd">Abd</option>
						<option value="Post">Post</option>
						<option value="Canc">Canc</option>
					</select>
				</div>
				
				<div class="col-md-2 col-sm-2 col-xs-6" ng-show="tmpData.isEdit">
					<label>นาที</label>
					<input type="checkbox" ng-model="tmpData.isEditTime">
					<input type="number" class="form-control" ng-model="tmpData.Minute" ng-disabled="!tmpData.isEditTime">
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-3 col-sm-3 col-xs-6">
					<label>อัตราต่อรอง</label>
					<select class="form-control" ng-model="tmpData.Odds">
						<option value="-1" >ไม่มีอัตราต่อรอง</option>
						<option value="0" >เสมอ</option>
						<?php
						foreach ($listOdd as $k => $v) {
							echo '<option value="' . (float)$k . '">' . $k . '</option>';
						}?>
					</select>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6">
					<label>ทีมที่ต้องการต่อ</label>
					<select name="TeamOdds[{{tmpData.id}}]" class="form-control" ng-model="tmpData.TeamOdds">
						<option value="0" >ไม่เลือก</option>
						<option value="1" >{{tmpData.Team1Show}}</option>
						<option value="2" >{{tmpData.Team2Show}}</option>
					</select>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6">
					<label>ฟันธง</label>
					<select name="TeamSelect[{{tmpData.id}}]" class="form-control" ng-model="tmpData.TeamSelect">
						<option value="0" >ไม่เลือก</option>
						<option value="1" >{{tmpData.Team1Show}}</option>
						<option value="2" >{{tmpData.Team2Show}}</option>
					</select>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6">
					<label>ทรรศนะฟุตบอล</label>
					<input class="form-control" name="Predict[{{tmpData.id}}]" ng-model="tmpData.Predict">
				</div>
			</div>
			
			<div class="row" style="margin-top: 20px;"><div class="col-md-12 col-sm-12 col-xs-12" >
			<div class="panel panel-primary">
				<div class="panel-heading"><h3 class="panel-title">ระบบควบคุม Livescore</h3></div>
				<div class="panel-body">
					<div class="col-sm-6 col-md-4">
						<button type="button" class="btn btn-lg btn-block" ng-class="{'btn-success' : tmpData.AutoMode==1,'btn-default' : tmpData.AutoMode!=1}" ng-click="update_auto($index,1)" ng-disabled="tmpData.AutoMode==1">Auto-Update</button>
					</div>
					<div class="col-sm-6 col-md-4">
						<button type="button" class="btn btn-lg btn-block" ng-class="{'btn-danger' : tmpData.AutoMode==0,'btn-default' : tmpData.AutoMode!=0}" ng-click="update_auto($index,0)" ng-disabled="tmpData.AutoMode==0">Stop-Update</button>
					</div>
					<div class="col-sm-6 col-md-4">
						<button type="button" class="btn btn-lg btn-block" ng-class="{'btn-success' : tmpData.AutoMode==-1,'btn-default' : tmpData.AutoMode!=-1}" ng-click="update_auto($index,-1)" ng-disabled="tmpData.AutoMode==-1">Manual-Update</button>
					</div>
				</div>
			</div>
			</div></div>

			<div class="row"><div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 15px;">
				<table class="table table-bordered">
					<tr>
						<td class="col-xs-3"><b><u>ช่องการถ่ายทอดสด</u></b></td>
						<td class="col-xs-6">
							<img ng-if="tmpData.TVLiveList.length >= 1" ng-repeat="tmpTV in tmpData.TVLiveList" src="http://202.183.165.189/uploads/tvlogo/{{tmpTV}}.png" alt="{{tmpTV}}" style="padding-right: 10px;">
						</td>
						<td class="col-xs-3" style="text-align: right;">
							<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#TVLiveList_{{$index}}" aria-expanded="false" ng-click="tv_click($index,tvlist)">
								เปิดรายชื่อทีวี เพื่อเลือกช่องถ่ายทอดสด
							</button>
						</td>
					</tr>
				</table>
			</div></div>
			
			<div class="row"><div class="col-md-12 col-sm-12 col-xs-12 collapse" id="TVLiveList_{{$index}}">
				<table class="table table-bordered" style="background-color: #FFFFFF;">
					<tbody><tr>
						<td colspan="5"><b>TV Analog</b></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="tv3" checklist-model="tmpData.TVLiveList" ><img src="http://202.183.165.189/uploads/tvlogo/tv3.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="tv5" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/tv5.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="tv7" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/tv7.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="tv9" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/tv9.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="tv11" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/tv11.png" height="20"></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="tpbs" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/tpbs.png" height="20"></td>
						<td colspan="4"></td>
					</tr>
					<tr>
						<td colspan="5"><b>TV Digital</b></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="tv5hd" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/tv5hd.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="tv11hd" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/tv11hd.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="tpbshd" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/tpbshd.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="tv3family" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/tv3family.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="mcotfamily" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/mcotfamily.png" height="20"></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="tnn" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/tnn.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="newtv" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/newtv.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="springnews" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/springnews.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="nation" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/nation.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="workpointtv" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/workpointtv.png" height="20"></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="true4u" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/true4u.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="gmmchannel" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/gmmchannel.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="now" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/now.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="ch8" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/ch8.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="tv3sd" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/tv3sd.png" height="20"></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="mono" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/mono.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="mcothd" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/mcothd.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="one" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/one.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="thairath" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/thairath.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="tv3hd" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/tv3hd.png" height="20"></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="amarin" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/amarin.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="pptv" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/pptv.png" height="20"></td>
						<td colspan="3"></td>
					</tr>
					<tr>
						<td colspan="5"><b>True Vision</b></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="tshd1" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/tshd1.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="tshd2" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/tshd2.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="tshd3" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/tshd3.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="tshd4" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/tshd4.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="ts1" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/ts1.png" height="20"></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="ts2" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/ts2.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="ts3" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/ts3.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="ts4" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/ts4.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="ts5" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/ts5.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="ts6" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/ts6.png" height="20"></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="ts7" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/ts7.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="tstennis" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/tstennis.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="hayha" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/hayha.png" height="20">(352)</td>
						<td><input type="checkbox" class="TVList" value="thaithai" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/thaithai.png" height="20"> (358)</td>
						<td><input type="checkbox" class="TVList" value="true" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/true.png" height="20"></td>
					</tr>
					<tr>
						<td colspan="5"><b>CTH</b></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="cthstd1" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/cthstd1.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="cthstd2" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/cthstd2.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="cthstd3" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/cthstd3.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="cthstd4" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/cthstd4.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="cthstd5" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/cthstd5.png" height="20"></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="cthstd6" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/cthstd6.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="cthstdx" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/cthstdx.png" height="20"></td>
						<td colspan="3"></td>
					</tr>
					<tr>
						<td colspan="5"><b>GMM</b></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="gmmclubchannel" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/gmmclubchannel.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="gmmfootballplus" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/gmmfootballplus.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="gmmfootballmax" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/gmmfootballmax.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="gmmfootballeuro" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/gmmfootballeuro.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="gmmfootballextrahd" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/gmmfootballextrahd.png" height="20"></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="gmmsportextreme" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/gmmsportextreme.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="gmmsportplus" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/gmmsportplus.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="gmmsportextra" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/gmmsportplus.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="gmmsportone" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/gmmsportplus.png" height="20"></td>
						<td colspan="1"></td>
					</tr>
					<tr>
						<td colspan="5"><b>Siamsport</b></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="siamsportnew" checklist-model="tmpData.TVLiveList">Siamsport News</td>
						<td><input type="checkbox" class="TVList" value="siamsportfootball" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/siamsportfootball.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="siamsportlive" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/siamsportlive.png" height="20"></td><td colspan="2"></td>
					</tr>
					<tr>
						<td colspan="5"><b>RS</b></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="sunchannel" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/sunchannel.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="worldcupchannel" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/worldcupchannel.png" height="20"></td>
						<td colspan="3"></td>
					</tr>
					<tr>
						<td colspan="5"><b>Bein Sport</b></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="bein" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/bein.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="bein1" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/bein1.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="bein2" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/bein2.png" height="20"></td>
						<td colspan="2"></td>
					</tr>
					<tr>
						<td colspan="5"><b>Fox Sport</b></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="foxsport" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/foxsport.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="foxsport2" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/foxsport2.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="foxsport3" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/foxsport3.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="foxsportplay" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/foxsportplay.png" height="20"></td>
						<td colspan="1"></td>
					</tr>
					<tr>
						<td colspan="5"><b>อื่นๆ</b></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="starsport" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/starsport.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="astrosupersport1" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/astrosupersport1.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="astrosupersport2" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/astrosupersport2.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="astrosupersporthd3" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/astrosupersporthd3.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="asn" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/asn.png" height="20"></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="asn2" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/asn2.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="bugaboo" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/bugaboo.png" height="20"></td>
						<td colspan="3"></td>
					</tr></tbody>
				</table>
			</div></div>

		</div>
		<div class="panel-footer">
			<div class="row"><div class="col-md-12 col-sm-12 col-xs-12">
				<button type="button" style="width: inherit;" class="btn btn-success btn-lg" ng-click="submit_match($index)">บันทึกด่วน</button>
			</div></div>
		</div>
		
	</div>
	
	<nav><ul class="pager">
		<li class="previous" ng-class="{disabled : isnotprevious}"><a href="#" class="service" ng-click="page_previous()"><span aria-hidden="true">&larr;</span> Older</a></li>
		<li class="next" ng-class="{disabled : isnotnext}"><a href="#" class="service" ng-click="page_next()">Newer <span aria-hidden="true">&rarr;</span></a></li>
	</ul></nav>

<script>
	
	function getCookieByName(str){
		var ca 			= 	document.cookie.split(';');
		var dataCookie	=	"";

		var name = str + "=";
		for(var i=0; i<ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0)==' ')
				c = c.substring(1);
			if (c.indexOf(name) != -1){
				dataCookie = c.substring(name.length,c.length);
				break;
			}
		}
		
		return dataCookie;
	}
	
	(function(angular) {
		var myApp = angular.module('CompApp', ["checklist-model"]);

		myApp.controller('CompController', ['$scope','$http','$interval', function($scope,$http,$interval) {
			
			tmpData = getCookieByName('matchTimeSearch');
			if(getCookieByName('matchTimeSearch')!=""){
				$scope.matchTimeSearch = parseInt(tmpData);
			}else{
				$scope.matchTimeSearch = 4;
			}
			
			tmpData = getCookieByName('zoneSelect_match');
			if(getCookieByName('zoneSelect_match')!=""){
				$scope.zoneSelect_match = parseInt(tmpData);
			}else{
				$scope.zoneSelect_match = 0;
			}
			
			tmpData = getCookieByName('leagueSelect_match');
			if(getCookieByName('leagueSelect_match')!=""){
				$scope.leagueSelect_match = parseInt(tmpData);
			}else{
				$scope.leagueSelect_match = 0;
			}
			
			tmpData = getCookieByName('yearSelect_match');
			if(getCookieByName('yearSelect_match')!=""){
				$scope.yearSelect_match = parseInt(tmpData);
			}else{
				$scope.yearSelect_match	= 0;
			}
			
			tmpData = getCookieByName('dateFromSelect_match');
			if(getCookieByName('dateFromSelect_match')!=""){
				$scope.dateFromSelect_match = tmpData;
			}else{
				$scope.dateFromSelect_match = "<?php if(date('G')>6) { echo date('Y-m-d 06:00'); }else{ echo date('Y-m-d 06:00',strtotime('-1 days')); } ?>";
			}
			
			tmpData = getCookieByName('dateToSelect_match');
			if(getCookieByName('dateToSelect_match')!=""){
				$scope.dateToSelect_match = tmpData;
			}else{
				$scope.dateToSelect_match = "<?php if(date('G')>6) { echo date('Y-m-d 05:59',strtotime('+1 days')); }else{ echo date('Y-m-d 05:59'); } ?>";
			}
			
			tmpData = getCookieByName('MatchStatus');
			if(getCookieByName('MatchStatus')!=""){
				$scope.MatchStatus = tmpData;
			}else{
				$scope.MatchStatus = "";
			}

			tmpData = getCookieByName('statusSelect_match');
			if(getCookieByName('statusSelect_match')!=""){
				$scope.statusSelect_match = parseInt(tmpData);
			}else{
				$scope.statusSelect_match =	-1;
			}

			$scope.bgColor		=	[];
			$scope.bgColor[0]	=	"panel-danger";
			$scope.bgColor[1]	=	"panel-success";
			
			$scope.bgColorFill		=	[];
			$scope.bgColorFill[0]	=	"#f2dede";
			$scope.bgColorFill[1]	=	"#dff0d8";

			$scope.isFromDate = false;
			$scope.isToDate = false;
			
			$scope.listMatch = [];
			$scope.listSearch = [];
			
			$scope.pagesize 		= 50;
			$scope.start 			= 0;
			$scope.isnotnext 		= true;
			$scope.isnotprevious 	= true;
			
			tmpData = getCookieByName('predicate');
			if(getCookieByName('predicate')!=""){
				$scope.predicate 		= 	tmpData;
			}else{
				$scope.predicate 		= 	"MatchDateTime";
			}
			
			tmpData = getCookieByName('reverse');
			if(getCookieByName('reverse')!=""){
				if(parseInt(tmpData) == -1){
					$scope.reverse 		= 	true;
				}else{
					$scope.reverse 		= 	false;
				}
			}else{
				$scope.reverse 		= 	false;
			}
			
			$scope.order = function(predicate) {
				$scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
				$scope.predicate = predicate;
				$scope.getListMatch(false,false);
			};
			
			$scope.page_previous = function(){
				if(!$scope.isnotprevious){
					$scope.start = $scope.start - $scope.pagesize;
					if($scope.start < 0){
						$scope.start = 0;
					}
					$scope.getListMatch(false,false);
				}
			}
			
			$scope.page_next = function(){
				if(!$scope.isnotnext){
					$scope.start = $scope.start + $scope.pagesize;
					$scope.getListMatch(false,false);
				}
			}
			
			$scope.togglePN	= function(index){
				$scope.listMatch[index].isPN	=	!$scope.listMatch[index].isPN;
			}

			$scope.toggleET	= function(index){
				$scope.listMatch[index].isET	=	!$scope.listMatch[index].isET;
			}
			
			$scope.set_search	= function(index){
				
				var tmpZone = $scope.zoneSelect_match;
				var tmpYear = $scope.yearSelect_match;
				
				$scope.zoneSelect_match = $scope.listSearch[index].zone;
				$scope.leagueSelect_match = $scope.listSearch[index].league;
				$scope.yearSelect_match = $scope.listSearch[index].year;
				$scope.matchTimeSearch = $scope.listSearch[index].matchtimesearch;
				$scope.dateFromSelect_match = $scope.listSearch[index].datefrom;
				$scope.dateToSelect_match = $scope.listSearch[index].dateto;
				$scope.statusSelect_match = $scope.listSearch[index].status;
				$scope.MatchStatus = $scope.listSearch[index].matchstatus;
				$scope.predicate = $scope.listSearch[index].sort;
				
				if(parseInt($scope.listSearch[index].order) == -1){
					$scope.reverse 		= 	true;
				}else{
					$scope.reverse 		= 	false;
				}
				
				if((tmpZone==$scope.zoneSelect_match)&&(tmpYear==$scope.yearSelect_match)){
					$scope.getListMatch(false,true);
				}else{
					$scope.getListMatch(true,true);
				}
				
			}
			
			$scope.delete_search	= function(index){
				var url = 'http://football.kapook.com/api/adminfootball/match/remove_search/' + $scope.listSearch[index].id;
				$http.post(url).success(function(data, status, headers, config) {
					if(data.is_success == 1){
						show_stack_bar_top('success','สำเร็จ','ลบการค้นหาสำเร็จ');
						$scope.getListSearch();
					}else{
						show_stack_bar_top('warning','ผิดพลาด','มีปัญหาการในการลบข้อมูล');
					}
				}).
				error(function(data, status, headers, config) {
					show_stack_bar_top('danger','ผิดพลาด','มีปัญหาการเชื่อมต่อ');
				});
			}
			
			$scope.add_search	= function(){
				var tmpsearchdata = {
					zone 			: 	$scope.zoneSelect_match,
					league 			: 	$scope.leagueSelect_match,
					year 			:	$scope.yearSelect_match,
					matchtimesearch : 	$scope.matchTimeSearch,
					datefrom 		: 	$scope.dateFromSelect_match,
					dateto 			: 	$scope.dateToSelect_match,
					status 			: 	$scope.statusSelect_match,
					matchstatus 	: 	$scope.MatchStatus,
					sort 			: 	$scope.predicate,
					order 			: 	($scope.reverse) ? -1 : 1,
					name 			: 	$scope.new_serach
				};
				$http({
					method: 'POST',
					url: "<?php echo BASE_HREF; ?>api/adminfootball/match/add_search",
					data: $.param(tmpsearchdata),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).
				success(function(data, status, headers, config) {
					if(data.is_success == 1){
						show_stack_bar_top('success','สำเร็จ','สร้างการค้นหาใหม่ได้แล้ว');
						$scope.getListSearch();
						$scope.new_serach = "";
					}else{
						show_stack_bar_top('warning','ผิดพลาด','กรุณาเติมชื่อให้กับการค้นหาใหม่ด้วย');
					}
				}).
				error(function(data, status, headers, config) {
					show_stack_bar_top('danger','ผิดพลาด','มีปัญหาการเชื่อมต่อ');
				});
			}
			
			$scope.submit_match	= function(index){
				
				var data = jQuery.extend(true, {}, $scope.listMatch[index]);
				
				if(!data.isEdit){
					delete data.Team1FTScore;
					delete data.Team2FTScore;
				}
				
				if((!data.isET)||(!data.isEdit)){
					delete data.Team1ETScore;
					delete data.Team2ETScore;
				}
				if((!data.isPN)||(!data.isEdit)){
					delete data.Team1PNScore;
					delete data.Team2PNScore;
				}
				
				if(!data.isEditTime){
					delete data.Minute;
				}
				
				delete data.AutoMode;
				delete data.isEdit
				
				//console.log(data);
				$http({
					method: 'POST',
					url: "<?php echo BASE_HREF; ?>api/adminfootball/match/update/" + data.id,
					data: $.param(data),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).
				success(function(data, status, headers, config) {
					if(data.is_success == 1){
						show_stack_bar_top('success','สำเร็จ','อัพเดทการแข่งขันเสร็จแล้ว');
						if($scope.listMatch[index].AutoMode == 1){
							$scope.listMatch[index].isEdit = false;
						}else{
							$scope.listMatch[index].isEdit = true;
						}
						$scope.listMatch[index].Minute = data.data_lastest.Minute;
					}else{
						show_stack_bar_top('danger','ผิดพลาด','ไม่สามารถอัพเดทการแข่งขันได้');
					}
					
				}).
				error(function(data, status, headers, config) {
					show_stack_bar_top('success','ผิดพลาด','ไม่สามารถอัพเดทการแข่งขันได้');
				});
			}
			
			$scope.update_auto	= function(index,value){
				
				data = { AutoMode : parseInt(value) };
				
				$http({
					method: 'POST',
					url: "<?php echo BASE_HREF; ?>api/adminfootball/match/update/" + $scope.listMatch[index].id,
					data: $.param(data),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).
				success(function(data, status, headers, config) {
					if(data.is_success == 1){
						$scope.listMatch[index].AutoMode = parseInt(value);
						show_stack_bar_top('success','สำเร็จ','อัพเดทการแข่งขันเสร็จแล้ว');
						if($scope.listMatch[index].AutoMode == 1){
							$scope.listMatch[index].isEdit = false;
						}else{
							$scope.listMatch[index].isEdit = true;
						}
					}else{
						show_stack_bar_top('danger','ผิดพลาด','ไม่สามารถอัพเดทการแข่งขันได้');
					}
					
				}).
				error(function(data, status, headers, config) {
					show_stack_bar_top('success','ผิดพลาด','ไม่สามารถอัพเดทการแข่งขันได้');
				});
			}

			$scope.getListMatch = function(isLoadLeague,isNewPage){
				
				if(isNewPage){
					$scope.start = 0;
				}
				
				document.cookie = "matchTimeSearch=" + $scope.matchTimeSearch + "; path=/api/adminfootball/";
				document.cookie = "zoneSelect_match=" + $scope.zoneSelect_match + "; path=/api/adminfootball/";
				document.cookie = "leagueSelect_match=" + $scope.leagueSelect_match + "; path=/api/adminfootball/";
				document.cookie = "yearSelect_match=" + $scope.yearSelect_match + "; path=/api/adminfootball/";
				document.cookie = "dateFromSelect_match=" + $scope.dateFromSelect_match + "; path=/api/adminfootball/";
				document.cookie = "dateToSelect_match=" + $scope.dateToSelect_match + "; path=/api/adminfootball/";
				document.cookie = "MatchStatus=" + $scope.MatchStatus + "; path=/api/adminfootball/";
				document.cookie = "statusSelect_match=" + $scope.statusSelect_match + "; path=/api/adminfootball/";
				document.cookie = "predicate=" + $scope.predicate + "; path=/api/adminfootball/";
				
				if($scope.reverse){
					document.cookie = "reverse=-1; path=/api/adminfootball/";
				}else{
					document.cookie = "reverse=1; path=/api/adminfootball/";
				}
				
				var url = 'http://football.kapook.com/api/adminfootball/livescore/getjson_new?size=' + $scope.pagesize + '&start=' + $scope.start + '&sordby=' + $scope.predicate;
				
				if($scope.reverse){
					url = url + '&typesort=desc';
				}else{
					url = url + '&typesort=asc';
				}
				
				if($scope.MatchStatus!=""){
					url = url + '&MatchStatus=' + $scope.MatchStatus;
				}
				
				if($scope.matchTimeSearch==4){
					url = url + '&dateFrom=' + $scope.dateFromSelect_match + '&dateTo=' + $scope.dateToSelect_match;
					$scope.isFromDate = false;
					$scope.isToDate = false;
				}else if($scope.matchTimeSearch==3){
					url = url + '&dateFrom=' + $scope.dateFromSelect_match;
					$scope.isFromDate = false;
					$scope.isToDate = true;
				}else if($scope.matchTimeSearch==2){
					url = url + '&dateTo=' + $scope.dateToSelect_match;
					$scope.isFromDate = true;
					$scope.isToDate = false;
				}else{
					$scope.isFromDate = true;
					$scope.isToDate = true;
				}
				
				
				if($scope.statusSelect_match!=-1){
					url = url + '&status=' + $scope.statusSelect_match;
				}
				
				//if Zone > 0 type
				if(($scope.zoneSelect_match > 0)&&(isLoadLeague)){
					url = url + '&zoneID=' + $scope.zoneSelect_match;
					$http.post('http://football.kapook.com/api/adminfootball/league/getjson/' + $scope.zoneSelect_match + '/' + $scope.yearSelect_match).success(function(data, status, headers, config) {
						$scope.listLeagueMatch			=	data;
						var isHas = false;
						for( var j in $scope.listLeagueMatch ){
							if( $scope.listLeagueMatch[j].id == $scope.leagueSelect_match ){
								isHas = true;
								break;
							}
						}
						if(!isHas){
							$scope.leagueSelect_match		=	0;
						}
						$scope.getMatch_phase_2(url);
					}).
					error(function(data, status, headers, config) {
						// called asynchronously if an error occurs
						// or server returns response with an error status.
					});
				}else{
					if(isLoadLeague){
						$scope.leagueSelect_match = 0;
						$scope.listLeagueMatch	= [];
					}
					$scope.getMatch_phase_2(url);
				}

			};
			
			$scope.getMatch_phase_2 = function(url){
				if($scope.leagueSelect_match > 0){
					url = url + '&leagueID=' + $scope.leagueSelect_match;
				}
						
				if($scope.yearSelect_match > 0){
					url = url + '&year=' + $scope.yearSelect_match;
				}
						
				$http.post(url).success(function(data, status, headers, config) {
					$scope.listMatch				=	[];
					$interval.cancel(stop);
					
					if(data.length >= $scope.pagesize){
						$scope.isnotnext = false;
					}else{
						$scope.isnotnext = true;
					}
					
					if($scope.start > 0){
						$scope.isnotprevious = false;
					}else{
						$scope.isnotprevious = true;
					}
							
					if(data.length){
						$scope.index_match			=	0;
						stop = $interval(function() {
							for(var i=0; i<5 ; i++){
								if(data[$scope.index_match].AutoMode == 1){
									data[$scope.index_match].isEdit = false;
								}else{
									data[$scope.index_match].isEdit = true;
								}
								$scope.listMatch.push(data[$scope.index_match]);
								$scope.index_match			=	parseInt($scope.index_match) + 1;
								if($scope.index_match >= data.length){
									$interval.cancel(stop);
									break;
								}
							}
						}, 10);
					}
				}).
				error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
				});
			}
			
			$scope.getListSearch = function(){
				var url = 'http://football.kapook.com/api/adminfootball/match/get_list_search';
				$http.post(url).success(function(data, status, headers, config) {
					$scope.listSearch = data;
				}).
				error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
				});
			};
			
			$scope.getListMatch(true,true);
			$scope.getListSearch();

		}]);

	})(window.angular);
	
	$(document).ready(function(){
		$('.MatchDate').datetimepicker({
			lang:'en',
			//timepicker:true,
			format:'Y-m-d H:i',
			scrollInput:false
		});
		$('.service').click(function(e) {
			e.preventDefault();
		});
	});	
</script>