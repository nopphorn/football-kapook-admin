<?php
	$listOdd = array(
		'0.25' => 'เสมอควบครึ่ง',
		'0.5' => 'ครึ่งลูก',
		'0.75' => 'ครึ่งควบลูก',
		'1' => 'หนึ่งลูก',
		'1.25' => 'ลูกควบลูกครึ่ง',
		'1.5' => 'ลูกครึ่ง',
		'1.75' => 'ลูกครึ่งควบสองลูก',
		'2' => 'สองลูก',
		'2.25' => 'สองลูกควบสองลูกครึ่ง',
		'2.5' => 'สองลูกครึ่ง',
		'2.75' => 'สองลูกครึ่งควบสามลูก',
		'3' => 'สามลูก',
		'3.25' => 'สามลูกควบสามลูกครึ่ง',
		'3.5' => 'สามลูกครึ่ง',
		'3.75' => 'สามลูกครึ่งควบสี่ลูก',
		'4' => 'สี่ลูก',
		'4.25' => 'สี่ลูกควบสี่ลูกครึ่ง',
		'4.5' => 'สี่ลูกครึ่ง',
		'4.75' => 'สี่ลูกครึ่งควบห้าลูก',
		'5' => 'ห้าลูก',
		'5.25' => 'ห้าลูกควบห้าลูกครึ่ง',
		'5.5' => 'ห้าลูกครึ่ง',
		'5.75' => 'ห้าลูกครึ่งควบหกลูก',
		'6' => 'หกลูก',
		'6.25' => 'หกลูกควบหกลูกครึ่ง',
		'6.5' => 'หกลูกครึ่ง',
		'6.75' => 'หกลูกครึ่งควบเจ็ดลูก',
		'7' => 'เจ็ดลูก',
		'7.25' => 'เจ็ดลูกควบเจ็ดลูกครึ่ง',
		'7.5' => 'เจ็ดลูกครึ่ง',
		'7.75' => 'เจ็ดลูกครึ่งควบแปดลูก',
		'8' => 'แปดลูก',
		'8.25' => 'แปดลูกควบแปดลูกครึ่ง',
		'8.5' => 'แปดลูกครึ่ง',
		'8.75' => 'แปดลูกครึ่งควบเก้าลูก',
		'9' => 'เก้าลูก',
		'9.25' => 'เก้าลูกควบเก้าลูกครึ่ง',
		'9.5' => 'เก้าลูกครึ่ง',
		'9.75' => 'เก้าลูกครึ่งควบสิบลูก',
	);
?>

<style type="text/css">
	.sortorder:after {
		content: '\25b2';
	}
	.sortorder.reverse:after {
		content: '\25bc';
	}
	.sort_click {
		cursor:pointer
	}
	
	.show-grid [class^=col-] {
		padding-top: 10px;
		padding-bottom: 10px;
		border: 1px solid #ddd;
		border: 1px solid rgba(86,61,124,.2);
	}
	
	.leaguename {
		overflow: hidden;
		word-break: break-all;
		text-overflow: ellipsis;
		white-space: nowrap;
	}
	
	.panel-heading-league {
		padding-bottom: 35px;
		padding-left: 0px;
		padding-right: 0px;
	}

	
</style>

<div class="container" ng-app="CompApp" ng-controller="CompController">
	<div class="panel panel-primary">
		<div class="panel-heading"><h3 class="panel-title">เงื่อนไขในการค้นหา</h3></div>
		<table class="table">
			<tbody>
				<tr><th style="background: #fbeaea;font-size: x-large;"><b><center>โซน/ลีก/ปี</center></b></th></tr>
				<tr><th>
					<div class="row form-group"><center>
						<div class="col-sm-4 col-md-4">
							<label>โซนที่ต้องการค้นหา</label>
								<select class="form-control" ng-model="zoneSelect_match" ng-change="getListMatch(true)">
								<option value="0">ทุกโซน</option>
								<?php foreach( $this->dataZone as $tmpZone ){ ?>
								<option value="<?php echo $tmpZone['id']; ?>"><?php echo $tmpZone['NameEN']; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col-sm-4 col-md-4">
							<label>ลีกที่ต้องการค้นหา</label>
							<select class="form-control" ng-model="leagueSelect_match" ng-change="getListMatch(false)">
								<option value="0">ทุกลีก</option>
								<option ng-repeat="tmpData in listLeagueMatch" value="{{tmpData.id}}">{{tmpData.NameTH}}</option>
							</select>
						</div>
						<div class="col-sm-4 col-md-4">
							<label>ปีที่ต้องการค้นหา</label>
							<select class="form-control" ng-model="yearSelect_match" ng-change="getListMatch(true)">
								<option value="0">ทุกปี</option>
								<option value="2013">2013/2014</option>
								<option value="2014">2014/2015</option>
								<option value="2015">2015/2016</option>
								<option value="2016">2016/2017</option>
								<option value="2017">2017/2018</option>
							</select>
						</div>
					</center></div>
				</th></tr>
				<tr><th style="background: #fbeaea;font-size: x-large;"><b><center>MatchDateTime</center></b></th></tr>
				<tr><th>
					<div class="row form-group">
						<div class="col-sm-3 col-md-3">
							<label class="radio-inline">
								<input type="radio" name="MatchTimeSearch" ng-model="matchTimeSearch" ng-value="1" ng-change="getListMatch(false)"> ไม่กำหนดเวลา
							</label>
						</div>
						<div class="col-sm-3 col-md-3">
							<label class="radio-inline">
								<input type="radio" name="MatchTimeSearch" ng-model="matchTimeSearch" ng-value="2" ng-change="getListMatch(false)">ก่อน...
							</label>
						</div>
						<div class="col-sm-3 col-md-3">
							<label class="radio-inline">
								<input type="radio" name="MatchTimeSearch" ng-model="matchTimeSearch" ng-value="3" ng-change="getListMatch(false)">หลัง...
							</label>
						</div>
						<div class="col-sm-3 col-md-3">
							<label class="radio-inline">
								<input type="radio" name="MatchTimeSearch" ng-model="matchTimeSearch" ng-value="4" ng-change="getListMatch(false)">ระหว่าง...
							</label>
						</div>
						<center>
							<div class="col-sm-6 col-md-6">
								<label>วันที่เริ่มการค้นหา</label>
								<input type="text" class="form-control MatchDate" placeholder="วันที่แข่งขัน" ng-model="dateFromSelect_match" ng-change="getListMatch(false)" ng-disabled="isFromDate">
							</div>
							<div class="col-sm-6 col-md-6">
								<label>วันที่สิ้นสุดการค้นหา</label>
								<input type="text" class="form-control MatchDate" placeholder="วันที่แข่งขัน" ng-model="dateToSelect_match" ng-change="getListMatch(false)" ng-disabled="isToDate">
							</div>
						</center>
					</div>
				</th></tr>
				<tr><th style="background: #fbeaea;font-size: x-large;"><b><center>อื่นๆ</center></b></th></tr>
				<tr><th>
					<div class="row form-group"><center>
						<div class="col-sm-2 col-md-2">
							<label>สถานะ</label>
							<select class="form-control" ng-model="statusSelect_match" ng-change="getListMatch(false)">
								<option value="-1">ทุกสถานะ</option>
								<option value="0">ปิด</option>
								<option value="1">เปิด</option>
							</select>
						</div>
						<div class="col-sm-2 col-md-2">
							<label>MatchStatus</label>
							<select class="form-control" ng-model="MatchStatus" ng-change="getListMatch(false)">
								<option value="">ทั้งหมด</option>
								<option value="Sched">Sched</option>
								<option value="1 HF">1 HF</option>
								<option value="H/T">H/T</option>
								<option value="2 HF">2 HF</option>
								<option value="E/T">E/T</option>
								<option value="Pen">Pen</option>
								<option value="Fin">Fin</option>
								<option value="Int">Int</option>
								<option value="Abd">Abd</option>
								<option value="Post">Post</option>
								<option value="Canc">Canc</option>
							</select>
						</div>
					</center></div>
				</th></tr>
			</tbody>
		</table>
		<div class="panel-body">
			
		</div>
		<div class="panel-body">
			
		</div>
	</div>

	<div ng-repeat="tmpData in listMatch" class="panel {{bgColor[tmpData.Status]}}" >
		<div class="panel-heading panel-heading-league">
			<div class="col-md-8 col-sm-7 col-xs-6 leaguename">
				<b>{{tmpData.XSLeagueCountry}}</b> : {{tmpData.XSLeagueName}} - {{tmpData.XSLeagueDesc}}
			</div>
			<div class="col-md-4 col-sm-5 col-xs-6">
				<a style="float: right;margin-top: -4px;" ng-href="{{tmpData.url}}" type="button" target="_blank" class="btn btn-primary">ไปยังหน้าแมตช์</a>
				<a style="float: right;margin-top: -4px;margin-right: 10px;" ng-href="<?php echo BASE_HREF; ?>api/adminfootball/match/detail/{{tmpData.id}}" type="button" target="_blank" class="btn btn-info">แก้ไขโดยละเอียด</a>
			</div>
		</div>
		<div class="panel-body">
			<div class="row"><div class="col-md-12 col-sm-12 col-xs-12">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td class="col-xs-3" rowspan="3"><center>{{tmpData.Team1Show}}</center></td>
						<td class="col-xs-2" ><input type="number" class="form-control" ng-model="tmpData.Team1FTScore"></td>
						<td class="col-xs-2" ><center>Full-Time</center></td>
						<td class="col-xs-2" ><input type="number" class="form-control" ng-model="tmpData.Team2FTScore"></td>
						<td class="col-xs-3" rowspan="3"><center>{{tmpData.Team2Show}}</center></td>
					</tr>
					<tr>
						<td><input type="number" class="form-control" ng-model="tmpData.Team1ETScore" ng-show="tmpData.isET" ng-disabled="!tmpData.isET"></td>
						<td><center><button class="btn" ng-class="{'btn-success' : tmpData.isET,'btn-danger' : !tmpData.isET}" type="button" id="ETButton" ng-click="toggleET($index)">Extra Time</button></center></td>
						<td><input type="number" class="form-control" ng-model="tmpData.Team2ETScore" ng-show="tmpData.isET" ng-disabled="!tmpData.isET"></td>
					</tr>
					<tr>
						<td><input type="number" class="form-control" ng-model="tmpData.Team1PNScore" ng-show="tmpData.isPN" ng-disabled="!tmpData.isPN"></td>
						<td><center><button class="btn" ng-class="{'btn-success' : tmpData.isPN,'btn-danger' : !tmpData.isPN}" type="button" id="PNButton" ng-click="togglePN($index)">Extra Time</button></center></td>
						<td><input type="number" class="form-control" ng-model="tmpData.Team2PNScore" ng-show="tmpData.isPN" ng-disabled="!tmpData.isPN"></td>
					</tr>
				</tbody>
			</table>
			</div></div>
			<div class="row">
				<div class="col-md-2 col-sm-2 col-xs-6">
					<label>XSMatchID</label>
					<p class="lead">{{tmpData.XSMatchID}}</p>
				</div>
				<div class="col-md-2 col-sm-2 col-xs-6">
					<label>KPMatchID</label>
					<p class="lead">{{tmpData.id}}</p>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<label>MatchDateTime</label>
					<p class="lead">{{tmpData.MatchDateTime}}</p>
				</div>
				<div class="col-md-2 col-sm-2 col-xs-6">
					<label>สถานะ</label>
					<select class="form-control" ng-model="tmpData.Status">
						<option value="0">ปิด</option>
						<option value="1">เปิด</option>
					</select>
				</div>
				<div class="col-md-2 col-sm-2 col-xs-6">
					<label>MatchStatus</label>
					<select class="form-control" ng-model="tmpData.MatchStatus">
						<option value="Sched">Sched</option>
						<option value="1 HF">1 HF</option>
						<option value="H/T">H/T</option>
						<option value="2 HF">2 HF</option>
						<option value="E/T">E/T</option>
						<option value="Pen">Pen</option>
						<option value="Fin">Fin</option>
						<option value="Int">Int</option>
						<option value="Abd">Abd</option>
						<option value="Post">Post</option>
						<option value="Canc">Canc</option>
					</select>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-3 col-sm-3 col-xs-6">
					<label>อัตราต่อรอง</label>
					<select name="Odds[{{tmpData.id}}]" class="form-control" ng-model="tmpData.Odds">
						<option value="-1" >ไม่มีอัตราต่อรอง</option>
						<option value="0" >เสมอ</option>
						<?php
						foreach ($listOdd as $k => $v) {
							echo '<option value="' . (float)$k . '">' . $k . '</option>';
						}?>
					</select>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6">
					<label>ทีมที่ต้องการต่อ</label>
					<select name="TeamOdds[{{tmpData.id}}]" class="form-control" ng-model="tmpData.TeamOdds">
						<option>ไม่เลือก</option>
						<option value="1" >{{tmpData.Team1Show}}</option>
						<option value="2" >{{tmpData.Team2Show}}</option>
					</select>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6">
					<label>ฟันธง</label>
					<select name="TeamSelect[{{tmpData.id}}]" class="form-control" ng-model="tmpData.TeamSelect">
						<option value="0" >ไม่เลือก</option>
						<option value="1" >{{tmpData.Team1Show}}</option>
						<option value="2" >{{tmpData.Team2Show}}</option>
					</select>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6">
					<label>ทรรศนะฟุตบอล</label>
					<input class="form-control" name="Predict[{{tmpData.id}}]" ng-model="tmpData.Predict">
				</div>
			</div>
			
			<div class="row"><div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 15px;">
				<table class="table table-bordered">
					<tr>
						<td class="col-xs-3"><b><u>ช่องการถ่ายทอดสด</u></b></td>
						<td class="col-xs-6">
							<!--<img ng-if="tmpData.TVLiveList.length >= 1" ng-repeat="tmpTV in tmpData.TVLiveList" src="http://202.183.165.189/uploads/tvlogo/{{tmpTV}}.png" alt="{{tmpTV}}" style="padding-right: 10px;">-->
						</td>
						<td class="col-xs-3" style="text-align: right;">
							<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#TVLiveList_{{tmpData.id}}" aria-expanded="false">
								เปิดรายชื่อทีวี เพื่อเลือกช่องถ่ายทอดสด
							</button>
						</td>
					</tr>
				</table>
			</div></div>
			
			<div class="row"><div class="col-md-12 col-sm-12 col-xs-12 collapse" id="TVLiveList_{{tmpData.id}}">
				<table class="table table-bordered">
					<tbody><tr>
						<td colspan="5"><b>TV Analog</b></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="tv3" checklist-model="tmpData.TVLiveList" ><img src="http://202.183.165.189/uploads/tvlogo/tv3.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="tv5" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/tv5.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="tv7" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/tv7.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="tv9" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/tv9.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="tv11" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/tv11.png" height="20"></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="tpbs" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/tpbs.png" height="20"></td>
						<td colspan="4"></td>
					</tr>
					<tr>
						<td colspan="5"><b>TV Digital</b></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="tv5hd" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/tv5hd.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="tv11hd" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/tv11hd.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="tpbshd" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/tpbshd.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="tv3family" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/tv3family.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="mcotfamily" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/mcotfamily.png" height="20"></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="tnn" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/tnn.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="newtv" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/newtv.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="springnews" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/springnews.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="nation" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/nation.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="workpointtv" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/workpointtv.png" height="20"></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="true4u" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/true4u.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="gmmchannel" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/gmmchannel.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="now" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/now.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="ch8" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/ch8.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="tv3sd" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/tv3sd.png" height="20"></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="mono" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/mono.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="mcothd" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/mcothd.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="one" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/one.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="thairath" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/thairath.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="tv3hd" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/tv3hd.png" height="20"></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="amarin" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/amarin.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="pptv" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/pptv.png" height="20"></td>
						<td colspan="3"></td>
					</tr>
					<tr>
						<td colspan="5"><b>True Vision</b></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="tshd1" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/tshd1.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="tshd2" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/tshd2.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="tshd3" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/tshd3.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="tshd4" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/tshd4.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="ts1" checklist-model="tmpData.TVLiveList"><img src="http://202.183.165.189/uploads/tvlogo/ts1.png" height="20"></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="ts2"><img src="http://202.183.165.189/uploads/tvlogo/ts2.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="ts3"><img src="http://202.183.165.189/uploads/tvlogo/ts3.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="ts4"><img src="http://202.183.165.189/uploads/tvlogo/ts4.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="ts5"><img src="http://202.183.165.189/uploads/tvlogo/ts5.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="ts6"><img src="http://202.183.165.189/uploads/tvlogo/ts6.png" height="20"></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="ts7"><img src="http://202.183.165.189/uploads/tvlogo/ts7.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="tstennis"><img src="http://202.183.165.189/uploads/tvlogo/tstennis.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="hayha"><img src="http://202.183.165.189/uploads/tvlogo/hayha.png" height="20">(352)</td>
						<td><input type="checkbox" class="TVList" value="thaithai"><img src="http://202.183.165.189/uploads/tvlogo/thaithai.png" height="20"> (358)</td>
						<td><input type="checkbox" class="TVList" value="true"><img src="http://202.183.165.189/uploads/tvlogo/true.png" height="20"></td>
					</tr>
					<tr>
						<td colspan="5"><b>CTH</b></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="cthstd1"><img src="http://202.183.165.189/uploads/tvlogo/cthstd1.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="cthstd2"><img src="http://202.183.165.189/uploads/tvlogo/cthstd2.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="cthstd3"><img src="http://202.183.165.189/uploads/tvlogo/cthstd3.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="cthstd4"><img src="http://202.183.165.189/uploads/tvlogo/cthstd4.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="cthstd5"><img src="http://202.183.165.189/uploads/tvlogo/cthstd5.png" height="20"></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="cthstd6"><img src="http://202.183.165.189/uploads/tvlogo/cthstd6.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="cthstdx"><img src="http://202.183.165.189/uploads/tvlogo/cthstdx.png" height="20"></td>
						<td colspan="3"></td>
					</tr>
					<tr>
						<td colspan="5"><b>GMM</b></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="gmmclubchannel"><img src="http://202.183.165.189/uploads/tvlogo/gmmclubchannel.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="gmmfootballplus"><img src="http://202.183.165.189/uploads/tvlogo/gmmfootballplus.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="gmmfootballmax"><img src="http://202.183.165.189/uploads/tvlogo/gmmfootballmax.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="gmmfootballeuro"><img src="http://202.183.165.189/uploads/tvlogo/gmmfootballeuro.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="gmmfootballextrahd"><img src="http://202.183.165.189/uploads/tvlogo/gmmfootballextrahd.png" height="20"></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="gmmsportextreme"><img src="http://202.183.165.189/uploads/tvlogo/gmmsportextreme.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="gmmsportplus"><img src="http://202.183.165.189/uploads/tvlogo/gmmsportplus.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="gmmsportextra"><img src="http://202.183.165.189/uploads/tvlogo/gmmsportplus.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="gmmsportone"><img src="http://202.183.165.189/uploads/tvlogo/gmmsportplus.png" height="20"></td>
						<td colspan="1"></td>
					</tr>
					<tr>
						<td colspan="5"><b>Siamsport</b></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="siamsportnew">Siamsport News</td>
						<td><input type="checkbox" class="TVList" value="siamsportfootball"><img src="http://202.183.165.189/uploads/tvlogo/siamsportfootball.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="siamsportlive"><img src="http://202.183.165.189/uploads/tvlogo/siamsportlive.png" height="20"></td><td colspan="2"></td>
					</tr>
					<tr>
						<td colspan="5"><b>RS</b></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="sunchannel"><img src="http://202.183.165.189/uploads/tvlogo/sunchannel.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="worldcupchannel"><img src="http://202.183.165.189/uploads/tvlogo/worldcupchannel.png" height="20"></td>
						<td colspan="3"></td>
					</tr>
					<tr>
						<td colspan="5"><b>Bein Sport</b></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="bein"><img src="http://202.183.165.189/uploads/tvlogo/bein.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="bein1"><img src="http://202.183.165.189/uploads/tvlogo/bein1.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="bein2"><img src="http://202.183.165.189/uploads/tvlogo/bein2.png" height="20"></td>
						<td colspan="2"></td>
					</tr>
					<tr>
						<td colspan="5"><b>Fox Sport</b></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="foxsport"><img src="http://202.183.165.189/uploads/tvlogo/foxsport.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="foxsport2"><img src="http://202.183.165.189/uploads/tvlogo/foxsport2.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="foxsport3"><img src="http://202.183.165.189/uploads/tvlogo/foxsport3.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="foxsportplay"><img src="http://202.183.165.189/uploads/tvlogo/foxsportplay.png" height="20"></td>
						<td colspan="1"></td>
					</tr>
					<tr>
						<td colspan="5"><b>อื่นๆ</b></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="starsport"><img src="http://202.183.165.189/uploads/tvlogo/starsport.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="astrosupersport1"><img src="http://202.183.165.189/uploads/tvlogo/astrosupersport1.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="astrosupersport2"><img src="http://202.183.165.189/uploads/tvlogo/astrosupersport2.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="astrosupersporthd3"><img src="http://202.183.165.189/uploads/tvlogo/astrosupersporthd3.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="asn"><img src="http://202.183.165.189/uploads/tvlogo/asn.png" height="20"></td>
					</tr>
					<tr>
						<td><input type="checkbox" class="TVList" value="asn2"><img src="http://202.183.165.189/uploads/tvlogo/asn2.png" height="20"></td>
						<td><input type="checkbox" class="TVList" value="bugaboo"><img src="http://202.183.165.189/uploads/tvlogo/bugaboo.png" height="20"></td>
						<td colspan="3"></td>
					</tr></tbody>
				</table>
			</div></div>
			
			
			
		</div>
		<div class="panel-footer">
			<div class="row"><div class="col-md-12 col-sm-12 col-xs-12">
				<button type="button" style="width: inherit;" class="btn btn-success btn-lg" ng-click="submit_match($index)">บันทึกด่วน</button>
			</div></div>
		</div>
		
	</div>

<script>
	
	function getCookieByName(str){
		var ca 			= 	document.cookie.split(';');
		var dataCookie	=	"";

		var name = str + "=";
		for(var i=0; i<ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0)==' ')
				c = c.substring(1);
			if (c.indexOf(name) != -1){
				dataCookie = c.substring(name.length,c.length);
				break;
			}
		}
		
		return dataCookie;
	}
	
	(function(angular) {
		var myApp = angular.module('CompApp', ["checklist-model"]);

		myApp.controller('CompController', ['$scope','$http', function($scope,$http) {
			
			tmpData = getCookieByName('matchTimeSearch');
			if(getCookieByName('matchTimeSearch')!=""){
				$scope.matchTimeSearch = parseInt(tmpData);
			}else{
				$scope.matchTimeSearch = 4;
			}
			
			tmpData = getCookieByName('zoneSelect_match');
			if(getCookieByName('zoneSelect_match')!=""){
				$scope.zoneSelect_match = parseInt(tmpData);
			}else{
				$scope.zoneSelect_match = 0;
			}
			
			tmpData = getCookieByName('leagueSelect_match');
			if(getCookieByName('leagueSelect_match')!=""){
				$scope.leagueSelect_match = parseInt(tmpData);
			}else{
				$scope.leagueSelect_match = 0;
			}
			
			tmpData = getCookieByName('yearSelect_match');
			if(getCookieByName('yearSelect_match')!=""){
				$scope.yearSelect_match = parseInt(tmpData);
			}else{
				$scope.yearSelect_match	= 0;
			}
			
			tmpData = getCookieByName('dateFromSelect_match');
			if(getCookieByName('dateFromSelect_match')!=""){
				$scope.dateFromSelect_match = tmpData;
			}else{
				$scope.dateFromSelect_match = "<?php if(date('G')>6) { echo date('Y-m-d'); }else{ echo date('Y-m-d',strtotime('-1 days')); } ?>";
			}
			
			tmpData = getCookieByName('dateToSelect_match');
			if(getCookieByName('dateToSelect_match')!=""){
				$scope.dateToSelect_match = tmpData;
			}else{
				$scope.dateToSelect_match = "<?php if(date('G')>6) { echo date('Y-m-d'); }else{ echo date('Y-m-d',strtotime('-1 days')); } ?>";
			}
			
			tmpData = getCookieByName('MatchStatus');
			if(getCookieByName('MatchStatus')!=""){
				$scope.MatchStatus = tmpData;
			}else{
				$scope.MatchStatus = "";
			}

			tmpData = getCookieByName('statusSelect_match');
			if(getCookieByName('statusSelect_match')!=""){
				$scope.statusSelect_match = parseInt(tmpData);
			}else{
				$scope.statusSelect_match =	-1;
			}
			
			$scope.bgColor		=	[];
			$scope.bgColor[0]	=	"panel-danger";
			$scope.bgColor[1]	=	"panel-success";

			$scope.isFromDate = false;
			$scope.isToDate = false;
			
			$scope.listMatch = [];
			
			$scope.togglePN	= function(index){
				$scope.listMatch[index].isPN	=	!$scope.listMatch[index].isPN;
			}
			
			$scope.toggleET	= function(index){
				$scope.listMatch[index].isET	=	!$scope.listMatch[index].isET;
			}
			
			$scope.submit_match	= function(index){
				if(!$scope.listMatch[index].isET){
					delete $scope.listMatch[index].Team1ETScore;
					delete $scope.listMatch[index].Team2ETScore;
				}
				if(!$scope.listMatch[index].isPN){
					delete $scope.listMatch[index].Team1PNScore;
					delete $scope.listMatch[index].Team2PNScore;
				}
				$http({
					method: 'POST',
					url: "<?php echo BASE_HREF; ?>api/adminfootball/match/update/" + $scope.listMatch[index].id,
					data: $.param($scope.listMatch[index]),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).
				success(function(data, status, headers, config) {
					if(data.is_success == 1){
						show_stack_bar_top('success','สำเร็จ','อัพเดทการแข่งขันเสร็จแล้ว');
					}else{
						show_stack_bar_top('success','ผิดพลาด','ไม่สามารถอัพเดทการแข่งขันได้');
					}
					//window.location.href = "<?php echo BASE_HREF; ?>api/adminfootball/match/index_2";
				}).
				error(function(data, status, headers, config) {
					show_stack_bar_top('success','ผิดพลาด','ไม่สามารถอัพเดทการแข่งขันได้');
				});
			}

			$scope.getListMatch = function(isZone){
				
				document.cookie = "matchTimeSearch=" + $scope.matchTimeSearch + "; path=/api/adminfootball/";
				document.cookie = "zoneSelect_match=" + $scope.zoneSelect_match + "; path=/api/adminfootball/";
				document.cookie = "leagueSelect_match=" + $scope.leagueSelect_match + "; path=/api/adminfootball/";
				document.cookie = "yearSelect_match=" + $scope.yearSelect_match + "; path=/api/adminfootball/";
				document.cookie = "dateFromSelect_match=" + $scope.dateFromSelect_match + "; path=/api/adminfootball/";
				document.cookie = "dateToSelect_match=" + $scope.dateToSelect_match + "; path=/api/adminfootball/";
				document.cookie = "MatchStatus=" + $scope.MatchStatus + "; path=/api/adminfootball/";
				document.cookie = "statusSelect_match=" + $scope.statusSelect_match + "; path=/api/adminfootball/";
				
				var url = 'http://football.kapook.com/api/adminfootball/livescore/getjson?size=100';
				
				if($scope.MatchStatus!=""){
					url = url + '&MatchStatus=' + $scope.MatchStatus;
				}
				
				if($scope.matchTimeSearch==4){
					url = url + '&dateFrom=' + $scope.dateFromSelect_match + '&dateTo=' + $scope.dateToSelect_match;
					$scope.isFromDate = false
					$scope.isToDate = false
				}else if($scope.matchTimeSearch==3){
					url = url + '&dateFrom=' + $scope.dateFromSelect_match;
					$scope.isFromDate = false
					$scope.isToDate = true
				}else if($scope.matchTimeSearch==2){
					url = url + '&dateTo=' + $scope.dateToSelect_match;
					$scope.isFromDate = true
					$scope.isToDate = false
				}else{
					$scope.isFromDate = true
					$scope.isToDate = true
				}
				
				
				if($scope.statusSelect_match!=-1){
					url = url + '&status=' + $scope.statusSelect_match;
				}
				
				if($scope.zoneSelect_match > 0){
					url = url + '&zoneID=' + $scope.zoneSelect_match;
					if(isZone){
						$scope.leagueSelect_match = 0;
						$http.post('http://football.kapook.com/api/adminfootball/league/getjson/' + $scope.zoneSelect_match + '/' + $scope.yearSelect_match).success(function(data, status, headers, config) {
							$scope.listLeagueMatch			=	data;
							var tmp_leagueSelect_match		=	$scope.leagueSelect_match;
							$scope.leagueSelect_match		=	0;
							for( var i in $scope.listLeagueMatch ){
								if( $scope.listLeagueMatch[i].id == tmp_leagueSelect_match ){
									$scope.leagueSelect_match	=	tmp_leagueSelect_match;
									break;
								}
							}
						}).
						error(function(data, status, headers, config) {
							// called asynchronously if an error occurs
							// or server returns response with an error status.
						});
					}
				}else{
					$scope.leagueSelect_match = 0;
					$scope.listLeagueMatch = [];
				}
				
				if($scope.leagueSelect_match > 0){
					url = url + '&leagueID=' + $scope.leagueSelect_match;
				}
				
				if($scope.yearSelect_match > 0){
					url = url + '&year=' + $scope.yearSelect_match;
				}
				
				$http.post(url).success(function(data, status, headers, config) {
					$scope.listMatch			=	data;
				}).
				error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
				});
			};
			
			$scope.getListMatch(true);
			
		}]);

	})(window.angular);
	
	$(document).ready(function(){
		$('.MatchDate').datetimepicker({
			lang:'en',
			timepicker:false,
			format:'Y-m-d',
			scrollInput:false
		});
	});
			
</script>