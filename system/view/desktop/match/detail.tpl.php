<?php
	$list_TV = array(
	
		0 => array(
			'nameType' 	=> 	'TV Analog',
			'listTV'	=>	array(
				'tv3' 	=> 	'<img src="http://202.183.165.189/uploads/tvlogo/tv3.png" height="20">',
				'tv5'	=>	'<img src="http://202.183.165.189/uploads/tvlogo/tv5.png" height="20">',
				'tv7'	=>	'<img src="http://202.183.165.189/uploads/tvlogo/tv7.png" height="20">',
				'tv9'	=>	'<img src="http://202.183.165.189/uploads/tvlogo/tv9.png" height="20">',
				'tv11'	=>	'<img src="http://202.183.165.189/uploads/tvlogo/tv11.png" height="20">',
				'tpbs'	=>	'<img src="http://202.183.165.189/uploads/tvlogo/tpbs.png" height="20">'
			)
		),

		1 => array(
			'nameType' 	=> 	'TV Digital',
			'listTV'	=>	array(
				'tv5hd'			=> '<img src="http://202.183.165.189/uploads/tvlogo/tv5hd.png" height="20">',
				'tv11hd'		=> '<img src="http://202.183.165.189/uploads/tvlogo/tv11hd.png" height="20">',
				'tpbshd'		=> '<img src="http://202.183.165.189/uploads/tvlogo/tpbshd.png" height="20">',
				'tv3family'		=> '<img src="http://202.183.165.189/uploads/tvlogo/tv3family.png" height="20">',
				'mcotfamily'	=> '<img src="http://202.183.165.189/uploads/tvlogo/mcotfamily.png" height="20">',
				'tnn'			=> '<img src="http://202.183.165.189/uploads/tvlogo/tnn.png" height="20">',
				'newtv'			=> '<img src="http://202.183.165.189/uploads/tvlogo/newtv.png" height="20">',
				'springnews'	=> '<img src="http://202.183.165.189/uploads/tvlogo/springnews.png" height="20">',
				'nation'		=> '<img src="http://202.183.165.189/uploads/tvlogo/nation.png" height="20">',
				'workpointtv' 	=> '<img src="http://202.183.165.189/uploads/tvlogo/workpointtv.png" height="20">',
				'true4u' 		=> '<img src="http://202.183.165.189/uploads/tvlogo/true4u.png" height="20">',
				'gmmchannel' 	=> '<img src="http://202.183.165.189/uploads/tvlogo/gmmchannel.png" height="20">',
				'now' 			=> '<img src="http://202.183.165.189/uploads/tvlogo/now.png" height="20">',
				'ch8' 			=> '<img src="http://202.183.165.189/uploads/tvlogo/ch8.png" height="20">',
				'tv3sd' 		=> '<img src="http://202.183.165.189/uploads/tvlogo/tv3sd.png" height="20">',
				'mono' 			=> '<img src="http://202.183.165.189/uploads/tvlogo/mono.png" height="20">',
				'mcothd' 		=> '<img src="http://202.183.165.189/uploads/tvlogo/mcothd.png" height="20">',
				'one' 			=> '<img src="http://202.183.165.189/uploads/tvlogo/one.png" height="20">',
				'thairath' 		=> '<img src="http://202.183.165.189/uploads/tvlogo/thairath.png" height="20">',
				'tv3hd' 		=> '<img src="http://202.183.165.189/uploads/tvlogo/tv3hd.png" height="20">',
				'amarin' 		=> '<img src="http://202.183.165.189/uploads/tvlogo/amarin.png" height="20">',
				'pptv' 			=> '<img src="http://202.183.165.189/uploads/tvlogo/pptv.png" height="20">'
			)
		),
		
		2 => array(
			'nameType' 		=> 	'True Vision',						
			'listTV'		=>	array(
				'tshd1'		=>	'<img src="http://202.183.165.189/uploads/tvlogo/tshd1.png" height="20">',
				'tshd2' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/tshd2.png" height="20">',
				'tshd3' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/tshd3.png" height="20">',
				'tshd4' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/tshd4.png" height="20">',
				'ts1' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/ts1.png" height="20">',
				'ts2' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/ts2.png" height="20">',
				'ts3' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/ts3.png" height="20">',
				'ts4' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/ts4.png" height="20">',
				'ts5' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/ts5.png" height="20">',
				'ts6' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/ts6.png" height="20">',
				'ts7' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/ts7.png" height="20">',
				'tstennis'	=>	'<img src="http://202.183.165.189/uploads/tvlogo/tstennis.png" height="20">',
				'hayha'		=>	'<img src="http://202.183.165.189/uploads/tvlogo/hayha.png" height="20">(352)',
				'thaithai'	=>	'<img src="http://202.183.165.189/uploads/tvlogo/thaithai.png" height="20"> (358)',
				'true' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/true.png" height="20">'
			)
		),
		
		3 => array(
			'nameType' 	=> 	'CTH',						
			'listTV'	=>	array(
				'cthstd1'	=>	'<img src="http://202.183.165.189/uploads/tvlogo/cthstd1.png" height="20">',
				'cthstd2' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/cthstd2.png" height="20">',
				'cthstd3' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/cthstd3.png" height="20">',
				'cthstd4' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/cthstd4.png" height="20">',
				'cthstd5' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/cthstd5.png" height="20">',
				'cthstd6' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/cthstd6.png" height="20">',
				'cthstdx'	=>	'<img src="http://202.183.165.189/uploads/tvlogo/cthstdx.png" height="20">'
			)
		),
		
		4 => array(
			'nameType' 	=> 	'GMM',						
			'listTV'	=>	array(
				'gmmclubchannel' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/gmmclubchannel.png" height="20">',
				'gmmfootballplus' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/gmmfootballplus.png" height="20">',
				'gmmfootballmax' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/gmmfootballmax.png" height="20">',
				'gmmfootballeuro' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/gmmfootballeuro.png" height="20">',
				'gmmfootballextrahd'	=>	'<img src="http://202.183.165.189/uploads/tvlogo/gmmfootballextrahd.png" height="20">',
				'gmmsportextreme'		=>	'<img src="http://202.183.165.189/uploads/tvlogo/gmmsportextreme.png" height="20">',
				'gmmsportplus'			=>	'<img src="http://202.183.165.189/uploads/tvlogo/gmmsportplus.png" height="20">',
				'gmmsportextra'			=>	'<img src="http://202.183.165.189/uploads/tvlogo/gmmsportplus.png" height="20">',
				'gmmsportone'			=>	'<img src="http://202.183.165.189/uploads/tvlogo/gmmsportplus.png" height="20">'
			)
		),
		
		5 => array(
			'nameType' 	=> 	'Siamsport',						
			'listTV'	=>	array(
				'siamsportnew'		=>	'Siamsport News',
				'siamsportfootball' =>	'<img src="http://202.183.165.189/uploads/tvlogo/siamsportfootball.png" height="20">',
				'siamsportlive' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/siamsportlive.png" height="20">'
			)
		),
		
		6 => array(
			'nameType' 	=> 	'RS',						
			'listTV'	=>	array(
				'sunchannel'		=>	'<img src="http://202.183.165.189/uploads/tvlogo/sunchannel.png" height="20">',
				'worldcupchannel' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/worldcupchannel.png" height="20">'
			)
		),
		
		7 => array(
			'nameType' 	=> 	'Bein Sport',						
			'listTV'	=>	array(
				'bein'		=>	'<img src="http://202.183.165.189/uploads/tvlogo/bein.png" height="20">',
				'bein1' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/bein1.png" height="20">',
				'bein2' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/bein2.png" height="20">'
			)
		),
		
		8 => array(
			'nameType' 	=> 	'Fox Sport',						
			'listTV'	=>	array(
				'foxsport' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/foxsport.png" height="20">',
				'foxsport2' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/foxsport2.png" height="20">',
				'foxsport3' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/foxsport3.png" height="20">',
				'foxsportplay' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/foxsportplay.png" height="20">'
			)
		),
		
		9 => array(
			'nameType' 	=> 	'อื่นๆ',						
			'listTV'	=>	array(
				'starsport' 			=>	'<img src="http://202.183.165.189/uploads/tvlogo/starsport.png" height="20">',
				'astrosupersport1' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/astrosupersport1.png" height="20">',
				'astrosupersport2' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/astrosupersport2.png" height="20">',
				'astrosupersporthd3' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/astrosupersporthd3.png" height="20">',
				'asn' 					=>	'<img src="http://202.183.165.189/uploads/tvlogo/asn.png" height="20">',
				'asn2' 					=>	'<img src="http://202.183.165.189/uploads/tvlogo/asn2.png" height="20">',
				'bugaboo' 				=>	'<img src="http://202.183.165.189/uploads/tvlogo/bugaboo.png" height="20">'
			)
		)
	);
	
	$listOdd = array(
		'0.25' => 'เสมอควบครึ่ง',
		'0.5' => 'ครึ่งลูก',
		'0.75' => 'ครึ่งควบลูก',
		'1' => 'หนึ่งลูก',
		'1.25' => 'ลูกควบลูกครึ่ง',
		'1.5' => 'ลูกครึ่ง',
		'1.75' => 'ลูกครึ่งควบสองลูก',
		'2' => 'สองลูก',
		'2.25' => 'สองลูกควบสองลูกครึ่ง',
		'2.5' => 'สองลูกครึ่ง',
		'2.75' => 'สองลูกครึ่งควบสามลูก',
		'3' => 'สามลูก',
		'3.25' => 'สามลูกควบสามลูกครึ่ง',
		'3.5' => 'สามลูกครึ่ง',
		'3.75' => 'สามลูกครึ่งควบสี่ลูก',
		'4' => 'สี่ลูก',
		'4.25' => 'สี่ลูกควบสี่ลูกครึ่ง',
		'4.5' => 'สี่ลูกครึ่ง',
		'4.75' => 'สี่ลูกครึ่งควบห้าลูก',
		'5' => 'ห้าลูก',
		'5.25' => 'ห้าลูกควบห้าลูกครึ่ง',
		'5.5' => 'ห้าลูกครึ่ง',
		'5.75' => 'ห้าลูกครึ่งควบหกลูก',
		'6' => 'หกลูก',
		'6.25' => 'หกลูกควบหกลูกครึ่ง',
		'6.5' => 'หกลูกครึ่ง',
		'6.75' => 'หกลูกครึ่งควบเจ็ดลูก',
		'7' => 'เจ็ดลูก',
		'7.25' => 'เจ็ดลูกควบเจ็ดลูกครึ่ง',
		'7.5' => 'เจ็ดลูกครึ่ง',
		'7.75' => 'เจ็ดลูกครึ่งควบแปดลูก',
		'8' => 'แปดลูก',
		'8.25' => 'แปดลูกควบแปดลูกครึ่ง',
		'8.5' => 'แปดลูกครึ่ง',
		'8.75' => 'แปดลูกครึ่งควบเก้าลูก',
		'9' => 'เก้าลูก',
		'9.25' => 'เก้าลูกควบเก้าลูกครึ่ง',
		'9.5' => 'เก้าลูกครึ่ง',
		'9.75' => 'เก้าลูกครึ่งควบสิบลูก',
	);
	
	$dateNoTime		=	substr($this->dataMatch['MatchDateTime'],0,4) . substr($this->dataMatch['MatchDateTime'],5,2) . substr($this->dataMatch['MatchDateTime'],8,2);
?>

<div class="container">
	<form action="<?php echo BASE_HREF; ?>api/adminfootball/match/update_detail/<?php echo $this->dataMatch['id']; ?>" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="id" value="<?php echo $this->dataMatch['id']; ?>">
		<input type="hidden" id="Team1KPID" name="Team1KPID" value="<?php echo $this->dataMatch['Team1KPID']; ?>">
		<input type="hidden" id="Team2KPID" name="Team2KPID" value="<?php echo $this->dataMatch['Team2KPID']; ?>">
		<input type="hidden" id="Team1" name="Team1" value="<?php echo $this->dataMatch['Team1']; ?>">
		<input type="hidden" id="Team2" name="Team2" value="<?php echo $this->dataMatch['Team2']; ?>">
		<input type="hidden" name="matchdate" id="matchdate" value="<?php echo $dateNoTime; ?>">
		<div class="row form-group"><center>
			<div class="col-sm-6 col-md-6"><button type="submit" class="btn btn-primary btn-lg btn-block">บันทึกการแข่งขัน</button></div>
			<div class="col-sm-6 col-md-6"><a href="<?php echo BASE_HREF; ?>api/adminfootball/match" class="btn btn-default btn-lg btn-block">กลับไปหน้าตารางการแข่งขัน</a></div>
		</center></div>
		<div class="row form-group">
			<div class=" col-xs-6">
				<div class="panel panel-primary">
					<div class="panel-heading"><h3 class="panel-title">ชื่อการแข่งขัน(เต็ม)</h3></div>
					<div class="panel-body">
						<img src="http://football.kapook.com/assets/img/flags/zone/<?php echo $this->dataMatch['KPZoneID']; ?>.png" style="width: 24px;"> <?php echo $this->dataMatch['XSLeagueName']; ?><br>
					</div>
				</div>
			</div>
			<div class=" col-xs-6">
				<div class="panel panel-primary">
					<div class="panel-heading"><h3 class="panel-title">ชื่อการแข่งขัน(ย่อ)</h3></div>
					<div class="panel-body">
						<?php echo $this->dataMatch['XSLeagueDesc']; ?>
					</div>
				</div>
			</div>
		</div>
		<?php if($this->dataMatch['AutoMode']==-1){ ?>
		<div class="alert alert-warning">
			<strong>คำเตือน!</strong><br>
			- หากการแข่งขันจบลง หรือยุติการอัพเดทข้อมูลแล้ว กรุณาปรับการควบคุม Livescore เป็นแบบ Stop-Update เพื่อลดการทำงานของ DB ในระบบ<br>
			- การบันทึกการแข่งขันทั้งหมด จะไม่บันทึก <b>"นาทีในการแข่งขัน(Minute for Manual Livescore)"</b>
			
		</div>
		<?php } ?>
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th class="col-xs-6" colspan="3"><div style="font-size: x-large;float: left;">Team1</div><button class="btn btn-default" type="button" style="float: right;" onclick="window.open('<?php echo BASE_HREF; ?>api/adminfootball/comp/get_listTeamForMatch/1','','height=400,width=400');" >ค้นหาทีม <span class="glyphicon glyphicon-search"></span></button></th>
					<th class="col-xs-6" colspan="3"><div style="font-size: x-large;float: right;">Team2</div><button class="btn btn-default" type="button" style="float: left;" onclick="window.open('<?php echo BASE_HREF; ?>api/adminfootball/comp/get_listTeamForMatch/2','','height=400,width=400');" >ค้นหาทีม <span class="glyphicon glyphicon-search"></span></button></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="col-xs-3" rowspan="3"><center><img id="Team1Logo" style="width: 125px;" src="<?php echo $this->dataMatch['Team1Logo']; ?>"></center></td>
					<?php if($this->dataMatch['AutoMode']!=1) { ?>
					<td class="col-xs-2" ><input type="number" class="form-control" name="Team1FTScore" value="<?php echo $this->dataMatch['Team1FTScore']; ?>"></td>
					<td class="col-xs-2" colspan="2"><center>Full-Time</center></td>
					<td class="col-xs-2" ><input type="number" class="form-control" name="Team2FTScore" value="<?php echo $this->dataMatch['Team2FTScore']; ?>"></td>
					<?php }else{ ?>
					<td class="col-xs-2" ><input type="number" class="form-control" value="<?php echo $this->dataMatch['Team1FTScore']; ?>" disabled></td>
					<td class="col-xs-2" colspan="2"><center>Full-Time</center></td>
					<td class="col-xs-2" ><input type="number" class="form-control" value="<?php echo $this->dataMatch['Team2FTScore']; ?>" disabled></td>
					<?php } ?>
					<td class="col-xs-3" rowspan="3"><center><img id="Team2Logo" style="width: 125px;" src="<?php echo $this->dataMatch['Team2Logo']; ?>"></center></td>
				</tr>
				<tr>
					<?php if(($this->dataMatch['isET'])&&($this->dataMatch['AutoMode']!=1)){ ?>
						<td><input type="number" class="form-control ETinput" name="Team1ETScore" value="<?php echo $this->dataMatch['Team1ETScore']; ?>"></td>
						<td colspan="2"><center><button class="btn btn-success" type="button" id="ETButton">Extra Time</button></center></td>
						<td><input type="number" class="form-control ETinput" name="Team2ETScore" value="<?php echo $this->dataMatch['Team2ETScore']; ?>"></td>
					<?php }else if($this->dataMatch['AutoMode']!=1){ ?>
						<td><input type="number" class="form-control ETinput" name="Team1ETScore" disabled ></td>
						<td colspan="2"><center><button class="btn btn-danger" type="button" id="ETButton">Extra Time</button></center></td>
						<td><input type="number" class="form-control ETinput" name="Team2ETScore" disabled ></td>
					<?php }else if($this->dataMatch['isET']){ ?>
						<td><input type="number" class="form-control" value="<?php echo $this->dataMatch['Team1ETScore']; ?>" disabled ></td>
						<td colspan="2"><center><button class="btn btn-success" type="button" disabled>Extra Time</button></center></td>
						<td><input type="number" class="form-control" value="<?php echo $this->dataMatch['Team2ETScore']; ?>" disabled ></td>
					<?php }else{ ?>
						<td><input type="number" class="form-control" disabled ></td>
						<td colspan="2"><center><button class="btn btn-danger" type="button" disabled>Extra Time</button></center></td>
						<td><input type="number" class="form-control" disabled ></td>
					<?php } ?>
				</tr>
				<tr>
					<?php if(($this->dataMatch['isPN'])&&($this->dataMatch['AutoMode']!=1)){ ?>
						<td><input type="number" class="form-control PNinput" name="Team1PNScore" value="<?php echo $this->dataMatch['Team1PNScore']; ?>"></td>
						<td colspan="2"><center><button class="btn btn-success" type="button" id="PNButton">จุดโทษ</button></center></td>
						<td><input type="number" class="form-control PNinput" name="Team2PNScore" value="<?php echo $this->dataMatch['Team2PNScore']; ?>"></td>
					<?php }else if($this->dataMatch['AutoMode']!=1){ ?>
						<td><input type="number" class="form-control PNinput" name="Team1PNScore" disabled ></td>
						<td colspan="2"><center><button class="btn btn-danger" type="button" id="PNButton">จุดโทษ</button></center></td>
						<td><input type="number" class="form-control PNinput" name="Team2PNScore" disabled ></td>
					<?php }else if($this->dataMatch['isPN']){ ?>
						<td><input type="number" class="form-control" disabled value="<?php echo $this->dataMatch['Team1PNScore']; ?>"></td>
						<td colspan="2"><center><button class="btn btn-success disabled" type="button">จุดโทษ</button></center></td>
						<td><input type="number" class="form-control" disabled value="<?php echo $this->dataMatch['Team2PNScore']; ?>"></td>
					<?php }else{ ?>
						<td><input type="number" class="form-control" disabled ></td>
						<td colspan="2"><center><button class="btn btn-danger disabled" type="button">จุดโทษ</button></center></td>
						<td><input type="number" class="form-control" disabled ></td>
					<?php } ?>
				</tr>
				<tr>
					<td><center><div id="Team1Name"><?php echo $this->dataMatch['Team1Show']; ?></div></center></td>
					<td colspan="4"><center>TEAM NAME</center></td>
					<td><center><div id="Team2Name"><?php echo $this->dataMatch['Team2Show']; ?></div></center></td>
				</tr>
			</tbody>
		</table>
		<div class="panel panel-primary">
			<div class="panel-heading"><h3 class="panel-title">ระบบควบคุม Livescore</h3></div>
			<div class="panel-body">
				<div class="col-sm-6 col-md-4">
					<a href="<?php echo BASE_HREF; ?>api/adminfootball/match/update_mini/<?php echo $this->dataMatch['id']; ?>?AutoMode=1" class="btn <?php if($this->dataMatch['AutoMode']==1) { echo 'btn-success'; }else{ echo 'btn-default'; } ?> btn-block" <?php if($this->dataMatch['AutoMode']==1) { echo 'disabled'; } ?> >Auto-Update</a>
				</div>
				<div class="col-sm-6 col-md-4">
					<a href="<?php echo BASE_HREF; ?>api/adminfootball/match/update_mini/<?php echo $this->dataMatch['id']; ?>?AutoMode=0" class="btn <?php if($this->dataMatch['AutoMode']==0) { echo 'btn-danger'; }else{ echo 'btn-default'; } ?> btn-block" <?php if($this->dataMatch['AutoMode']==0) { echo 'disabled'; } ?> >Stop-Update</a>
				</div>
				<div class="col-sm-6 col-md-4">
					<a href="<?php echo BASE_HREF; ?>api/adminfootball/match/update_mini/<?php echo $this->dataMatch['id']; ?>?AutoMode=-1" class="btn <?php if($this->dataMatch['AutoMode']==-1) { echo 'btn-success'; }else{ echo 'btn-default'; } ?> btn-block" <?php if($this->dataMatch['AutoMode']==-1) { echo 'disabled'; } ?> >Manual-Update</a>
				</div>
			</div>
		</div>
		<?php if($this->dataMatch['AutoMode']==-1){ ?>
		<div class="form-group row">
			<div class="col-xs-10">
				<div class="panel panel-primary ">
					<div class="panel-heading"><h3 class="panel-title">MatchStatus for Manual Livescore</h3></div>
					<div class="panel-body">
						<div class="col-sm-1 col-md-2">
							<a href="<?php echo BASE_HREF; ?>api/adminfootball/match/update_mini/<?php echo $this->dataMatch['id']; ?>?MatchStatusType=0" class="btn <?php if($this->dataMatch['MatchStatus']=="Sched") { echo 'btn-success'; }else{ echo 'btn-default'; } ?> btn-block" <?php if($this->dataMatch['MatchStatus']=="Sched") { echo 'disabled'; } ?> >Sched</a>
						</div>
						<div class="col-sm-1 col-md-2">
							<a href="<?php echo BASE_HREF; ?>api/adminfootball/match/update_mini/<?php echo $this->dataMatch['id']; ?>?MatchStatusType=1" class="btn <?php if($this->dataMatch['MatchStatus']=="1 HF") { echo 'btn-success'; }else{ echo 'btn-default'; } ?> btn-block" <?php if($this->dataMatch['MatchStatus']=="1 HF") { echo 'disabled'; } ?> >1 HF</a>
						</div>
						<div class="col-sm-1 col-md-2">	
							<a href="<?php echo BASE_HREF; ?>api/adminfootball/match/update_mini/<?php echo $this->dataMatch['id']; ?>?MatchStatusType=2" class="btn <?php if($this->dataMatch['MatchStatus']=="H/T") { echo 'btn-success'; }else{ echo 'btn-default'; } ?> btn-block" <?php if($this->dataMatch['MatchStatus']=="H/T") { echo 'disabled'; } ?> >H/T</a>
						</div>
						<div class="col-sm-1 col-md-2">	
							<a href="<?php echo BASE_HREF; ?>api/adminfootball/match/update_mini/<?php echo $this->dataMatch['id']; ?>?MatchStatusType=3" class="btn <?php if($this->dataMatch['MatchStatus']=="2 HF") { echo 'btn-success'; }else{ echo 'btn-default'; } ?> btn-block" <?php if($this->dataMatch['MatchStatus']=="2 HF") { echo 'disabled'; } ?> >2 HF</a>
						</div>
						<div class="col-sm-1 col-md-2">	
							<a href="<?php echo BASE_HREF; ?>api/adminfootball/match/update_mini/<?php echo $this->dataMatch['id']; ?>?MatchStatusType=4" class="btn <?php if($this->dataMatch['MatchStatus']=="E/T") { echo 'btn-success'; }else{ echo 'btn-default'; } ?> btn-block" <?php if($this->dataMatch['MatchStatus']=="E/T") { echo 'disabled'; } ?> >E/T</a>
						</div>
						<div class="col-sm-1 col-md-2">	
							<a href="<?php echo BASE_HREF; ?>api/adminfootball/match/update_mini/<?php echo $this->dataMatch['id']; ?>?MatchStatusType=5" class="btn <?php if($this->dataMatch['MatchStatus']=="Pen") { echo 'btn-success'; }else{ echo 'btn-default'; } ?> btn-block" <?php if($this->dataMatch['MatchStatus']=="Pen") { echo 'disabled'; } ?> >Pen</a>
						</div>
						<div class="col-sm-1 col-md-2">	
							<a href="<?php echo BASE_HREF; ?>api/adminfootball/match/update_mini/<?php echo $this->dataMatch['id']; ?>?MatchStatusType=6" class="btn <?php if($this->dataMatch['MatchStatus']=="Fin") { echo 'btn-success'; }else{ echo 'btn-default'; } ?> btn-block" <?php if($this->dataMatch['MatchStatus']=="Fin") { echo 'disabled'; } ?> >Fin</a>
						</div>
						<div class="col-sm-1 col-md-2">	
							<a href="<?php echo BASE_HREF; ?>api/adminfootball/match/update_mini/<?php echo $this->dataMatch['id']; ?>?MatchStatusType=7" class="btn <?php if($this->dataMatch['MatchStatus']=="Int") { echo 'btn-success'; }else{ echo 'btn-default'; } ?> btn-block" <?php if($this->dataMatch['MatchStatus']=="Int") { echo 'disabled'; } ?> >Int</a>
						</div>
						<div class="col-sm-1 col-md-2">	
							<a href="<?php echo BASE_HREF; ?>api/adminfootball/match/update_mini/<?php echo $this->dataMatch['id']; ?>?MatchStatusType=8" class="btn <?php if($this->dataMatch['MatchStatus']=="Abd") { echo 'btn-success'; }else{ echo 'btn-default'; } ?> btn-block" <?php if($this->dataMatch['MatchStatus']=="Abd") { echo 'disabled'; } ?> >Abd</a>
						</div>
						<div class="col-sm-1 col-md-2">	
							<a href="<?php echo BASE_HREF; ?>api/adminfootball/match/update_mini/<?php echo $this->dataMatch['id']; ?>?MatchStatusType=9" class="btn <?php if($this->dataMatch['MatchStatus']=="Post") { echo 'btn-success'; }else{ echo 'btn-default'; } ?> btn-block" <?php if($this->dataMatch['MatchStatus']=="Post") { echo 'disabled'; } ?> >Post</a>
						</div>
						<div class="col-sm-1 col-md-2">	
							<a href="<?php echo BASE_HREF; ?>api/adminfootball/match/update_mini/<?php echo $this->dataMatch['id']; ?>?MatchStatusType=10" class="btn <?php if($this->dataMatch['MatchStatus']=="Canc") { echo 'btn-success'; }else{ echo 'btn-default'; } ?> btn-block" <?php if($this->dataMatch['MatchStatus']=="Canc") { echo 'disabled'; } ?> >Canc</a>
						</div>
					</div>
				</div>
			</div>
			<?php if(
				($this->dataMatch['MatchStatus']=='1 HF')||
				($this->dataMatch['MatchStatus']=='2 HF')||
				($this->dataMatch['MatchStatus']=='E/T')
			){ ?>
			<div class="col-xs-2">
				<div class="panel panel-primary ">
					<div class="panel-heading"><h3 class="panel-title">Minute for Manual Livescore</h3></div>
					<div class="panel-body"><center>
						<input type="number" name="Minute" id="MinuteInput" class="form-control" style="width: 109px;height: 100%;font-size: 43pt;text-align: right;" value="<?php echo $this->dataMatch['Minute']; ?>">
						<button type="button" class="btn btn-success" onclick="update_minute()">อัพเดทนาที</button>
					</center></div>
				</div>
			</div>
			<?php } ?>
		</div>
		<?php } ?>
		<p class="bg-warning"></p>
		<div class="form-group">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">รายชื่อผู้ยิงประตู</h3>
				</div>
				<div class="panel-body">
					<table class="table table-bordered" id="tableScorers">
						<tr>
							<td><b>นาที</b></td>
							<td><b>ชื่อผู้เล่น</b></td>
							<td><b>ทีม</b></td>
							<td><b>จุดโทษ</b></td>
							<td><b>ทำเข้าประตูตัวเอง</b></td>
							<td><b>ต่อเวลาพิเศษ</b></td>
							<td><b>ลบรายชื่อ</b></td>
						</tr>
						<?php
							$sizeScorers	=	count($this->dataMatch['LastScorers']);
							for( $i=0 ; $i<$sizeScorers ; $i++ ){
								if($this->dataMatch['LastScorers'][$i][0]=='('){
									$teamplay		=	1;
									$dataScorers = explode(")" , ltrim($this->dataMatch['LastScorers'][$i], "("));
									//var_dump($dataScorers);
									$playerName		=	$dataScorers[1];
									$min			=	$dataScorers[0];
									
								}else{
									$teamplay		=	2;
									$dataScorers = explode("(" , rtrim($this->dataMatch['LastScorers'][$i], ")"));
									$playerName		=	$dataScorers[0];
									$min			=	$dataScorers[1];
								}
								
								//Pen
								$pos = strpos($min, 'Pen');
								if($pos === false){
									$isPen	=	false;
								}else{
									$isPen	=	true;
									$min 		= 	str_replace('Pen', '', $min);
								}
								
								//Extra Time
								$pos = strpos($min, 'ET');
								if($pos === false){
									$isET		=	false;
								}else{
									$isET		=	true;
									$min 		= 	str_replace('ET', '', $min);
								}
								
								//Own Goal
								$pos = strpos($min, 'Own');
								if($pos === false){
									$isOwn	=	false;
								}else{
									$isOwn	=	true;
									$min 		= 	str_replace('Own', '', $min);
								}
								
								if($this->dataMatch['AutoMode']!=1){
								?>
								<tr>
									<td><input type="input" size="5" name="inputMinScorers[]" value="<?php echo $min; ?>" class="form-control"></td>
									<td><input name="inputNameScorers[]" type="input" class="form-control" value="<?php echo $playerName; ?>"></td>
									<td>
										<select name="inputTeamScorers[]" class="form-control">
											<option value="0">----กรุณาเลือกทีม----</option>
											<option value="1" <?php if($teamplay==1) echo 'SELECTED'; ?>><?php echo $this->dataMatch['Team1']; ?></option>
											<option value="2" <?php if($teamplay==2) echo 'SELECTED'; ?>><?php echo $this->dataMatch['Team2']; ?></option>
										</select>
									</td>
									<td><input name="inputPN[]" type="checkbox" value="<?php echo $i; ?>" <?php if($isPen) echo 'CHECKED'; ?> ></td>
									<td><input name="inputOwnGoal[]" type="checkbox" value="<?php echo $i; ?>" <?php if($isOwn) echo 'CHECKED'; ?> ></td>
									<td><input name="inputET[]" type="checkbox" value="<?php echo $i; ?>" <?php if($isET) echo 'CHECKED'; ?> ></td>
									<td><button class="deleteIt btn btn-danger" type="button" >ลบ</button></td>
								</tr>
								<?php
								}else{ ?>
								<tr>
									<td><input type="input" size="5" value="<?php echo $min; ?>" class="form-control" disabled></td>
									<td><input type="input" class="form-control" value="<?php echo $playerName; ?>" disabled></td>
									<td>
										<select class="form-control" disabled>
											<option value="0">----กรุณาเลือกทีม----</option>
											<option value="1" <?php if($teamplay==1) echo 'SELECTED'; ?>><?php echo $this->dataMatch['Team1']; ?></option>
											<option value="2" <?php if($teamplay==2) echo 'SELECTED'; ?>><?php echo $this->dataMatch['Team2']; ?></option>
										</select>
									</td>
									<td><input type="checkbox" value="<?php echo $i; ?>" <?php if($isPen) echo 'CHECKED'; ?> disabled></td>
									<td><input type="checkbox" value="<?php echo $i; ?>" <?php if($isOwn) echo 'CHECKED'; ?> disabled></td>
									<td><input type="checkbox" value="<?php echo $i; ?>" <?php if($isET) echo 'CHECKED'; ?> disabled></td>
									<td></td>
								</tr>
								<?php
								}
							}
						?>
					</table>
				</div>
				<?php if($this->dataMatch['AutoMode']!=1){ ?>
				<div class="panel-footer">
					<div class="row"><div class="col-md-12 col-sm-12 col-xs-12">
						<button type="button" style="width: inherit;" class="btn btn-success btn-lg" onclick="scorersAdd()">เพิ่มผู้ยิงประตู</button>
					</div></div>
				</div>
				<?php } ?>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-xs-6">
				<label for="MatchDateTime">Match Time</label>
				<!--<input type="text" class="form-control" id="MatchDateTime" name="MatchDateTime" placeholder="เวลาการแข่งขัน" value="<?php/*
					if(isset($this->dataMatch)) {
						echo $this->dataMatch['MatchDateTime'];
					}else{
						echo date('Y-m-d H:i:s');
					}*/ ?>">-->
					
				<p class="lead"><?php echo $this->dataMatch['MatchDateTime']; ?></p>
			</div>
			<div class="col-xs-6">
				<label for="MatchStatus">Status Match</label>
				<?php if($this->dataMatch['AutoMode']==-1){ ?>
				<p class="lead"><?php echo $this->dataMatch['MatchStatus']; ?></p>
				<?php }else{ ?>
				<select class="form-control" name="MatchStatus" id="MatchStatus">
					<option value="Sched" <?php if($this->dataMatch['MatchStatus']=='Sched'){ echo 'selected'; } ?>>Sched</option>
					<option value="1 HF" <?php if($this->dataMatch['MatchStatus']=='1 HF'){ echo 'selected'; } ?>>1 HF</option>
					<option value="H/T" <?php if($this->dataMatch['MatchStatus']=='H/T'){ echo 'selected'; } ?>>H/T</option>
					<option value="2 HF" <?php if($this->dataMatch['MatchStatus']=='2 HF'){ echo 'selected'; } ?>>2 HF</option>
					<option value="E/T" <?php if($this->dataMatch['MatchStatus']=='E/T'){ echo 'selected'; } ?>>E/T</option>
					<option value="Pen" <?php if($this->dataMatch['MatchStatus']=='Pen'){ echo 'selected'; } ?>>Pen</option>
					<option value="Fin" <?php if($this->dataMatch['MatchStatus']=='Fin'){ echo 'selected'; } ?>>Fin</option>
					<option value="Int" <?php if($this->dataMatch['MatchStatus']=='Int'){ echo 'selected'; } ?>>Int</option>
					<option value="Abd" <?php if($this->dataMatch['MatchStatus']=='Abd'){ echo 'selected'; } ?>>Abd</option>
					<option value="Post" <?php if($this->dataMatch['MatchStatus']=='Post'){ echo 'selected'; } ?>>Post</option>
					<option value="Canc" <?php if($this->dataMatch['MatchStatus']=='Canc'){ echo 'selected'; } ?>>Canc</option>
				</select>
				<?php } ?>
			</div>
		</div>
		<div class="form-group">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">ช่องการถ่ายทอดสด</h3>
				</div>
				<div class="panel-body">
					<table class="table table-bordered">
						<?php
						for( $j=0,$sizeTV=count($list_TV) ; $j< $sizeTV ; $j++ )
						{
							?><tr>
								<td colspan="5"><b><?php echo $list_TV[$j]['nameType']; ?></b></td>
							</tr>
							<?php
							echo '<tr>';
							$countTV = 5;
							foreach ($list_TV[$j]['listTV'] as $k => $v) {
								if (in_array($k, $this->dataMatch['TVLiveList']))
									$check = 'CHECKED';
								else
									$check = '';
								echo '<td><input name="TVLiveList[]" type="checkbox" class="TVList" value="' . $k . '" ' . $check . '>' . $v . '</td>';
														
								$countTV--;
								if($countTV <= 0){
									$countTV = 5;
									echo '</tr><tr>';
								}
							}
							if($countTV!=5)
								echo '<td colspan="'. $countTV .'"></td>';
								echo '</tr>';
						}
						?>
					</table>
				</div>
			</div>
		</div>
		<div class="form-group">
			 <div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">วิเคราะห์บอล</h3>
				</div>
				<div class="panel-body">
					<textarea name="Analysis" class="summernote"><?php echo $this->dataMatch['Analysis']; ?></textarea >
				</div>
			</div>
		</div>
		<div class="form-group">
			 <div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">สรุปผลบอล</h3>
				</div>
				<div class="panel-body">
					<textarea name="Result" class="summernote"><?php echo $this->dataMatch['Result']; ?></textarea >
				</div>
			</div>
		</div>
		<div class="form-group">
			 <div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Embed Code</h3>
				</div>
				<div class="panel-body">
					<textarea name="EmbedCode" class="form-control" rows="3"><?php echo $this->dataMatch['EmbedCode']; ?></textarea >
				</div>
			</div>
		</div>
		<div class="form-group">
			 <div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">รูปภาพข่าว(เดิม)</h3>
				</div>
				<div class="panel-body">
					<table class="table table-bordered">
						<?php
						$picperrow 		=	5;
						$piccount		=	count($this->dataMatch['Picture']);
						?>
						<?php for($i=0;$i<$piccount;$i++) {
							if( ($i%$picperrow) == 0){ ?><tr><?php } ?>
								<td width="20%">
									<img style="width: 100%;" src="http://202.183.165.189/uploads/match/<?php echo $dateNoTime . '/' . $this->dataMatch['Picture'][$i]; ?>"><br>
									<input name="listDeletePicture[]" class="imgOld" type="checkbox" value="<?php echo $i; ?>">ลบรูปภาพนี้
								</td>
							<?php if( ($i%$picperrow) == ($picperrow-1)){ ?></tr><?php }
						}
						if( ($i%$picperrow) != 0 ){ ?></tr><?php } ?>
					</table>
				</div>
			</div>
		</div>
		<div class="form-group">
			 <div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">รูปภาพข่าว(ใหม่)</h3>
				</div>
				<div class="panel-body">
					<label class="control-label">Select File</label>
					<input id="fileUpload" name="fileUpload[]" type="file" multiple class="file-loading">
					<div id="errorBlock" class="help-block"></div>
				</div>
			</div>
		</div>
		<div class="form-group">
			 <div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">อัตราต่อรอง,ฟันธง,คำถามชวนคุย</h3>
				</div>
				<div class="panel-body">
					<div class="col-xs-12">
						<label>คำถามชวนคุย</label>
						<input type="text" class="form-control" name="question" placeholder="คำถามชวนคุย" value="<?php echo $this->dataMatch['question']; ?>">
					</div>

					<div class="col-xs-6">
						<div class="panel panel-default " style="margin-top: 20px;">
							<div class="panel-heading">
								<h3 class="panel-title">อัตราต่อรอง</h3>
							</div>
							<div class="panel-body">
								<label>อัตราต่อรอง</label>
								<select name="Odds" id="Odds" class="form-control" <?php if($this->dataMatch['Odds']<=0) { echo 'disabled'; } ?>>
									<option value="-1.0" >-- select --</option>
									<?php
										foreach ($listOdd as $k => $v) {
											echo '<option value="' . (float)$k . '" ';
											if((float)$this->dataMatch['Odds']===(float)$k)
											{
												echo 'selected';
											}
											echo '>' . $v . '</option>';
										}
									?>
								</select>
								<div class="radio">
									<label><input type="radio" name="TeamOdds" value="-1" <?php if($this->dataMatch['Odds'] == -1){ echo 'checked'; } ?>> ไม่มีอัตราต่อรอง</label>
								</div>
								<div class="radio">
									<label><input type="radio" name="TeamOdds" value="0" <?php if($this->dataMatch['Odds'] == 0){ echo 'checked'; } ?>> เสมอ</label>
								</div>
								<div class="radio">
									<label>
										<input type="radio" name="TeamOdds" value="1" <?php if($this->dataMatch['TeamOdds'] == 1){ echo 'checked'; } ?>>
										<img style="width: 30px;" src="<?php echo $this->dataMatch['Team1Logo']; ?>">
										<?php echo $this->dataMatch['Team1']; ?>
									</label>
								</div>
								<div class="radio">
									<label>
										<input type="radio" name="TeamOdds" value="2" <?php if($this->dataMatch['TeamOdds'] == 2){ echo 'checked'; } ?>>
										<img style="width: 30px;" src="<?php echo $this->dataMatch['Team2Logo']; ?>">
										<?php echo $this->dataMatch['Team2']; ?>
									</label>
								</div>
							</div>
						</div>
					</div>
						
					<div class="col-xs-6"><center>
						<div class="panel panel-default " style="margin-top: 20px;">
							<div class="panel-heading">
								<h3 class="panel-title">ฟันธง</h3>
							</div>
							<div class="panel-body">
								<div class="radio-inline">
									<label>
										<input type="radio" name="TeamSelect" value="1" <?php if($this->dataMatch['TeamSelect'] == 1){ echo 'checked'; } ?>>
										<img style="width: 50px;" src="<?php echo $this->dataMatch['Team1Logo']; ?>"><br>
										<?php echo $this->dataMatch['Team1']; ?>
									</label>
								</div>
								<div class="radio-inline">
									<label>
										<input type="radio" name="TeamSelect" value="0" <?php if($this->dataMatch['TeamSelect'] == 0){ echo 'checked'; } ?>>
										<img style="width: 50px;" src="http://football.kapook.com/uploads/logo/default.png"><br>
										ไม่ฟันธง
									</label>
								</div>
								<div class="radio-inline">
									<label>
										<input type="radio" name="TeamSelect" value="2" <?php if($this->dataMatch['TeamSelect'] == 2){ echo 'checked'; } ?>>
										<img style="width: 50px;" src="<?php echo $this->dataMatch['Team2Logo']; ?>"><br>
										<?php echo $this->dataMatch['Team2']; ?>
									</label>
								</div>
								<br><br>
								<label>ทรรศนะฟุตบอล</label>
								<input type="text" class="form-control" name="Predict" placeholder="ทรรศนะฟุตบอล" value="<?php echo $this->dataMatch['Predict']; ?>">
							</div>
						</div>
					</center></div>
				</div>
			</div>
		</div>
		<div class="form-group">
			 <div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">FB OG Image</h3>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-sm-4 col-md-4">
							<div class="thumbnail">
								<?php if(strlen($this->dataMatch['PictureOG'])){ ?>
								<img src="<?php echo 'http://202.183.165.189/uploads/match/og/' . $this->dataMatch['PictureOG']; ?>">
								<?php }else{ ?>
								<img src="<?php echo BASE_HREF; ?>/api/adminfootball/assets/img/noimage.png">
								<?php } ?>
								<div class="caption">
									<input type="radio" name="PictureOGSelect" value="1" <?php if(!strlen($this->dataMatch['PictureOG'])){ ?> disabled <?php }else{ ?> checked <?php } ?>> ใช้รูปปัจจุบัน
								</div>
							</div>
						</div>
						<div class="col-sm-8 col-md-8">
							<div class="thumbnail">
								<div class="caption">
									<input type="radio" name="PictureOGSelect" value="2"> อัพโหลดรูปใหม่
									<input type="file" name="PictureOG" id="PictureOG" disabled>
								</div>
							</div>
						</div>
						<div class="col-sm-8 col-md-8">
							<div class="thumbnail">
								<div class="caption">
									<input type="radio" name="PictureOGSelect" value="3" <?php if(!strlen($this->dataMatch['PictureOG'])){ ?> checked <?php } ?>> ไม่ใช้รูปภาพ
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row form-group"><center>
			<div class="col-sm-6 col-md-6"><button type="submit" class="btn btn-primary btn-lg btn-block">บันทึกการแข่งขัน</button></div>
			<div class="col-sm-6 col-md-6"><a href="<?php echo BASE_HREF; ?>api/adminfootball/match" class="btn btn-default btn-lg btn-block">กลับไปหน้าตารางการแข่งขัน</a></div>
		</center></div>
	</form>
</div>

<?php
if(	($this->dataMatch['AutoMode']==-1) &&
	(
		($this->dataMatch['MatchStatus']=='1 HF')||
		($this->dataMatch['MatchStatus']=='2 HF')||
		($this->dataMatch['MatchStatus']=='E/T')
	)
){ ?>
<form action="<?php echo BASE_HREF; ?>api/adminfootball/match/update_mini/<?php echo $this->dataMatch['id']; ?>" method="POST" id="minuteform">
	<input type="hidden" name="id" value="<?php echo $this->dataMatch['id']; ?>">
	<input type="hidden" name="Minute" id="Minute" value="<?php echo $this->dataMatch['Minute']; ?>">
</form>
<?php } ?>

<script>

	function resetNumCheckBox(){
		var i=0;
		$('#tableScorers input[type="checkbox"]').each(function() {
			$(this).val(i/3>>0);
			i = i+1;
		});
	}
	
	function update_minute(){
		$('#Minute').val($('#MinuteInput').val());
		$('#minuteform').submit();
	}
	
	function scorersAdd() {
		var tmpstr = '<tr>';
		tmpstr = tmpstr + '<td><input type="input" size="5" name="inputMinScorers[]" value="" class="form-control"></td>';
			tmpstr = tmpstr + '<td ><input name="inputNameScorers[]" type="input" class="form-control" value=""></td>';
			tmpstr = tmpstr + '<td >';
				tmpstr = tmpstr + '<select name="inputTeamScorers[]" class="form-control">';
					tmpstr = tmpstr + '<option value="0">----กรุณาเลือกทีม----</option>';
					tmpstr = tmpstr + '<option value="1"><?php echo $this->dataMatch['Team1']; ?></option>';
					tmpstr = tmpstr + '<option value="2"><?php echo $this->dataMatch['Team2']; ?></option>';
				tmpstr = tmpstr + '</select>';
			tmpstr = tmpstr + '</td>';
			tmpstr = tmpstr + '<td><input name="inputPN[]" type="checkbox" value=""></td>';
			tmpstr = tmpstr + '<td><input name="inputOwnGoal[]" type="checkbox" value=""></td>';
			tmpstr = tmpstr + '<td><input name="inputET[]" type="checkbox" value=""></td>';
			tmpstr = tmpstr + '<td><button class="deleteIt btn btn-danger" type="button" >ลบ</button></td>';
		tmpstr = tmpstr + '</tr>';
		$("#tableScorers").append(tmpstr);
			
		$( ".deleteIt" ).unbind( "click" ).click(function() {
			$(this).parent().parent().remove();
			resetNumCheckBox();
		});
			
		resetNumCheckBox();
	}

	$(document).ready(function(){
		$('#MatchDateTime').datetimepicker({
			lang:'en',
			format:'Y-m-d H:i:s'
		});
		
		$(".TVList").each(function(){
			if($(this).is( ":checked" )){
				$(this).parent().css( "background-color", "#9cf596" );
			}
		});
		$('.summernote').summernote({
			height: 600,                 // set editor height
			//focus: true                  // set focus to editable area after initializing summernote
		});
		$("#fileUpload").fileinput({
			uploadUrl: 'http://football.kapook.com/api/adminfootball/match/update_check_post/',
			//maxFilePreviewSize: 10240,
			showUpload : false,
			browseOnZoneClick : true,
			layoutTemplates : {
				actionUpload: ''
			}
		});
		$( ".deleteIt" ).click(function() {
			$(this).parent().parent().remove();
			resetNumCheckBox();
		});
	});
	
	$(".TVList").change(function(){
		if($(this).is( ":checked" )){
			$(this).parent().css( "background-color", "#9cf596" );
		}
		else{
			$(this).parent().css( "background-color", "" );
		}
	});
	
	$(".imgOld").change(function(){
		if($(this).is( ":checked" )){
			$(this).parent().css( "background-color", "#ff3939" );
		}
		else{
			$(this).parent().css( "background-color", "" );
		}
	});
	
	$("#ETButton").click(function(){
		if($(this).hasClass( "btn-success" )){
			$(this).removeClass( "btn-success" );
			$(this).addClass( "btn-danger" );
			$(".ETinput").prop('disabled', true);
			$(".ETinput").val("");
		}
		else{
			$(this).removeClass( "btn-danger" );
			$(this).addClass( "btn-success" );
			$(".ETinput").prop('disabled', false);
			$(".ETinput").val("0");
		}
	});
	
	$("#PNButton").click(function(){
		if($(this).hasClass( "btn-success" )){
			$(this).removeClass( "btn-success" );
			$(this).addClass( "btn-danger" );
			$(".PNinput").prop('disabled', true);
			$(".PNinput").val("");
		}
		else{
			$(this).removeClass( "btn-danger" );
			$(this).addClass( "btn-success" );
			$(".PNinput").prop('disabled', false);
			$(".PNinput").val("0");
		}
	});
	
	$('input[type=radio][name=TeamOdds]').change(function() {
		if (parseFloat(this.value) <= 0) {
			$( "#Odds" ).prop( "disabled", true );
			$( "#Odds" ).val("-1.0");
		}
		else {
			$( "#Odds" ).prop( "disabled", false );
		}
	});
	
	$('input[type=radio][name=PictureOGSelect]').change(function() {
		if (this.value == '2') {
			$( "#PictureOG" ).prop( "disabled", false );
		}
		else {
			$( "#PictureOG" ).prop( "disabled", true );
		}
	});
	
</script>