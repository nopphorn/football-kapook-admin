<div class="container" style="padding-bottom: 60px;">
	<?php if($this->dataPlayer) { ?>
	<form role="form" action="<?php echo BASE_HREF; ?>api/adminfootball/player/update" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="id" value="<?php echo $this->dataPlayer['id']; ?>">
	<?php }else{ ?>
	<form role="form" action="<?php echo BASE_HREF; ?>api/adminfootball/player/create" method="POST" enctype="multipart/form-data">
	<?php } ?>
		<div class="form-group">
			<label>Player Data</label>
			
			<div class="row">
				<div class="col-sm-4 col-md-4">
					<div class="thumbnail">
						<?php if(strlen($this->dataPlayer['img'])){ ?>
						<img src="<?php echo str_replace("football.kapook.com", "202.183.165.189", $this->dataPlayer['img']); ?>">
						<?php }else{ ?>
						<img src="http://football.kapook.com/uploads/scorer/noface.jpg">
						<?php } ?>
						<div class="caption">
							<input type="radio" name="imgSelect" value="1" <?php if(!strlen($this->dataPlayer['img'])){ ?> disabled <?php }else{ ?> checked <?php } ?>> ใช้รูปปัจจุบัน
							<input type="radio" name="imgSelect" value="2"> อัพโหลดรูปใหม่
							<input type="radio" name="imgSelect" value="3" <?php if(!strlen($this->dataPlayer['img'])){ ?> checked <?php } ?>> ไม่ใช้รูปภาพ
							
							<input type="file" name="imgPlayer" id="imgPlayer" disabled>
						</div>
					</div>
				</div>
				
				<div class="col-sm-8 col-md-8">
					<div class="panel panel-default ">
						<div class="panel-heading">
							<h3 class="panel-title">ชื่อผู้เล่น</h3>
						</div>
						<div class="panel-body"><input class="form-control" type="text" name="name" value="<?php echo $this->dataPlayer['name']; ?>"></div>
					</div>
				</div>
				
				<!--<div class="col-sm-8 col-md-8">
					<div class="panel panel-default ">
						<div class="panel-heading">
							<h3 class="panel-title">ทีม</h3>
						</div>
						<div class="panel-body">
							<input class="form-control" type="hidden" name="team_id" id="team_id" value="0">
							<img id="TeamLogo" style="width: 30px;float: left;" src="http://football.kapook.com/uploads/logo/default.png">
							<div style="padding-top: 5px;padding-left: 5px;float: left;" id="TeamName">N/A</div>
							<button class="btn btn-default" type="button" style="float: right;" onclick="window.open('<?php echo BASE_HREF; ?>api/adminfootball/player/get_listTeamForPlayer','','height=400,width=400');">ค้นหาทีม <span class="glyphicon glyphicon-search"></span></button>
						</div>
					</div>
				</div>-->
				
			</div>
		</div>
		<div class="row form-group"><center>
			<div class="col-sm-6 col-md-6"><button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button></div>
			<div class="col-sm-6 col-md-6"><a href="<?php echo BASE_HREF; ?>api/adminfootball/player/" role="button" class="btn btn-default btn-lg btn-block">Back</a></div>
		</center></div>
	</form>
</div>

	<script language="JavaScript">
		$('input[type=radio][name=imgSelect]').change(function() {
			if (this.value == '2') {
				$( "#imgPlayer" ).prop( "disabled", false );
			}
			else {
				$( "#imgPlayer" ).prop( "disabled", true );
			}
		});
	</script>