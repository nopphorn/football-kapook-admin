<?php
function nav_page($current_page,$max_page,$keyword = ''){
	
	$slot = 5;
	$number_page = array();
	$number_page[] = $current_page;
	$slot--;
	$i = 1;
	$end_head = false;
	$end_tail = false;
	while(1){
		
		if(($current_page - $i)>=1){
			array_unshift($number_page,$current_page-$i);
			$slot--;
		}else{
			$end_head = true;
		}
		
		if($slot == 0){
			break;
		}
		
		if(($current_page + $i)<=$max_page){
			$number_page[] = $current_page+$i;
			$slot--;
		}else{
			$end_tail = true;
		}
		
		if($slot == 0){
			break;
		}
		
		if($end_head && $end_tail){
			break;
		}
		$i++;
	}

	echo '<nav><ul class="pagination pagination-lg">';
	// Previous
	if($current_page > 1){
		echo '<li><a href="' . BASE_HREF . 'api/adminfootball/player/?page=' . ($current_page-1);
		if(strlen($keyword)>=1){
			echo '&keyword=' . $keyword;
		}
		echo '" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
	}else{
		echo '<li class="disabled"><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
	}
	// Page Number
	foreach($number_page as $number){
		if($number == $current_page){
			echo '<li class="active"><a href="#">';
		}else{
			echo '<li><a href="' . BASE_HREF . 'api/adminfootball/player/?page=' . $number;
			if(strlen($keyword)>=1){
				echo '&keyword=' . $keyword;
			}
			echo '">';
		}
		echo $number . '</a></li>';
	}
	// Next
	if($current_page < $max_page){
		echo '<li><a href="' . BASE_HREF . 'api/adminfootball/player/?page=' . ($current_page+1);
		if(strlen($keyword)>=1){
			echo '&keyword=' . $keyword;
		}
		echo '" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
	}else{
		echo '<li class="disabled"><a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
	}
	echo '</ul></nav>';
}
?>
<div class="container">
	<div class="row form-group">
		<div class="col-sm-3 col-md-3"><a href="<?php echo BASE_HREF; ?>api/adminfootball/player/info" class="btn btn-success btn-block">สร้างผู้เล่นใหม่</a></div>
	</div>
	<hr style="margin-top: 0px;margin-bottom: -10px;">
	<center>
	<?php
		nav_page($this->arrPlayer['page'],$this->arrPlayer['page_all'],$this->arrPlayer['keyword']);
	?>
	</center>
	<div class="row form-group">
		<div class="col-sm-6 col-md-6">
			<form action="<?php echo BASE_HREF; ?>api/adminfootball/player/">
				<div class="input-group">
					<span class="input-group-btn">
						<button class="btn btn-default" type="submit">ค้นหา</button>
					</span>
					<input type="text" name="keyword" class="form-control" placeholder="ค้นหาผู้เล่น" value="<?php echo $this->arrPlayer['keyword']; ?>">
				</div>
			</form>
		</div>
	</div>
	<table class="table table-bordered table-striped" style="margin-bottom: 0px;">
		<colgroup>
			<col class="col-xs-2">
			<col class="col-xs-8">
			<col class="col-xs-1">
			<col class="col-xs-1">
		</colgroup>
		<thead>
			<tr>
				<th>รูปภาพ</th>
				<th>ชื่อผู้เล่น</th>
				<th>แก้ไข</th>
				<th>ลบ</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($this->arrPlayer["list"] as $tmpData) { ?>
			<tr>
				<td>
					<?php if(strlen($tmpData['img'])){ ?>
					<img src="<?php echo str_replace("football.kapook.com", "202.183.165.189", $tmpData['img']); ?>" style="width: 100%;">
					<?php }else{ ?>
					<img src="http://football.kapook.com/uploads/scorer/noface.jpg" style="width: 100%;">
					<?php } ?>
				</td>
				<td style="vertical-align: middle;"><?php echo $tmpData['name']; ?></td>
				<td style="vertical-align: middle;"><a href="<?php echo BASE_HREF; ?>api/adminfootball/player/info/<?php echo $tmpData['id']; ?>" type="button" class="btn btn-default">แก้ไข</a></td>
				<td style="vertical-align: middle;"><a onclick="return confirm('แน่ใจแล้วนะ ว่าจะลบผู้เล่นคนนี้?')" href="<?php echo BASE_HREF; ?>api/adminfootball/player/delete/<?php echo $tmpData['id']; ?>" type="button" class="btn btn-danger">ลบ</a></td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
	<center>
	<?php
		nav_page($this->arrPlayer['page'],$this->arrPlayer['page_all'],$this->arrPlayer['keyword']);
	?>
	</center>
	
</div>