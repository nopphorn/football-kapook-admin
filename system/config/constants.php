<?php

if (!defined('MINIZONE'))
    exit;

/* -- Define : Base URL -- */
define('BASE_FOLDER','api\/adminfootball');
define('BASE_API', 'http://202.183.165.189/');
define('BASE_HREF', 'http://football.kapook.com/');
define('BASE_HREF2', 'http://football.kapook.com');
define('BASE_HREF_ADMIN', 'http://football.kapook.com/api/adminfootball/');
define('SERV_NAME', 'football.kapook.com');
define('FB_APP_ID', '306795119462');
define('FB_APP_SECRET', '5882cb894fa2b90cf4aed89557a1736a');


define('PATH_UPLOADS', $_SERVER['DOCUMENT_ROOT'] . '/uploads');
define('FOLDER_VIEW_DESKTOP', 'desktop');
define('FOLDER_VIEW_MOBILE', 'mobile');

## Portal data
define('PORTAL_ID', 28);
define('PORTAL_NAME', 'football');
define('URL_KM', 'http://signup-demo.kapook.com/');

// hook HTTP_REQUEST_URL for Login Error
parse_str($_SERVER['QUERY_STRING'],$query_string);
if($query_string['error'] != '') {
    unset($query_string['error']);
    
    if(is_array($query_string) and sizeof($query_string) > 0 ) $query_string_url = '?'.http_build_query($query_string);
    define('HTTP_REQUEST_URL', BASE_HREF2."{$_SERVER['REDIRECT_URL']}{$query_string_url}");
} else {
    define('HTTP_REQUEST_URL', BASE_HREF2."{$_SERVER['REQUEST_URI']}");
}
define('BASE_PATH_TEMPLATE', 'http://my.kapook.com/templateContent/theme1_2012/');


/* -- Define : Admin -- */
define('TITLE_ADMIN', 'Admin : football.kapook.com');
define('WEB_NAME', 'Football');
define('PATH_UPLOADS', $_SERVER['DOCUMENT_ROOT'] . '/uploads');

define('FOLDER_AM', 'kadmin');
define('BASE_AM', BASE_HREF . FOLDER_AM);

define('API_BASE_URL', 'http://kapi.kapook.com/');
?>