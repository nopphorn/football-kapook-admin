<?php

if (!defined('MINIZONE'))
    exit;

/* -- MongoDB -- */

$config['football']['host'] = 'localhost';
$config['football']['port'] = 27017;
$config['football']['db'] = 'football';
$config['football']['user'] = '';
$config['football']['pass'] = '';
$config['football']['persist'] = TRUE;
$config['football']['persist_key'] = '';
?>
