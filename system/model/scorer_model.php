<?php

class Scorer_model extends Mongo_model
{

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct(){
        parent::__construct('football');
		$this->collection			=		$this->db->football_comp_scorers;
		$this->upload_lib 			= 		$this->minizone->library('upload_lib');
		
    }

    function create($arrData){
		
		if(intval($arrData['player_id'])<=0){
			return false;
		}
		if(intval($arrData['team_id'])<=0){
			return false;
		}
		if(intval($arrData['goal'])<0){
			return false;
		}
		
		$arrData['player_id']	=	intval($arrData['player_id']);
		$arrData['team_id']		=	intval($arrData['team_id']);
		$arrData['goal']		=	intval($arrData['goal']);
		$arrData['comp_id']		=	intval($arrData['comp_id']);
		
		//Add a new Scorer
		$cursor						=	$this->collection->find();
		if($cursor->count(true)<=0){
			$arrData['id']			=	1;
		}else{
			$cursor->sort( array( 'id' => -1 ) );
			$cursor->limit(1);
			$cursor->next();
			$dataTmp					=	$cursor->current();
			$arrData['id']				=	((int)$dataTmp['id']+1);
		}
		
		$options = array(
			"w" => 1,
			"j" => true,
		);
		
		try {
			$this->collection->insert($arrData,$options);
		} catch (MongoCursorException $ex) {
			return false;
		}
		
		return true;

	}
	
	function update($id,$arrData){

		$tmpData	=	$this->collection->findOne(array( 'id' => intval($id) ));
		
		if(!$tmpData){
			return false;
		}
		
		if(intval($arrData['player_id'])<=0){
			return false;
		}
		if(intval($arrData['team_id'])<=0){
			return false;
		}
		if(intval($arrData['goal'])<0){
			return false;
		}
		
		$arrData['player_id']	=	intval($arrData['player_id']);
		$arrData['team_id']		=	intval($arrData['team_id']);
		$arrData['goal']		=	intval($arrData['goal']);
		$arrData['comp_id']		=	intval($arrData['comp_id']);
		$arrData['id']			=	intval($id);
		
		$options = array(
			"w" => 1,
		);
		
		try {
			$this->collection->update(array("id" => intval($arrData['id'])), $arrData, $options);
		} catch (MongoCursorException $ex) {
			return false;
		}
		
		return true;

	}
	
	function update_goal($id,$arrData){

		$tmpData	=	$this->collection->findOne(array( 'id' => intval($id) ));
		
		if(!$tmpData){
			return false;
		}
		
		if(intval($arrData['goal'])<0){
			return false;
		}
		
		$options = array(
			"w" => 1,
		);
		
		try {
			$this->collection->update(array("id" => intval($arrData['id'])), array( '$set' => array( 'goal' => intval($arrData['goal'])) ), $options);
		} catch (MongoCursorException $ex) {
			return false;
		}
		
		return true;

	}
	
	function delete($id = -1){
		
		$options = array(
			"justOne" 	=> 	true
		);
		
		$arrData['id']	=	intval($id);
		
		try {
			$this->collection->remove($arrData,$options);
			
		} catch (MongoCursorException $ex) {
			return false;
		}
		
		return true;

	}
	
	function deleteByPlayerID($player_id = -1){
		
		$options = array(
			"justOne" 	=> 	true
		);
		
		$arrData['player_id']	=	intval($player_id);
		
		try {
			$this->collection->remove($arrData,$options);
			
		} catch (MongoCursorException $ex) {
			return false;
		}
		
		return true;

	}
	
	function load(){
		
		$cursor		=	$this->collection->find()->sort( array( 'id' => -1 ) );
		$arrData	=	array();
		
		foreach ( $cursor as $id => $value ){
			$arrData[]	=	$value;
		}
		
		return $arrData;

	}
	
	function loadByID($id){
		
		$tmpData	=	$this->collection->findOne( array( 'id' => intval($id) ) );
		return $tmpData;

	}
	
	function loadByCompID($comp_id,$size = 0){
		
		$cursor		=	$this->collection->find(array( 'comp_id' => intval($comp_id) ))->sort( array( 'goal' => -1 ) );
		if($size > 0){
			$cursor->limit($size);
		}
		$arrData	=	array();
		
		foreach ( $cursor as $id => $value ){
			$arrData[]	=	$value;
		}
		
		return $arrData;

	}
	
}
