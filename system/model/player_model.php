<?php

class Player_model extends Mongo_model
{

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct(){
        parent::__construct('football');
		$this->collection			=		$this->db->football_player;
		$this->upload_lib 			= 		$this->minizone->library('upload_lib');
		
    }

    function create($arrData,$files){
		
		if(!is_string($arrData['name'])){
			return false;
		}
		
		//Add a new Competition
		$cursor						=	$this->collection->find();
		if($cursor->count(true)<=0){
			$arrData['id']			=	1;
		}else{
			$cursor->sort( array( 'id' => -1 ) );
			$cursor->limit(1);
			$cursor->next();
			$dataTmp					=	$cursor->current();
			$arrData['id']				=	((int)$dataTmp['id']+1);
		}
		
		if($arrData['imgSelect']=='1'){ //old image
			$arrData['img']	=	$tmpData['img'];
		}else if($arrData['imgSelect']=='2'){ //new image
			
			$returnData		=		$this->upload_lib->uploadImage($files['imgPlayer'] , $arrData['id'], "/var/web/football.kapook.com/html/uploads/player/");
			
			if( $returnData[0] != 1 ){
				return false;
			}
			
			$arrData['img']	=	BASE_HREF . 'uploads/player/' . $returnData[1];
			
		}else{ // no image
			$arrData['img']	=	'';
		}
		
		if(isset($arrData['imgSelect'])){
			unset($arrData['imgSelect']);
		}
		
		$options = array(
			"w" => 1,
			"j" => true,
		);
		
		try {
			$this->collection->insert($arrData,$options);
		} catch (MongoCursorException $ex) {
			return false;
		}
		
		return true;

	}
	
	function update($id,$arrData,$files){

		$tmpData	=	$this->collection->findOne(array( 'id' => intval($id) ));
		
		if(!$tmpData){
			return false;
		}
		
		$arrData['id']	=	intval($id);
		
		if(!is_string($arrData['name'])){
			return false;
		}
		
		if($arrData['imgSelect']=='1'){ //old image
			$arrData['img']	=	$tmpData['img'];
		}else if($arrData['imgSelect']=='2'){ //new image
			
			$returnData		=		$this->upload_lib->uploadImage($files['imgPlayer'] , $arrData['id'], "/var/web/football.kapook.com/html/uploads/player/");
			
			if( $returnData[0] != 1 ){
				return false;
			}
			
			$arrData['img']	=	BASE_HREF . 'uploads/player/' . $returnData[1];
			
		}else{ // no OG
			$arrData['img']	=	'';
		}
		
		if(isset($arrData['imgSelect'])){
			unset($arrData['imgSelect']);
		}
		
		$options = array(
			"w" => 1,
		);
		
		try {
			$this->collection->update(array("id" => intval($arrData["id"])), $arrData, $options);
		} catch (MongoCursorException $ex) {
			return false;
		}
		
		return true;

	}
	
	function delete($id = -1){
		
		$options = array(
			"justOne" 	=> 	true
		);
		
		$arrData['id']	=	intval($id);
		
		try {
			$this->collection->remove($arrData,$options);
			
		} catch (MongoCursorException $ex) {
			return false;
		}
		
		return true;

	}
	
	function load($size = 10,$page = 1,$keyword = null){
		if($keyword != null){
			$query 		= array('name' => new MongoRegex("/$keyword/i"));
			$cursor		=	$this->collection->find($query)->sort( array( 'id' => -1 ) );
		}else{
			$cursor		=	$this->collection->find()->sort( array( 'id' => -1 ) );
		}
		$cursor->limit($size);
		$cursor->skip(($page-1)*$size);	
		
		$arrData				=	array();
		$arrData["list"] 		= 	array();
		
		$arrData["size_all"] 	= 	$cursor->count();
		$arrData["size"] 		= 	$cursor->count(true);
		$arrData["page"] 		=	$page;
		if (($arrData["size_all"] % $size)==0) {
			$arrData["page_all"] 		=	floor($arrData["size_all"]/$size);
		}else{
			$arrData["page_all"] 		=	floor($arrData["size_all"]/$size)+1;
		}
		$arrData["keyword"] 	=	$keyword;
		
		
		foreach ( $cursor as $id => $value ){
			$arrData["list"][]	=	$value;
		}

		return $arrData;

	}
	
	function loadByID($id){
		
		$tmpData	=	$this->collection->findOne( array( 'id' => intval($id) ) );
		return $tmpData;

	}
	
	function get_player_by_keyword($keyword = ''){

		$arrCond 		= 	array('name' 	=> new MongoRegex("/^$keyword/i"));
		/*---------------------------------------------------------------------------------------*/
		
		$cursor					=		$this->collection->find($arrCond);
		$cursor->sort( array( 'name' => 1 ) );
		
		$arrData	=	array();
		foreach ( $cursor as $id => $value ){
			$arrData[]	=	$value;
		}
		return $arrData;
		
	}
	
}
