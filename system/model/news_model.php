<?php

class News_model extends Mongo_model
{

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct()
    {
        parent::__construct('football');
		$this->collection			=		$this->db->football_news;
    }
	
	function getnews($ListCountry = array(),$ListKeyword = array(),$ListKeyword2 = array(),$ListKeyword3 = array(),$pagesize = 8,$page = 1){

		$arrCond				=		array();
		foreach($ListCountry as $tmpData){
			$arrCond['$or'][]		= 		array('extra.kapookfootball_News_Country'=>strval($tmpData));
		}
		foreach($ListKeyword as $tmpData){
			$arrCond['$or'][]		= 		array('extra.kapookfootball_News_CountryOtherKeyword'=>strval($tmpData));
		}
		foreach($ListKeyword2 as $tmpData){
			$arrCond['$or'][]		= 		array('extra.kapookfootball_News_CountryOtherKeyword2'=>strval($tmpData));
		}
		foreach($ListKeyword3 as $tmpData){
			$arrCond['$or'][]		= 		array('extra.kapookfootball_News_CountryOtherKeyword3'=>strval($tmpData));
		}
		/*---------------------------------------------------------------------------------------*/
		
		$cursor					=		$this->collection->find($arrCond);
		$cursor->sort( array( 'cd' => -1 ) );
		$cursor->limit($pagesize);
		$cursor->skip(($page-1)*$pagesize);		
		
		$arrData					=		array();
		$arrData['NewsList']		=		array();
		$arrData['total_pages']		=		intval($cursor->count() / $pagesize);
		if($cursor->count() % $pagesize > 0){
			$arrData['total_pages']++;
		}
		$arrData['paged']			=		$page;
		
		foreach ( $cursor as $id => $value ){
			if($value['extra']['kapookfootball_News_URL']==''){
				$URL='http://football.kapook.com/news-'. $value['news_id'];                             
			}else{
				$URL=$value['extra']['kapookfootball_News_URL'];   
			}
			if ($value['news_id']<=21632) {
				$path_pic_thum = 'http://football.kapook.com/cms/uploadnews/news/thumbnail/' . $value['picture']['thumbnail']['src'];
				$path_pic_large = 'http://football.kapook.com/cms/uploadnews/news/picture/' . $value['picture']['large']['src'];
			}else{
				$path_pic_thum = 'http://football.kapook.com/cms/upload/thumbnail/' . $value['picture']['thumbnail']['src'];
				$path_pic_large = 'http://football.kapook.com/cms/upload/large/' . $value['picture']['large']['src'];
			}
			$arrData['NewsList'][] = array(
				'_id'				=>	$value['news_id'],
				'content_id'		=>	$value['news_id'],
				'zone_id'			=>	'',
				'portal_id'			=>	27,
				'subject'			=>	$value['detail'][0]['subject'],
				'title'				=>	$value['detail'][0]['title'],
				'thumbnail'			=>	empty($value['picture']['thumbnail']['src']) ? '' : $path_pic_thum,
				'picture'			=>	empty($value['picture']['large']['src']) ? '' : $path_pic_large,
				'link'				=> $URL,
				'CreateDate'		=>	$value['cd']->sec,
				'LastUpdate'		=>	$value['ld']->sec,
				'Views'				=>	$value['stat']['view'],
				'attr'				=>	array(/*
					'kapookfootball_News_ModuleKey' 			=> $row['kapookfootball_News_ModuleKey'],
					'kapookfootball_News_Language' 				=> $row['kapookfootball_News_Language'],
					'kapookfootball_News_Country' 				=> $row['kapookfootball_News_Country'],*/
					'kapookfootball_News_CountryOtherKeyword' 	=> $value['extra']['kapookfootball_News_CountryOtherKeyword1'],
					'kapookfootball_News_CountryOtherKeyword2' 	=> $value['extra']['kapookfootball_News_CountryOtherKeyword2'],
					'kapookfootball_News_CountryOtherKeyword3' 	=> $value['extra']['kapookfootball_News_CountryOtherKeyword3'],
					/*'kapookfootball_News_Team' 					=> $row['kapookfootball_News_Team'],
					'kapookfootball_News_HilightPage' 			=> (int)$row['kapookfootball_News_HilightPage'],
					'kapookfootball_News_BreakNews' 			=> (int)$row['kapookfootball_News_BreakNews'],
					'kapookfootball_News_ShowNews' 				=> (int)$row['kapookfootball_News_ShowNews'],
					'kapookfootball_News_Zonenews' 				=> (int)$row['kapookfootball_News_Zonenews'],
					'kapookfootball_News_Group' 				=> $row['kapookfootball_News_Group'],
					'kapookfootball_News_Status' 				=> $row['kapookfootball_News_Status'],
					'kapookfootball_News_Order' 				=> (int)$row['kapookfootball_News_Order'],
					'kapookfootball_News_EuroHilight' 			=> (int)$row['kapookfootball_News_EuroHilight'],
					'kapookfootball_News_URL' 					=> $row['kapookfootball_News_URL'],
					'kapookfootball_News_VDO_Premium' 			=> $row['kapookfootball_News_VDO_Premium'],
					'kapookfootball_News_HTMLFileName' 			=> iconv('TIS-620', 'UTF-8//IGNORE', $row['kapookfootball_News_HTMLFileName'] )
				*/)
			);
		}
		return $arrData;
		
	}

}
