<?php

class Zone_model extends Mongo_model
{

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct()
    {
        parent::__construct('football');
		$this->collection		=		$this->db->football_zone;
    }
	
	function get_zone(){
		$cursor					=		$this->collection->find();
		$cursor->sort( array( 'NameEN' => 1 ) );
		
		$arrData	=	array();
		foreach ( $cursor as $id => $value ){
			$arrData[]	=	$value;
		}
		return $arrData;
	}
	
	function get_zone_by_id($zone_id){
		$arrCond				=		array();
		
		if($zone_id<=0){
			return null;
		}

		$arrCond['id']			=		intval($zone_id);
		
		$arrData				=		$this->collection->findOne($arrCond);
		return $arrData;
	}
	
	function update_by_id($arrData){

		$tmpData	=	$this->collection->findOne(array( 'id' => intval($arrData['id']) ));
		
		if(!$tmpData){
			return false;
		}
		
		$arrData['id']			=	intval($arrData['id']);
		$arrData['Status']		=	intval($arrData['Status']);
		$arrData['AutoStatus']	=	intval($arrData['AutoStatus']);
		
		if(!is_string($arrData['NameTH'])){
			return false;
		}
		
		$options = array(
			"w" => 1,
		);
		
		try {
			$this->collection->update(array("id" => intval($arrData["id"])), array( '$set' => $arrData ), $options);
		} catch (MongoCursorException $ex) {
			return false;
		}
		
		return true;

	}
	
	function update($arrData){
	
		foreach($arrData['NameTH'] as $key => $value){

			$tmpData	=	$this->collection->findOne(array( 'id' => intval($key) ));
			
			if(!$tmpData){
				return false;
			}
			
			$tmpData['Status']		=	intval($arrData['Status'][$key]);
			$tmpData['AutoStatus']	=	intval($arrData['AutoStatus'][$key]);
			
			if(!is_string($value)){
				return false;
			}
			
			$tmpData['NameTH']		=	$value;
			
			$options = array(
				"w" => 1,
			);
			
			try {
				$this->collection->update(array("id" => intval($key)), array( '$set' => $tmpData ), $options);
			} catch (MongoCursorException $ex) {
				return false;
			}
		
		}
		
		return true;

	}
}
