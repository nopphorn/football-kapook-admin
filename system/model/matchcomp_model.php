<?php

class Matchcomp_model extends Mongo_model
{

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct()
    {
        parent::__construct('football');
		$this->collection			=		$this->db->football_comp_match;
		
    }
	
	function get_by_comp($comp_id,$type = 0,$isIncludeRound = false){
	
		$arrAttr['comp_id']		=		intval($comp_id);
		if(!$isIncludeRound){
			$arrAttr['round_id']	=		-1;
		}
		if((intval($type) == 1)||(intval($type) == 2)||(intval($type) == 3)){
			$arrAttr['type']		=		intval($type);
		}
		
		$cursor		=	$this->collection->find($arrAttr);
		$arrData	=	array();
		foreach ( $cursor as $value ){
			$arrData[]	=	$value;
		}
		return $arrData;
	}
	
	function get_by_round($comp_id,$round_id,$type = 0){
	
		$arrAttr['comp_id']		=		intval($comp_id);
		$arrAttr['round_id']	=		intval($round_id);
		if((intval($type) == 1)||(intval($type) == 2)||(intval($type) == 3)){
			$arrAttr['type']		=		intval($type);
		}
		
		$cursor		=	$this->collection->find($arrAttr);
		$arrData	=	array();
		foreach ( $cursor as $value ){
			$arrData[]	=	$value;
		}
		return $arrData;
	}
	
	function loadListMatchDummy($comp_id,$round_id = 0,$dateFrom = '',$dateTo = '',$sort = 'desc'){
		
		$arrCond['comp_id']		=		intval($comp_id);
		if(intval($round_id)!=0){
			$arrCond['round_id']	=		intval($round_id);
		}
		$arrCond['type']		=		2;
		
		if(strlen($dateFrom)>0){
			$arrCond['MatchDateTime']['$gte']		=		$dateFrom . ' 06:00:00';
		}
		if(strlen($dateTo)>0){
			$arrCond['MatchDateTime']['$lte']		=		$dateTo . ' 05:59:59';
		}
	
		$cursor		=		$this->collection->find($arrCond);
		
		if($sort == 'desc'){
			$cursor->sort( array( 'MatchDateTime' => -1 ) );
		}else if($sort == 'asc'){
			$cursor->sort( array( 'MatchDateTime' => 1 ) );
		}

		$arrData	=	array();
		foreach ( $cursor as  $value ){
			$arrData[]	=	$value;
		}
		return $arrData;
		
	}
	
	function clear_listLeagueForMatch($comp_id,$round_id = null){
		
		if($round_id == null){
			try {
				$cursor		=	$this->collection->remove(
					array( 'comp_id' => intval($comp_id), 'type' => 1 )
				);
			} catch (MongoCursorException $ex) {
				return false;
			}
		}else{
			try {
				$cursor		=	$this->collection->remove(
					array( 'comp_id' => intval($comp_id) , 'round_id' => intval($round_id), 'type' => 1 )
				);
			} catch (MongoCursorException $ex) {
				return false;
			}
		}
		
		return true;
		
	}
	
	function clear_MatchDummy($comp_id,$round_id = null){
	
		if($round_id == null){
			try {
				$cursor		=	$this->collection->remove(
					array( 'comp_id' => intval($comp_id), 'type' => 2 )
				);
			} catch (MongoCursorException $ex) {
				return false;
			}
		}else{
			try {
				$cursor		=	$this->collection->remove(
					array( 'comp_id' => intval($comp_id) , 'round_id' => intval($round_id), 'type' => 2 )
				);
			} catch (MongoCursorException $ex) {
				return false;
			}
		}
		
		return true;
		
	}
	
	function add_listLeagueForMatch($comp_id,$round_id,$league_id){
	
		//Add a new League
		$cursor						=	$this->collection->find();
		if($cursor->count(true)<=0){
			$arrData['id']			=	1;
		}else{
			$cursor->sort( array( 'id' => -1 ) );
			$cursor->limit(1);
			$cursor->next();
			$dataTmp					=	$cursor->current();
			$arrData['id']				=	((int)$dataTmp['id']+1);
		}
		
		$arrData['comp_id']			=	intval($comp_id);
		$arrData['round_id']		=	intval($round_id);
		$arrData['league_id']		=	intval($league_id);
		$arrData['type']			=	1;
		
		$options = array(
			"w" => 1,
			"j" => true,
		);
		
		try {
			$this->collection->insert($arrData,$options);
		} catch (MongoCursorException $ex) {
			return false;
		}
		
		return true;
		
	}
	
	function create_MatchDummy($comp_id,$round_id,$arrData){

		$cursor						=	$this->collection->find();
		if($cursor->count(true)<=0){
			$arrData['id']			=	1;
		}else{
			$cursor->sort( array( 'id' => -1 ) );
			$cursor->limit(1);
			$cursor->next();
			$dataTmp					=	$cursor->current();
			$arrData['id']				=	((int)$dataTmp['id']+1);
		}
		
		$arrData['comp_id']				=	intval($comp_id);
		$arrData['round_id']			=	intval($round_id);
		$arrData['Team1FTScore']		=	intval($arrData['Team1FTScore']);
		$arrData['Team2FTScore']		=	intval($arrData['Team2FTScore']);
		$arrData['type']				=	2;
		
		$options = array(
			"w" => 1,
			"j" => true,
		);
		
		try {
			$this->collection->insert($arrData,$options);
		} catch (MongoCursorException $ex) {
			return false;
		}
		
		return true;
	}
	
	function update_MatchDummy($id,$arrData){

		$arrData['id']					=	intval($id);
		$arrData['comp_id']				=	intval($arrData['comp_id']);
		$arrData['round_id']			=	intval($arrData['round_id']);
		$arrData['Team1FTScore']		=	intval($arrData['Team1FTScore']);
		$arrData['Team2FTScore']		=	intval($arrData['Team2FTScore']);
		$arrData['type']				=	2;
		
		$options = array(
			"w" => 1
		);
		
		try {
			$this->collection->update(array("id" => intval($id)), $arrData, $options);
		} catch (MongoCursorException $ex) {
			return false;
		}
		
		return true;
	}
	
	function delete_MatchDummy($id){
	
		try {
			$cursor		=	$this->collection->remove(
				array( 'id' => intval($id) , 'type' => 2 )
			);
		} catch (MongoCursorException $ex) {
			return false;
		}
		
		return true;
		
	}
	
	function get_MatchDummy($id){
		$tmpData	=	$this->collection->findOne( array( 'id' => intval($id) , 'type' => 2 ) );
		return $tmpData;
	}
	
}
