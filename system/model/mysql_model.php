<?php if (!defined('MINIZONE')) exit;

class Mysql_model
{
    var $db;
    var $minizone;
    
    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct($name='default')
    {
        
        $this->minizone = minizone::getzone();
        $this->db = $this->minizone->library('mysql_lib', $name);
    }
    
    ///////////////////////////////////////////////// Destruct /////////////////////////////////////////////////
    function __destruct()
    {
        // $this->db->close();
    }
    
}
/* End of file mysql_model.php */
/* Location: ./model/mysql_model.php */