<?php

class Match_model extends Mongo_model
{

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct()
    {
        parent::__construct('football');
		$this->collection			=		$this->db->football_match;
		
    }
	
	function get_by_listLeagueID($arrLeagueID,$dateFrom = '',$dateTo = '',$sort = 'desc'){
	
		$arrCond								=		array();
		$arrCond['KPLeagueID']['$in']			=		$arrLeagueID;
		if(strlen($dateFrom)>0){
			$arrCond['MatchDateTime']['$gte']		=		$dateFrom . ' 06:00:00';
		}
		if(strlen($dateTo)>0){
			$arrCond['MatchDateTime']['$lte']		=		$dateTo . ' 05:59:59';
		}
	
		$cursor									=		$this->collection->find($arrCond);
		
		if($sort == 'desc'){
			$cursor->sort( array( 'MatchDateTime' => -1 ) );
		}else if($sort == 'asc'){
			$cursor->sort( array( 'MatchDateTime' => 1 ) );
		}
		
		$arrData	=	array();
		foreach ( $cursor as $value ){
			$arrData[]	=	$value;
		}
		return $arrData;
	}
	
	function get_by_listLeagueAndMatch($arrLeagueID,$arrMatchID){
		
		$arrData	=	array();
	
		if((!count($arrLeagueID))&&(!count($arrMatchID))){
			return $arrData;
		}
	
		$arrCond										=		array();
		$arrLeagueTeam									=		array();
		if(count($arrLeagueID)){
			$arrCond['$or'][]['KPLeagueID']['$in']		=		$arrLeagueID;
		}
		if(count($arrMatchID)){
			$arrCond['$or'][]['id']['$in']				=		$arrMatchID;
		}
	
		$cursor					=		$this->collection->find($arrCond);
		$cursor->sort( array( 'MatchDateTime' => -1 ) );

		foreach ( $cursor as $value ){
			$arrData[]	=	$value;
		}
		return $arrData;
	}
	
	function get_by_LeagueAndTeam($LeagueID,$TeamID){
	
		$arrCond					=		array();
		$arrCond['KPLeagueID']						=		$LeagueID;
		$arrCond['$or'][]['Team1KPID']				=		$TeamID;
		$arrCond['$or'][]['Team2KPID']				=		$TeamID;
	
		$cursor					=		$this->collection->find($arrCond);
		$cursor->sort( array( 'MatchDateTime' => -1 ) );
		
		$arrData	=	array();
		foreach ( $cursor as $value ){
			$arrData[]	=	$value;
		}
		return $arrData;
	}
	
	function get_match_new($data){
		
		/*
			inside of $data
			- dateFrom
			- dateTo
			- leagueID
			- zoneID
			- year
			- size
			- status
			- MatchStatus
			- start
			- sordby
			- typesort
			
		*/
		$arrCond				=		array();
		
		if(isset($data['dateFrom'])){
			$arrCond['MatchDateTime']['$gte']		=		$data['dateFrom'] . ' :00';
		}
		
		if(isset($data['dateTo'])){
			$arrCond['MatchDateTime']['$lte']		=		$data['dateTo'] . ' :00';
		}
		
		if(isset($data['leagueID'])){
			$arrCond['KPLeagueID']					=		intval($data['leagueID']);
		}
		
		if(isset($data['zoneID'])){
			$arrCond['KPLeagueCountryID']			=		intval($data['zoneID']);
		}
		
		if(isset($data['status'])){
			$arrCond['Status']						=		intval($data['status']);
		}
		
		if(isset($data['year'])){
			$arrCond['XSLeagueYear']				=		intval($data['year']) . '/' . (intval($data['year'])+1);
		}
		
		if(isset($data['MatchStatus'])){
			$arrCond['MatchStatus']					=		$data['MatchStatus'];
		}
		
		$cursor						=		$this->collection->find($arrCond);
		if(isset($data['size'])){
			$cursor->limit(intval($data['size']));
		}
		if(isset($data['start'])){
			$cursor->skip(intval($data['start']));
		}
		if(isset($data['sordby'])){
			if(isset($data['typesort'])){
				if($data['typesort'] == 'asc'){
					$cursor->sort( array( $data['sordby'] => 1 ) );
				}else{
					$cursor->sort( array( $data['sordby'] => -1 ) );
				}
			}else{
				$cursor->sort( array( $data['sordby'] => -1 ) );
			}
		}else{
			$cursor->sort( array( 'MatchDateTime' => -1 ) );
		}

		$arrData	=	array();
		foreach ( $cursor as $id => $value ){
			$arrData[]	=	$value;
		}
		return $arrData;
	}
	
	function get_match($data){
		
		/*
			inside of $data
			- dateFrom
			- dateTo
			- leagueID
			- zoneID
			- year
			- size
			- status
			- MatchStatus
			
		*/
		$arrCond				=		array();
		
		if(isset($data['dateFrom'])){
			$arrCond['MatchDateTime']['$gte']		=		$data['dateFrom'] . ' 06:00:00';
		}
		
		if(isset($data['dateTo'])){
			$arrCond['MatchDateTime']['$lte']		=		date("Y-m-d 05:59:59",strtotime($data['dateTo'] . ' + 1 days'));
		}
		
		if(isset($data['leagueID'])){
			$arrCond['KPLeagueID']					=		intval($data['leagueID']);
		}
		
		if(isset($data['zoneID'])){
			$arrCond['KPLeagueCountryID']			=		intval($data['zoneID']);
		}
		
		if(isset($data['status'])){
			$arrCond['Status']						=		intval($data['status']);
		}
		
		if(isset($data['year'])){
			$arrCond['XSLeagueYear']				=		intval($data['year']) . '/' . (intval($data['year'])+1);
		}
		
		if(isset($data['MatchStatus'])){
			$arrCond['MatchStatus']					=		$data['MatchStatus'];
		}
		
		$cursor						=		$this->collection->find($arrCond);
		if(isset($data['size'])){
			$cursor->limit(intval($data['size']));
		}
		$cursor->sort( array( 'MatchDateTime' => -1 ) );

		$arrData	=	array();
		foreach ( $cursor as $id => $value ){
			$arrData[]	=	$value;
		}
		return $arrData;
	}
	
	function get_match_by_id($match_id){
		$arrCond				=		array();
		
		if($match_id<=0){
			return null;
		}

		$arrCond['id']			=		intval($match_id);
		
		$arrData				=		$this->collection->findOne($arrCond);
		return $arrData;
	}
	
	function add_match_by_id($arrData){
		$options = array(
			"w" => 1,
			"j" => true,
		);
		
		try {
			$this->collection->insert($arrData,$options);
		} catch (MongoCursorException $ex) {
			return false;
		}
		
		return true;
	}
	
	function del_match_by_id($match_id){
		$arrCond				=		array();
		
		if($match_id<=0){
			return null;
		}

		$arrCond['id']			=		intval($match_id);
		
		try {
			$cursor		=	$this->collection->remove($arrCond);
		} catch (MongoCursorException $ex) {
			return false;
		}
		
		return true;
	}
	
	function update($match_id,$arrData,$files){
		
		$this->upload_lib = $this->minizone->library('upload_lib');
		
		$tmpData	=	$this->collection->findOne(array( 'id' => intval($match_id) ));
		if(!$tmpData){
			return false;
		}
		unset($tmpData['_id']);
		
		$sumData	=	array();
		$isPicture	=	false;
		
		if( isset($arrData['MatchStatusType']) && isset($arrData['MatchStatus']) ){
			return false; // Must not come in same time.
		}
		// Status
		if(isset($arrData['Status'])){
			if((intval($arrData['Status'])!=1)&&(intval($arrData['Status'])!=0)){
				return false;
			}
			$sumData['Status']		=		intval($arrData['Status']);
		}
		
		// Status
		if(isset($arrData['MatchStatus'])){
			if(	(intval($tmpData['AutoMode'])==-1)	&&
				(intval($arrData['MatchStatus'])!=intval($tmpData['MatchStatus']))
			){
				if($arrData['MatchStatus'] == '1 HF'){
					$sumData['Minute'] = 0;
					$sumData['StartDateTime']	=	date('Y-m-d H:i:s');
				}else if($arrData['MatchStatus'] == '2 HF'){
					$sumData['Minute'] = 45;
					$sumData['StartDateTime']	=	date('Y-m-d H:i:s');
				}else if($arrData['MatchStatus'] == 'E/T'){
					$sumData['Minute'] = 90;
					$sumData['StartDateTime']	=	date('Y-m-d H:i:s');
				}
			}
			$sumData['MatchStatus']		=	$arrData['MatchStatus'];
			$tmpData['MatchStatus']		=	$arrData['MatchStatus']; //For AutoMode
		}
		
		if(isset($arrData['AutoMode'])){
			if((intval($arrData['AutoMode'])!=1)&&(intval($arrData['AutoMode'])!=0)&&(intval($arrData['AutoMode'])!=-1)){
				return false;
			}
			$sumData['AutoMode']		=		intval($arrData['AutoMode']);
			$tmpData['AutoMode']		=		intval($arrData['AutoMode']); //For Minute
			if(	(intval($arrData['AutoMode'])==-1)	&&
				(intval($arrData['AutoMode'])!=intval($tmpData['AutoMode']))
			){
				if($tmpData['MatchStatus'] == '1 HF'){
					$sumData['Minute']	=	0;
					$sumData['StartDateTime']	=	date('Y-m-d H:i:s');
				}else if($tmpData['MatchStatus'] == '2 HF'){
					$sumData['Minute']	=	45;
					$sumData['StartDateTime']	=	date('Y-m-d H:i:s');
				}else if($tmpData['MatchStatus'] == 'E/T'){
					$sumData['Minute']	=	90;
					$sumData['StartDateTime']	=	date('Y-m-d H:i:s');
				}
			}
		}
		
		if(isset($arrData['MatchStatusType'])){
			if($arrData['MatchStatusType'] == '0'){
				$sumData['MatchStatus'] = 'Sched';
			}else if($arrData['MatchStatusType'] == '1'){
				$sumData['MatchStatus'] = '1 HF';
				$sumData['Minute'] = 0;
				$sumData['StartDateTime']	=	date('Y-m-d H:i:s');
			}else if($arrData['MatchStatusType'] == '2'){
				$sumData['MatchStatus'] = 'H/T';
			}else if($arrData['MatchStatusType'] == '3'){
				$sumData['MatchStatus'] = '2 HF';
				$sumData['Minute'] = 45;
				$sumData['StartDateTime']	=	date('Y-m-d H:i:s');
			}else if($arrData['MatchStatusType'] == '4'){
				$sumData['MatchStatus'] = 'E/T';
				$sumData['Minute'] = 90;
				$sumData['StartDateTime']	=	date('Y-m-d H:i:s');
			}else if($arrData['MatchStatusType'] == '5'){
				$sumData['MatchStatus'] = 'Pen';
			}else if($arrData['MatchStatusType'] == '6'){
				$sumData['MatchStatus'] = 'Fin';
			}else if($arrData['MatchStatusType'] == '7'){
				$sumData['MatchStatus'] = 'Int';
			}else if($arrData['MatchStatusType'] == '8'){
				$sumData['MatchStatus'] = 'Abd';
			}else if($arrData['MatchStatusType'] == '9'){
				$sumData['MatchStatus'] = 'Post';
			}else if($arrData['MatchStatusType'] == '10'){
				$sumData['MatchStatus'] = 'Canc';
			}else{
				return false;
			}
		}
		
		if(isset($arrData['Minute'])){
			$sumData['Minute'] = intval($arrData['Minute']);
			if($tmpData['AutoMode']==-1){
				if($tmpData['MatchStatus'] == '1 HF'){
					$sumData['StartDateTime']	=	date('Y-m-d H:i:s',strtotime('-' . $sumData['Minute'] . 'minutes'));
				}else if($tmpData['MatchStatus'] == '2 HF'){
					$sumData['StartDateTime']	=	date('Y-m-d H:i:s',strtotime('-' . ($sumData['Minute']-45) . 'minutes'));
				}else if($tmpData['MatchStatus'] == 'E/T'){
					$sumData['StartDateTime']	=	date('Y-m-d H:i:s',strtotime('-' . ($sumData['Minute']-90) . 'minutes'));
				}
			}
		}
		
		// Scorers
		if(isset($arrData['inputNameScorers'])){
			$size	=	count($arrData['inputNameScorers']);
			for( $i=0 ; $i < $size ; $i++ ){
				if(($arrData['inputTeamScorers'][$i]!='1')&&($arrData['inputTeamScorers'][$i]!='2')){
					return false;
				}

				$addword	=	'';
				if(in_array($i, $arrData['inputET'])){
					$addword	=	$addword . 'ET';
				}
					
				if(in_array($i, $arrData['inputPN'])){
					$addword	=	$addword . 'Pen';
				}
					
				if(in_array($i, $arrData['inputOwnGoal'])){
					$addword	=	$addword . 'Own';
				}
					
				if($arrData['inputTeamScorers'][$i]=='1'){
					$arrData['inputNameScorers'][$i]	=	'(' . $addword . $arrData['inputMinScorers'][$i] . ')' . $arrData['inputNameScorers'][$i];
				}else{
					$arrData['inputNameScorers'][$i]	=	$arrData['inputNameScorers'][$i] . '(' . $addword . $arrData['inputMinScorers'][$i] . ')';
				}
			}
			$sumData['LastScorers']	=	$arrData['inputNameScorers'];
		}

		//listDeletePicture
		if(!empty($arrData['listDeletePicture']))
		{
			for($i=0,$sizedelete=count($arrData['listDeletePicture']) ; $i<$sizedelete ; $i++)
			{
				$isPicture = true;
				$indexDelete = (int)$arrData['listDeletePicture'][$i];
				if(!empty($tmpData['Picture'][$indexDelete]))
				{
					//unlink('../uploads/match/' . $arrData['matchdate'] . '/' . $dataMongo['Picture'][$indexDelete]);
					unlink('/var/web/football.kapook.com/html/uploads/match/' . $arrData['matchdate'] . '/' . $tmpData['Picture'][$indexDelete]);
					unset($tmpData['Picture'][$indexDelete]);
				}
			}
		}
		
		if((count($files['fileUpload']['error']) >= 1)&&($files['fileUpload']['error'][0]!=4)){
			//Create a match Folder
			if(!is_dir('/var/web/football.kapook.com/html/uploads/match/' . $arrData['matchdate'])){
				if(!mkdir('/var/web/football.kapook.com/html/uploads/match/' . $arrData['matchdate'],0777,true)){ return false; }
			}
			//Upload a new pic
			for( $i=0,$sizepic=count($files['fileUpload']['error']) ; $i<$sizepic ; $i++ ){
				$isPicture 			= 		true;
				$tmpPic['name']		=		$files['fileUpload']['name'][$i];
				$tmpPic['type']		=		$files['fileUpload']['type'][$i];
				$tmpPic['tmp_name']	=		$files['fileUpload']['tmp_name'][$i];
				$tmpPic['error']	=		$files['fileUpload']['error'][$i];
				$tmpPic['size']		=		$files['fileUpload']['size'][$i];
				
				$gtod 				= 		gettimeofday();
				$usec 				= 		$gtod['usec'];
				
				$tmpfileName		=		$match_id . '-' . date('YmdHis') . (int)$usec;
				$returnData			=		$this->upload_lib->uploadImage($tmpPic , $tmpfileName, '/var/web/football.kapook.com/html/uploads/match/' . $arrData['matchdate'] . '/');
				if( $returnData[0] != 1 ){
					return false;
				}
				$tmpData['Picture'][]	=	$returnData[1];
			}
		}
		
		if($isPicture){
			$sumData['Picture']	= $tmpData['Picture'];
		}
		
		//FB OG Image
		if($arrData['PictureOGSelect']=='1'){ //old image
			//$arrData['PictureOG']	=	$tmpData['PictureOG'];
		}else if($arrData['PictureOGSelect']=='2'){ //new image
			$returnData		=		$this->upload_lib->uploadImage($files['PictureOG'] , $match_id, "/var/web/football.kapook.com/html/uploads/match/og/");
			if( $returnData[0] != 1 ){
				return false;
			}
			$sumData['PictureOG']	=	$returnData[1];
		}else if($arrData['PictureOGSelect']=='3'){ // no OG
			unlink('/var/web/football.kapook.com/html/uploads/match/og/' . $tmpData['PictureOG']);
			$sumData['PictureOG']	=	'';
		}

		//FTScore
		if( (isset($arrData['Team1FTScore'])) && (isset($arrData['Team2FTScore'])) ){
			$sumData['FTScore']			=	$arrData['Team1FTScore'] . '-' . $arrData['Team2FTScore'];
			$sumData['Team1FTScore']	=	intval($arrData['Team1FTScore']);
			$sumData['Team2FTScore']	=	intval($arrData['Team2FTScore']);
		}
		//ETScore
		if( (isset($arrData['Team1ETScore'])) && (isset($arrData['Team2ETScore'])) ){
			$sumData['ETScore']		=	$arrData['Team1ETScore'] . '-' . $arrData['Team2ETScore'];
		}else if($arrData['isEditET']){
			$sumData['ETScore']		=	'-';
		}
		//PNScore
		if( (isset($arrData['Team1PNScore'])) && (isset($arrData['Team2PNScore'])) ){
			$sumData['PNScore']		=	$arrData['Team1PNScore'] . '-' . $arrData['Team2PNScore'];
		}else if($arrData['isEditPN']){
			$sumData['PNScore']		=	'-';
		}

		//TeamOdds & Odds
		if(isset($arrData['TeamOdds'])){
			if($arrData['TeamOdds'] == '-1'){
				$sumData['TeamOdds']	=	0;
				$sumData['Odds']		=	-1.0;
			}elseif($arrData['TeamOdds'] == '0'){
				if($sumData['Odds']==-1.0){
					$sumData['Odds']		=	-1.0;
				}else{
					$sumData['Odds']		=	0.0;
				}
				$sumData['TeamOdds']	=	0;
			}else{
				$sumData['TeamOdds']	=	intval($arrData['TeamOdds']);
				$sumData['Odds']		=	(float)$arrData['Odds'];
			}
		}

		//Other
		if(isset($arrData['TVLiveList'])){
			$sumData['TVLiveList']		=	$arrData['TVLiveList'];
		}
		if(isset($arrData['Analysis'])){
			if(strlen($arrData['Analysis'])&&($arrData['Analysis']!='<br />')){
				$sumData['Analysis']		=	$arrData['Analysis'];
			}else{
				$sumData['Analysis']	=	null;
			}
		}

		if(isset($arrData['Result'])){
			if(strlen($arrData['Result'])&&($arrData['Result']!='<br />')){
				$sumData['Result']		=	$arrData['Result'];
			}else{
				$sumData['Result']		=	null;
			}
		}
		if(isset($arrData['question'])){
			if(strlen($arrData['question'])&&($arrData['question']!='<br />')){
				$sumData['question']		=	$arrData['question'];
			}else{
				$sumData['question']		=	null;
			}
		}
		if(isset($arrData['EmbedCode'])){
			if(strlen($arrData['EmbedCode'])){
				$sumData['EmbedCode']		=	$arrData['EmbedCode'];
			}
		}
		if(isset($arrData['Predict'])){
			if(strlen($arrData['Predict'])&&($arrData['Predict']!='<br />')){
				$sumData['Predict']		=	$arrData['Predict'];
			}else{
				$sumData['Predict']		=	null;
			}
			
		}
		if(isset($arrData['TeamSelect'])){
			if(strlen($arrData['TeamSelect'])){
				$sumData['TeamSelect']		=	intval($arrData['TeamSelect']);
			}else{
				$sumData['TeamSelect']		=	0;
			}
		}
		if(isset($arrData['Team1KPID'])){
			$sumData['Team1KPID']		=	intval($arrData['Team1KPID']);
		}
		if(isset($arrData['Team2KPID'])){
			$sumData['Team2KPID']		=	intval($arrData['Team2KPID']);
		}
		if(isset($arrData['Team1'])){
			$sumData['Team1']			=	$arrData['Team1'];
		}
		if(isset($arrData['Team2'])){
			$sumData['Team2']			=	$arrData['Team2'];
		}

		$options = array(
			"w" => 1,
		);
		
		try {
			$this->collection->update(array("id" => intval($match_id)), array( '$set' => $sumData ), $options);
		} catch (MongoCursorException $ex) {
			return false;
		}

		return true;
	}
	
	function delete_dup(){
		$ops = array(
			array(
				'$group' => array(
					'_id' => '$id',
					'count' => array('$sum' => 1)
				),
			),
			array(
				'$sort' => array('count' => -1)
			)
		);
		try {
			$data	=	$this->collection->aggregate($ops);
		} catch (MongoCursorException $ex) {
			return false;
		}
		if(isset($data['result'])){
			return $data['result'];
		}else{
			return false;
		}
	}
	
}
