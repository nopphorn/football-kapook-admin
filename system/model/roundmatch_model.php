<?php

class RoundMatch_model extends Mongo_model
{

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct()
    {
        parent::__construct('football');
		$this->collection			=		$this->db->football_comp_round_match;
    }
	
	function create_RoundMatch($comp_id,$data){
		if(!is_string($data['name'])){
			return false;
		}
		if(intval($comp_id)<=0){
			return false;
		}
		$data['comp_id'] = intval($comp_id);
		
		//Add a new Competition
		$cursor						=	$this->collection->find();
		if($cursor->count(true)<=0){
			$data['id']			=	1;
		}else{
			$cursor->sort( array( 'id' => -1 ) );
			$cursor->limit(1);
			$cursor->next();
			$dataTmp				=	$cursor->current();
			$data['id']				=	((int)$dataTmp['id']+1);
		}
		
		$cursor							=	$this->collection->find(array( 'comp_id' => intval($comp_id) ));
		if($cursor->count(true)<=0){
			$data['order']			=	1;
		}else{
			$cursor->sort( array( 'order' => -1 ) );
			$cursor->limit(1);
			$cursor->next();
			$dataTmp				=	$cursor->current();
			$data['order']			=	((int)$dataTmp['order']+1);
		}
		
		$options = array(
			"w" => 1,
			"j" => true,
		);
		
		try {
			$this->collection->insert($data,$options);
		} catch (MongoCursorException $ex) {
			return false;
		}
		
		return true;
	}
	
	function update_RoundMatch($id,$data){
		$data['id']					=	intval($id);
		$data['comp_id']			=	intval($data['comp_id']);
		
		$options = array(
			"w" => 1
		);
		
		try {
			$this->collection->update(array("id" => intval($id)), array('$set' => $data), $options);
		} catch (MongoCursorException $ex) {
			return false;
		}
		
		return true;
	}
	
	function delete_RoundMatch($id = -1){
		
		$options = array(
			"justOne" 	=> 	true
		);
		
		$arrData['id']	=	intval($id);
		
		try {
			$this->collection->remove($arrData,$options);
			
		} catch (MongoCursorException $ex) {
			return false;
		}
		
		return true;

	}
	
	function clear_by_comp_id($comp_id){
		try {
			$cursor		=	$this->collection->remove(
				array( 'comp_id' => intval($comp_id))
			);
		} catch (MongoCursorException $ex) {
			return false;
		}
		return true;
	}
	
	function loadListRound($comp_id){
	
		$cursor		=	$this->collection->find(array( 'comp_id' => intval($comp_id)));
		$cursor->sort( array( 'order' => 1 ) );
		$arrData	=	array();
		foreach ( $cursor as $id => $value ){
			$arrData[]	=	$value;
		}
		return $arrData;
	}
	
	function loadByID($id){
	
		$tmpData	=	$this->collection->findOne( array( 'id' => intval($id) ) );
		return $tmpData;
	}
	
	function order_up($round_id){
		
		// get itself
		$dataRound		=	$this->collection->findOne(array( 'id' => intval($round_id) ));
		// get upper
		$cursor			=	$this->collection->find(
			array(
				'order' => array( '$lt' => $dataRound['order']),
				'comp_id' => $dataRound['comp_id'],
			)
		);
		if($cursor->count(true)<=0){
			return false;
		}
		$cursor->sort( array( 'order' => -1 ) );
		$cursor->limit(1);
		$cursor->next();
		$dataTmp					=	$cursor->current();
		// swap value
		$tmpOrder = $dataTmp['order'];
		$dataTmp['order'] = $dataRound['order'];
		$dataRound['order'] = $tmpOrder;
		
		$options = array(
			"w" => 1
		);
		
		try {
			$this->collection->update(array("id" => intval($dataTmp['id'])), $dataTmp, $options);
			$this->collection->update(array("id" => intval($dataRound['id'])), $dataRound, $options);
		} catch (MongoCursorException $ex) {
			return false;
		}
		return true;
		
	}
	
	function order_down($round_id){
		
		// get itself
		$dataRound		=	$this->collection->findOne(array( 'id' => intval($round_id) ));
		// get upper
		$cursor						=	$this->collection->find(
			array(
				'order' => array( '$gt' => $dataRound['order']),
				'comp_id' => $dataRound['comp_id'],
			)
		);
		if($cursor->count(true)<=0){
			return false;
		}
		$cursor->sort( array( 'order' => 1 ) );
		$cursor->limit(1);
		$cursor->next();
		$dataTmp					=	$cursor->current();
		// swap value
		$tmpOrder = $dataTmp['order'];
		$dataTmp['order'] = $dataRound['order'];
		$dataRound['order'] = $tmpOrder;
		
		$options = array(
			"w" => 1
		);
		
		try {
			$this->collection->update(array("id" => intval($dataTmp['id'])), $dataTmp, $options);
			$this->collection->update(array("id" => intval($dataRound['id'])), $dataRound, $options);
		} catch (MongoCursorException $ex) {
			return false;
		}
		return true;
		
	}
	
}
