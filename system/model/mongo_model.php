<?php if (!defined('MINIZONE')) exit;

class Mongo_model 
{
    var $db;
    var $current_collection;
    var $default_filed = array();
    
    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct($name='default') 
    {
        $config = minizone::getzone()->config('mongo', $name);
        
        try {
            $mongo = new Mongo($config['host'].':'.$config['port']);
        } catch (Exception $exc) {
            die('ไม่สามารถเชื่อมต่อฐานข้อมูลได้ค่ะ Error::'.$exc->getMessage());
        }
        

        $this->db = $mongo->selectDB($config['db']);

        /* -- ========= Set : Minizone ========= -- */
        $this->minizone = minizone::getzone();
        $this->memcache_lib = $this->minizone->library('memcache_lib');
    }
   
    /**
	 * Get
	 *
	 * Get Data by Condition .
	 * This method will look in any model directories available to CI.
	 * return array() or Null 
	
	 *
	 * @param	array $condition condition to query data.
         * @param	array $filed filed to select for this query.
     */
    function Get($condition=array(),$filed=array()) {
        if(!is_null($filed)) {
            $filed_select = $filed;
        } else {
            $filed_select = $this->default_filed;
        }
        return $this->current_collection->findOne($condition,$filed_select);
    }
    
    function get_by_id($_id,$filed=Null) {
        
        if(!is_object($_id)) {
            $con['_id'] = new MongoId((String)$_id);
        }
        return $this->Get($con,$filed); 
    }
    
    function GetAll($condition=array(),$limit=10,$page=1,$sort=array()) {
        
        if($page == '') $page = 1;
        
        $skip = ($page-1)*$limit;
        
        $result = $this->current_collection->find($condition)->skip($skip)->limit($limit)->sort($sort);
        
        if($result->count() > 0) {
            $path = $this->minizone->pathsURI();
            $data['data'] = array_values(iterator_to_array($result));
            $data['total_rows'] = $result->count();
            $data['limit'] = $limit;
            $data['paged'] = $page;
        } else {
            $data = false;
        }
        
        return $data;
        
    }
    
    ///////////////////////////////////////////////// Generel : Select list (All) /////////////////////////////////////////////////
    protected function _listLimit($collection, $filter=array(), $select=array(), $limit=10, $sort=array('_id'=>-1))
    {
        $result = $this->$collection->find($filter, $select)->skip(0)->limit($limit)->sort($sort);
        $data = iterator_to_array($result);
        
        /* -- Return -- */
        return $data;
    }
    
    ///////////////////////////////////////////////// Generel : Select list (All) /////////////////////////////////////////////////
    protected function _listAll($collection, $filter=array(), $select=array(), $sort=array('_id'=>-1))
    {
        $result = $this->$collection->find($filter, $select)->sort($sort);
        $data = iterator_to_array($result);
        
        /* -- Return -- */
        return $data;
    }
    
    ///////////////////////////////////////////////// Generel : Select list (Page) /////////////////////////////////////////////////
    protected function _list($collection, $url, $filter=array(), $select=array(), $page=1, $limit=10, $sort=array('_id'=>-1))
    {
        /* -- Load : Library -- */
        $this->pager_admin_lib = $this->minizone->library('pager_admin_lib');
        
        if ($page=='') $page = 1;
        
        $skip = abs($page-1)*$limit;
        $result = $this->$collection->find($filter, $select)->skip($skip)->limit($limit)->sort($sort);
        
        $data['total'] = $result->count();
        list($data['pg'], $limit, $skip) = $this->pager_admin_lib->page_css($limit, $data['total'], $url, $page);
        
        $data['data'] = iterator_to_array($result);
        
        /* -- Return -- */
        return $data;
    }
    
    ///////////////////////////////////////////////// Generel : Update (Auto ID) /////////////////////////////////////////////////
    protected function _auto_inc($collection, $filter, $update=array('$inc'=>array('seq'=>1)), $return=null, $option=null)
    {
        if ($return==null) $return = array('seq'=>1);
        if ($option==null) $option = array('new'=>true);
        
        /* -- Return -- */
        return $this->db->$collection->findAndModify($filter, $update, $return, $option);
    }
    
    ///////////////////////////////////////////////// Update : Filename /////////////////////////////////////////////////
    function update_filename($id, $filename, $type, $prefix)
    {
        switch ($type) {
            case 'admin' :
                $data['picture.avatar'] = $filename;
                $collection = 'admin';
                break;
            
            case 'slide' :
                $data['picture'] = $filename;
                $collection = 'slide';
                break;
            
            case 'star' :
                if ($prefix == 'top') $data['picture.topstar'] = $filename;
                else if ($prefix == 'large') $data['picture.large'] = $filename;
                else $data['picture.thumb'] = $filename;
                $collection = 'star';
                break;
                
            case 'channel' :
                $data['picture'] = $filename;
                $collection = 'channel';
                break;
        }
        
        /* -- DB -- */
        $filter = array('_id' => new MongoID($id));
        $update = array('$set' => $data);
        $option = array('upsert' => true);
        
        /* -- Return -- */
        return $this->db->$collection->update($filter, $update, $option);
    }
    
}
/* End of file my_model.php */
/* Location: ./system/model/my_model.php */