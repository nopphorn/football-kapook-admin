<?php

class Search_match_model extends Mongo_model
{

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct()
    {
        parent::__construct('football');
		$this->collection		=		$this->db->football_search_match;
    }
	
	function get_list(){
		$cursor					=		$this->collection->find();
		$cursor->sort( array( 'create_date' => 1 ) );
		
		$arrData	=	array();
		foreach ( $cursor as $id => $value ){
			$arrData[]	=	$value;
		}
		return $arrData;
	}
	
	function add($arrData){
	
		//Add a new League
		/*
		- id
		- zone
		- league
		- year
		- matchtimesearch
		- datefrom
		- dateto
		- status
		- matchstatus
		- sort
		- order
		- name
		- create_date[new]
		*/
		
		if(!strlen($arrData['name'])){
			return false;
		}
		
		$cursor						=	$this->collection->find();
		if($cursor->count(true)<=0){
			$arrData['id']			=	1;
		}else{
			$cursor->sort( array( 'id' => -1 ) );
			$cursor->limit(1);
			$cursor->next();
			$dataTmp					=	$cursor->current();
			$arrData['id']				=	((int)$dataTmp['id']+1);
		}
		
		$arrData['zone']					=	intval($arrData['zone']);
		$arrData['league']					=	intval($arrData['league']);
		$arrData['year']					=	intval($arrData['year']);
		$arrData['matchtimesearch']			=	intval($arrData['matchtimesearch']);
		$arrData['status']					=	intval($arrData['status']);
		$arrData['order']					=	intval($arrData['order']);
		
		$arrData['create_date']				=	date('Y-m-d H:i:s');
		
		$options = array(
			"w" => 1,
			"j" => true,
		);
		
		try {
			$this->collection->insert($arrData,$options);
		} catch (MongoCursorException $ex) {
			return false;
		}
		
		return true;
		
	}
	
	function remove($id){
		try {
			$cursor		=	$this->collection->remove(
				array( 'id' => intval($id) )
			);
		} catch (MongoCursorException $ex) {
			return false;
		}
		
		return true;
	}
	
}
