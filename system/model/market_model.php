<?php

class Market_model extends Mongo_model
{

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct(){
        parent::__construct('football');
		$this->collection			=		$this->db->football_player_market;
		
    }
	
	function load_all(){
		$cursor		=	$this->collection->find()->sort( array( 'id' => -1 ) );
		$arrData	=	array();
		
		foreach ( $cursor as $id => $value ){
			$arrData[]	=	$value;
		}

		return $arrData;
	}
	
	function load($size = 10,$page = 1){

		$cursor		=	$this->collection->find()->sort( array( 'id' => -1 ) );
	
		$cursor->limit($size);
		$cursor->skip(($page-1)*$size);	
		
		$arrData				=	array();
		$arrData["list"] 		= 	array();
		
		$arrData["size_all"] 	= 	$cursor->count();
		$arrData["size"] 		= 	$cursor->count(true);
		$arrData["page"] 		=	$page;
		if (($arrData["size_all"] % $size)==0) {
			$arrData["page_all"] 		=	floor($arrData["size_all"]/$size);
		}else{
			$arrData["page_all"] 		=	floor($arrData["size_all"]/$size)+1;
		}

		foreach ( $cursor as $id => $value ){
			$arrData["list"][]	=	$value;
		}

		return $arrData;

	}
	
	function loadSortByTime($size = 10,$page = 1){

		$cursor		=	$this->collection->find()->sort( array( 'Date' => -1 ) );
	
		$cursor->limit($size);
		$cursor->skip(($page-1)*$size);	
		
		$arrData				=	array();
		$arrData["list"] 		= 	array();
		
		$arrData["size_all"] 	= 	$cursor->count();
		$arrData["size"] 		= 	$cursor->count(true);
		$arrData["page"] 		=	$page;
		if (($arrData["size_all"] % $size)==0) {
			$arrData["page_all"] 		=	floor($arrData["size_all"]/$size);
		}else{
			$arrData["page_all"] 		=	floor($arrData["size_all"]/$size)+1;
		}

		foreach ( $cursor as $id => $value ){
			$arrData["list"][]	=	$value;
		}

		return $arrData;

	}
	
	function loadByID($id){
		
		$tmpData	=	$this->collection->findOne( array( 'id' => intval($id) ) );
		return $tmpData;

	}
	
	function create($arrData){
	
		if(intval($arrData['PlayerID']) <=0 ){
			return false;
		}

		$cursor						=	$this->collection->find();
		if($cursor->count(true)<=0){
			$arrData['id']			=	1;
		}else{
			$cursor->sort( array( 'id' => -1 ) );
			$cursor->limit(1);
			$cursor->next();
			$dataTmp					=	$cursor->current();
			$arrData['id']				=	((int)$dataTmp['id']+1);
		}
		$arrData['PlayerID']			=	intval($arrData['PlayerID']);
		$arrData['FromTeamKPID']		=	intval($arrData['FromTeamKPID']);
		$arrData['ToTeamKPID']			=	intval($arrData['ToTeamKPID']);
		
		$options = array(
			"w" => 1,
			"j" => true
		);
		
		try {
			$this->collection->insert($arrData,$options);
		} catch (MongoCursorException $ex) {
			return false;
		}
		
		return true;

	}
	
	function update($id,$arrData){

		$tmpData	=	$this->collection->findOne(array( 'id' => intval($id) ));
		
		if(!$tmpData){
			return false;
		}
		
		$arrData['id']	=	intval($id);
		
		$arrData['PlayerID']			=	intval($arrData['PlayerID']);
		$arrData['FromTeamKPID']		=	intval($arrData['FromTeamKPID']);
		$arrData['ToTeamKPID']			=	intval($arrData['ToTeamKPID']);

		$options = array(
			"w" => 1,
		);
		
		try {
			$this->collection->update(array("id" => intval($arrData["id"])), array('$set' => $arrData), $options);
		} catch (MongoCursorException $ex) {
			return false;
		}
		
		return true;

	}
	
	function delete($id = -1){
		
		$options = array(
			"justOne" 	=> 	true
		);
		
		$arrData['id']	=	intval($id);
		
		try {
			$this->collection->remove($arrData,$options);
			
		} catch (MongoCursorException $ex) {
			return false;
		}
		
		return true;

	}
	
}
