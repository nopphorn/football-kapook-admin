<?php

class Comp_model extends Mongo_model
{

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct()
    {
        parent::__construct('football');
		$this->collection			=		$this->db->football_comp;
		
    }

    function create($arrData){
		
		if(!is_string($arrData['nameTH'])){
			return false;
		}
		if(!is_string($arrData['nameTHShort'])){
			return false;
		}
		if(!is_string($arrData['nameURL'])){
			return false;
		}
		if(!ctype_alnum($arrData['nameURL'])){
			return false;
		}
		
		//Add a new Competition
		$cursor						=	$this->collection->find();
		if($cursor->count(true)<=0){
			$arrData['id']			=	1;
		}else{
			$cursor->sort( array( 'id' => -1 ) );
			$cursor->limit(1);
			$cursor->next();
			$dataTmp					=	$cursor->current();
			$arrData['id']				=	((int)$dataTmp['id']+1);
		}
		
		$arrData['nameURLRootRoute']	=	'';
		$arrData['SEOTitle']			=	'';
		$arrData['SEOdescription']		=	'';
		$arrData['SEOkeywords']			=	'';
		$arrData['FBtitle']				=	'';
		$arrData['FBdescription']		=	'';
		$arrData['FBimg']				=	'';
		
		//News
		$arrData['kapookfootball_News_Country']					=	array();
		$arrData['kapookfootball_News_CountryOtherKeyword']		=	array();
		$arrData['kapookfootball_News_CountryOtherKeyword2']	=	array();
		$arrData['kapookfootball_News_CountryOtherKeyword3']	=	array();
		
		//Table And Program Selected Tabs
		$arrData['TabTable']		=	-1;
		$arrData['TabProgram']		=	-1;
		
		$options = array(
			"w" => 1,
			"j" => true,
		);
		
		try {
			$this->collection->insert($arrData,$options);
		} catch (MongoCursorException $ex) {
			return false;
		}
		
		return true;

	}
	
	function update($id,$arrData,$files){
	
		$this->upload_lib = $this->minizone->library('upload_lib');
	
		$tmpData	=	$this->collection->findOne(array( 'id' => intval($id) ));
		
		if(!$tmpData){
			return false;
		}
		
		$arrData['id']	=	intval($id);
		
		if(!is_string($arrData['nameTH'])){
			return false;
		}
		if(!is_string($arrData['nameTHShort'])){
			return false;
		}
		if(!is_string($arrData['nameURL'])){
			return false;
		}
		if(!ctype_alnum($arrData['nameURL'])){
			return false;
		}
		
		if($arrData['FBimgSelect']=='1'){ //old image
			$arrData['FBimg']	=	$tmpData['FBimg'];
		}else if($arrData['FBimgSelect']=='2'){ //new image
			
			$returnData		=		$this->upload_lib->uploadImage($files['FBimg'] , $id, "/var/web/football.kapook.com/html/uploads/comp/og/");
			
			if( $returnData[0] != 1 ){
				return false;
			}
			
			$arrData['FBimg']	=	BASE_HREF . 'uploads/comp/og/' . $returnData[1];
			
		}else{ // no OG
			$arrData['FBimg']	=	'';
		}
		
		if(isset($arrData['FBimgSelect'])){
			unset($arrData['FBimgSelect']);
		}
		
		$options = array(
			"w" => 1,
		);
		
		try {
			$this->collection->update(array("id" => intval($arrData["id"])), array( '$set' => $arrData ), $options);
		} catch (MongoCursorException $ex) {
			return false;
		}
		
		return true;

	}
	
	function update_tabtable($id,$round_id = -1){
	
		$tmpData	=	$this->collection->findOne(array( 'id' => intval($id) ));
		
		if(!$tmpData){
			return false;
		}
		
		$arrData['id']			=	intval($id);
		$arrData['TabTable']	=	intval($round_id);
		
		$options = array(
			"w" => 1,
		);
		
		try {
			$this->collection->update(array("id" => intval($arrData["id"])), array( '$set' => $arrData ), $options);
		} catch (MongoCursorException $ex) {
			return false;
		}
		
		$this->memcache_lib->delete('Comp-Info-' . $tmpData['nameURL']);
		$this->memcache_lib->delete('ProgramTournament-' . $tmpData['nameURL']);
		
		return true;

	}
	
	function update_tabprogram($id,$round_id = -1){
		
		$tmpData	=	$this->collection->findOne(array( 'id' => intval($id) ));
		
		if(!$tmpData){
			return false;
		}
		
		$arrData['id']			=	intval($id);
		$arrData['TabProgram']	=	intval($round_id);
		
		$options = array(
			"w" => 1,
		);
		
		try {
			$this->collection->update(array("id" => intval($arrData["id"])), array( '$set' => $arrData ), $options);
		} catch (MongoCursorException $ex) {
			return false;
		}
		
		$this->memcache_lib->delete('Comp-Info-' . $tmpData['nameURL']);
		$this->memcache_lib->delete('ProgramTournament-' . $tmpData['nameURL']);
		
		return true;

	}
	
	function update_news($id,$arrData){
	
		$tmpData	=	$this->collection->findOne(array( 'id' => intval($id) ));
		
		if(!$tmpData){
			return false;
		}
		
		$arrData['id']	=	intval($id);
		
		if(!isset($arrData['kapookfootball_News_Country'])){
			$arrData['kapookfootball_News_Country']			=		array();
		}
		
		if(strlen($arrData['kapookfootball_News_CountryOtherKeyword'])>0){
			$arrData['kapookfootball_News_CountryOtherKeyword']			=		explode( ',' , $arrData['kapookfootball_News_CountryOtherKeyword'] );
		}else{
			$arrData['kapookfootball_News_CountryOtherKeyword']			=		array();
		}
		
		if(strlen($arrData['kapookfootball_News_CountryOtherKeyword2'])>0){
			$arrData['kapookfootball_News_CountryOtherKeyword2']		=		explode( ',' , $arrData['kapookfootball_News_CountryOtherKeyword2'] );
		}else{
			$arrData['kapookfootball_News_CountryOtherKeyword2']			=		array();
		}
		
		if(strlen($arrData['kapookfootball_News_CountryOtherKeyword3'])>0){
			$arrData['kapookfootball_News_CountryOtherKeyword3']		=		explode( ',' , $arrData['kapookfootball_News_CountryOtherKeyword3'] );
		}else{
			$arrData['kapookfootball_News_CountryOtherKeyword3']			=		array();
		}

		$options = array(
			"w" => 1,
		);
		
		try {
			$this->collection->update(array("id" => intval($arrData["id"])), array( '$set' => $arrData ), $options);
		} catch (MongoCursorException $ex) {
			return false;
		}
		
		return true;

	}
	
	function delete($id = -1){
		
		$options = array(
			"justOne" 	=> 	true
		);
		
		$arrData['id']	=	intval($id);
		
		try {
			$this->collection->remove($arrData,$options);
			
		} catch (MongoCursorException $ex) {
			return false;
		}
		
		return true;

	}
	
	function load(){
		
		$cursor		=	$this->collection->find()->sort( array( 'id' => -1 ) );
		$arrData	=	array();
		
		foreach ( $cursor as $id => $value ){
			$arrData[]	=	$value;
		}
		
		return $arrData;

	}
	
	function loadByID($id){
		
		$tmpData	=	$this->collection->findOne( array( 'id' => intval($id) ) );
		return $tmpData;

	}
	
	function loadByName($cupname){
		
		$tmpData	=	$this->collection->findOne( array( 'nameURL' => strval($cupname) ) );
		return $tmpData;

	}
	
}
