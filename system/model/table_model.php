<?php

class Table_model extends Mongo_model
{

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct()
    {
        parent::__construct('football');
		$this->collection				=		$this->db->football_comp_table;
    }
	
	function loadByID($id){
		
		$tmpData	=	$this->collection->findOne( array( 'id' => intval($id) ) );
		return $tmpData;

	}
	
	function create_tableSelect($comp_id,$round_id,$arrData){

		$cursor						=	$this->collection->find();
		if($cursor->count(true)<=0){
			$arrData['id']			=	1;
		}else{
			$cursor->sort( array( 'id' => -1 ) );
			$cursor->limit(1);
			$cursor->next();
			$dataTmp					=	$cursor->current();
			$arrData['id']				=	((int)$dataTmp['id']+1);
		}
		
		$arrData['comp_id']				=	intval($comp_id);
		$arrData['round_id']			=	intval($round_id);
		$arrData['type']				=	1;
		
		if(isset($arrData['league_list'])){
			foreach($arrData['league_list'] as $i => $value){
				$arrData['league_list'][$i]		=		intval($value);
			}
		}else{
			$arrData['league_list']		=	array();
		}
		
		if(isset($arrData['match_list'])){
			foreach($arrData['match_list'] as $i => $value){
				$arrData['match_list'][$i]		=		intval($value);
			}
		}else{
			$arrData['match_list']		=	array();
		}
		
		$team_list_arr					=	array();
		if(isset($arrData['team_list'])){
			foreach($arrData['team_list'] as $i => $value){
				$team_list_arr[$i]['team_id']	=		intval($value);
				$team_list_arr[$i]['league_id']	=		intval($arrData['team_league_list'][$i]);
			}
		}
		if(isset($arrData['team_league_list'])){
			unset($arrData['team_league_list']);
		}
		$arrData['team_list']			=	$team_list_arr;
		
		$cursor							=	$this->collection->find(array( 'comp_id' => intval($comp_id) ));
		if($cursor->count(true)<=0){
			$arrData['order']			=	1;
		}else{
			$cursor->sort( array( 'order' => -1 ) );
			$cursor->limit(1);
			$cursor->next();
			$dataTmp					=	$cursor->current();
			$arrData['order']			=	((int)$dataTmp['order']+1);
		}
		
		$options = array(
			"w" => 1,
			"j" => true,
		);
		
		try {
			$this->collection->insert($arrData,$options);
		} catch (MongoCursorException $ex) {
			return false;
		}
		
		return true;
	}
	
	function update_tableSelect($id,$arrData){
		
		$arrData['id']					=	intval($id);
		$arrData['comp_id']				=	intval($arrData['comp_id']);
		$arrData['round_id']			=	intval($arrData['round_id']);
		
		if(isset($arrData['league_list'])){
			foreach($arrData['league_list'] as $i => $value){
				$arrData['league_list'][$i]		=		intval($value);
			}
		}else{
			$arrData['league_list']		=	array();
		}
		
		if(isset($arrData['match_list'])){
			foreach($arrData['match_list'] as $i => $value){
				$arrData['match_list'][$i]		=		intval($value);
			}
		}else{
			$arrData['match_list']		=	array();
		}
		
		$team_list_arr					=	array();
		if(isset($arrData['team_list'])){
			foreach($arrData['team_list'] as $i => $value){
				$team_list_arr[$i]['team_id']	=		intval($value);
				$team_list_arr[$i]['league_id']	=		intval($arrData['team_league_list'][$i]);
			}
		}
		if(isset($arrData['team_league_list'])){
			unset($arrData['team_league_list']);
		}
		$arrData['team_list']			=	$team_list_arr;
		
		$options = array(
			"w" => 1
		);
		
		try {
			$this->collection->update(array("id" => intval($id)), array( '$set' => $arrData), $options);
		} catch (MongoCursorException $ex) {
			return false;
		}
		
		return true;
	}
	
	function create_tableDummy($comp_id,$round_id,$arrData){

		$cursor						=	$this->collection->find();
		if($cursor->count(true)<=0){
			$arrData['id']			=	1;
		}else{
			$cursor->sort( array( 'id' => -1 ) );
			$cursor->limit(1);
			$cursor->next();
			$dataTmp					=	$cursor->current();
			$arrData['id']				=	((int)$dataTmp['id']+1);
		}
		
		$arrData['comp_id']				=	intval($comp_id);
		$arrData['type']				=	2;
		$arrData['type_order']			=	intval($arrData['type_order']);
		$arrData['round_id']			=	intval($round_id);
		
		foreach($arrData['TeamData'] as $key => $tmpData){
			$arrData['TeamData'][$key]['win']			=		intval($tmpData['win']);
			$arrData['TeamData'][$key]['draw']			=		intval($tmpData['draw']);
			$arrData['TeamData'][$key]['lose']			=		intval($tmpData['lose']);
			$arrData['TeamData'][$key]['goal_for']		=		intval($tmpData['goal_for']);
			$arrData['TeamData'][$key]['goal_against']	=		intval($tmpData['goal_against']);
			$arrData['TeamData'][$key]['sp_point']		=		intval($tmpData['sp_point']);
		}
		
		$cursor							=	$this->collection->find(array( 'comp_id' => intval($comp_id) ));
		if($cursor->count(true)<=0){
			$arrData['order']			=	1;
		}else{
			$cursor->sort( array( 'order' => -1 ) );
			$cursor->limit(1);
			$cursor->next();
			$dataTmp					=	$cursor->current();
			$arrData['order']			=	((int)$dataTmp['order']+1);
		}
		
		$options = array(
			"w" => 1,
			"j" => true,
		);
		
		try {
			$this->collection->insert($arrData,$options);
		} catch (MongoCursorException $ex) {
			return false;
		}
		return true;
		
	}
	
	function update_tableDummy($id,$arrData){
		
		$arrData['id']					=	intval($id);
		$arrData['comp_id']				=	intval($arrData['comp_id']);
		$arrData['round_id']			=	intval($arrData['round_id']);
		$arrData['type_order']			=	intval($arrData['type_order']);
		
		foreach($arrData['TeamData'] as $key => $tmpData){
			$arrData['TeamData'][$key]['win']			=		intval($tmpData['win']);
			$arrData['TeamData'][$key]['draw']			=		intval($tmpData['draw']);
			$arrData['TeamData'][$key]['lose']			=		intval($tmpData['lose']);
			$arrData['TeamData'][$key]['goal_for']		=		intval($tmpData['goal_for']);
			$arrData['TeamData'][$key]['goal_against']	=		intval($tmpData['goal_against']);
			$arrData['TeamData'][$key]['sp_point']		=		intval($tmpData['sp_point']);
		}
		
		$options = array(
			"w" => 1
		);
		
		try {
			$this->collection->update(array("id" => intval($id)), array( '$set' => $arrData), $options);
		} catch (MongoCursorException $ex) {
			return false;
		}
		return true;
		
	}
	
	function create_tableTournament($comp_id,$round_id,$arrData){

		$cursor						=	$this->collection->find();
		if($cursor->count(true)<=0){
			$arrData['id']			=	1;
		}else{
			$cursor->sort( array( 'id' => -1 ) );
			$cursor->limit(1);
			$cursor->next();
			$dataTmp					=	$cursor->current();
			$arrData['id']				=	((int)$dataTmp['id']+1);
		}
		
		$arrData['comp_id']				=	intval($comp_id);
		$arrData['type']				=	3;
		$arrData['round_id']			=	intval($round_id);
		
		$SizeTeam						=	intval($arrData['SizeTeam']);
		$SizeTeam						=	$SizeTeam/2;
		
		$RoundData						=	array();
		$isFinal						=	false;
		
		while(1){
			$tmpData					=	array();
			
			if($SizeTeam<=1){
				$isFinal = true;
			}
			
			if(($isFinal)&&(intval($arrData['isThird'])==1)){
				$SizeTeam = 2;
			}
			for($i=0;$i<$SizeTeam;$i++){
				$tmpMatch				=	array();
				
				$tmpData[]				=	array(
					"Team1" => "",
					"Team2" => "",
					"Team1KPID" => 0,
					"Team2KPID" => 0,
					"Team1FTScore" => 0,
					"Team2FTScore" => 0,
					"Team1Logo" => "",
					"Team2Logo" => "",
					"Team1URL" => "",
					"Team2URL" => "",
					"TeamWinner" => 0,
					"MatchData" => array()
				);
			}
			$RoundData[]		=		$tmpData;
			if($isFinal){
				break;
			}
			$SizeTeam = $SizeTeam/2;
		}
		
		$arrData['RoundData']	=		$RoundData;
		
		$cursor							=	$this->collection->find(array( 'comp_id' => intval($comp_id) ));
		if($cursor->count(true)<=0){
			$arrData['order']			=	1;
		}else{
			$cursor->sort( array( 'order' => -1 ) );
			$cursor->limit(1);
			$cursor->next();
			$dataTmp					=	$cursor->current();
			$arrData['order']			=	((int)$dataTmp['order']+1);
		}
		
		$options = array(
			"w" => 1,
			"j" => true,
		);
		
		try {
			$this->collection->insert($arrData,$options);
		} catch (MongoCursorException $ex) {
			return false;
		}
		return true;
	}
	
	function update_tableTournament($id,$arrData){
		$arrData['id']					=	intval($id);
		$arrData['comp_id']				=	intval($arrData['comp_id']);
		$arrData['round_id']			=	intval($arrData['round_id']);

		$options = array(
			"w" => 1
		);
		
		try {
			$this->collection->update(array("id" => intval($id)), array( '$set' => $arrData), $options);
		} catch (MongoCursorException $ex) {
			return false;
		}
		return true;
	}
	
	function update_tableTournamentMatch($arrData){
		/*$arrData['id']					=	intval($arrData['id']);
		$arrData['comp_id']				=	intval($arrData['comp_id']);
		$arrData['round_id']			=	intval($arrData['round_id']);*/
		
		$round_index					=	intval($arrData['round_index']);
		$match_index					=	intval($arrData['match_index']);
		
		
		$tmpTable	=	$this->loadByID(intval($arrData['id']));
		if($tmpTable['type']!=3){
			return false;
		}
		unset($tmpTable['_id']);
		
		$dataMatch		=		array();
		$dataMatch['Team1']				=		$arrData['Team1'];
		$dataMatch['Team2']				=		$arrData['Team2'];
		$dataMatch['Team1FTScore']		=		intval($arrData['Team1FTScore']);
		$dataMatch['Team2FTScore']		=		intval($arrData['Team2FTScore']);
		$dataMatch['Team1KPID']			=		intval($arrData['Team1KPID']);
		$dataMatch['Team2KPID']			=		intval($arrData['Team2KPID']);
		$dataMatch['Team1Logo']			=		$arrData['Team1Logo'];
		$dataMatch['Team2Logo']			=		$arrData['Team2Logo'];
		$dataMatch['Team1URL']			=		$arrData['Team1URL'];
		$dataMatch['Team2URL']			=		$arrData['Team2URL'];
		$dataMatch['TeamWinner']		=		intval($arrData['TeamWinner']);
		$dataMatch['MatchData']			=		array();
		
		foreach($arrData['MatchStatus_arr'] as $key => $value){
			$tmpData					=		array();
			$tmpData['Team1']			=		$arrData['Team1_arr'][$key];
			$tmpData['Team2']			=		$arrData['Team2_arr'][$key];
			$tmpData['Team1FTScore']	=		$arrData['Team1FTScore_arr'][$key];
			$tmpData['Team2FTScore']	=		$arrData['Team2FTScore_arr'][$key];
			$tmpData['Team1Logo']		=		$arrData['Team1Logo_arr'][$key];
			$tmpData['Team2Logo']		=		$arrData['Team2Logo_arr'][$key];
			$tmpData['Team1URL']		=		$arrData['Team1URL_arr'][$key];
			$tmpData['Team2URL']		=		$arrData['Team2URL_arr'][$key];
			$tmpData['MatchDateTime']	=		$arrData['MatchDateTime_arr'][$key];
			$tmpData['MatchStatus']		=		$arrData['MatchStatus_arr'][$key];
			$tmpData['MatchURL']		=		$arrData['MatchURL_arr'][$key];
			$dataMatch['MatchData'][]	=		$tmpData;
		}
		
		$tmpTable['RoundData'][$round_index][$match_index]		=		$dataMatch;
		
		$options = array(
			"w" => 1
		);
		
		try {
			$this->collection->update(array("id" => intval($arrData['id'])), array( '$set' => $tmpTable ), $options);
		} catch (MongoCursorException $ex) {
			echo 'WAT';
			return false;
		}
		return true;
	}
	
	function delete($id){
	
		try {
			$cursor		=	$this->collection->remove(
				array( 'id' => intval($id) )
			);
		} catch (MongoCursorException $ex) {
			return false;
		}
		
		return true;
		
	}
	
	function loadListTable($comp_id,$round_id = 0){
		
		if($round_id == 0){
			$cursor		=	$this->collection->find(array( 'comp_id' => intval($comp_id)));	
		}else{
			$cursor		=	$this->collection->find(array( 'comp_id' => intval($comp_id), 'round_id' => intval($round_id) ));	
		}
		$cursor->sort( array( 'order' => 1 ) );
		$arrData	=	array();
		foreach ( $cursor as $id => $value ){
			$arrData[]	=	$value;
		}
		return $arrData;
	}
	
	function loadListTeamForTable($table_id){
	
		$dataTable		=	$this->collection->findOne(array( 'id' => intval($table_id) , 'type' => 2 ));
		return $dataTable['TeamData'];
		
	}
	
	function order_up($table_id){
		
		// get itself
		$dataTable		=	$this->collection->findOne(array( 'id' => intval($table_id) ));
		// get upper
		$cursor			=	$this->collection->find(
			array(
				'order' => array( '$lt' => $dataTable['order']),
				'comp_id' => $dataTable['comp_id'],
			)
		);
		if($cursor->count(true)<=0){
			return false;
		}
		$cursor->sort( array( 'order' => -1 ) );
		$cursor->limit(1);
		$cursor->next();
		$dataTmp					=	$cursor->current();
		// swap value
		$tmpOrder = $dataTmp['order'];
		$dataTmp['order'] = $dataTable['order'];
		$dataTable['order'] = $tmpOrder;
		
		$options = array(
			"w" => 1
		);
		
		try {
			$this->collection->update(array("id" => intval($dataTmp['id'])), $dataTmp, $options);
			$this->collection->update(array("id" => intval($dataTable['id'])), $dataTable, $options);
		} catch (MongoCursorException $ex) {
			return false;
		}
		return true;
		
	}
	
	function order_down($table_id){
		
		// get itself
		$dataTable		=	$this->collection->findOne(array( 'id' => intval($table_id) ));
		// get upper
		$cursor						=	$this->collection->find(
			array(
				'order' => array( '$gt' => $dataTable['order']),
				'comp_id' => $dataTable['comp_id'],
			)
		);
		if($cursor->count(true)<=0){
			return false;
		}
		$cursor->sort( array( 'order' => 1 ) );
		$cursor->limit(1);
		$cursor->next();
		$dataTmp					=	$cursor->current();
		// swap value
		$tmpOrder = $dataTmp['order'];
		$dataTmp['order'] = $dataTable['order'];
		$dataTable['order'] = $tmpOrder;
		
		$options = array(
			"w" => 1
		);
		
		try {
			$this->collection->update(array("id" => intval($dataTmp['id'])), $dataTmp, $options);
			$this->collection->update(array("id" => intval($dataTable['id'])), $dataTable, $options);
		} catch (MongoCursorException $ex) {
			return false;
		}
		return true;
		
	}
	
	function clear_Table($comp_id,$round_id = null){
		
		if($round_id == null){
			try {
				$cursor		=	$this->collection->remove(
					array( 'comp_id' => intval($comp_id) )
				);
			} catch (MongoCursorException $ex) {
				return false;
			}
		}else{
			try {
				$cursor		=	$this->collection->remove(
					array( 'comp_id' => intval($comp_id),'round_id' => intval($round_id) )
				);
			} catch (MongoCursorException $ex) {
				return false;
			}
		}
		
		return true;
		
	}
	
}
