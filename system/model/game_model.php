<?php

class Game_model extends Mongo_model
{

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct()
    {
        parent::__construct('football');
		$this->collection_ranking_season		=		$this->db->football_ranking_season;
    }
	
	function get_list_user_season(){
		$ops = array(
			array( '$match' => array( 'season' => '2015' ) ),
			array(
				'$group'=> array (
					'_id' => '$id',
					'total' => array( '$sum' =>  1 )
				)
			),
			array( '$sort' => array ( 'total' => -1 ) ),
			array( '$match' => array( 'total' => array( '$gte' => 2 )) )
		);
		$results = $this->collection_ranking_season->aggregate($ops);
		return $results;
	}
	
	function delete_duplicate($id,$amount){
		while ($amount > 1) {
			$this->collection_ranking_season->remove(array('id' => $id,'season' => '2015'), array("justOne" => true));
			$amount--;
		}
	}
}
