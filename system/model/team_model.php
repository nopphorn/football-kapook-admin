<?php

class Team_model extends Mongo_model
{

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct()
    {
        parent::__construct('football');
		$this->collection			=		$this->db->football_team;
    }
	
	function file_get_curl($url){
	
		$data_post	=	parse_url($url, PHP_URL_PATH);
	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_post);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_COOKIESESSION, true);
		//curl_setopt($ch, CURLOPT_COOKIE, "uid={$_COOKIE['uid']};is_login={$_COOKIE['is_login']}");
		curl_setopt($ch, CURLOPT_TIMEOUT, 120);
		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			$this->errNO = curl_errno($ch);
			$this->errMSG = curl_error($ch);
			return false;
		}
		curl_close($ch);
		return $result;
		
	}
	
	function get_team_by_keyword($keyword = ''){

		$arrCond				=		array();
		$arrCond['$or'] 		= 		array(
			array('NameEN' 		=> new MongoRegex("/^$keyword/i")),
			array('NameTH' 		=> new MongoRegex("/^$keyword/i")),
			array('NameTHShort' => new MongoRegex("/^$keyword/i")),
		);
		/*---------------------------------------------------------------------------------------*/
		
		$cursor					=		$this->collection->find($arrCond);
		$cursor->sort( array( 'NameEN' => 1 ) );
		
		$arrData	=	array();
		foreach ( $cursor as $id => $value ){
			$arrData[]	=	$value;
		}
		return $arrData;
		
	}
	
	function loadByID($id){
		
		$tmpData	=	$this->collection->findOne( array( 'id' => intval($id) ) );
		return $tmpData;

	}
	
	function update($team_id,$arrData,$files){
		
		$this->upload_lib = $this->minizone->library('upload_lib');
		
		$tmpData	=	$this->collection->findOne(array( 'id' => intval($team_id) ));
		if(!$tmpData){
			return false;
		}
		unset($tmpData['_id']);
		
		$sumData	=	array();
		
		// NameTH
		if(isset($arrData['NameTH'])){
			$sumData['NameTH']		=	$arrData['NameTH'];
		}
		// NameTHShort
		if(isset($arrData['NameTHShort'])){
			$sumData['NameTHShort']		=	$arrData['NameTHShort'];
		}
		// Status
		if(isset($arrData['Status'])){
			if((intval($arrData['Status'])!=1)&&(intval($arrData['Status'])!=0)){
				return false;
			}
			$sumData['Status']		=		intval($arrData['Status']);
		}
		// Info
		if(isset($arrData['Info'])){
			$sumData['Info']		=	$arrData['Info'];
		}
		// EmbedCode
		if(isset($arrData['EmbedCode'])){
			$sumData['EmbedCode']		=	$arrData['EmbedCode'];
		}
		//
		if(isset($files['UploadLogo'])){
			$filename 		= 		str_replace(' ','-',$tmpData['NameEN']);
			$returnData		=		$this->upload_lib->uploadImage($files['UploadLogo'] , $filename, "/var/web/football.kapook.com/html/uploads/logo/");
			
			if( $returnData[0] != 1 ){
				return false;
			}
		}

		$options = array(
			"w" => 1,
		);
		
		try {
			$this->collection->update(array("id" => intval($team_id)), array( '$set' => $sumData ), $options);
		} catch (MongoCursorException $ex) {
			return false;
		}

		return true;
	}
	
	/*
	function loadByID($id){
		
		if (intval($id)) {

			$url = 'http://52.74.132.163/api/id/' . $id;
            $result = json_decode($this->file_get_curl($url),true);
			
			echo $result['id'];

            return $result;
        }
        else {
            return false;
        }

	}*/
	
}
