<?php

class League_model extends Mongo_model
{

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct()
    {
        parent::__construct('football');
		$this->collection		=		$this->db->football_league;
    }
	
	function get_league($zone_id,$year){
		
		$arrCond				=		array();
		
		if($zone_id>0){
			$arrCond['KPZoneID']	=		intval($zone_id);
		}
		
		if($year>0){
			$arrCond['Year']		=		intval($year) . '/' . (intval($year)+1);
		}
		
		$arrCond['Year']
		
		$cursor						=		$this->collection->find($arrCond);
		$cursor->sort( array( 'NameEN' => 1 ) );
		
		$arrData	=	array();
		foreach ( $cursor as $id => $value ){
			$arrData[]	=	$value;
		}
		return $arrData;
	}
	
	function get_league_by_id($league_id){
		
		
		$arrCond				=		array();
		
		if($league_id<=0){
			return null;
		}

		$arrCond['id']			=		intval($league_id);
		
		$arrData				=		$this->collection->findOne($arrCond);
		return $arrData;
	}
	
	function update_by_id($arrData){

		$tmpData	=	$this->collection->findOne(array( 'id' => intval($arrData['id']) ));
		
		if(!$tmpData){
			return false;
		}
		
		$arrData['id']			=	intval($arrData['id']);
		$arrData['Priority']	=	intval($arrData['Priority']);
		$arrData['Status']		=	intval($arrData['Status']);
		$arrData['AutoStatus']	=	intval($arrData['AutoStatus']);
		
		if(!is_string($arrData['NameTH'])){
			return false;
		}
		
		if(!is_string($arrData['NameTHShort'])){
			return false;
		}
		
		$options = array(
			"w" => 1,
		);
		
		try {
			$this->collection->update(array("id" => intval($arrData["id"])), array( '$set' => $arrData ), $options);
		} catch (MongoCursorException $ex) {
			return false;
		}
		
		return true;

	}
	
	function update($arrData){
	
		foreach($arrData['NameTH'] as $key => $value){

			$tmpData	=	$this->collection->findOne(array( 'id' => intval($key) ));
			
			if(!$tmpData){
				return false;
			}
			
			$tmpData['Priority']	=	intval($arrData['Priority'][$key]);
			$tmpData['Status']		=	intval($arrData['Status'][$key]);
			$tmpData['AutoStatus']	=	intval($arrData['AutoStatus'][$key]);
			
			if(!is_string($value)){
				return false;
			}
			
			$tmpData['NameTH']			=	$value;
			$tmpData['NameTHShort']		=	$arrData['NameTHShort'][$key];
			
			$options = array(
				"w" => 1,
			);
			
			try {
				$this->collection->update(array("id" => intval($key)), array( '$set' => $tmpData ), $options);
			} catch (MongoCursorException $ex) {
				return false;
			}
		
		}
		
		return true;

	}
}
