<?php

if (!defined('MINIZONE'))
    exit;

class Home extends My_con {

    var $view;
    var $minizone;

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct() {
        parent::__construct();
		$this->view->assign('site_id', 1);
    }

    ///////////////////////////////////////////////// Index /////////////////////////////////////////////////
    function index() {

        $this->view->assign('mem_lib', $this->mem_lib);
		
		$breadcrum[0]['text'] = 'Home';
        $breadcrum[0]['active'] = true;
        parent::_setBreadcrum($breadcrum);
		
		$this->_header();
		$this->view->render($this->root_view.'/index.tpl');
		$this->_footer();
    }
}
