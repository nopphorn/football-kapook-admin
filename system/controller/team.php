<?php

if (!defined('MINIZONE'))
    exit;

class Team extends My_con {

    var $view;
    var $minizone;

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct() {
        parent::__construct();
		$this->zone_model = $this->minizone->model('zone_model');
		$this->team_model = $this->minizone->model('team_model');
		$this->match_model = $this->minizone->model('match_model');
		$this->view->assign('site_id', 6);
    }
	
	function file_get_curl($url){
		$data_post	=	parse_url($url, PHP_URL_PATH);
	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_post);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_COOKIESESSION, true);
		//curl_setopt($ch, CURLOPT_COOKIE, "uid={$_COOKIE['uid']};is_login={$_COOKIE['is_login']}");
		curl_setopt($ch, CURLOPT_TIMEOUT, 120);
		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			$this->errNO = curl_errno($ch);
			$this->errMSG = curl_error($ch);
			return false;
		}
		curl_close($ch);
		return $result;
	}

    ///////////////////////////////////////////////// Operation_For Json /////////////////////////////////////////////////
	function index(){
		$this->view->assign('mem_lib', $this->mem_lib);
		
		$dataZone	=	$this->zone_model->get_zone();
		$this->view->assign('dataZone', $dataZone);
		
		$breadcrum[0]['text'] = 'Home';
        $breadcrum[0]['link'] = BASE_HREF_ADMIN;
		$breadcrum[1]['text'] = 'ทีม';
		$breadcrum[1]['active'] = true;
		parent::_setBreadcrum($breadcrum);

		$this->_header();
		$this->view->render($this->root_view.'/team/index.tpl');
		$this->_footer();
	}
	
	function detail($team_id = '') {
		$this->view->assign('mem_lib', $this->mem_lib);
		
		$dataTeam	=	$this->team_model->loadByID(intval($team_id));
		if(!$dataTeam){
			header( "location: " . BASE_HREF . "api/adminfootball/team" );
			exit;
		}
		$Logo 						= 		str_replace(' ','-',$dataTeam['NameEN']).'.png';
		$Logo_MC					=		$this->mem_lib->get('Football2014-Team-Logo-' . $Logo);
		if($Logo_MC==true){
			$dataTeam['Logo'] 		= 		'http://football.kapook.com/uploads/logo/' . $Logo;
		}else{
			$dataTeam['Logo'] 		= 		'http://football.kapook.com/uploads/logo/default.png';
		}
		
		$this->view->assign('dataTeam', $dataTeam);
		
		$breadcrum[0]['text'] = 'Home';
        $breadcrum[0]['link'] = BASE_HREF_ADMIN;
		$breadcrum[1]['text'] = 'ทีม';
		$breadcrum[1]['link'] = BASE_HREF_ADMIN . 'team';
		if(strlen($dataTeam['NameTH'])){
			$breadcrum[2]['text'] = $dataTeam['NameTH'];
		}else if (strlen($dataTeam['NameTHShort'])){
			$breadcrum[2]['text'] = $dataTeam['NameTHShort'];
		}else{
			$breadcrum[2]['text'] = $dataTeam['NameEN'];
		}
		$breadcrum[2]['active'] = true;
		parent::_setBreadcrum($breadcrum);

		$this->_header();
		$this->view->render($this->root_view.'/team/detail.tpl');
		$this->_footer();
	}
	
	function getList(){
		$return		=		$this->team_model->get_team_by_keyword('');
		
		if($_REQUEST['type_output']==1){
			$returnData		=		array();
			foreach($return as $tmpData){
				$returnData[]		=		array(
					'value'		=>		$tmpData['id'],
					'name'		=>		$tmpData['NameEN']
				);
			}
		}else{
			$returnData		=		$return;
		}
		
		if($_REQUEST['type']=='json'){
			echo json_encode($returnData);
		}else{
			var_dump($returnData);
		}
	}
	
	function update($team_id = ''){
		header('Content-Type: application/json');
		if($this->team_model->update(intval($team_id),$_REQUEST,$_FILES)){
			$dataTeam	=	$this->team_model->loadByID(intval($team_id));
			echo json_encode(array( 'is_success' => 1 , 'data' => $_REQUEST , 'data_lastest' => $dataTeam ));
		}else{
			echo json_encode(array( 'is_success' => 0 , 'data' => $_REQUEST ));
		}
	}
	
	function update_detail($team_id = ''){
		$arrPOST		=		array();
		if($this->team_model->update(intval($team_id),$_REQUEST,$_FILES)){
			$dataTeam	=	$this->team_model->loadByID(intval($team_id));
			$arrPOST['success_update_team']		=	1;
			
			$url = "http://202.183.165.24/football/purge/uploads/logo/" . str_replace(' ','-',$dataTeam['NameEN']) . ".png";
			$result		=	$this->file_get_curl($url);
			$url = "http://202.183.165.178/football/purge/uploads/logo/" . str_replace(' ','-',$dataTeam['NameEN']) . ".png";
			$result		=	$this->file_get_curl($url);
			
		}else{
			$arrPOST['error_update_team']		=	1;
		}
		
		$this->view->assign('path', 'team/detail/' . $team_id);
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function getByLeague($id){

		//Get Match List
		$data_arr = array( "leagueID" => $id );
		$returnMatchlist = $this->match_model->get_match($data_arr);
		
		$listTmpTeam = array();
		$returnData = array();
		
		foreach($returnMatchlist as $tmpData){
			if(!in_array($tmpData["Team1KPID"], $listTmpTeam)){
				$listTmpTeam[] = intval($tmpData["Team1KPID"]);
			}
			if(!in_array($tmpData["Team2KPID"], $listTmpTeam)){
				$listTmpTeam[] = intval($tmpData["Team2KPID"]);
			}
		}
		
		foreach($listTmpTeam as $team_id){
			$tmpTeam					=		$this->team_model->loadByID($team_id);
			
			$Logo 						= 		str_replace(' ','-',$tmpTeam['NameEN']).'.png';
			$Logo_MC					=		$this->mem_lib->get('Football2014-Team-Logo-' . $Logo);
			if($Logo_MC==true){
				$tmpTeam['Logo'] 		= 		'http://football.kapook.com/uploads/logo/' . $Logo;
			}else{
				$tmpTeam['Logo'] 		= 		'http://football.kapook.com/uploads/logo/default.png';
			}
			
			$tmpTeam['url'] 			= 		strtolower('http://football.kapook.com/team-'.$tmpTeam['NameEN']);
			$tmpTeam['url']				=		preg_replace('/\s+/', '-', $tmpTeam['url']);
			
			$returnData[] 				= 		$tmpTeam;
			
		}
		
		if($_REQUEST['type']=='json'){
			echo json_encode($returnData);
		}else{
			var_dump($returnData);
		}
		
	}
	
}
