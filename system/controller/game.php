<?php

if (!defined('MINIZONE'))
    exit;

class Game extends My_con {

    var $view;
    var $minizone;

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct() {
        parent::__construct();
		$this->game_model = $this->minizone->model('game_model');
		
		//$this->view->assign('site_id', 8);
    }

    ///////////////////////////////////////////////// Index /////////////////////////////////////////////////
	function index() {
		//echo '5555';
		$data = $this->game_model->get_list_user_season();
		foreach($data['result'] as $item){
			$this->game_model->delete_duplicate(intval($item['_id']),intval($item['total']));
			echo $item['_id'] . "<br/>";
		}
	}
}
