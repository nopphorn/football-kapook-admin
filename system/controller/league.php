<?php

if (!defined('MINIZONE'))
    exit;

class League extends My_con {

    var $view;
    var $minizone;

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct() {
        parent::__construct();
		$this->league_model = $this->minizone->model('league_model');
		$this->zone_model = $this->minizone->model('zone_model');
		$this->view->assign('site_id', 5);
    }

	///////////////////////////////////////////////// Operation /////////////////////////////////////////////////
	function getjson($zone_id = 0,$year = 0){
		$returnData		=		$this->league_model->get_league($zone_id,$year);
		echo json_encode($returnData);
	}
	
	///////////////////////////////////////////////// Index /////////////////////////////////////////////////
	function index() {

        $this->view->assign('mem_lib', $this->mem_lib);
		
		$dataZone	=	$this->zone_model->get_zone();
		$this->view->assign('dataZone', $dataZone);
		
		$breadcrum[0]['text'] = 'Home';
        $breadcrum[0]['link'] = BASE_HREF_ADMIN;
		$breadcrum[1]['text'] = 'ลีก';
		$breadcrum[1]['active'] = true;
		parent::_setBreadcrum($breadcrum);

		$this->_header();
		$this->view->render($this->root_view.'/league/index.tpl');
		$this->_footer();

    }
	
	function update(){
		$arrPOST		=		array();
		$isSuccess		=		false;
		$this->view->assign('path', 'league');
		if(is_array($_REQUEST['NameTH'])){
			$isSuccess	= $this->league_model->update($_REQUEST);
		}else{
			$isSuccess	= $this->league_model->update_by_id($_REQUEST);
		}
		if($isSuccess){
			$arrPOST['success_update_league']		=	1;
		}else{
			$arrPOST['error_update_league']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
		
	}
	
}
