<?php

if (!defined('MINIZONE'))
    exit;

class Zone extends My_con {

    var $view;
    var $minizone;

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct() {
        parent::__construct();
		$this->zone_model = $this->minizone->model('zone_model');
		$this->view->assign('site_id', 4);
    }

    ///////////////////////////////////////////////// Index /////////////////////////////////////////////////
    function index() {

        $this->view->assign('mem_lib', $this->mem_lib);
		
		$breadcrum[0]['text'] = 'Home';
        $breadcrum[0]['link'] = BASE_HREF_ADMIN;
		$breadcrum[1]['text'] = 'โซน';
		$breadcrum[1]['active'] = true;
		parent::_setBreadcrum($breadcrum);

		$this->_header();
		$this->view->render($this->root_view.'/zone/index.tpl');
		$this->_footer();

    }
	
	function get_zone(){
		
		$returnData = $this->zone_model->get_zone();
	
		if($_REQUEST['type']=='json'){
			echo json_encode($returnData);
		}else{
			var_dump($returnData);
		}
	}
	
	function update(){
		$arrPOST		=		array();
		$isSuccess		=		false;
		$this->view->assign('path', 'zone');
		if(is_array($_REQUEST['NameTH'])){
			$isSuccess	= $this->zone_model->update($_REQUEST);
		}else{
			$isSuccess	= $this->zone_model->update_by_id($_REQUEST);
		}
		if($isSuccess){
			$arrPOST['success_update_zone']		=	1;
		}else{
			$arrPOST['error_update_zone']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
		
	}
	
	
}
