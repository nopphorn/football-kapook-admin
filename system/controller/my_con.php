<?php if (!defined('MINIZONE')) exit;

class My_con 
{
    var $view; 
    var $minizone;
    
    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct($mustLogin = true) 
    {
     
        /* -- Set : Header -- */
        header ('Content-type: text/html; charset=utf-8');
        
        /* -- ========= Set : Minizone ========= -- */
        $this->minizone = minizone::getzone();
        
        /* -- ========= Load : Libraries ========= -- */
        $this->mem_lib = $this->minizone->library('memcache_lib'); 
        
        $this->login_lib 			= $this->minizone->library('getlogin_lib'); 
        $this->getIP_lib 			= $this->minizone->library('getIP_lib');
		$this->bundit_authen_lib 	= $this->minizone->library('bundit_authen_lib');
        $_SERVER['REMOTE_ADDR']=$this->getIP_lib->getUserIP();
        /* -- ========= Load : Model ========= -- */
        /* -- ========= View ========= -- */
        $this->view = $this->minizone->view();
        $this->root_view = FOLDER_VIEW_DESKTOP;
		
        $this->view->assign('mem_lib', $this->mem_lib);
        $this->view->assign('minizone', $this->minizone);
        $this->view->assign('view', $this->view);
        $this->view->assign('login_obj', $this->login_lib);
        $this->view->assign('root_view', $this->root_view);

		if($mustLogin){
		
			if(!is_bool(Login) and $_REQUEST['error'] > 0) {
				$this->view->assign('loginError', $_REQUEST['error']);
			}
			
			if(!$this->bundit_authen_lib->checkCookie()){
				print('Login now or forever hold your clicks...1');
				exit;
			}
			
			$url			= 	'http://timesheet.kapook.com/api/list_all.php?timesheetUserID=' . $_COOKIE["UserID"] . '&GenerateKey='. $_COOKIE["GenerateKey"] ;
			$data 			=	json_decode(file_get_contents($url), true);

			if(empty($data['Data'][0]['AllowServiceID'])){
				print('Login now or forever hold your clicks...2');
				exit;
			}

			$listAllowID	=	explode( "," , $data['Data'][0]['AllowServiceID'] );

			if (!in_array('90', $listAllowID)) {
				print('Login now or forever hold your clicks...3');
				exit;
			}
		
		}

    }
    ///////////////////////////////////////////////// Header /////////////////////////////////////////////////
    protected function _detect_device() 
    {
        $this->mobile_detect_lib = $this->minizone->library('mobile_detect_lib');
        if ($this->mobile_detect_lib->isMobile() && $_GET['view'] != 'full') {
            $this->root_view = FOLDER_VIEW_MOBILE;
        } else {
            $this->root_view = FOLDER_VIEW_DESKTOP;
        } 
    }
    
    ///////////////////////////////////////////////// Header /////////////////////////////////////////////////
    protected function _header($template='header.tpl') 
    {
       
        /* -- Load : Template (s) -- */
        $this->view->render($this->root_view.'/'.$template);
    }
    
    ///////////////////////////////////////////////// Footer /////////////////////////////////////////////////
    protected function _footer($template='footer.tpl') 
    {
        
        /* -- Load : Template (s) -- */
        $this->view->render($this->root_view.'/'.$template);
    }
    
    ///////////////////////////////////////////////// Blank /////////////////////////////////////////////////
    protected function _blank($msg, $redirect=TRUE,$template) 
    {
        $this->view->assign('msg', $msg);
        if ($redirect == TRUE)
            $this->view->assign('url', $_SERVER['HTTP_REFERER']);
        
        /* -- Load : Template (s) -- */
        $this->_header($template);
        $this->view->render($this->root_view.'/includes/blank.tpl');
        $this->_footer();
    }
    
    ///////////////////////////////////////////////// Set : Social Search /////////////////////////////////////////////////
    protected function _setSocial($data)
    {
        /* -- Set : Data (SEO) -- */
        if ($data['seo_title'] != '') $seo['seo_title'] = $data['seo_title'];
        if ($data['seo_description'] != '') $seo['seo_description'] = $data['seo_description'];
        if ($data['seo_keywords'] != '') $seo['seo_keywords'] = $data['seo_keywords'];

        /* -- Set : Data (SMO) -- */
        if ($data['fb_title'] != '') $smo['fb_title'] = $data['fb_title'];
        if ($data['fb_description'] != '') $smo['fb_description'] = $data['fb_description'];
        if ($data['fb_img'] != '') $smo['fb_img'] = "{$data['fb_img']}";

        /* -- Assign : Data (SEO & SMO) -- */
        $this->view->assign('seo', $seo);
        $this->view->assign('smo', $smo);
    }
    
    ///////////////////////////////////////////////// Set : Social Search /////////////////////////////////////////////////
    protected function _setBreadcrum($data=null) 
    {
        /* -- Set : Data (SEO) -- */
        if (is_array($data)) {
            $i = 0;
            foreach ($data as $p) {
                $arr_breadcrum[$i]['text'] = $p['text'];
                $arr_breadcrum[$i]['link'] = $p['link'];
                $arr_breadcrum[$i]['active'] = $p['active'];
                $i++;
            }
        } else {
            $arr_breadcrum[0]['text'] = 'หน้าแรกฟุตบอล';
            $arr_breadcrum[0]['link'] = BASE_HREF;
        }
        
        foreach ($arr_breadcrum as $b) {
            if ($b['active']) 
                $breadcrum .= '<li class="active">'.$b['text'].'</li>';
            elseif ($b['link']) 
                $breadcrum .= '<li><a href="'.$b['link'].'">'.$b['text'].'</a></li>';
            else
                $breadcrum .= '<li>'.$b['text'].'</li>';
        }

        /* -- Assign -- */
        $this->view->assign('breadcrum', $breadcrum);
    }
    
    
    
}
/* End of file my_con.php */
/* Location: ./system/controller/my_con.php */