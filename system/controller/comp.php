<?php

if (!defined('MINIZONE'))
    exit;

class Comp extends My_con {

    var $view;
    var $minizone;

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct() {
        parent::__construct();
		$this->comp_model = $this->minizone->model('comp_model');
		$this->team_model = $this->minizone->model('team_model');
		$this->table_model = $this->minizone->model('table_model');
		$this->zone_model = $this->minizone->model('zone_model');
		$this->matchcomp_model = $this->minizone->model('matchcomp_model');
		$this->match_model = $this->minizone->model('match_model');
		$this->league_model = $this->minizone->model('league_model');
		$this->scorer_model = $this->minizone->model('scorer_model');
		$this->player_model = $this->minizone->model('player_model');
		$this->roundmatch_model = $this->minizone->model('roundmatch_model');
		$this->roundtable_model = $this->minizone->model('roundtable_model');
		$this->view->assign('site_id', 2);
    }

    ///////////////////////////////////////////////// Index /////////////////////////////////////////////////
    function index() {

        $this->view->assign('mem_lib', $this->mem_lib);
		
		$arrComp	=	$this->comp_model->load();
		$this->view->assign('arrComp', $arrComp);
		
		$breadcrum[0]['text'] = 'Home';
        $breadcrum[0]['link'] = BASE_HREF_ADMIN;
		$breadcrum[1]['text'] = 'การแข่งขัน';
		$breadcrum[1]['active'] = true;
		parent::_setBreadcrum($breadcrum);

		$this->_header();
		$this->view->render($this->root_view.'/comp/index.tpl');
		$this->_footer();
    }
	
	private function compare_point($a,$b)
	{
		if ($a['point'] == $b['point'])
		{
			if($a['goal_dif'] == $b['goal_dif'])
			{
				if ($a['goal_for'] == $b['goal_for'])
				{
					if ($a['order'] == $b['order'])
					{
						if((!isset($a['NameEN']))||(!isset($b['NameEN'])))
							return 0;
						elseif ($a['NameEN'] == $b['NameEN']){
							return 0;
						}
						return ($a['NameEN'] > $b['NameEN']) ? -1 : 1;
					}
					return ($a['order'] > $b['order']) ? -1 : 1;
				}
				return ($a['goal_for'] > $b['goal_for']) ? -1 : 1;
			}
			return ($a['goal_dif'] > $b['goal_dif']) ? -1 : 1;
		}
		return ($a['point'] > $b['point']) ? -1 : 1;
	}
	
	///////////////////////////////////////////////// Info /////////////////////////////////////////////////
	function info($id) {
		
		
		
        $this->view->assign('mem_lib', $this->mem_lib);
		
		$Comp	=	$this->comp_model->loadByID($id);
		$this->view->assign('dataComp', $Comp);
		
		if(!$Comp){
			header( "location: " . BASE_HREF . "api/adminfootball/comp/" );
			exit;
		}
		
		$breadcrum[0]['text'] = 'Home';
        $breadcrum[0]['link'] = BASE_HREF_ADMIN;
		$breadcrum[1]['text'] = 'การแข่งขัน';
		$breadcrum[1]['link'] = BASE_HREF_ADMIN . 'comp';
		$breadcrum[2]['text'] = 'ข้อมูลการแข่งขัน ' . $Comp['nameTHShort'];
		$breadcrum[2]['active'] = true;
		parent::_setBreadcrum($breadcrum);

		$this->_header();
		$this->view->render($this->root_view.'/comp/info.tpl');
		$this->_footer();
		
    }
	///////////////////////////////////////////////// Info /////////////////////////////////////////////////
	function news($id) {
		
        $this->view->assign('mem_lib', $this->mem_lib);
		
		$Comp	=	$this->comp_model->loadByID($id);
		$this->view->assign('dataComp', $Comp);
		
		if(!$Comp){
			header( "location: " . BASE_HREF . "api/adminfootball/comp/" );
			exit;
		}
		
		$breadcrum[0]['text'] = 'Home';
        $breadcrum[0]['link'] = BASE_HREF_ADMIN;
		$breadcrum[1]['text'] = 'การแข่งขัน';
		$breadcrum[1]['link'] = BASE_HREF_ADMIN . 'comp';
		$breadcrum[2]['text'] = 'ข่าวการแข่งขัน ' . $Comp['nameTHShort'];
		$breadcrum[2]['active'] = true;
		parent::_setBreadcrum($breadcrum);

		$this->_header();
		$this->view->render($this->root_view.'/comp/news.tpl');
		$this->_footer();
		
    }
	///////////////////////////////////////////////// Program /////////////////////////////////////////////////
	function program($id) {
		
        $this->view->assign('mem_lib', $this->mem_lib);
		
		$Comp	=	$this->comp_model->loadByID($id);
		$this->view->assign('dataComp', $Comp);
		
		if(!$Comp){
			header( "location: " . BASE_HREF . "api/adminfootball/comp/" );
			exit;
		}
		
		/*-----------------------------------Find by league ID-----------------------------------*/
		$listCompLeague			=	$this->matchcomp_model->get_by_comp($id,1);
		$dataLeague				=	array();
		$listLeague				=	array();
		foreach ( $listCompLeague as $value ){
			$listLeague[]		=	$this->league_model->get_league_by_id($value['league_id']);
			$dataLeague[]		=	intval($value['league_id']);
		}
		$this->view->assign('listLeague', $listLeague);
		$listMatchLeague 		= 	$this->match_model->get_by_listLeagueID($dataLeague);
		/*-----------------------------------Find a Dammy-----------------------------------*/
		$listMatchDummy			=	$this->matchcomp_model->loadListMatchDummy($id,-1);
		$listMatch				=	array();
		$indexDummy =	0;
		foreach ( $listMatchLeague as $value ){
			for( ; $indexDummy < count($listMatchDummy)  ; $indexDummy++ ){
				if( strtotime($listMatchDummy[$indexDummy]['MatchDateTime']) > strtotime($value['MatchDateTime']) ){
					$listMatchDummy[$indexDummy]['FTScore']		=		intval($listMatchDummy[$indexDummy]['Team1FTScore']) . '-' . intval($listMatchDummy[$indexDummy]['Team2FTScore']);
					$listMatchDummy[$indexDummy]['dummy_id']	=		$listMatchDummy[$indexDummy]['id'];
					$listMatchDummy[$indexDummy]['id']			=		'(' . $listMatchDummy[$indexDummy]['id'] . ')';
					$listMatch[]								=		$listMatchDummy[$indexDummy];
				}else{
					break;
				}
			}
			$listMatch[]	=	$value;
		}
		for( ; $indexDummy < count($listMatchDummy)  ; $indexDummy++ ){
			$listMatchDummy[$indexDummy]['FTScore']		=		intval($listMatchDummy[$indexDummy]['Team1FTScore']) . '-' . intval($listMatchDummy[$indexDummy]['Team2FTScore']);
			$listMatchDummy[$indexDummy]['dummy_id']	=		$listMatchDummy[$indexDummy]['id'];
			$listMatchDummy[$indexDummy]['id']			=		'(' . $listMatchDummy[$indexDummy]['id'] . ')';
			$listMatch[]								=		$listMatchDummy[$indexDummy];
		}
		$this->view->assign('listMatch', $listMatch);
		/*-----------------------------------Load a round list-----------------------------------*/
		$listRound			=	$this->roundmatch_model->loadListRound($id);
		$this->view->assign('listRound', $listRound);
		
		$this->view->assign('site_page', 'program');
		
		$breadcrum[0]['text'] = 'Home';
        $breadcrum[0]['link'] = BASE_HREF_ADMIN;
		$breadcrum[1]['text'] = 'การแข่งขัน';
		$breadcrum[1]['link'] = BASE_HREF_ADMIN . 'comp';
		$breadcrum[2]['text'] = 'โปรแกรมการแข่งขัน' . $Comp['nameTHShort'];
		$breadcrum[2]['active'] = true;
		parent::_setBreadcrum($breadcrum);

		$this->_header();
		$this->view->render($this->root_view.'/comp/program.tpl');
		$this->_footer();
		
    }
	
	function program_round($comp_id,$roundmacth_id=0) {
		
        $this->view->assign('mem_lib', $this->mem_lib);
		
		$Comp	=	$this->comp_model->loadByID($comp_id);
		$this->view->assign('dataComp', $Comp);
		if(!$Comp){
			header( "location: " . BASE_HREF . "api/adminfootball/comp/" );
			exit;
		}
		$RoundMatch	= $this->roundmatch_model->loadByID($roundmacth_id);
		$this->view->assign('dataRoundMatch', $RoundMatch);
		if($RoundMatch){
			/*-----------------------------------Find by league ID-----------------------------------*/
			$listCompLeague			=	$this->matchcomp_model->get_by_round($comp_id,$roundmacth_id,1);
			$dataLeague				=	array();
			$listLeague				=	array();
			foreach ( $listCompLeague as $value ){
				$listLeague[]		=	$this->league_model->get_league_by_id($value['league_id']);
				$dataLeague[]		=	intval($value['league_id']);
			}
			$this->view->assign('listLeague', $listLeague);
			$listMatchLeague 		= 	$this->match_model->get_by_listLeagueID($dataLeague);
			/*-----------------------------------Find a Dammy-----------------------------------*/
			$listMatchDummy			=	$this->matchcomp_model->loadListMatchDummy($comp_id,$roundmacth_id);
			$listMatch				=	array();
			$indexDummy =	0;
			foreach ( $listMatchLeague as $value ){
				for( ; $indexDummy < count($listMatchDummy)  ; $indexDummy++ ){
					if( strtotime($listMatchDummy[$indexDummy]['MatchDateTime']) > strtotime($value['MatchDateTime']) ){
						$listMatchDummy[$indexDummy]['FTScore']		=		intval($listMatchDummy[$indexDummy]['Team1FTScore']) . '-' . intval($listMatchDummy[$indexDummy]['Team2FTScore']);
						$listMatchDummy[$indexDummy]['dummy_id']	=		$listMatchDummy[$indexDummy]['id'];
						$listMatchDummy[$indexDummy]['id']			=		'(' . $listMatchDummy[$indexDummy]['id'] . ')';
						$listMatch[]								=		$listMatchDummy[$indexDummy];
					}else{
						break;
					}
				}
				$listMatch[]	=	$value;
			}
			for( ; $indexDummy < count($listMatchDummy)  ; $indexDummy++ ){
				$listMatchDummy[$indexDummy]['FTScore']		=		intval($listMatchDummy[$indexDummy]['Team1FTScore']) . '-' . intval($listMatchDummy[$indexDummy]['Team2FTScore']);
				$listMatchDummy[$indexDummy]['dummy_id']	=		$listMatchDummy[$indexDummy]['id'];
				$listMatchDummy[$indexDummy]['id']			=		'(' . $listMatchDummy[$indexDummy]['id'] . ')';
				$listMatch[]								=		$listMatchDummy[$indexDummy];
			}
			$this->view->assign('listMatch', $listMatch);
		}
		
		$this->view->assign('site_page', 'program_round');
		
		$breadcrum[0]['text'] = 'Home';
        $breadcrum[0]['link'] = BASE_HREF_ADMIN;
		$breadcrum[1]['text'] = 'การแข่งขัน';
		$breadcrum[1]['link'] = BASE_HREF_ADMIN . 'comp';
		$breadcrum[2]['text'] = 'โปรแกรมการแข่งขัน' . $Comp['nameTHShort'];
		$breadcrum[2]['link'] = BASE_HREF_ADMIN . 'comp/program/' . $comp_id;
		if($RoundMatch){
			$breadcrum[3]['text'] = 'รอบการแข่งขัน ' . $RoundMatch['name'];
		}else{
			$breadcrum[3]['text'] = 'สร้างรอบการแข่งขัน';
		}
		$breadcrum[3]['active'] = true;
		parent::_setBreadcrum($breadcrum);

		$this->_header();
		$this->view->render($this->root_view.'/comp/program.tpl');
		$this->_footer();
		
    }
	
	function program_league($id) {
	
        $this->view->assign('mem_lib', $this->mem_lib);
		
		$Comp	=	$this->comp_model->loadByID($id);
		$this->view->assign('dataComp', $Comp);
		
		if(!$Comp){
			header( "location: " . BASE_HREF . "api/adminfootball/comp/" );
			exit;
		}
		
		$dataZone	=	$this->zone_model->get_zone();
		$this->view->assign('dataZone', $dataZone);
		
		$breadcrum[0]['text'] = 'Home';
        $breadcrum[0]['link'] = BASE_HREF_ADMIN;
		$breadcrum[1]['text'] = 'การแข่งขัน';
		$breadcrum[1]['link'] = BASE_HREF_ADMIN . 'comp';
		$breadcrum[2]['text'] = 'โปรแกรมการแข่งขัน' . $Comp['nameTHShort'];
		$breadcrum[2]['link'] = BASE_HREF_ADMIN . 'comp/program/' . $id;
		$breadcrum[3]['text'] = 'ลีกที่นำมาใช้สร้างตาราง';
		$breadcrum[3]['active'] = true;
		parent::_setBreadcrum($breadcrum);

		$this->_header();
		$this->view->render($this->root_view.'/comp/program_league.tpl');
		$this->_footer();
		
    }
	
	function program_round_league($comp_id,$roundmatch_id) {
	
        $this->view->assign('mem_lib', $this->mem_lib);
		
		$Comp	=	$this->comp_model->loadByID($comp_id);
		$this->view->assign('dataComp', $Comp);
		
		if(!$Comp){
			header( "location: " . BASE_HREF . "api/adminfootball/comp/" );
			exit;
		}
		
		$Round	=	$this->roundmatch_model->loadByID($roundmatch_id);
		$this->view->assign('dataRound', $Round);
		
		if(!$Round){
			header( "location: " . BASE_HREF . "api/adminfootball/comp/program/" . $Comp['id']);
			exit;
		}
		
		$dataZone	=	$this->zone_model->get_zone();
		$this->view->assign('dataZone', $dataZone);
		
		$breadcrum[0]['text'] = 'Home';
        $breadcrum[0]['link'] = BASE_HREF_ADMIN;
		$breadcrum[1]['text'] = 'การแข่งขัน';
		$breadcrum[1]['link'] = BASE_HREF_ADMIN . 'comp';
		$breadcrum[2]['text'] = 'โปรแกรมการแข่งขัน' . $Comp['nameTHShort'];
		$breadcrum[2]['link'] = BASE_HREF_ADMIN . 'comp/program/' . $comp_id;
		$breadcrum[3]['text'] = 'รอบการแข่งขัน ' . $Round['name'];
		$breadcrum[3]['link'] = BASE_HREF_ADMIN . 'comp/program_round/' . $comp_id . '/' . $roundmatch_id;
		$breadcrum[4]['text'] = 'ลีกที่นำมาใช้สร้างตาราง';
		$breadcrum[4]['active'] = true;
		parent::_setBreadcrum($breadcrum);

		$this->_header();
		$this->view->render($this->root_view.'/comp/program_league.tpl');
		$this->_footer();
		
    }
	
	function program_dummy($id,$match_id = 0) {

        $this->view->assign('mem_lib', $this->mem_lib);
		
		$Comp	=	$this->comp_model->loadByID($id);
		$this->view->assign('dataComp', $Comp);
		
		if(!$Comp){
			header( "location: " . BASE_HREF . "api/adminfootball/comp/" );
			exit;
		}
		
		if($match_id > 0){
			$dataMatch		=		$this->matchcomp_model->get_MatchDummy($match_id);
			$this->view->assign('dataMatch', $dataMatch);
		}
		
		$breadcrum[0]['text'] = 'Home';
        $breadcrum[0]['link'] = BASE_HREF_ADMIN;
		$breadcrum[1]['text'] = 'การแข่งขัน';
		$breadcrum[1]['link'] = BASE_HREF_ADMIN . 'comp';
		$breadcrum[2]['text'] = 'โปรแกรมการแข่งขัน' . $Comp['nameTHShort'];
		$breadcrum[2]['link'] = BASE_HREF_ADMIN . 'comp/program/' . $id;
		if($dataMatch){
			$breadcrum[3]['text'] = 'แก้ไขคู่การแข่งขัน';
		}else{
			$breadcrum[3]['text'] = 'สร้างคู่การแข่งขันใหม่';
		}
		$breadcrum[3]['active'] = true;
		parent::_setBreadcrum($breadcrum);

		$this->_header();
		$this->view->render($this->root_view.'/comp/program_dummy.tpl');
		$this->_footer();
		
    }
	
	function program_round_dummy($id,$roundmatch_id,$match_id = 0) {

        $this->view->assign('mem_lib', $this->mem_lib);
		
		$Comp	=	$this->comp_model->loadByID($id);
		$this->view->assign('dataComp', $Comp);
		
		if(!$Comp){
			header( "location: " . BASE_HREF . "api/adminfootball/comp/" );
			exit;
		}
		
		$Round	=	$this->roundmatch_model->loadByID($roundmatch_id);
		$this->view->assign('dataRound', $Round);
		
		if(!$Round){
			header( "location: " . BASE_HREF . "api/adminfootball/comp/program/" . $Comp['id']);
			exit;
		}
		
		if($match_id > 0){
			$dataMatch		=		$this->matchcomp_model->get_MatchDummy($match_id);
			$this->view->assign('dataMatch', $dataMatch);
		}
		
		$breadcrum[0]['text'] = 'Home';
        $breadcrum[0]['link'] = BASE_HREF_ADMIN;
		$breadcrum[1]['text'] = 'การแข่งขัน';
		$breadcrum[1]['link'] = BASE_HREF_ADMIN . 'comp';
		$breadcrum[2]['text'] = 'โปรแกรมการแข่งขัน' . $Comp['nameTHShort'];
		$breadcrum[2]['link'] = BASE_HREF_ADMIN . 'comp/program/' . $id;
		$breadcrum[3]['text'] = 'รอบการแข่งขัน ' . $Round['name'];
		$breadcrum[3]['link'] = BASE_HREF_ADMIN . 'comp/program_round/' . $id . '/' . $roundmatch_id;
		if($dataMatch){
			$breadcrum[4]['text'] = 'แก้ไขคู่การแข่งขัน';
		}else{
			$breadcrum[4]['text'] = 'สร้างคู่การแข่งขันใหม่';
		}
		$breadcrum[4]['active'] = true;
		parent::_setBreadcrum($breadcrum);

		$this->_header();
		$this->view->render($this->root_view.'/comp/program_dummy.tpl');
		$this->_footer();
		
    }
	///////////////////////////////////////////////// table /////////////////////////////////////////////////
	function table($id) {
		
        $this->view->assign('mem_lib', $this->mem_lib);
		
		$Comp	=	$this->comp_model->loadByID($id);
		$this->view->assign('dataComp', $Comp);
		
		if(!$Comp){
			header( "location: " . BASE_HREF . "api/adminfootball/comp/" );
			exit;
		}
		
		$listTable		=		$this->table_model->loadListTable($id,-1);
		foreach($listTable as $i => $tmpData){
			if($tmpData['type']==1){
				$TableData			=	array();
				$dataListMatchID	=	array();
				$dataListMatch		=	$this->match_model->get_by_listLeagueAndMatch($tmpData['league_list'],$tmpData['match_list']);
				
				foreach($dataListMatch as $tmpMatch){
					// Team 1
					$dataListMatchID[] = $tmpMatch["id"];
					$teamid_1	=	$tmpMatch['Team1KPID'];
					if(!isset($TableData[$tmpMatch['Team1KPID']])){
						
						$dataTeam	=	$this->team_model->loadByID(intval($teamid_1));

						if(!empty($dataTeam['NameTH'])){
							$strTeam	=	$dataTeam['NameTH'];
						}else if(!empty($dataTeam['NameTHShort'])){
							$strTeam	=	$dataTeam['NameTHShort'];
						}else{
							$strTeam	=	$dataTeam['NameEN'];
						}
						
						$Logo 						= 		str_replace(' ','-',$dataTeam['NameEN']).'.png';
						$Logo_MC					=		$this->mem_lib->get('Football2014-Team-Logo-' . $Logo);
						if($Logo_MC==true){
							$pathlogo 				= 		'http://football.kapook.com/uploads/logo/' . $Logo;
						}else{
							$pathlogo 				= 		'http://football.kapook.com/uploads/logo/default.png';
						}
						
						$TableData[$teamid_1]['name']			=		$strTeam;
						$TableData[$teamid_1]['NameEN']			=		$dataTeam['NameEN'];
						$TableData[$teamid_1]['NameTHShort']	=		$dataTeam['NameTHShort'];
						$TableData[$teamid_1]['url']			=		'http://football.kapook.com/team-' . strtolower(str_replace(" ","-",$dataTeam['NameEN']));
						$TableData[$teamid_1]['logo']			=		$pathlogo;
						$TableData[$teamid_1]['sp_point']		=		0;
						
						$TableData[$teamid_1]['win']			=		0;
						$TableData[$teamid_1]['draw']			=		0;
						$TableData[$teamid_1]['lose']			=		0;
						$TableData[$teamid_1]['point']			=		0;
						$TableData[$teamid_1]['goal_for']		=		0;
						$TableData[$teamid_1]['goal_against']	=		0;
						$TableData[$teamid_1]['goal_dif']		=		0;
						
					}
					// Team 2
					$teamid_2	=	$tmpMatch['Team2KPID'];
					if(!isset($TableData[$tmpMatch['Team2KPID']])){
						
						$dataTeam	=	$this->team_model->loadByID(intval($teamid_2));
						
						if(!empty($dataTeam['NameTH'])){
							$strTeam	=	$dataTeam['NameTH'];
						}else if(!empty($dataTeam['NameTHShort'])){
							$strTeam	=	$dataTeam['NameTHShort'];
						}else{
							$strTeam	=	$dataTeam['NameEN'];
						}
						
						$Logo 						= 		str_replace(' ','-',$dataTeam['NameEN']).'.png';
						$Logo_MC					=		$this->mem_lib->get('Football2014-Team-Logo-' . $Logo);
						if($Logo_MC==true){
							$pathlogo 				= 		'http://football.kapook.com/uploads/logo/' . $Logo;
						}else{
							$pathlogo 				= 		'http://football.kapook.com/uploads/logo/default.png';
						}
						
						$TableData[$teamid_2]['name']			=		$strTeam;
						$TableData[$teamid_2]['NameEN']			=		$dataTeam['NameEN'];
						$TableData[$teamid_2]['NameTHShort']	=		$dataTeam['NameTHShort'];
						$TableData[$teamid_2]['url']			=		'http://football.kapook.com/team-' . strtolower(str_replace(" ","-",$dataTeam['NameEN']));
						$TableData[$teamid_2]['logo']			=		$pathlogo;
						$TableData[$teamid_2]['sp_point']		=		0;
						
						$TableData[$teamid_2]['win']			=		0;
						$TableData[$teamid_2]['draw']			=		0;
						$TableData[$teamid_2]['lose']			=		0;
						$TableData[$teamid_2]['point']			=		0;
						$TableData[$teamid_2]['goal_for']		=		0;
						$TableData[$teamid_2]['goal_against']	=		0;
						$TableData[$teamid_2]['goal_dif']		=		0;
						
					}
					if($tmpMatch['MatchStatus']=='Fin'){
						//Team 1 > Team 2
						if($tmpMatch['Team1FTScore'] > $tmpMatch['Team2FTScore']){
							$TableData[$teamid_1]['win']++;
							$TableData[$teamid_1]['point']	=	$TableData[$teamid_1]['point']+3;
							
							$TableData[$teamid_2]['lose']++;
						}
						//Team 1 < Team 2
						else if($tmpMatch['Team1FTScore'] < $tmpMatch['Team2FTScore']){
							$TableData[$teamid_1]['lose']++;
							
							$TableData[$teamid_2]['win']++;
							$TableData[$teamid_2]['point']	=	$TableData[$teamid_2]['point']+3;
						}
						//Team 1 < Team 2
						else{
							$TableData[$teamid_1]['draw']++;
							$TableData[$teamid_1]['point']++;
							
							$TableData[$teamid_2]['draw']++;
							$TableData[$teamid_2]['point']++;
						}
						
						$TableData[$teamid_1]['goal_for']		=		$TableData[$teamid_1]['goal_for']		+		$tmpMatch['Team1FTScore'];
						$TableData[$teamid_1]['goal_against']	=		$TableData[$teamid_1]['goal_against']	+		$tmpMatch['Team2FTScore'];
						$TableData[$teamid_1]['goal_dif']		=		$TableData[$teamid_1]['goal_dif']		+		($tmpMatch['Team1FTScore']-$tmpMatch['Team2FTScore']);
							
						$TableData[$teamid_2]['goal_for']		=		$TableData[$teamid_2]['goal_for']		+		$tmpMatch['Team2FTScore'];
						$TableData[$teamid_2]['goal_against']	=		$TableData[$teamid_2]['goal_against']	+		$tmpMatch['Team1FTScore'];
						$TableData[$teamid_2]['goal_dif']		=		$TableData[$teamid_2]['goal_dif']		+		($tmpMatch['Team2FTScore']-$tmpMatch['Team1FTScore']);
						
					}
				}
				
				foreach($tmpData['team_list'] as $tmpTeamAndLeague){
					$tmpTeamID = $tmpTeamAndLeague['team_id'];
					$tmpLeagueID = $tmpTeamAndLeague['league_id'];
					
					$tmpListMatchByTeam = $this->match_model->get_by_LeagueAndTeam($tmpLeagueID,$tmpTeamID);
					
					foreach($tmpListMatchByTeam as $tmpMatch){
					
						if(!isset($TableData[$tmpTeamID])){

							$dataTeam	=	$this->team_model->loadByID(intval($tmpTeamID));

							if(!empty($dataTeam['NameTH'])){
								$strTeam	=	$dataTeam['NameTH'];
							}else if(!empty($dataTeam['NameTHShort'])){
								$strTeam	=	$dataTeam['NameTHShort'];
							}else{
								$strTeam	=	$dataTeam['NameEN'];
							}
								
							$Logo 						= 		str_replace(' ','-',$dataTeam['NameEN']).'.png';
							$Logo_MC					=		$this->mem_lib->get('Football2014-Team-Logo-' . $Logo);
							if($Logo_MC==true){
								$pathlogo 				= 		'http://football.kapook.com/uploads/logo/' . $Logo;
							}else{
								$pathlogo 				= 		'http://football.kapook.com/uploads/logo/default.png';
							}
								
							$TableData[$tmpTeamID]['name']			=		$strTeam;
							$TableData[$tmpTeamID]['NameEN']		=		$dataTeam['NameEN'];
							$TableData[$tmpTeamID]['NameTHShort']	=		$dataTeam['NameTHShort'];
							$TableData[$tmpTeamID]['url']			=		'http://football.kapook.com/team-' . strtolower(str_replace(" ","-",$dataTeam['NameEN']));
							$TableData[$tmpTeamID]['logo']			=		$pathlogo;
							$TableData[$tmpTeamID]['sp_point']		=		0;
								
							$TableData[$tmpTeamID]['win']			=		0;
							$TableData[$tmpTeamID]['draw']			=		0;
							$TableData[$tmpTeamID]['lose']			=		0;
							$TableData[$tmpTeamID]['point']			=		0;
							$TableData[$tmpTeamID]['goal_for']		=		0;
							$TableData[$tmpTeamID]['goal_against']	=		0;
							$TableData[$tmpTeamID]['goal_dif']		=		0;
						}
					
						if (in_array($tmpMatch["id"], $dataListMatchID)){
							continue;
						}
					
						if($tmpMatch['MatchStatus']=='Fin'){
							if($tmpMatch['Team2KPID'] == $tmpTeamID){
								$tmpScore = $tmpMatch['Team1FTScore'];
								$tmpMatch['Team1FTScore'] = $tmpMatch['Team2FTScore'];
								$tmpMatch['Team2FTScore'] = $tmpScore;
							}

							//Team 1 > Team 2
							if($tmpMatch['Team1FTScore'] > $tmpMatch['Team2FTScore']){
								$TableData[$tmpTeamID]['win']++;
								$TableData[$tmpTeamID]['point']	=	$TableData[$tmpTeamID]['point']+3;
							}
							//Team 1 < Team 2
							else if($tmpMatch['Team1FTScore'] < $tmpMatch['Team2FTScore']){
								$TableData[$tmpTeamID]['lose']++;
							}
							//Team 1 < Team 2
							else{
								$TableData[$tmpTeamID]['draw']++;
								$TableData[$tmpTeamID]['point']++;
							}
							
							$TableData[$tmpTeamID]['goal_for']		=		$TableData[$tmpTeamID]['goal_for']		+		$tmpMatch['Team1FTScore'];
							$TableData[$tmpTeamID]['goal_against']	=		$TableData[$tmpTeamID]['goal_against']	+		$tmpMatch['Team2FTScore'];
							$TableData[$tmpTeamID]['goal_dif']		=		$TableData[$tmpTeamID]['goal_dif']		+		($tmpMatch['Team1FTScore']-$tmpMatch['Team2FTScore']);
							
						}
					}
				}
				$listTable[$i]['TeamData']				=		$TableData;
			}else if($tmpData['type']==2){
				foreach($listTable[$i]['TeamData'] as $j => $tmpTeam){
					
					if(strlen($tmpTeam['logo'])<=0){
						$listTable[$i]['TeamData'][$j]['logo']		=		'http://football.kapook.com/uploads/logo/default.png';
					}
					$listTable[$i]['TeamData'][$j]['goal_dif']		=		$listTable[$i]['TeamData'][$j]['goal_for'] - $listTable[$i]['TeamData'][$j]['goal_against'];
					$listTable[$i]['TeamData'][$j]['point']			=		($listTable[$i]['TeamData'][$j]['win']*3) + $listTable[$i]['TeamData'][$j]['draw'];
				}
			}
			if(($tmpData['type']!=2)||($tmpData['type_order']!=2)){
				$leaderboard		=		$listTable[$i]['TeamData'];
				usort($leaderboard,array("Comp", "compare_point"));
				$listTable[$i]['TeamData']		=		$leaderboard;
			}
		}
		$this->view->assign('listTable', $listTable);
		/*-----------------------------------Load a round list-----------------------------------*/
		$listRound			=	$this->roundtable_model->loadListRound($id);
		$this->view->assign('listRound', $listRound);
		
		$this->view->assign('site_page', 'table');
		
		$breadcrum[0]['text'] = 'Home';
        $breadcrum[0]['link'] = BASE_HREF_ADMIN;
		$breadcrum[1]['text'] = 'การแข่งขัน';
		$breadcrum[1]['link'] = BASE_HREF_ADMIN . 'comp';
		$breadcrum[2]['text'] = 'ตารางคะแนนการแข่งขัน' . $Comp['nameTHShort'];
		$breadcrum[2]['active'] = true;
		parent::_setBreadcrum($breadcrum);


		$this->_header();
		$this->view->render($this->root_view.'/comp/table.tpl');
		$this->_footer();
		
    }
	
	function table_round($comp_id,$roundtable_id=0) {
		
        $this->view->assign('mem_lib', $this->mem_lib);
		
		$Comp	=	$this->comp_model->loadByID($comp_id);
		$this->view->assign('dataComp', $Comp);
		if(!$Comp){
			header( "location: " . BASE_HREF . "api/adminfootball/comp/" );
			exit;
		}
		
		$RoundTable	= $this->roundtable_model->loadByID($roundtable_id);
		$this->view->assign('dataRoundTable', $RoundTable);
		if($RoundTable){
			$listTable		=		$this->table_model->loadListTable($comp_id,$roundtable_id);
			foreach($listTable as $i => $tmpData){
				if($tmpData['type']==1){
					$TableData			=	array();
					$dataListMatchID	=	array();
					$dataListMatch		=	$this->match_model->get_by_listLeagueAndMatch($tmpData['league_list'],$tmpData['match_list']);
					
					foreach($dataListMatch as $tmpMatch){
						// Team 1
						$dataListMatchID[] = $tmpMatch["id"];
						$teamid_1	=	$tmpMatch['Team1KPID'];
						if(!isset($TableData[$tmpMatch['Team1KPID']])){
							
							$dataTeam	=	$this->team_model->loadByID(intval($teamid_1));

							if(!empty($dataTeam['NameTH'])){
								$strTeam	=	$dataTeam['NameTH'];
							}else if(!empty($dataTeam['NameTHShort'])){
								$strTeam	=	$dataTeam['NameTHShort'];
							}else{
								$strTeam	=	$dataTeam['NameEN'];
							}
							
							$Logo 						= 		str_replace(' ','-',$dataTeam['NameEN']).'.png';
							$Logo_MC					=		$this->mem_lib->get('Football2014-Team-Logo-' . $Logo);
							if($Logo_MC==true){
								$pathlogo 				= 		'http://football.kapook.com/uploads/logo/' . $Logo;
							}else{
								$pathlogo 				= 		'http://football.kapook.com/uploads/logo/default.png';
							}
							
							$TableData[$teamid_1]['name']			=		$strTeam;
							$TableData[$teamid_1]['NameEN']			=		$dataTeam['NameEN'];
							$TableData[$teamid_1]['NameTHShort']	=		$dataTeam['NameTHShort'];
							$TableData[$teamid_1]['url']			=		'http://football.kapook.com/team-' . strtolower(str_replace(" ","-",$dataTeam['NameEN']));
							$TableData[$teamid_1]['logo']			=		$pathlogo;
							$TableData[$teamid_1]['sp_point']		=		0;
							
							$TableData[$teamid_1]['win']			=		0;
							$TableData[$teamid_1]['draw']			=		0;
							$TableData[$teamid_1]['lose']			=		0;
							$TableData[$teamid_1]['point']			=		0;
							$TableData[$teamid_1]['goal_for']		=		0;
							$TableData[$teamid_1]['goal_against']	=		0;
							$TableData[$teamid_1]['goal_dif']		=		0;
							
						}
						// Team 2
						$teamid_2	=	$tmpMatch['Team2KPID'];
						if(!isset($TableData[$tmpMatch['Team2KPID']])){
							
							$dataTeam	=	$this->team_model->loadByID(intval($teamid_2));
							
							if(!empty($dataTeam['NameTH'])){
								$strTeam	=	$dataTeam['NameTH'];
							}else if(!empty($dataTeam['NameTHShort'])){
								$strTeam	=	$dataTeam['NameTHShort'];
							}else{
								$strTeam	=	$dataTeam['NameEN'];
							}
							
							$Logo 						= 		str_replace(' ','-',$dataTeam['NameEN']).'.png';
							$Logo_MC					=		$this->mem_lib->get('Football2014-Team-Logo-' . $Logo);
							if($Logo_MC==true){
								$pathlogo 				= 		'http://football.kapook.com/uploads/logo/' . $Logo;
							}else{
								$pathlogo 				= 		'http://football.kapook.com/uploads/logo/default.png';
							}
							
							$TableData[$teamid_2]['name']			=		$strTeam;
							$TableData[$teamid_2]['NameEN']			=		$dataTeam['NameEN'];
							$TableData[$teamid_2]['NameTHShort']	=		$dataTeam['NameTHShort'];
							$TableData[$teamid_2]['url']			=		'http://football.kapook.com/team-' . strtolower(str_replace(" ","-",$dataTeam['NameEN']));
							$TableData[$teamid_2]['logo']			=		$pathlogo;
							$TableData[$teamid_2]['sp_point']		=		0;
							
							$TableData[$teamid_2]['win']			=		0;
							$TableData[$teamid_2]['draw']			=		0;
							$TableData[$teamid_2]['lose']			=		0;
							$TableData[$teamid_2]['point']			=		0;
							$TableData[$teamid_2]['goal_for']		=		0;
							$TableData[$teamid_2]['goal_against']	=		0;
							$TableData[$teamid_2]['goal_dif']		=		0;
							
						}
						if($tmpMatch['MatchStatus']=='Fin'){
							//Team 1 > Team 2
							if($tmpMatch['Team1FTScore'] > $tmpMatch['Team2FTScore']){
								$TableData[$teamid_1]['win']++;
								$TableData[$teamid_1]['point']	=	$TableData[$teamid_1]['point']+3;
								
								$TableData[$teamid_2]['lose']++;
							}
							//Team 1 < Team 2
							else if($tmpMatch['Team1FTScore'] < $tmpMatch['Team2FTScore']){
								$TableData[$teamid_1]['lose']++;
								
								$TableData[$teamid_2]['win']++;
								$TableData[$teamid_2]['point']	=	$TableData[$teamid_2]['point']+3;
							}
							//Team 1 < Team 2
							else{
								$TableData[$teamid_1]['draw']++;
								$TableData[$teamid_1]['point']++;
								
								$TableData[$teamid_2]['draw']++;
								$TableData[$teamid_2]['point']++;
							}
							
							$TableData[$teamid_1]['goal_for']		=		$TableData[$teamid_1]['goal_for']		+		$tmpMatch['Team1FTScore'];
							$TableData[$teamid_1]['goal_against']	=		$TableData[$teamid_1]['goal_against']	+		$tmpMatch['Team2FTScore'];
							$TableData[$teamid_1]['goal_dif']		=		$TableData[$teamid_1]['goal_dif']		+		($tmpMatch['Team1FTScore']-$tmpMatch['Team2FTScore']);
								
							$TableData[$teamid_2]['goal_for']		=		$TableData[$teamid_2]['goal_for']		+		$tmpMatch['Team2FTScore'];
							$TableData[$teamid_2]['goal_against']	=		$TableData[$teamid_2]['goal_against']	+		$tmpMatch['Team1FTScore'];
							$TableData[$teamid_2]['goal_dif']		=		$TableData[$teamid_2]['goal_dif']		+		($tmpMatch['Team2FTScore']-$tmpMatch['Team1FTScore']);
							
						}
					}
					
					foreach($tmpData['team_list'] as $tmpTeamAndLeague){
						$tmpTeamID = $tmpTeamAndLeague['team_id'];
						$tmpLeagueID = $tmpTeamAndLeague['league_id'];
						
						$tmpListMatchByTeam = $this->match_model->get_by_LeagueAndTeam($tmpLeagueID,$tmpTeamID);
						
						foreach($tmpListMatchByTeam as $tmpMatch){
						
							if(!isset($TableData[$tmpTeamID])){

								$dataTeam	=	$this->team_model->loadByID(intval($tmpTeamID));

								if(!empty($dataTeam['NameTH'])){
									$strTeam	=	$dataTeam['NameTH'];
								}else if(!empty($dataTeam['NameTHShort'])){
									$strTeam	=	$dataTeam['NameTHShort'];
								}else{
									$strTeam	=	$dataTeam['NameEN'];
								}
									
								$Logo 						= 		str_replace(' ','-',$dataTeam['NameEN']).'.png';
								$Logo_MC					=		$this->mem_lib->get('Football2014-Team-Logo-' . $Logo);
								if($Logo_MC==true){
									$pathlogo 				= 		'http://football.kapook.com/uploads/logo/' . $Logo;
								}else{
									$pathlogo 				= 		'http://football.kapook.com/uploads/logo/default.png';
								}
									
								$TableData[$tmpTeamID]['name']			=		$strTeam;
								$TableData[$tmpTeamID]['NameEN']		=		$dataTeam['NameEN'];
								$TableData[$tmpTeamID]['NameTHShort']	=		$dataTeam['NameTHShort'];
								$TableData[$tmpTeamID]['url']			=		'http://football.kapook.com/team-' . strtolower(str_replace(" ","-",$dataTeam['NameEN']));
								$TableData[$tmpTeamID]['logo']			=		$pathlogo;
								$TableData[$tmpTeamID]['sp_point']		=		0;
									
								$TableData[$tmpTeamID]['win']			=		0;
								$TableData[$tmpTeamID]['draw']			=		0;
								$TableData[$tmpTeamID]['lose']			=		0;
								$TableData[$tmpTeamID]['point']			=		0;
								$TableData[$tmpTeamID]['goal_for']		=		0;
								$TableData[$tmpTeamID]['goal_against']	=		0;
								$TableData[$tmpTeamID]['goal_dif']		=		0;
							}
						
							if (in_array($tmpMatch["id"], $dataListMatchID)){
								continue;
							}
						
							if($tmpMatch['MatchStatus']=='Fin'){
								if($tmpMatch['Team2KPID'] == $tmpTeamID){
									$tmpScore = $tmpMatch['Team1FTScore'];
									$tmpMatch['Team1FTScore'] = $tmpMatch['Team2FTScore'];
									$tmpMatch['Team2FTScore'] = $tmpScore;
								}

								//Team 1 > Team 2
								if($tmpMatch['Team1FTScore'] > $tmpMatch['Team2FTScore']){
									$TableData[$tmpTeamID]['win']++;
									$TableData[$tmpTeamID]['point']	=	$TableData[$tmpTeamID]['point']+3;
								}
								//Team 1 < Team 2
								else if($tmpMatch['Team1FTScore'] < $tmpMatch['Team2FTScore']){
									$TableData[$tmpTeamID]['lose']++;
								}
								//Team 1 < Team 2
								else{
									$TableData[$tmpTeamID]['draw']++;
									$TableData[$tmpTeamID]['point']++;
								}
								
								$TableData[$tmpTeamID]['goal_for']		=		$TableData[$tmpTeamID]['goal_for']		+		$tmpMatch['Team1FTScore'];
								$TableData[$tmpTeamID]['goal_against']	=		$TableData[$tmpTeamID]['goal_against']	+		$tmpMatch['Team2FTScore'];
								$TableData[$tmpTeamID]['goal_dif']		=		$TableData[$tmpTeamID]['goal_dif']		+		($tmpMatch['Team1FTScore']-$tmpMatch['Team2FTScore']);
								
							}
						}
					}
					$listTable[$i]['TeamData']				=		$TableData;
				}else if($tmpData['type']==2){
					foreach($listTable[$i]['TeamData'] as $j => $tmpTeam){
						
						if(strlen($tmpTeam['logo'])<=0){
							$listTable[$i]['TeamData'][$j]['logo']		=		'http://football.kapook.com/uploads/logo/default.png';
						}
						$listTable[$i]['TeamData'][$j]['goal_dif']		=		$listTable[$i]['TeamData'][$j]['goal_for'] - $listTable[$i]['TeamData'][$j]['goal_against'];
						$listTable[$i]['TeamData'][$j]['point']			=		($listTable[$i]['TeamData'][$j]['win']*3) + $listTable[$i]['TeamData'][$j]['draw'];
					}
				}
				if(($tmpData['type']!=2)||($tmpData['type_order']!=2)){
					$leaderboard		=		$listTable[$i]['TeamData'];
					usort($leaderboard,array("Comp", "compare_point"));
					$listTable[$i]['TeamData']		=		$leaderboard;
				}
			}
			$this->view->assign('listTable', $listTable);
		}
		
		$this->view->assign('site_page', 'table_round');
		
		$breadcrum[0]['text'] = 'Home';
        $breadcrum[0]['link'] = BASE_HREF_ADMIN;
		$breadcrum[1]['text'] = 'การแข่งขัน';
		$breadcrum[1]['link'] = BASE_HREF_ADMIN . 'comp';
		$breadcrum[2]['text'] = 'ตารางคะแนนการแข่งขัน' . $Comp['nameTHShort'];
		$breadcrum[2]['link'] = BASE_HREF_ADMIN . 'comp/table/' . $comp_id;
		if($RoundTable){
			$breadcrum[3]['text'] = 'ตารางคะแนนของ ' . $RoundTable['name'];
		}else{
			$breadcrum[3]['text'] = 'สร้างรอบการแข่งขันสำหรับตางรางคะแนน';
		}
		$breadcrum[3]['active'] = true;
		parent::_setBreadcrum($breadcrum);

		$this->_header();
		$this->view->render($this->root_view.'/comp/table.tpl');
		$this->_footer();
		
    }
	
	function table_select($comp_id,$table_id = 0) {
		
        $this->view->assign('mem_lib', $this->mem_lib);
		
		$Comp	=	$this->comp_model->loadByID($comp_id);
		$this->view->assign('dataComp', $Comp);
		
		if(!$Comp){
			header( "location: " . BASE_HREF . "api/adminfootball/comp/" );
			exit;
		}
		
		$dataZone	=	$this->zone_model->get_zone();
		$this->view->assign('dataZone', $dataZone);
		
		if(intval($table_id) > 0){
			$Table	=	$this->table_model->loadByID($table_id);
			$this->view->assign('dataTable', $Table);
			
			if(!$Table){
				header( "location: " . BASE_HREF . "api/adminfootball/comp/table/" . $comp_id );
				exit;
			}
		}
		
		$breadcrum[0]['text'] = 'Home';
        $breadcrum[0]['link'] = BASE_HREF_ADMIN;
		$breadcrum[1]['text'] = 'การแข่งขัน';
		$breadcrum[1]['link'] = BASE_HREF_ADMIN . 'comp';
		$breadcrum[2]['text'] = 'ตารางคะแนนการแข่งขัน' . $Comp['nameTHShort'];
		$breadcrum[2]['link'] = BASE_HREF_ADMIN . 'comp/table/' . $comp_id;
		if($table_id > 0){
			$breadcrum[3]['text'] = 'ตารางคะแนน ' . $Table['name'];
		}else{
			$breadcrum[3]['text'] = 'สร้างตารางคะแนนใหม่';
		}
		$breadcrum[3]['active'] = true;
		parent::_setBreadcrum($breadcrum);

		$this->_header();
		$this->view->render($this->root_view.'/comp/table_select.tpl');
		$this->_footer();
		
    }
	
	function table_round_select($comp_id,$roundtable_id,$table_id = 0) {
		
        $this->view->assign('mem_lib', $this->mem_lib);
		
		$Comp	=	$this->comp_model->loadByID($comp_id);
		$this->view->assign('dataComp', $Comp);
		
		if(!$Comp){
			header( "location: " . BASE_HREF . "api/adminfootball/comp/" );
			exit;
		}
		
		$RoundTable	= $this->roundtable_model->loadByID($roundtable_id);
		$this->view->assign('dataRound', $RoundTable);
		
		if(!$RoundTable){
			header( "location: " . BASE_HREF . "api/adminfootball/comp/table/" . $comp_id );
			exit;
		}
		
		if(intval($table_id) > 0){
			$Table	=	$this->table_model->loadByID($table_id);
			$this->view->assign('dataTable', $Table);
			
			if(!$Table){
				header( "location: " . BASE_HREF . "api/adminfootball/comp/table/" . $comp_id );
				exit;
			}
		}
		
		$dataZone	=	$this->zone_model->get_zone();
		$this->view->assign('dataZone', $dataZone);
		
		$breadcrum[0]['text'] = 'Home';
        $breadcrum[0]['link'] = BASE_HREF_ADMIN;
		$breadcrum[1]['text'] = 'การแข่งขัน';
		$breadcrum[1]['link'] = BASE_HREF_ADMIN . 'comp';
		$breadcrum[2]['text'] = 'ตารางคะแนนการแข่งขัน' . $Comp['nameTHShort'];
		$breadcrum[2]['link'] = BASE_HREF_ADMIN . 'comp/table/' . $comp_id;
		$breadcrum[3]['text'] = 'ตารางคะแนนของ ' . $RoundTable['name'];
		$breadcrum[3]['link'] = BASE_HREF_ADMIN . 'comp/table_round/' . $comp_id . '/' . $roundtable_id;
		if($table_id > 0){
			$breadcrum[4]['text'] = 'ตารางคะแนน ' . $Table['name'];
		}else{
			$breadcrum[4]['text'] = 'สร้างตารางคะแนนใหม่';
		}
		$breadcrum[4]['active'] = true;
		parent::_setBreadcrum($breadcrum);

		$this->_header();
		$this->view->render($this->root_view.'/comp/table_select.tpl');
		$this->_footer();
		
    }
	
	function table_dummy($comp_id,$table_id = 0) {
		
        $this->view->assign('mem_lib', $this->mem_lib);
		
		$Comp	=	$this->comp_model->loadByID($comp_id);
		$this->view->assign('dataComp', $Comp);
		
		if(!$Comp){
			header( "location: " . BASE_HREF . "api/adminfootball/comp/" );
			exit;
		}

		if(intval($table_id) > 0){
			$Table	=	$this->table_model->loadByID($table_id);
			$this->view->assign('dataTable', $Table);
			
			if(!$Table){
				header( "location: " . BASE_HREF . "api/adminfootball/comp/table/" . $comp_id );
				exit;
			}
		}
		
		$breadcrum[0]['text'] = 'Home';
        $breadcrum[0]['link'] = BASE_HREF_ADMIN;
		$breadcrum[1]['text'] = 'การแข่งขัน';
		$breadcrum[1]['link'] = BASE_HREF_ADMIN . 'comp';
		$breadcrum[2]['text'] = 'ตารางคะแนนการแข่งขัน' . $Comp['nameTHShort'];
		$breadcrum[2]['link'] = BASE_HREF_ADMIN . 'comp/table/' . $comp_id;
		if($table_id > 0){
			$breadcrum[3]['text'] = 'ตารางคะแนน ' . $Table['name'];
		}else{
			$breadcrum[3]['text'] = 'สร้างตารางคะแนนใหม่';
		}
		parent::_setBreadcrum($breadcrum);

		$this->_header();
		$this->view->render($this->root_view.'/comp/table_dummy.tpl');
		$this->_footer();
		
    }
	
	function table_round_dummy($comp_id,$roundtable_id,$table_id = 0) {
		
        $this->view->assign('mem_lib', $this->mem_lib);
		
		$Comp	=	$this->comp_model->loadByID($comp_id);
		$this->view->assign('dataComp', $Comp);
		
		if(!$Comp){
			header( "location: " . BASE_HREF . "api/adminfootball/comp/" );
			exit;
		}
		
		$RoundTable	= $this->roundtable_model->loadByID($roundtable_id);
		$this->view->assign('dataRound', $RoundTable);
		if(!$RoundTable){
			header( "location: " . BASE_HREF . "api/adminfootball/comp/table/" . $comp_id );
			exit;
		}
		
		if(intval($table_id) > 0){
			$Table	=	$this->table_model->loadByID($table_id);
			$this->view->assign('dataTable', $Table);
			
			if(!$Table){
				header( "location: " . BASE_HREF . "api/adminfootball/comp/table/" . $comp_id );
				exit;
			}
		}
		
		$breadcrum[0]['text'] = 'Home';
        $breadcrum[0]['link'] = BASE_HREF_ADMIN;
		$breadcrum[1]['text'] = 'การแข่งขัน';
		$breadcrum[1]['link'] = BASE_HREF_ADMIN . 'comp';
		$breadcrum[2]['text'] = 'ตารางคะแนนการแข่งขัน' . $Comp['nameTHShort'];
		$breadcrum[2]['link'] = BASE_HREF_ADMIN . 'comp/table/' . $comp_id;
		$breadcrum[3]['text'] = 'ตารางคะแนนของ ' . $RoundTable['name'];
		$breadcrum[3]['link'] = BASE_HREF_ADMIN . 'comp/table_round/' . $comp_id . '/' . $roundtable_id;
		if($table_id > 0){
			$breadcrum[4]['text'] = 'ตารางคะแนน ' . $Table['name'];
		}else{
			$breadcrum[4]['text'] = 'สร้างตารางคะแนนใหม่';
		}
		$breadcrum[4]['active'] = true;
		parent::_setBreadcrum($breadcrum);

		$this->_header();
		$this->view->render($this->root_view.'/comp/table_dummy.tpl');
		$this->_footer();
		
    }
	
	function table_tournament($comp_id,$table_id = 0) {
		
        $this->view->assign('mem_lib', $this->mem_lib);
		
		$Comp	=	$this->comp_model->loadByID($comp_id);
		$this->view->assign('dataComp', $Comp);
		
		if(!$Comp){
			header( "location: " . BASE_HREF . "api/adminfootball/comp/" );
			exit;
		}

		if(intval($table_id) > 0){
			$Table	=	$this->table_model->loadByID($table_id);
			$this->view->assign('dataTable', $Table);
			
			if(!$Table){
				header( "location: " . BASE_HREF . "api/adminfootball/comp/table/" . $comp_id );
				exit;
			}
		}
		
		$breadcrum[0]['text'] = 'Home';
        $breadcrum[0]['link'] = BASE_HREF_ADMIN;
		$breadcrum[1]['text'] = 'การแข่งขัน';
		$breadcrum[1]['link'] = BASE_HREF_ADMIN . 'comp';
		$breadcrum[2]['text'] = 'ตารางคะแนนการแข่งขัน' . $Comp['nameTHShort'];
		$breadcrum[2]['link'] = BASE_HREF_ADMIN . 'comp/table/' . $comp_id;
		if($table_id > 0){
			$breadcrum[3]['text'] = 'ตารางคะแนน ' . $Table['name'];
		}else{
			$breadcrum[3]['text'] = 'สร้างตารางคะแนนใหม่';
		}
		$breadcrum[3]['active'] = true;
		parent::_setBreadcrum($breadcrum);

		$this->_header();
		$this->view->render($this->root_view.'/comp/table_tournament.tpl');
		$this->_footer();
		
    }
	
	function table_tournament_match($comp_id,$table_id,$round_index,$match_index) {

        $this->view->assign('mem_lib', $this->mem_lib);
		
		$Comp	=	$this->comp_model->loadByID($comp_id);
		$this->view->assign('dataComp', $Comp);
		
		if(!$Comp){
			header( "location: " . BASE_HREF . "api/adminfootball/comp/" );
			exit;
		}

		if(intval($table_id) > 0){
			$Table	=	$this->table_model->loadByID($table_id);
			$this->view->assign('dataTable', $Table);
			
			if(!$Table){
				header( "location: " . BASE_HREF . "api/adminfootball/comp/table/" . $comp_id );
				exit;
			}
		}
		
		$this->view->assign('round_index', $round_index);
		$this->view->assign('match_index', $match_index);
		
		$breadcrum[0]['text'] = 'Home';
        $breadcrum[0]['link'] = BASE_HREF_ADMIN;
		$breadcrum[1]['text'] = 'การแข่งขัน';
		$breadcrum[1]['link'] = BASE_HREF_ADMIN . 'comp';
		$breadcrum[2]['text'] = 'ตารางคะแนนการแข่งขัน' . $Comp['nameTHShort'];
		$breadcrum[2]['link'] = BASE_HREF_ADMIN . 'comp/table/' . $comp_id;
		$breadcrum[3]['text'] = 'ตารางคะแนน ' . $Table['name'];
		$breadcrum[3]['link'] = BASE_HREF_ADMIN . 'comp/table_tournament/' . $comp_id . '/' . $table_id;
		$breadcrum[4]['text'] = 'แก้ไขการแข่งขัน';
		$breadcrum[4]['active'] = true;
		parent::_setBreadcrum($breadcrum);

		$this->_header();
		$this->view->render($this->root_view.'/comp/table_tournament_match.tpl');
		$this->_footer();
		
    }
	
	function table_round_tournament($comp_id,$round_id,$table_id = 0) {
		
        $this->view->assign('mem_lib', $this->mem_lib);
		
		$Comp	=	$this->comp_model->loadByID($comp_id);
		$this->view->assign('dataComp', $Comp);
		if(!$Comp){
			header( "location: " . BASE_HREF . "api/adminfootball/comp/" );
			exit;
		}
		
		$RoundTable	= $this->roundtable_model->loadByID($round_id);
		$this->view->assign('dataRound', $RoundTable);
		if(!$RoundTable){
			header( "location: " . BASE_HREF . "api/adminfootball/comp/table/" . $comp_id );
			exit;
		}

		if(intval($table_id) > 0){
			$Table	=	$this->table_model->loadByID($table_id);
			$this->view->assign('dataTable', $Table);
			
			if(!$Table){
				header( "location: " . BASE_HREF . "api/adminfootball/comp/table/" . $comp_id );
				exit;
			}
		}
		
		$breadcrum[0]['text'] = 'Home';
        $breadcrum[0]['link'] = BASE_HREF_ADMIN;
		$breadcrum[1]['text'] = 'การแข่งขัน';
		$breadcrum[1]['link'] = BASE_HREF_ADMIN . 'comp';
		$breadcrum[2]['text'] = 'ตารางคะแนนการแข่งขัน' . $Comp['nameTHShort'];
		$breadcrum[2]['link'] = BASE_HREF_ADMIN . 'comp/table/' . $comp_id;
		$breadcrum[3]['text'] = 'ตารางคะแนนของ ' . $RoundTable['name'];
		$breadcrum[3]['link'] = BASE_HREF_ADMIN . 'comp/table_round/' . $comp_id . '/' . $round_id;
		if($table_id > 0){
			$breadcrum[4]['text'] = 'ตารางคะแนน ' . $Table['name'];
		}else{
			$breadcrum[4]['text'] = 'สร้างตารางคะแนนใหม่';
		}
		$breadcrum[4]['active'] = true;
		parent::_setBreadcrum($breadcrum);

		$this->_header();
		$this->view->render($this->root_view.'/comp/table_tournament.tpl');
		$this->_footer();
		
    }
	
	function table_round_tournament_match($comp_id,$table_id,$round_id,$round_index,$match_index) {

        $this->view->assign('mem_lib', $this->mem_lib);
		
		$Comp	=	$this->comp_model->loadByID($comp_id);
		$this->view->assign('dataComp', $Comp);
		
		if(!$Comp){
			header( "location: " . BASE_HREF . "api/adminfootball/comp/" );
			exit;
		}
		
		$RoundTable	= $this->roundtable_model->loadByID($round_id);
		$this->view->assign('dataRound', $RoundTable);
		if(!$RoundTable){
			header( "location: " . BASE_HREF . "api/adminfootball/comp/table/" . $comp_id );
			exit;
		}

		if(intval($table_id) > 0){
			$Table	=	$this->table_model->loadByID($table_id);
			$this->view->assign('dataTable', $Table);
			
			if(!$Table){
				header( "location: " . BASE_HREF . "api/adminfootball/comp/table/" . $comp_id );
				exit;
			}
		}
		
		$this->view->assign('round_index', $round_index);
		$this->view->assign('match_index', $match_index);
		
		$breadcrum[0]['text'] = 'Home';
        $breadcrum[0]['link'] = BASE_HREF_ADMIN;
		$breadcrum[1]['text'] = 'การแข่งขัน';
		$breadcrum[1]['link'] = BASE_HREF_ADMIN . 'comp';
		$breadcrum[2]['text'] = 'ตารางคะแนนการแข่งขัน' . $Comp['nameTHShort'];
		$breadcrum[2]['link'] = BASE_HREF_ADMIN . 'comp/table/' . $comp_id;
		$breadcrum[3]['text'] = 'ตารางคะแนนของ ' . $RoundTable['name'];
		$breadcrum[3]['link'] = BASE_HREF_ADMIN . 'comp/table_round/' . $comp_id . '/' . $round_id;
		$breadcrum[4]['text'] = 'ตารางคะแนน ' . $Table['name'];
		$breadcrum[4]['link'] = BASE_HREF_ADMIN . 'comp/table_round_tournament/' . $comp_id . '/' . $round_id . '/' . $table_id;
		$breadcrum[5]['text'] = 'แก้ไขการแข่งขัน';
		$breadcrum[5]['active'] = true;
		parent::_setBreadcrum($breadcrum);

		$this->_header();
		$this->view->render($this->root_view.'/comp/table_tournament_match.tpl');
		$this->_footer();
		
    }

	///////////////////////////////////////////////// scorers /////////////////////////////////////////////////
	function scorers($id) {
		
        $this->view->assign('mem_lib', $this->mem_lib);
		
		$Comp	=	$this->comp_model->loadByID($id);
		$this->view->assign('dataComp', $Comp);
		
		if(!$Comp){
			header( "location: " . BASE_HREF . "api/adminfootball/comp/" );
			exit;
		}
		
		$listScorer		=		$this->scorer_model->loadByCompID($id);
		
		foreach($listScorer as $key => $tmpData){
			$tmpTeam		=		$this->team_model->loadByID($tmpData['team_id']);
			$Logo 			= 		str_replace(' ','-',$tmpTeam['NameEN']).'.png';
			$Logo_MC		=		$this->mem_lib->get('Football2014-Team-Logo-' . $Logo);
			if($Logo_MC==true){
				$tmpTeam['Logo'] 	= 		'http://football.kapook.com/uploads/logo/' . $Logo;
			}else{
				$tmpTeam['Logo'] 	= 		'http://football.kapook.com/uploads/logo/default.png';
			}
			
			$tmpPlayer		=		$this->player_model->loadByID($tmpData['player_id']);
			
			$listScorer[$key]['dataTeam']		=		$tmpTeam;
			$listScorer[$key]['dataPlayer']		=		$tmpPlayer;
		}
		
		$this->view->assign('listScorer', $listScorer);
		
		$breadcrum[0]['text'] = 'Home';
        $breadcrum[0]['link'] = BASE_HREF_ADMIN;
		$breadcrum[1]['text'] = 'การแข่งขัน';
		$breadcrum[1]['link'] = BASE_HREF_ADMIN . 'comp';
		$breadcrum[2]['text'] = 'ดาวซัลโวการแข่งขัน' . $Comp['nameTHShort'];
		$breadcrum[2]['active'] = true;
		parent::_setBreadcrum($breadcrum);

		$this->_header();
		$this->view->render($this->root_view.'/comp/scorers.tpl');
		$this->_footer();
		
    }
	
	function scorers_select($comp_id,$id= 0) {
		
        $this->view->assign('mem_lib', $this->mem_lib);
		
		//Comp
		$Comp	=	$this->comp_model->loadByID($comp_id);
		$this->view->assign('dataComp', $Comp);
		
		if(!$Comp){
			header( "location: " . BASE_HREF . "api/adminfootball/comp/" );
			exit;
		}
		
		if(intval($id) > 0){
		
			//Scorer
			$Scorer	=	$this->scorer_model->loadByID($id);
			$this->view->assign('dataScorer', $Scorer);
			
			if(!$Scorer){
				header( "location: " . BASE_HREF . "api/adminfootball/comp/scorers/" . $comp_id );
				exit;
			}
			
			//Player
			$dataPlayer		=	$this->player_model->loadByID($Scorer['player_id']);
			$this->view->assign('dataPlayer', $dataPlayer);
			
			if(!$dataPlayer){
				header( "location: " . BASE_HREF . "api/adminfootball/comp/scorers/" . $comp_id );
				exit;
			}
			
			//Team
			$dataTeam					=		$this->team_model->loadByID($Scorer['team_id']);
			
			$Logo 						= 		str_replace(' ','-',$dataTeam['NameEN']).'.png';
			$Logo_MC					=		$this->mem_lib->get('Football2014-Team-Logo-' . $Logo);
			if($Logo_MC==true){
				$dataTeam['Logo'] 	= 		'http://football.kapook.com/uploads/logo/' . $Logo;
			}else{
				$dataTeam['Logo'] 	= 		'http://football.kapook.com/uploads/logo/default.png';
			}
			
			$this->view->assign('dataTeam', $dataTeam);
			
			if(!$dataTeam){
				header( "location: " . BASE_HREF . "api/adminfootball/comp/scorers/" . $comp_id );
				exit;
			}
		
		}
		
		$breadcrum[0]['text'] = 'Home';
        $breadcrum[0]['link'] = BASE_HREF_ADMIN;
		$breadcrum[1]['text'] = 'การแข่งขัน';
		$breadcrum[1]['link'] = BASE_HREF_ADMIN . 'comp';
		$breadcrum[2]['text'] = 'ดาวซัลโวการแข่งขัน' . $Comp['nameTHShort'];
		$breadcrum[2]['link'] = BASE_HREF_ADMIN . 'comp/scorers/' . $comp_id;
		$breadcrum[3]['text'] = 'เพิ่มผู้เล่นดาวซัลโว ' . $Comp['nameTHShort'];
		$breadcrum[3]['active'] = true;
		parent::_setBreadcrum($breadcrum);

		$this->_header();
		$this->view->render($this->root_view.'/comp/scorers_player.tpl');
		$this->_footer();
		
    }
	///////////////////////////////////////////////// Operation_Comp /////////////////////////////////////////////////
	function create(){
		$arrPOST		=		array();
		$this->view->assign('path', 'comp');
		if($this->comp_model->create($_REQUEST)){
			$arrPOST['success_create_comp']		=	1;
		}else{
			$arrPOST['error_create_comp']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function update(){
		$arrPOST		=		array();
		if($this->comp_model->update($_REQUEST['id'],$_REQUEST,$_FILES)){
			$arrPOST['success_update_comp']		=	1;
			$this->view->assign('path', 'comp');
		}else{
			$arrPOST['error_update_comp']		=	1;
			$this->view->assign('path', 'comp/info/' . $_REQUEST['id']);
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function update_news(){
		$arrPOST		=		array();
		if($this->comp_model->update_news($_REQUEST['id'],$_REQUEST)){
			$arrPOST['success_update_comp']		=	1;
			$this->view->assign('path', 'comp');
		}else{
			$arrPOST['error_update_comp']		=	1;
			$this->view->assign('path', 'comp/info/' . $_REQUEST['id']);
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function delete($id){
		$arrPOST		=		array();
		$this->view->assign('path', 'comp');
		
				
		//Clear a Match
		$this->matchcomp_model->clear_listLeagueForMatch($id);
		$this->matchcomp_model->clear_MatchDummy($id);

		//Clear a Table
		$this->table_model->clear_Table($id);
		
		//Clear a Round
		$this->roundmatch_model->clear_by_comp_id($id);
		
		if($this->comp_model->delete($id)){
			$arrPOST['success_delete_comp']		=	1;
		}else{
			$arrPOST['error_delete_comp']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	///////////////////////////////////////////////// Operation_Match /////////////////////////////////////////////////
	function get_listleagueformatch($comp_id = 0,$round_id = -1){
		if($round_id < 0){
			$listMatchLeague		=	$this->matchcomp_model->get_by_comp($comp_id,1);
		}else{
			$listMatchLeague		=	$this->matchcomp_model->get_by_round($comp_id,$round_id,1);
		}
		$returnData				=	array();
		foreach ( $listMatchLeague as $value ){
			$returnData[]		=	$this->league_model->get_league_by_id($value['league_id']);
		}
		if($_REQUEST['type']=='json'){
			echo json_encode($returnData);
		}else{
			var_dump($returnData);
		}
	}
	
	function get_listTeamForMatchDummy($side,$index_match = -1){
		if(strlen($_REQUEST['keyword'])>0){
			$returnData		=		$this->team_model->get_team_by_keyword($_REQUEST['keyword']);
		}else{
			$returnData		=		array();
		}
		$this->view->assign('mem_lib', $this->mem_lib);
		$this->view->assign('listTeam', $returnData);
		$this->view->assign('keyword', $_REQUEST['keyword']);
		$this->view->assign('side', $side);
		$this->view->assign('index_match', $index_match);
		$this->view->render($this->root_view.'/popup_teamlist_dummy.tpl');
	}
	
	function get_listTeamForMatch($side,$index_match = -1){
		if(strlen($_REQUEST['keyword'])>0){
			$returnData		=		$this->team_model->get_team_by_keyword($_REQUEST['keyword']);
		}else{
			$returnData		=		array();
		}
		$this->view->assign('mem_lib', $this->mem_lib);
		$this->view->assign('listTeam', $returnData);
		$this->view->assign('keyword', $_REQUEST['keyword']);
		$this->view->assign('side', $side);
		$this->view->assign('index_match', $index_match);
		$this->view->render($this->root_view.'/popup_teamlist_match.tpl');
	}
	
	function update_listleagueformatch(){
		//get data from $_REQUEST['league']
		if(intval($_REQUEST['round_id']) > 0){
			$this->matchcomp_model->clear_listLeagueForMatch($_REQUEST['comp_id'],$_REQUEST['round_id']);
		}else{
			$this->matchcomp_model->clear_listLeagueForMatch($_REQUEST['comp_id'],-1);
		}
		
		foreach( $_REQUEST['league'] as $tmpLeagueID ){
			if(intval($_REQUEST['round_id']) > 0){
				$this->matchcomp_model->add_listLeagueForMatch($_REQUEST['comp_id'],$_REQUEST['round_id'],$tmpLeagueID);
			}else{
				$this->matchcomp_model->add_listLeagueForMatch($_REQUEST['comp_id'],-1,$tmpLeagueID);
			}
		}
		
		if(intval($_REQUEST['round_id']) > 0){
			$this->view->assign('path', 'comp/program_round/' . $_REQUEST['comp_id'] . '/' . $_REQUEST['round_id']);
			$arrPOST['success_add_league_comp_round']		=	1;
		}else{
			$this->view->assign('path', 'comp/program/' . $_REQUEST['comp_id']);
			$arrPOST['success_add_league_comp']		=	1;
		}
		
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function create_dummyMatch(){
		//get data from $_REQUEST
		$arrPOST		=		array();
		$this->view->assign('path', 'comp/program/' . $_REQUEST['comp_id']);
		if($this->matchcomp_model->create_MatchDummy($_REQUEST['comp_id'],-1,$_REQUEST)){
			$arrPOST['success_create_match']		=	1;
		}else{
			$arrPOST['error_create_match']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function update_dummyMatch(){
		//get data from $_REQUEST
		$arrPOST		=		array();
		$this->view->assign('path', 'comp/program/' . $_REQUEST['comp_id']);
		if($this->matchcomp_model->update_MatchDummy($_REQUEST['id'],$_REQUEST)){
			$arrPOST['success_update_match']		=	1;
		}else{
			$arrPOST['error_update_match']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function create_round_dummyMatch(){
		//get data from $_REQUEST
		$arrPOST		=		array();
		$this->view->assign('path', 'comp/program_round/' . $_REQUEST['comp_id'] . '/' . $_REQUEST['round_id']);
		if($this->matchcomp_model->create_MatchDummy($_REQUEST['comp_id'],$_REQUEST['round_id'],$_REQUEST)){
			$arrPOST['success_create_match']		=	1;
		}else{
			$arrPOST['error_create_match']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function update_round_dummyMatch(){
		//get data from $_REQUEST
		$arrPOST		=		array();
		$this->view->assign('path', 'comp/program_round/' . $_REQUEST['comp_id'] . '/' . $_REQUEST['round_id']);
		if($this->matchcomp_model->update_MatchDummy($_REQUEST['id'],$_REQUEST)){
			$arrPOST['success_update_match']		=	1;
		}else{
			$arrPOST['error_update_match']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function delete_dummyMatch($id){
		$arrPOST		=		array();
		if(intval($_REQUEST['round_id'])>0){
			$this->view->assign('path', 'comp/program_round/' . $_REQUEST['comp_id'] . '/' . $_REQUEST['round_id']);
		}else{
			$this->view->assign('path', 'comp/program/' . $_REQUEST['comp_id']);
		}
		if($this->matchcomp_model->delete_MatchDummy($id)){
			$arrPOST['success_delete_match']		=	1;
		}else{
			$arrPOST['error_delete_match']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function create_roundmatch(){
		//get data from $_REQUEST
		$arrPOST		=		array();
		$this->view->assign('path', 'comp/program/' . $_REQUEST['comp_id']);
		if($this->roundmatch_model->create_RoundMatch($_REQUEST['comp_id'],$_REQUEST)){
			$arrPOST['success_create_roundmatch']		=	1;
		}else{
			$arrPOST['error_create_roundmatch']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
		var_dump($_REQUEST);
	}
	
	function update_roundmatch(){
		//get data from $_REQUEST
		$arrPOST		=		array();
		$this->view->assign('path', 'comp/program_round/' . $_REQUEST['comp_id'] . '/'. $_REQUEST['id']);
		if($this->roundmatch_model->update_RoundMatch($_REQUEST['id'],$_REQUEST)){
			$arrPOST['success_create_roundmatch']		=	1;
		}else{
			$arrPOST['error_create_roundmatch']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
		var_dump($_REQUEST);
	}
	
	function delete_roundmatch($id){
		//get data from $_REQUEST
		$arrPOST		=		array();
		$this->view->assign('path', 'comp/program/' . $_REQUEST['comp_id']);
		if($this->roundmatch_model->delete_RoundMatch($id)){
			$arrPOST['success_delete_roundmatch']		=	1;
			$this->matchcomp_model->clear_listLeagueForMatch(intval($_REQUEST['comp_id']),$id);
			$this->matchcomp_model->clear_MatchDummy(intval($_REQUEST['comp_id']),$id);
		}else{
			$arrPOST['error_delete_roundmatch']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
		var_dump($_REQUEST);
	}
	
	function order_up_roundmatch($comp_id,$roundmacth_id = 0){
		$arrPOST		=		array();
		$this->view->assign('path', 'comp/program/' . $comp_id);
		if($this->roundmatch_model->order_up($roundmacth_id)){
			$arrPOST['success_order_up_roundmatch']		=	1;
		}else{
			$arrPOST['error_order_up_roundmatch']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function order_down_roundmatch($comp_id,$roundmacth_id = 0){
		$arrPOST		=		array();
		$this->view->assign('path', 'comp/program/' . $comp_id);
		if($this->roundmatch_model->order_down($roundmacth_id)){
			$arrPOST['success_order_down_roundmatch']		=	1;
		}else{
			$arrPOST['error_order_down_roundmatch']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	///////////////////////////////////////////////// Operation_Table /////////////////////////////////////////////////
	function get_listleagueandmatchfortable($table_id){
	
		$dataTable	=	$this->table_model->loadByID($table_id);
	
		$arrDataLeague	=	array();
		$arrDataMatch	=	array();
		$arrDataTeam	=	array();
		
		foreach ( $dataTable['league_list'] as $value ){
			$arrDataLeague[]	=	$this->league_model->get_league_by_id($value);
		}
		foreach ( $dataTable['match_list'] as $value ){
			$arrDataMatch[]		=	$this->match_model->get_match_by_id($value);
		}
		foreach ( $dataTable['team_list'] as $value ){
			$tmpTeam			=	$this->team_model->loadByID($value["team_id"]);
			$tmpLeague			=	$this->league_model->get_league_by_id($value["league_id"]);
			$arrDataTeam[]		=	array(
				"team_id"			=>		$tmpTeam["id"],
				"TeamEN"			=>		$tmpTeam["NameEN"],
				"TeamTH"			=>		$tmpTeam["NameTH"],
				"league_id"			=>		$tmpLeague["id"],
				"LeagueEN"			=>		$tmpLeague["NameEN"],
				"LeagueTH"			=>		$tmpLeague["NameTH"],
				"LeagueDescription"	=>		$tmpLeague["Description"]
			);
		}
		
		$returnData	=	array( 'league_list' => $arrDataLeague , 'match_list' => $arrDataMatch, 'team_list' => $arrDataTeam );
		
		if($_REQUEST['type']=='json'){
			echo json_encode($returnData);
		}else{
			var_dump($returnData);
		}
	}
	
	function get_listteamfortable($table_id){
		$returnData		=		$this->table_model->loadListTeamForTable($table_id);
		if($_REQUEST['type']=='json'){
			echo json_encode($returnData);
		}else{
			var_dump($returnData);
		}
	}
	
	function create_table_select(){
		//get data from $_REQUEST
		$arrPOST		=		array();
		$this->view->assign('path', 'comp/table/' . $_REQUEST['comp_id']);
		if($this->table_model->create_tableSelect($_REQUEST['comp_id'],-1,$_REQUEST)){
			$arrPOST['success_create_table']		=	1;
		}else{
			$arrPOST['error_create_table']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function update_table_select(){
		//get data from $_REQUEST
		$arrPOST		=		array();
		$this->view->assign('path', 'comp/table/' . $_REQUEST['comp_id']);
		if($this->table_model->update_tableSelect($_REQUEST['id'],$_REQUEST)){
			$arrPOST['success_update_table']		=	1;
		}else{
			$arrPOST['error_update_table']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function create_table_round_select(){
		//get data from $_REQUEST
		$arrPOST		=		array();
		$this->view->assign('path', 'comp/table_round/' . $_REQUEST['comp_id'] . '/' . $_REQUEST['round_id']);
		if($this->table_model->create_tableSelect($_REQUEST['comp_id'],$_REQUEST['round_id'],$_REQUEST)){
			$arrPOST['success_create_table']	=	1;
		}else{
			$arrPOST['error_create_table']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function update_table_round_select(){
		//get data from $_REQUEST
		$arrPOST		=		array();
		$this->view->assign('path', 'comp/table_round/' . $_REQUEST['comp_id'] . '/' . $_REQUEST['round_id']);
		if($this->table_model->update_tableSelect($_REQUEST['id'],$_REQUEST)){
			$arrPOST['success_update_table']		=	1;
		}else{
			$arrPOST['error_update_table']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function create_table_dummy(){
		//get data from $_REQUEST
		$arrPOST		=		array();
		$this->view->assign('path', 'comp/table/' . $_REQUEST['comp_id']);
		if($this->table_model->create_tableDummy($_REQUEST['comp_id'],-1,$_REQUEST)){
			$arrPOST['success_create_table']		=	1;
		}else{
			$arrPOST['error_create_table']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function update_table_dummy(){
		//get data from $_REQUEST
		$arrPOST		=		array();
		$this->view->assign('path', 'comp/table/' . $_REQUEST['comp_id']);
		if($this->table_model->update_tableDummy($_REQUEST['id'],$_REQUEST)){
			$arrPOST['success_update_table']		=	1;
		}else{
			$arrPOST['error_update_table']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function create_table_round_dummy(){
		//get data from $_REQUEST
		$arrPOST		=		array();
		$this->view->assign('path', 'comp/table_round/' . $_REQUEST['comp_id'] . '/' . $_REQUEST['round_id']);
		if($this->table_model->create_tableDummy($_REQUEST['comp_id'],$_REQUEST['round_id'],$_REQUEST)){
			$arrPOST['success_create_table']		=	1;
		}else{
			$arrPOST['error_create_table']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function update_table_round_dummy(){
		//get data from $_REQUEST
		$arrPOST		=		array();
		$this->view->assign('path', 'comp/table_round/' . $_REQUEST['comp_id'] . '/' . $_REQUEST['round_id']);
		if($this->table_model->update_tableDummy($_REQUEST['id'],$_REQUEST)){
			$arrPOST['success_update_table']		=	1;
		}else{
			$arrPOST['error_update_table']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function create_table_tournament(){
		//get data from $_REQUEST
		$arrPOST		=		array();
		$this->view->assign('path', 'comp/table/' . $_REQUEST['comp_id']);
		if($this->table_model->create_tableTournament($_REQUEST['comp_id'],-1,$_REQUEST)){
			$arrPOST['success_create_table']		=	1;
		}else{
			$arrPOST['error_create_table']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function update_table_tournament(){
		//get data from $_REQUEST
		$arrPOST		=		array();
		$this->view->assign('path', 'comp/table/' . $_REQUEST['comp_id']);
		if($this->table_model->update_tableTournament($_REQUEST['id'],$_REQUEST)){
			$arrPOST['success_update_table']	=	1;
		}else{
			$arrPOST['error_update_table']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function create_table_round_tournament(){
		//get data from $_REQUEST
		$arrPOST		=		array();
		$this->view->assign('path', 'comp/table_round/' . $_REQUEST['comp_id'] . '/' . $_REQUEST['round_id']);
		if($this->table_model->create_tableTournament($_REQUEST['comp_id'],$_REQUEST['round_id'],$_REQUEST)){
			$arrPOST['success_create_table']		=	1;
		}else{
			$arrPOST['error_create_table']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function update_table_round_tournament(){
		//get data from $_REQUEST
		$arrPOST		=		array();
		$this->view->assign('path', 'comp/table_round/' . $_REQUEST['comp_id'] . '/' . $_REQUEST['round_id']);
		if($this->table_model->update_tableTournament($_REQUEST['id'],$_REQUEST)){
			$arrPOST['success_update_table']	=	1;
		}else{
			$arrPOST['error_update_table']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function update_table_tournament_match(){
		//get data from $_REQUEST
		$arrPOST		=		array();
		if(intval($_REQUEST['round_id'])>0){
			$this->view->assign('path', 'comp/table_round_tournament/' . $_REQUEST['comp_id'] . '/' . $_REQUEST['round_id'] . '/' . $_REQUEST['id']);
		}else{
			$this->view->assign('path', 'comp/table_tournament/' . $_REQUEST['comp_id'] . '/' . $_REQUEST['id']);
		}
		if($this->table_model->update_tableTournamentMatch($_REQUEST)){
			$arrPOST['success_update_table']		=	1;
		}else{
			$arrPOST['error_update_table']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	
	function delete_table($id){
		$arrPOST		=		array();
		if(intval($_REQUEST['round_id'])>0){
			$this->view->assign('path', 'comp/table_round/' . $_REQUEST['comp_id'] . '/' . $_REQUEST['round_id']);
		}else{
			$this->view->assign('path', 'comp/table/' . $_REQUEST['comp_id']);
		}
		
		if($this->table_model->delete($id)){
			$arrPOST['success_delete_table']		=	1;
		}else{
			$arrPOST['error_delete_table']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function order_up_table($comp_id,$table_id = 0){
		$arrPOST		=		array();
		$this->view->assign('path', 'comp/table/' . $comp_id);
		if($this->table_model->order_up($table_id)){
			$arrPOST['success_order_up_table']		=	1;
		}else{
			$arrPOST['error_order_up_table']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function order_down_table($comp_id,$table_id = 0){
		$arrPOST		=		array();
		$this->view->assign('path', 'comp/table_round/' . $comp_id);
		if($this->table_model->order_down($table_id)){
			$arrPOST['success_order_up_table']		=	1;
		}else{
			$arrPOST['error_order_up_table']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function order_up_tableinround($comp_id,$round_id =0,$table_id = 0){
		$arrPOST		=		array();
		$this->view->assign('path', 'comp/table_round/' . $comp_id . '/' . $round_id);
		if($this->table_model->order_up($table_id)){
			$arrPOST['success_order_up_table']		=	1;
		}else{
			$arrPOST['error_order_up_table']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function order_down_tableinround($comp_id,$round_id =0,$table_id = 0){
		$arrPOST		=		array();
		$this->view->assign('path', 'comp/table/' . $comp_id . '/' . $round_id);
		if($this->table_model->order_down($table_id)){
			$arrPOST['success_order_up_table']		=	1;
		}else{
			$arrPOST['error_order_up_table']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function create_roundtable(){
		//get data from $_REQUEST
		$arrPOST		=		array();
		$this->view->assign('path', 'comp/table/' . $_REQUEST['comp_id']);
		if($this->roundtable_model->create_RoundTable($_REQUEST['comp_id'],$_REQUEST)){
			$arrPOST['success_create_roundtable']		=	1;
		}else{
			$arrPOST['error_create_roundtable']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
		var_dump($_REQUEST);
	}
	
	function update_roundtable(){
		//get data from $_REQUEST
		$arrPOST		=		array();
		$this->view->assign('path', 'comp/table_round/' . $_REQUEST['comp_id'] . '/'. $_REQUEST['id']);
		if($this->roundtable_model->update_RoundTable($_REQUEST['id'],$_REQUEST)){
			$arrPOST['success_create_roundtable']		=	1;
		}else{
			$arrPOST['error_create_roundtable']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
		var_dump($_REQUEST);
	}
	
	function delete_roundtable($id){
		//get data from $_REQUEST
		$arrPOST		=		array();
		$this->view->assign('path', 'comp/table/' . $_REQUEST['comp_id']);
		if($this->roundtable_model->delete_RoundTable($id)){
			$arrPOST['success_delete_roundtable']		=	1;
			$this->table_model->clear_Table(intval($_REQUEST['comp_id']),$id);
		}else{
			$arrPOST['error_delete_roundtable']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
		var_dump($_REQUEST);
	}
	
	function order_up_roundtable($comp_id,$roundmacth_id = 0){
		$arrPOST		=		array();
		$this->view->assign('path', 'comp/table/' . $comp_id);
		if($this->roundtable_model->order_up($roundmacth_id)){
			$arrPOST['success_order_up_roundtable']		=	1;
		}else{
			$arrPOST['error_order_up_roundtable']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function order_down_roundtable($comp_id,$roundmacth_id = 0){
		$arrPOST		=		array();
		$this->view->assign('path', 'comp/table/' . $comp_id);
		if($this->roundtable_model->order_down($roundmacth_id)){
			$arrPOST['success_order_down_roundtable']		=	1;
		}else{
			$arrPOST['error_order_down_roundtable']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function refresh_table($comp_id,$table_id = 0) {
        $arrPOST		=		array();
		$this->view->assign('path', 'comp/');
		
		$Comp	=	$this->comp_model->loadByID($comp_id);
		$this->mem_lib->delete('Comp-Table-' . $Comp['nameURL']);
		$arrPOST['success_refresh_table']		=	1;
		
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
    }
	
	///////////////////////////////////////////////// Operation_Scorer /////////////////////////////////////////////////
	function create_scorer(){
		//get data from $_REQUEST
		$arrPOST		=		array();
		$this->view->assign('path', 'comp/scorers/' . $_REQUEST['comp_id']);
		if($this->scorer_model->create($_REQUEST)){
			$arrPOST['success_create_scorer']		=	1;
		}else{
			$arrPOST['error_create_scorer']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function update_scorer(){
		//get data from $_REQUEST
		$arrPOST		=		array();
		$this->view->assign('path', 'comp/scorers_select/' . $_REQUEST['comp_id'] . '/' . $_REQUEST['id']);
		if($this->scorer_model->update($_REQUEST['id'],$_REQUEST)){
			$arrPOST['success_update_scorer']		=	1;
		}else{
			$arrPOST['error_update_scorer']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function update_scorer_goal(){
		//get data from $_REQUEST
		$arrPOST		=		array();
		$this->view->assign('path', 'comp/scorers/' . $_REQUEST['comp_id']);
		if($this->scorer_model->update_goal($_REQUEST['id'],$_REQUEST)){
			$arrPOST['success_update_scorer']		=	1;
		}else{
			$arrPOST['error_update_scorer']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function delete_scorer($id){
		$arrPOST		=		array();
		$this->view->assign('path', 'comp/scorers/' . $_REQUEST['comp_id']);
		if($this->scorer_model->delete($id)){
			$arrPOST['success_delete_match']		=	1;
		}else{
			$arrPOST['error_delete_match']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	///////////////////////////////////////////////// Operation_Tabs /////////////////////////////////////////////////
	function set_tabtable($comp_id,$roundtable_id = -1){
		$arrPOST		=		array();
		$this->view->assign('path', 'comp/table/' . $comp_id);
		if($this->comp_model->update_tabtable(intval($comp_id),intval($roundtable_id))){
			$arrPOST['success_update_tabtable']		=	1;
		}else{
			$arrPOST['error_update_tabtable']		=	1;
		}
		
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function set_tabprogram($comp_id,$roundmatch_id = -1){
		$arrPOST		=		array();
		$this->view->assign('path', 'comp/program/' . $comp_id);
		if($this->comp_model->update_tabprogram(intval($comp_id),intval($roundmatch_id))){
			$arrPOST['success_update_tabprogram']		=	1;
		}else{
			$arrPOST['error_update_tabprogram']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	///////////////////////////////////////////////// Operation_News /////////////////////////////////////////////////
	function get_listTeamKeyword($id){
		$Comp	=	$this->comp_model->loadByID($id);
		$returnData				=	array();
		foreach($Comp['kapookfootball_News_CountryOtherKeyword3'] as $tmpID){
			$tmpTeam		=		$this->team_model->loadByID(intval($tmpID));
			$returnData[]	=		array( 'value' => $tmpTeam['id'],'name' => $tmpTeam['NameEN']);
		}
		if($_REQUEST['type']=='json'){
			echo json_encode($returnData);
		}else{
			var_dump($returnData);
		}
	}
	
}
