<?php

if (!defined('MINIZONE'))
    exit;

class Livescore extends My_con {

    var $view;
    var $minizone;

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct() {
        parent::__construct();
		$this->match_model = $this->minizone->model('match_model');
		$this->team_model = $this->minizone->model('team_model');
		$this->league_model = $this->minizone->model('league_model');
		$this->zone_model = $this->minizone->model('zone_model');
    }

	///////////////////////////////////////////////// Operation /////////////////////////////////////////////////
	function getjson(){
		$returnData		=		$this->match_model->get_match($_REQUEST);
		foreach($returnData as $key => $tmpData){
			//Zone
			$tmpZoneData = $this->mem_lib->get('Football2014-Zone-' . $tmpData['KPLeagueCountryID']);
			//var_dump($tmpZoneData);
			if(!$tmpZoneData){
				$tmpZoneData = $this->league_model->get_league_by_id(intval($tmpData['KPLeagueCountryID']));
				$this->mem_lib->set('Football2014-Zone-' . $tmpData['KPLeagueCountryID'],$tmpZoneData);
			}
			if(strlen($tmpZoneData['NameTH'])){
				$returnData[$key]['XSLeagueCountry']	=	$tmpZoneData['NameTH'];
			}
			
			//League
			$tmpLeagueData = $this->mem_lib->get('Football2014-League-' . $tmpData['KPLeagueID']);
			if(!$tmpLeagueData){
				$tmpLeagueData = $this->league_model->get_league_by_id(intval($tmpData['KPLeagueID']));
				$this->mem_lib->set('Football2014-League-'.$tmpData['KPLeagueID'],$tmpLeagueData);
			}
			if(strlen($tmpLeagueData['NameTH'])){
				$returnData[$key]['XSLeagueName']	=	$tmpLeagueData['NameTH'];
			}
			if(strlen($tmpLeagueData['NameTH'])){
				$returnData[$key]['XSLeagueDesc']	=	$tmpLeagueData['NameTHShort'];
			}
			
			//Team1
			$dataTeam = $this->mem_lib->get('Football2014-Team-'.$tmpData['Team1KPID']);
			if(!$dataTeam){
				$dataTeam = $this->team_model->loadByID(intval($tmpData['Team1KPID']));
				$this->mem_lib->set('Football2014-Team-'.$tmpData['Team1KPID'],$dataTeam);
			}
			if(!empty($dataTeam['NameTH'])){
				$returnData[$key]['Team1Show']		=	$dataTeam['NameTH'];
			}else if(!empty($dataTeam['NameTHShort'])){
				$returnData[$key]['Team1Show']		=	$dataTeam['NameTHShort'];
			}else{
				$returnData[$key]['Team1Show']		=	$dataTeam['NameEN'];
			}	
			$Logo 						= 		str_replace(' ','-',$dataTeam['NameEN']).'.png';
			$Logo_MC					=		$this->mem_lib->get('Football2014-Team-Logo-' . $Logo);
			if($Logo_MC==true){
				$returnData[$key]['Team1Logo'] = 		'http://football.kapook.com/uploads/logo/' . $Logo;
			}else{
				$returnData[$key]['Team1Logo'] = 		'http://football.kapook.com/uploads/logo/default.png';
			}
			
			//Team2
			$dataTeam = $this->mem_lib->get('Football2014-Team-'.$tmpData['Team2KPID']);
			if(!$dataTeam){
				$dataTeam = $this->team_model->loadByID(intval($tmpData['Team2KPID']));
				$this->mem_lib->set('Football2014-Team-'.$tmpData['Team2KPID'],$dataTeam);
			}
			if(!empty($dataTeam['NameTH'])){
				$returnData[$key]['Team2Show']		=	$dataTeam['NameTH'];
			}else if(!empty($dataTeam['NameTHShort'])){
				$returnData[$key]['Team2Show']		=	$dataTeam['NameTHShort'];
			}else{
				$returnData[$key]['Team2Show']		=	$dataTeam['NameEN'];
			}	
			$Logo 						= 		str_replace(' ','-',$dataTeam['NameEN']).'.png';
			$Logo_MC					=		$this->mem_lib->get('Football2014-Team-Logo-' . $Logo);
			if($Logo_MC==true){
				$returnData[$key]['Team2Logo'] = 		'http://football.kapook.com/uploads/logo/' . $Logo;
			}else{
				$returnData[$key]['Team2Logo'] = 		'http://football.kapook.com/uploads/logo/default.png';
			}
			
			//ETScore
			if($tmpData['ETScore']=='-'){
				$returnData[$key]['isET']			=		false;
				$returnData[$key]['Team1ETScore']	=		0;
				$returnData[$key]['Team2ETScore']	=		0;
			}else{
				$returnData[$key]['isET']			=		true;
				$tmpET 								=	 	explode('-',$tmpData['ETScore']);
				$returnData[$key]['Team1ETScore']	=		intval($tmpET[0]);
				$returnData[$key]['Team2ETScore']	=		intval($tmpET[1]);
			}
			
			//PNScore
			if($tmpData['PNScore']=='-'){
				$returnData[$key]['isPN']			=		false;
				$returnData[$key]['Team1PNScore']	=		0;
				$returnData[$key]['Team2PNScore']	=		0;
			}else{
				$returnData[$key]['isPN']			=		true;
				$tmpPN 								=	 	explode('-',$tmpData['PNScore']);
				$returnData[$key]['Team1PNScore']	=		intval($tmpPN[0]);
				$returnData[$key]['Team2PNScore']	=		intval($tmpPN[1]);
			}

			$returnData[$key]['url']				=	strtolower('http://football.kapook.com/match-'.$tmpData['id'].'-'.$tmpData['Team1'].'-'.$tmpData['Team2']);
			$returnData[$key]['url'] 				= 	preg_replace('/\s+/', '-', $returnData[$key]['url']);
			
			//Delete Not Used data
			unset($returnData[$key]['MatchDateTimeMongo']);
			unset($returnData[$key]['LastUpdate']);
			unset($returnData[$key]['LastUpdateMongo']);
			unset($returnData[$key]['Team1LP']);
			unset($returnData[$key]['Team2LP']);
			unset($returnData[$key]['Team1YC']);
			unset($returnData[$key]['Team2YC']);
			unset($returnData[$key]['Team1RC']);
			unset($returnData[$key]['Team2RC']);
			unset($returnData[$key]['LastScorers']);
			unset($returnData[$key]['CreateDateMongo']);
			unset($returnData[$key]['CreateDate']);
			unset($returnData[$key]['View']);
			//unset($returnData[$key]['AutoMode']);
			unset($returnData[$key]['PictureOG']);
			unset($returnData[$key]['Analysis']);
			unset($returnData[$key]['Result']);
			unset($returnData[$key]['question']);
			unset($returnData[$key]['count_playside']);
			unset($returnData[$key]['count_playside_team1']);
			unset($returnData[$key]['count_playside_team2']);
			unset($returnData[$key]['Picture']);
		}
		echo json_encode($returnData);
	}
	
	function getjson_new(){
		$returnData		=		$this->match_model->get_match_new($_REQUEST);
		foreach($returnData as $key => $tmpData){
			//Zone
			$tmpZoneData = $this->mem_lib->get('Football2014-Zone-' . $tmpData['KPLeagueCountryID']);
			//var_dump($tmpZoneData);
			if(!$tmpZoneData){
				$tmpZoneData = $this->league_model->get_league_by_id(intval($tmpData['KPLeagueCountryID']));
				$this->mem_lib->set('Football2014-Zone-' . $tmpData['KPLeagueCountryID'],$tmpZoneData);
			}
			if(strlen($tmpZoneData['NameTH'])){
				$returnData[$key]['XSLeagueCountry']	=	$tmpZoneData['NameTH'];
			}
			
			//League
			$tmpLeagueData = $this->mem_lib->get('Football2014-League-' . $tmpData['KPLeagueID']);
			if(!$tmpLeagueData){
				$tmpLeagueData = $this->league_model->get_league_by_id(intval($tmpData['KPLeagueID']));
				$this->mem_lib->set('Football2014-League-'.$tmpData['KPLeagueID'],$tmpLeagueData);
			}
			if(strlen($tmpLeagueData['NameTH'])){
				$returnData[$key]['XSLeagueName']	=	$tmpLeagueData['NameTH'];
			}
			if(strlen($tmpLeagueData['NameTH'])){
				$returnData[$key]['XSLeagueDesc']	=	$tmpLeagueData['NameTHShort'];
			}
			
			//Team1
			$dataTeam = $this->mem_lib->get('Football2014-Team-'.$tmpData['Team1KPID']);
			if(!$dataTeam){
				$dataTeam = $this->team_model->loadByID(intval($tmpData['Team1KPID']));
				$this->mem_lib->set('Football2014-Team-'.$tmpData['Team1KPID'],$dataTeam);
			}
			if(!empty($dataTeam['NameTH'])){
				$returnData[$key]['Team1Show']		=	$dataTeam['NameTH'];
			}else if(!empty($dataTeam['NameTHShort'])){
				$returnData[$key]['Team1Show']		=	$dataTeam['NameTHShort'];
			}else{
				$returnData[$key]['Team1Show']		=	$dataTeam['NameEN'];
			}	
			$Logo 						= 		str_replace(' ','-',$dataTeam['NameEN']).'.png';
			$Logo_MC					=		$this->mem_lib->get('Football2014-Team-Logo-' . $Logo);
			if($Logo_MC==true){
				$returnData[$key]['Team1Logo'] = 		'http://football.kapook.com/uploads/logo/' . $Logo;
			}else{
				$returnData[$key]['Team1Logo'] = 		'http://football.kapook.com/uploads/logo/default.png';
			}
			
			//Team2
			$dataTeam = $this->mem_lib->get('Football2014-Team-'.$tmpData['Team2KPID']);
			if(!$dataTeam){
				$dataTeam = $this->team_model->loadByID(intval($tmpData['Team2KPID']));
				$this->mem_lib->set('Football2014-Team-'.$tmpData['Team2KPID'],$dataTeam);
			}
			if(!empty($dataTeam['NameTH'])){
				$returnData[$key]['Team2Show']		=	$dataTeam['NameTH'];
			}else if(!empty($dataTeam['NameTHShort'])){
				$returnData[$key]['Team2Show']		=	$dataTeam['NameTHShort'];
			}else{
				$returnData[$key]['Team2Show']		=	$dataTeam['NameEN'];
			}	
			$Logo 						= 		str_replace(' ','-',$dataTeam['NameEN']).'.png';
			$Logo_MC					=		$this->mem_lib->get('Football2014-Team-Logo-' . $Logo);
			if($Logo_MC==true){
				$returnData[$key]['Team2Logo'] = 		'http://football.kapook.com/uploads/logo/' . $Logo;
			}else{
				$returnData[$key]['Team2Logo'] = 		'http://football.kapook.com/uploads/logo/default.png';
			}
			
			//ETScore
			if($tmpData['ETScore']=='-'){
				$returnData[$key]['isET']			=		false;
				$returnData[$key]['Team1ETScore']	=		0;
				$returnData[$key]['Team2ETScore']	=		0;
			}else{
				$returnData[$key]['isET']			=		true;
				$tmpET 								=	 	explode('-',$tmpData['ETScore']);
				$returnData[$key]['Team1ETScore']	=		intval($tmpET[0]);
				$returnData[$key]['Team2ETScore']	=		intval($tmpET[1]);
			}
			
			//PNScore
			if($tmpData['PNScore']=='-'){
				$returnData[$key]['isPN']			=		false;
				$returnData[$key]['Team1PNScore']	=		0;
				$returnData[$key]['Team2PNScore']	=		0;
			}else{
				$returnData[$key]['isPN']			=		true;
				$tmpPN 								=	 	explode('-',$tmpData['PNScore']);
				$returnData[$key]['Team1PNScore']	=		intval($tmpPN[0]);
				$returnData[$key]['Team2PNScore']	=		intval($tmpPN[1]);
			}

			$returnData[$key]['url']				=	strtolower('http://football.kapook.com/match-'.$tmpData['id'].'-'.$tmpData['Team1'].'-'.$tmpData['Team2']);
			$returnData[$key]['url'] 				= 	preg_replace('/\s+/', '-', $returnData[$key]['url']);
			
			if(!isset($returnData[$key]['TeamOdds'])){
				$returnData[$key]['TeamOdds'] 		= 	0;
			}
			
			if($returnData[$key]['Odds'] <= 0){
				$returnData[$key]['TeamOdds'] 		= 	0;
			}
			
			if(!isset($returnData[$key]['TeamSelect'])){
				$returnData[$key]['TeamSelect']		=	0;
			}
			
			//Delete Not Used data
			unset($returnData[$key]['MatchDateTimeMongo']);
			unset($returnData[$key]['LastUpdate']);
			unset($returnData[$key]['LastUpdateMongo']);
			unset($returnData[$key]['Team1LP']);
			unset($returnData[$key]['Team2LP']);
			unset($returnData[$key]['Team1YC']);
			unset($returnData[$key]['Team2YC']);
			unset($returnData[$key]['Team1RC']);
			unset($returnData[$key]['Team2RC']);
			unset($returnData[$key]['LastScorers']);
			unset($returnData[$key]['CreateDateMongo']);
			unset($returnData[$key]['CreateDate']);
			unset($returnData[$key]['View']);
			//unset($returnData[$key]['AutoMode']);
			unset($returnData[$key]['PictureOG']);
			unset($returnData[$key]['Analysis']);
			unset($returnData[$key]['Result']);
			unset($returnData[$key]['question']);
			unset($returnData[$key]['count_playside']);
			unset($returnData[$key]['count_playside_team1']);
			unset($returnData[$key]['count_playside_team2']);
			unset($returnData[$key]['Picture']);
		}
		echo json_encode($returnData);
	}
}
