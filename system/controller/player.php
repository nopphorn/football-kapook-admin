<?php

if (!defined('MINIZONE'))
    exit;

class Player extends My_con {

    var $view;
    var $minizone;

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct() {
        parent::__construct();
		$this->player_model = $this->minizone->model('player_model');
		$this->scorer_model = $this->minizone->model('scorer_model');
		$this->team_model = $this->minizone->model('team_model');
		$this->view->assign('site_id', 3);
    }

    ///////////////////////////////////////////////// Index /////////////////////////////////////////////////
    function index() {

        $this->view->assign('mem_lib', $this->mem_lib);
		
		if(isset($_REQUEST['page'])){
			$page = intval($_REQUEST['page']);
		}else{
			$page = 1;
		}
		
		if(isset($_REQUEST['keyword'])){
			$arrPlayer	=	$this->player_model->load(10,intval($page),$_REQUEST['keyword']);
		}else{
			$arrPlayer	=	$this->player_model->load(10,intval($page));
		}

		$this->view->assign('arrPlayer', $arrPlayer);

		$breadcrum[0]['text'] = 'Home';
        $breadcrum[0]['link'] = BASE_HREF_ADMIN;
		$breadcrum[1]['text'] = 'ผู้เล่น';
		$breadcrum[1]['active'] = true;
		parent::_setBreadcrum($breadcrum);
		
		$this->_header();
		$this->view->render($this->root_view.'/player/index.tpl');
		$this->_footer();

    }
	function info($id = 0) {

        $this->view->assign('mem_lib', $this->mem_lib);
		
		if($id > 0){
			$dataPlayer	=	$this->player_model->loadByID($id);
			$this->view->assign('dataPlayer', $dataPlayer);
		}
		
		$breadcrum[0]['text'] = 'Home';
        $breadcrum[0]['link'] = BASE_HREF_ADMIN;
		$breadcrum[1]['text'] = 'ผู้เล่น';
		$breadcrum[1]['link'] = BASE_HREF_ADMIN . 'player';
		$breadcrum[2]['text'] = 'จัดการผู้เล่น';
		$breadcrum[2]['active'] = true;
		parent::_setBreadcrum($breadcrum);

		$this->_header();
		$this->view->render($this->root_view.'/player/info.tpl');
		$this->_footer();
    }
	///////////////////////////////////////////////// Operation_Player /////////////////////////////////////////////////
	function create(){
		$arrPOST		=		array();
		$this->view->assign('path', 'player');
		if($this->player_model->create($_REQUEST,$_FILES)){
			$arrPOST['success_create_player']		=	1;
		}else{
			$arrPOST['error_create_player']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function update(){
		$arrPOST		=		array();
		if($this->player_model->update($_REQUEST['id'],$_REQUEST,$_FILES)){
			$arrPOST['success_update_player']		=	1;
			$this->view->assign('path', 'player');
		}else{
			$arrPOST['error_update_player']		=	1;
			$this->view->assign('path', 'player/info/' . $_REQUEST['id']);
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function delete($id){
		$arrPOST		=		array();
		$this->view->assign('path', 'player');

		if($this->player_model->delete($id)){
			$arrPOST['success_delete_player']		=	1;
			$this->scorer_model->deleteByPlayerID($id);
		}else{
			$arrPOST['error_delete_player']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function get_listTeamForPlayer(){
		if(strlen($_REQUEST['keyword'])>0){
			$returnData		=		$this->team_model->get_team_by_keyword($_REQUEST['keyword']);
		}else{
			$returnData		=		array();
		}
		$this->view->assign('mem_lib', $this->mem_lib);
		$this->view->assign('listTeam', $returnData);
		$this->view->assign('keyword', $_REQUEST['keyword']);
		$this->view->render($this->root_view.'/popup_teamlist_player.tpl');
	}
	
	function get_listPlayerForScorer(){
		if(strlen($_REQUEST['keyword'])>0){
			$returnData		=		$this->player_model->get_player_by_keyword($_REQUEST['keyword']);
		}else{
			$returnData		=		array();
		}
		$this->view->assign('mem_lib', $this->mem_lib);
		$this->view->assign('listPlayer', $returnData);
		$this->view->assign('keyword', $_REQUEST['keyword']);
		$this->view->render($this->root_view.'/popup_playerlist_player.tpl');
	}
}
