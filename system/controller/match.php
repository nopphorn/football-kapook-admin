<?php

if (!defined('MINIZONE'))
    exit;

class Match extends My_con {

    var $view;
    var $minizone;

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct() {
        parent::__construct();
		$this->zone_model 	= $this->minizone->model('zone_model');
		$this->league_model = $this->minizone->model('league_model');
		$this->match_model 	= $this->minizone->model('match_model');
		$this->team_model 	= $this->minizone->model('team_model');
		$this->search_match_model = $this->minizone->model('search_match_model');
		$this->view->assign('site_id', 7);
    }

    ///////////////////////////////////////////////// Index /////////////////////////////////////////////////
    function index() {
        $this->view->assign('mem_lib', $this->mem_lib);
		
		$breadcrum[0]['text'] = 'Home';
        $breadcrum[0]['link'] = BASE_HREF_ADMIN;
		$breadcrum[1]['text'] = 'การแข่งขัน';
		$breadcrum[1]['active'] = true;
		parent::_setBreadcrum($breadcrum);
		
		$dataZone	=	$this->zone_model->get_zone();
		$this->view->assign('dataZone', $dataZone);

		$this->_header();
		$this->view->render($this->root_view.'/match/index.tpl');
		$this->_footer();
    }
	
	function index_2() {
        $this->view->assign('mem_lib', $this->mem_lib);
		
		$breadcrum[0]['text'] = 'Home';
        $breadcrum[0]['link'] = BASE_HREF_ADMIN;
		$breadcrum[1]['text'] = 'การแข่งขัน';
		$breadcrum[1]['active'] = true;
		parent::_setBreadcrum($breadcrum);
		
		$dataZone	=	$this->zone_model->get_zone();
		$this->view->assign('dataZone', $dataZone);

		$this->_header();
		$this->view->render($this->root_view.'/match/index_2.tpl');
		$this->_footer();
    }
	
	function detail($match_id = '') {
        $this->view->assign('mem_lib', $this->mem_lib);
		
		$dataMatch	=	$this->match_model->get_match_by_id(intval($match_id));
		if(!$dataMatch){
			header( "location: " . BASE_HREF . "api/adminfootball/match" );
			exit;
		}
		//Team1
		$dataTeam					=		$this->team_model->loadByID(intval($dataMatch['Team1KPID']));
		if(!empty($dataTeam['NameTH'])){
			$dataMatch['Team1Show']		=	$dataTeam['NameTH'];
		}else if(!empty($dataTeam['NameTHShort'])){
			$dataMatch['Team1Show']		=	$dataTeam['NameTHShort'];
		}else{
			$dataMatch['Team1Show']		=	$dataTeam['NameEN'];
		}	
		$Logo 						= 		str_replace(' ','-',$dataTeam['NameEN']).'.png';
		$Logo_MC					=		$this->mem_lib->get('Football2014-Team-Logo-' . $Logo);
		if($Logo_MC==true){
			$dataMatch['Team1Logo'] = 		'http://football.kapook.com/uploads/logo/' . $Logo;
		}else{
			$dataMatch['Team1Logo'] = 		'http://football.kapook.com/uploads/logo/default.png';
		}
		
		//Team2
		$dataTeam					=		$this->team_model->loadByID(intval($dataMatch['Team2KPID']));
		if(!empty($dataTeam['NameTH'])){
			$dataMatch['Team2Show']		=	$dataTeam['NameTH'];
		}else if(!empty($dataTeam['NameTHShort'])){
			$dataMatch['Team2Show']		=	$dataTeam['NameTHShort'];
		}else{
			$dataMatch['Team2Show']		=	$dataTeam['NameEN'];
		}	
		$Logo 						= 		str_replace(' ','-',$dataTeam['NameEN']).'.png';
		$Logo_MC					=		$this->mem_lib->get('Football2014-Team-Logo-' . $Logo);
		if($Logo_MC==true){
			$dataMatch['Team2Logo'] = 		'http://football.kapook.com/uploads/logo/' . $Logo;
		}else{
			$dataMatch['Team2Logo'] = 		'http://football.kapook.com/uploads/logo/default.png';
		}
		
		//ETScore
		if($dataMatch['ETScore']=='-'){
			$dataMatch['isET']			=		false;
		}else{
			$dataMatch['isET']			=		true;
			$tmpET 						=	 	explode('-',$dataMatch['ETScore']);
			$dataMatch['Team1ETScore']	=		intval($tmpET[0]);
			$dataMatch['Team2ETScore']	=		intval($tmpET[1]);
		}
		
		//PNScore
		if($dataMatch['PNScore']=='-'){
			$dataMatch['isPN']			=		false;
		}else{
			$dataMatch['isPN']			=		true;
			$tmpPN 						=	 	explode('-',$dataMatch['PNScore']);
			$dataMatch['Team1PNScore']	=		intval($tmpPN[0]);
			$dataMatch['Team2PNScore']	=		intval($tmpPN[1]);
		}
		
		//League Data
		$tmpLeagueData = $this->league_model->get_league_by_id(intval($dataMatch['KPLeagueID']));
		if(strlen($tmpLeagueData['NameTH'])){
			$dataMatch['XSLeagueName']	=	$tmpLeagueData['NameTH'];
		}
		if(strlen($tmpLeagueData['NameTH'])){
			$dataMatch['XSLeagueDesc']	=	$tmpLeagueData['NameTHShort'];
		}
		$dataMatch['KPZoneID']			=	intval($tmpLeagueData['KPZoneID']);

		$this->view->assign('dataMatch', $dataMatch);
		
		$breadcrum[0]['text'] = 'Home';
        $breadcrum[0]['link'] = BASE_HREF_ADMIN;
		$breadcrum[1]['text'] = 'การแข่งขัน';
		$breadcrum[1]['link'] = BASE_HREF_ADMIN . 'match';
		$breadcrum[2]['text'] = 'แก้ไข';
		$breadcrum[2]['active'] = true;
		parent::_setBreadcrum($breadcrum);

		$this->_header();
		$this->view->render($this->root_view.'/match/detail.tpl');
		$this->_footer();

    }
	
	function update($match_id = '') {
		header('Content-Type: application/json');
		$arrPOST		=		array();
		if(!isset($_REQUEST['TVLiveList'])){
			$_REQUEST['TVLiveList']		=	array();
		}
		
		$_REQUEST['isEditET']		=	true;
		$_REQUEST['isEditPN']		=	true;
		
		if($this->match_model->update(intval($match_id),$_REQUEST,$_FILES)){
			$dataMatch	=	$this->match_model->get_match_by_id(intval($match_id));
			echo json_encode(array( 'is_success' => 1 , 'data' => $_REQUEST , 'data_lastest' => $dataMatch ));
		}else{
			echo json_encode(array( 'is_success' => 0 , 'data' => $_REQUEST ));
		}
	}
	
	function update_mini($match_id = '') {
		$arrPOST		=		array();
		if($this->match_model->update(intval($match_id),$_REQUEST,$_FILES)){
			$arrPOST['success_update_match']		=	1;
		}else{
			$arrPOST['success_update_match']		=	1;
		}
		$this->view->assign('path', 'match/detail/' . $match_id);
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function update_detail($match_id = '') {
		$arrPOST		=		array();
		if(!isset($_REQUEST['TVLiveList'])){
			$_REQUEST['TVLiveList']		=	array();
		}
		if(!isset($_REQUEST['inputNameScorers'])){
			$_REQUEST['inputNameScorers']		=	array();
		}
		$_REQUEST['isEditET']		=	true;
		$_REQUEST['isEditPN']		=	true;

		if($this->match_model->update(intval($match_id),$_REQUEST,$_FILES)){
			$arrPOST['success_update_match']		=	1;
		}else{
			$arrPOST['success_error_match']		=	1;
		}

		$this->view->assign('path', 'match/detail/' . $match_id);
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function tv_list() {
		$this->view->render($this->root_view.'/match/tvlist.tpl');
	}
	
	function delete_dup() {
		header('Content-Type: application/json');
		$lismatch = $this->match_model->delete_dup();
		foreach($lismatch as $data){
			if($data['count']<=1){
				break;
			}
			echo $data['_id'] . ':' .  $data['count'] . '<br>';
			$dataMatch	=	$this->match_model->get_match_by_id(intval($data['_id']));
			unset($dataMatch['_id']);
			
			$this->match_model->del_match_by_id(intval($data['_id']));
			$this->match_model->add_match_by_id($dataMatch);
		}
		echo json_encode(array( 'is_success' => 1 , 'data' => $_REQUEST ));
	}
	
	function get_list_search() {
		header('Content-Type: application/json');
		$returnData = $this->search_match_model->get_list();
		echo json_encode($returnData);
	}
	
	function add_search() {
		header('Content-Type: application/json');
		if($this->search_match_model->add($_REQUEST)){
			echo json_encode(array( 'is_success' => 1 , 'data' => $_REQUEST ));
		}else{
			echo json_encode(array( 'is_success' => 0 , 'data' => $_REQUEST ));
		}
	}
	
	function remove_search($search_id = -1) {
		if($this->search_match_model->remove(intval($search_id))){
			echo json_encode(array( 'is_success' => 1 , 'data' => $_REQUEST ));
		}else{
			echo json_encode(array( 'is_success' => 0 , 'data' => $_REQUEST ));
		}
	}
	
}
