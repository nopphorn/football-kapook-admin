<?php

if (!defined('MINIZONE'))
    exit;

class Market extends My_con {

    var $view;
    var $minizone;

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct() {
        parent::__construct();
		$this->player_model = $this->minizone->model('player_model');
		$this->team_model = $this->minizone->model('team_model');
		$this->market_model = $this->minizone->model('market_model');
		$this->view->assign('site_id', 8);
    }

    ///////////////////////////////////////////////// Index /////////////////////////////////////////////////
	function index() {
		if(intval($_REQUEST['page']) > 0){
			$page = intval($_REQUEST['page']);
		}else{
			$page = 1;
		}
		
		$data_market = $this->market_model->load(10,$page);
		foreach($data_market['list'] as $key => $item){
			// Player
			$tmpPlayer		=	$this->player_model->loadByID(intval($item['PlayerID']));
			$data_market['list'][$key]['PlayerData'] = $tmpPlayer;
			//var_dump($tmpPlayer);
			// Team Out
			$tmpTeamOut		=	$this->team_model->loadByID(intval($item['FromTeamKPID']));
			$Logo 			= 	str_replace(' ','-',$tmpTeamOut['NameEN']).'.png';
			$Logo_MC		=	$this->mem_lib->get('Football2014-Team-Logo-' . $Logo);
			if($Logo_MC==true){
				$tmpTeamOut['Logo'] 	= 		'http://football.kapook.com/uploads/logo/' . $Logo;
			}else{
				$tmpTeamOut['Logo'] 	= 		'http://football.kapook.com/uploads/logo/default.png';
			}
			$data_market['list'][$key]['TeamOutData'] = $tmpTeamOut;
			// Team In
			$tmpTeamIn		=	$this->team_model->loadByID(intval($item['ToTeamKPID']));
			$Logo 			= 	str_replace(' ','-',$tmpTeamIn['NameEN']).'.png';
			$Logo_MC		=	$this->mem_lib->get('Football2014-Team-Logo-' . $Logo);
			if($Logo_MC==true){
				$tmpTeamIn['Logo'] 	= 		'http://football.kapook.com/uploads/logo/' . $Logo;
			}else{
				$tmpTeamIn['Logo'] 	= 		'http://football.kapook.com/uploads/logo/default.png';
			}
			$data_market['list'][$key]['TeamInData'] = $tmpTeamIn;
		}
		
		$this->view->assign('data_market', $data_market);
		
		$breadcrum[0]['text'] = 'Home';
        $breadcrum[0]['link'] = BASE_HREF_ADMIN;
		$breadcrum[1]['text'] = 'ตลาดซื้อขายนักเตะ';
		$breadcrum[1]['active'] = true;
		parent::_setBreadcrum($breadcrum);

		$this->_header();
		$this->view->render($this->root_view.'/market/index.tpl');
		$this->_footer();
	}
	
	function info($id = 0) {
		
		$this->view->assign('mem_lib', $this->mem_lib);
		
		if($id > 0){
			$dataMarket	=	$this->market_model->loadByID($id);

			// Player
			$tmpPlayer		=	$this->player_model->loadByID(intval($dataMarket['PlayerID']));
			$dataMarket['PlayerData'] = $tmpPlayer;

			// Team Out
			$tmpTeamOut		=	$this->team_model->loadByID(intval($dataMarket['FromTeamKPID']));
			$Logo 			= 	str_replace(' ','-',$tmpTeamOut['NameEN']).'.png';
			$Logo_MC		=	$this->mem_lib->get('Football2014-Team-Logo-' . $Logo);
			if($Logo_MC==true){
				$tmpTeamOut['Logo'] 	= 		'http://football.kapook.com/uploads/logo/' . $Logo;
			}else{
				$tmpTeamOut['Logo'] 	= 		'http://football.kapook.com/uploads/logo/default.png';
			}
			$dataMarket['TeamOutData'] = $tmpTeamOut;
			// Team In
			$tmpTeamIn		=	$this->team_model->loadByID(intval($dataMarket['ToTeamKPID']));
			$Logo 			= 	str_replace(' ','-',$tmpTeamIn['NameEN']).'.png';
			$Logo_MC		=	$this->mem_lib->get('Football2014-Team-Logo-' . $Logo);
			if($Logo_MC==true){
				$tmpTeamIn['Logo'] 	= 		'http://football.kapook.com/uploads/logo/' . $Logo;
			}else{
				$tmpTeamIn['Logo'] 	= 		'http://football.kapook.com/uploads/logo/default.png';
			}
			$dataMarket['TeamInData'] = $tmpTeamIn;
			
			$this->view->assign('dataMarket', $dataMarket);
		}
		
		$breadcrum[0]['text'] = 'Home';
        $breadcrum[0]['link'] = BASE_HREF_ADMIN;
		$breadcrum[1]['text'] = 'ตลาดซื้อขายนักเตะ';
		$breadcrum[1]['link'] = BASE_HREF_ADMIN . 'market';
		$breadcrum[2]['text'] = 'จัดการซื้อขายนักเตะ';
		$breadcrum[2]['active'] = true;
		parent::_setBreadcrum($breadcrum);

		$this->_header();
		$this->view->render($this->root_view.'/market/info.tpl');
		$this->_footer();
	}
	
    function duplicate_player_from_market() {
	
		$data_market = $this->market_model->load_all();
		foreach($data_market as $item){
			$dataPlayer = $this->player_model->load(10,1,$item['NameTH']);
			if(!$dataPlayer['size']){
				$data_player['name'] = $item['NameTH'];
				$data_player['imgSelect'] = '3';
				$this->player_model->create($data_player,null);
			}else{
				$item["PlayerID"]	=	$dataPlayer["list"][0]["id"];
				$this->market_model->update($item['id'],$item);
			}
		}

		echo 'Finish';
		
    }
	
	function get_listTeamForOut(){
		if(strlen($_REQUEST['keyword'])>0){
			$returnData		=		$this->team_model->get_team_by_keyword($_REQUEST['keyword']);
		}else{
			$returnData		=		array();
		}
		$this->view->assign('mem_lib', $this->mem_lib);
		$this->view->assign('listTeam', $returnData);
		$this->view->assign('keyword', $_REQUEST['keyword']);
		$this->view->assign('isOut', true);
		$this->view->render($this->root_view.'/popup_teamlist_market.tpl');
	}
	
	function get_listTeamForIn(){
		if(strlen($_REQUEST['keyword'])>0){
			$returnData		=		$this->team_model->get_team_by_keyword($_REQUEST['keyword']);
		}else{
			$returnData		=		array();
		}
		$this->view->assign('mem_lib', $this->mem_lib);
		$this->view->assign('listTeam', $returnData);
		$this->view->assign('keyword', $_REQUEST['keyword']);
		$this->view->assign('isOut', false);
		$this->view->render($this->root_view.'/popup_teamlist_market.tpl');
	}
	
	function create(){
		$arrPOST		=		array();
		$this->view->assign('path', 'market');
		if($this->market_model->create($_REQUEST)){
			$arrPOST['success_create_market']		=	1;
		}else{
			$arrPOST['error_create_market']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function update(){
		$arrPOST		=		array();
		if($this->market_model->update($_REQUEST['id'],$_REQUEST)){
			$arrPOST['success_update_market']		=	1;
			$this->view->assign('path', 'market');
		}else{
			$arrPOST['error_update_market']		=	1;
			$this->view->assign('path', 'player/info/' . $_REQUEST['id']);
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
	
	function delete($id){
		$arrPOST		=		array();
		$this->view->assign('path', 'market');

		if($this->market_model->delete($id)){
			$arrPOST['success_delete_market']		=	1;
		}else{
			$arrPOST['error_delete_market']		=	1;
		}
		$this->view->assign('arrData', $arrPOST);
		$this->view->render($this->root_view.'/redirect.tpl');
	}
}
