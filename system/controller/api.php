<?php

if (!defined('MINIZONE'))
    exit;

class Api extends My_con {

    var $view;
    var $minizone;
	var $teamDataTmp = array();

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct() {
        parent::__construct(false);
		$this->comp_model = $this->minizone->model('comp_model');
		$this->team_model = $this->minizone->model('team_model');
		$this->table_model = $this->minizone->model('table_model');
		$this->zone_model = $this->minizone->model('zone_model');
		$this->matchcomp_model = $this->minizone->model('matchcomp_model');
		$this->match_model = $this->minizone->model('match_model');
		//$this->league_model = $this->minizone->model('league_model');
		$this->scorer_model = $this->minizone->model('scorer_model');
		$this->player_model = $this->minizone->model('player_model');
		$this->news_model = $this->minizone->model('news_model');
		$this->market_model = $this->minizone->model('market_model');
		$this->roundmatch_model = $this->minizone->model('roundmatch_model');
		$this->roundtable_model	= $this->minizone->model('roundtable_model');
		
		$this->view->assign('site_id', 2);
    }
	
	function index($cupname = '',$command = null) {
		phpinfo();
    }
	
	private function setting_match_dummy($infoMatch){
		$infoMatch['FTScore']		=		intval($infoMatch['Team1FTScore']) . '-' . intval($infoMatch['Team2FTScore']);
		$infoMatch['dummy_id']		=		$infoMatch['id'];
		//$infoMatch['id']			=		'(' . $infoMatch['id'] . ')';
		unset($infoMatch['id']);
		if(!strlen($infoMatch['Team1Logo'])){
			$infoMatch['Team1Logo']	=	'http://football.kapook.com/uploads/logo/default.png';
		}
		if(!strlen($infoMatch['Team2Logo'])){
			$infoMatch['Team2Logo']	=	'http://football.kapook.com/uploads/logo/default.png';
		}
		return $infoMatch;
	}
	
	private function setting_match_real($infoMatch){
		$matchURL = strtolower('http://football.kapook.com/match-'.$infoMatch['id'].'-'.$infoMatch['Team1'].'-'.$infoMatch['Team2']);
		$infoMatch['MatchPageURL'] = preg_replace('/\s+/', '-', $matchURL);
						
		///////////////////////////////////Team 1////////////////////////////////////////////////
		$team_id_1		=		$infoMatch['Team1KPID'];
		if(!isset($teamDataTmp[$team_id_1])){
			$teamDataTmp[$team_id_1]		=		$this->team_model->loadByID($team_id_1);
		}
		//Team Name
		if(!empty($teamDataTmp[$team_id_1]['NameTHShort'])){
			$strTeam	=	$teamDataTmp[$team_id_1]['NameTHShort'];
		}else if(!empty($teamDataTmp[$team_id_1]['NameTH'])){
			$strTeam	=	$teamDataTmp[$team_id_1]['NameTH'];
		}else{
			$strTeam	=	$teamDataTmp[$team_id_1]['NameEN'];
		}
		$infoMatch['Team1']	=	$strTeam;
						
		//Team Logo
		$Logo 						= 		str_replace(' ','-',$teamDataTmp[$team_id_1]['NameEN']).'.png';
		$Logo_MC					=		$this->mem_lib->get('Football2014-Team-Logo-' . $Logo);
		if($Logo_MC==true){
			$pathlogo 				= 		'http://football.kapook.com/uploads/logo/' . $Logo;
		}else{
			$pathlogo 				= 		'http://football.kapook.com/uploads/logo/default.png';
		}
		$infoMatch['Team1Logo']			=		$pathlogo;
						
		//Team Link
		$infoMatch['Team1URL']			=		'http://football.kapook.com/team-' . strtolower(str_replace(" ","-",$teamDataTmp[$team_id_1]['NameEN']));
						
		///////////////////////////////////Team 2////////////////////////////////////////////////
		$team_id_2		=		$infoMatch['Team2KPID'];
		if(!isset($teamDataTmp[$team_id_2])){
			$teamDataTmp[$team_id_2]		=		$this->team_model->loadByID($team_id_2);
		}
		//Team Name
		if(!empty($teamDataTmp[$team_id_2]['NameTHShort'])){
			$strTeam	=	$teamDataTmp[$team_id_2]['NameTHShort'];
		}else if(!empty($teamDataTmp[$team_id_2]['NameTH'])){
			$strTeam	=	$teamDataTmp[$team_id_2]['NameTH'];
		}else{
			$strTeam	=	$teamDataTmp[$team_id_2]['NameEN'];
		}
		$infoMatch['Team2']	=	$strTeam;
						
		//Team Logo
		$Logo 						= 		str_replace(' ','-',$teamDataTmp[$team_id_2]['NameEN']).'.png';
		$Logo_MC					=		$this->mem_lib->get('Football2014-Team-Logo-' . $Logo);
		if($Logo_MC==true){
			$pathlogo 				= 		'http://football.kapook.com/uploads/logo/' . $Logo;
		}else{
			$pathlogo 				= 		'http://football.kapook.com/uploads/logo/default.png';
		}
		$infoMatch['Team2Logo']			=		$pathlogo;
						
		//Team Link
		$infoMatch['Team2URL']			=		'http://football.kapook.com/team-' . strtolower(str_replace(" ","-",$teamDataTmp[$team_id_2]['NameEN']));
		return $infoMatch;
	}
	
	private function compare_point($a,$b)
	{
		if ($a['point'] == $b['point'])
		{
			if($a['goal_dif'] == $b['goal_dif'])
			{
				if ($a['goal_for'] == $b['goal_for'])
				{
					if ($a['order'] == $b['order'])
					{
						if((!isset($a['NameEN']))||(!isset($b['NameEN']))){
							if((!isset($a['index']))||(!isset($b['index']))){
								return 0;
							}
							elseif ($a['index'] == $b['index']){
								return 0;
							}
							return ($a['index'] < $b['index']) ? -1 : 1;
						}
						elseif ($a['NameEN'] == $b['NameEN']){
							return 0;
						}
						return ($a['NameEN'] > $b['NameEN']) ? -1 : 1;
					}
					return ($a['order'] > $b['order']) ? -1 : 1;
				}
				return ($a['goal_for'] > $b['goal_for']) ? -1 : 1;
			}
			return ($a['goal_dif'] > $b['goal_dif']) ? -1 : 1;
		}
		return ($a['point'] > $b['point']) ? -1 : 1;
	}

    function Comp($cupname = '',$command = null,$option = null,$option_2 = null) {
	
		$result			=		array();
		$expire			=		3600;
		$expire_match 	= 		60;
		
		if(strlen($cupname) > 0){
			$result 	= 	$this->mem_lib->get('Comp-Info-'.$cupname);
			if ((!$result)||($_REQUEST['remove_cache'] == 1)){
				$result		=	$this->comp_model->loadByName($cupname);
				if (!$result){
					$result		=		-1;
				}
				$this->mem_lib->set('Comp-Info-'.$cupname, $result, MEMCACHE_COMPRESSED, $expire);
			}
			if($command == 'info'){
				//$result	=	$this->comp_model->loadByName($cupname);
			}
		}
		
		// Print a data
		if($_REQUEST['type']=='json'){
			header('Content-Type: application/json');
			echo json_encode($result);
		}else{
			var_dump($result);
		}
		
    }
	
	function Program($cupname = '') {
		$result			=		array();
		$expire			=		3600;
		$expire_match 	= 		60;
		
		if(strlen($cupname) > 0){
			$result 	= 	$this->mem_lib->get('Comp-Info-'.$cupname);
			if ((!$result)||($_REQUEST['remove_cache'] == 1)){
				$result		=	$this->comp_model->loadByName($cupname);
				if (!$result){
					$result		=		-1;
				}
				$this->mem_lib->set('Comp-Info-'.$cupname, $result, MEMCACHE_COMPRESSED, $expire);
			}
			if($result != -1){
				$id		=		$result['id'];
				// List Of League
				$listCompLeague			=	$this->mem_lib->get('Comp-Program-ListLeague-' . $cupname . '-All');
				if((!$listCompLeague) || ($_REQUEST['remove_cache'] == 1)){
					$listCompLeague			=	$this->matchcomp_model->get_by_comp($id,1,true);
					if(!$listCompLeague){
						$this->mem_lib->set('Comp-Program-ListLeague-' . $cupname . '-All', -1, MEMCACHE_COMPRESSED, $expire);
					}else{
						$this->mem_lib->set('Comp-Program-ListLeague-' . $cupname . '-All', $listCompLeague, MEMCACHE_COMPRESSED, $expire);
					}
					$result['cache_listLeague'] = false;
				}else{
					if($listCompLeague==-1){
						$listCompLeague = array();
					}
					$result['cache_listLeague'] = true;
				}
				
				if(isset($_REQUEST['date'])){ // by date or "today"
					$dataLeague				=	array();
					foreach ( $listCompLeague as $value ){
						$dataLeague[]		=	intval($value['league_id']);
					}
					$namecache = 'Comp-Program-' . $cupname . '-' . $_REQUEST['date'];
					$listMatch = $this->mem_lib->get($namecache);
					
					if ( (!$listMatch) || ($_REQUEST['remove_cache'] == 1) ){
						if($_REQUEST['date'] == 'today'){
							$revert		=		false;
							$onlyDate	=		'';
							if(date('G')>=12){
								$today					=	date("Y-m-d");
								$listMatchLeague 		= 	$this->match_model->get_by_listLeagueID($dataLeague,$today,'','asc');
								$listMatchDummy			=	$this->matchcomp_model->loadListMatchDummy($id,0,$today,'','asc');
								if((count($listMatchLeague)<=0)&&(count($listMatchDummy)<=0)){
									$listMatchLeague 		= 	$this->match_model->get_by_listLeagueID($dataLeague,"",$toDate,'desc');
									$listMatchDummy			=	$this->matchcomp_model->loadListMatchDummy($id,0,"",$toDate,'desc');
									$revert					=	true;
								}
							}else{
								$fromDate				=	date("Y-m-d", strtotime("-1 day"));
								$toDate					=	date("Y-m-d");
								$listMatchLeague 		= 	$this->match_model->get_by_listLeagueID($dataLeague,$fromDate,$toDate,'desc');
								$listMatchDummy			=	$this->matchcomp_model->loadListMatchDummy($id,0,$fromDate,$toDate,'desc');
								
								if((count($listMatchLeague)<=0)&&(count($listMatchDummy)<=0)){
									if(date('G')>=6){
										$today					=	date("Y-m-d");
									}else{
										$today					=	date("Y-m-d", strtotime("-1 day"));
									}
									$listMatchLeague 		= 	$this->match_model->get_by_listLeagueID($dataLeague,$today,'','asc');
									$listMatchDummy			=	$this->matchcomp_model->loadListMatchDummy($id,0,$today,'','asc');
									
									if((count($listMatchLeague)<=0)&&(count($listMatchDummy)<=0)){
										$listMatchLeague 		= 	$this->match_model->get_by_listLeagueID($dataLeague,"",$toDate,'desc');
										$listMatchDummy			=	$this->matchcomp_model->loadListMatchDummy($id,0,"",$toDate,'desc');
										$revert					=	true;
									}
									
								}else{
									$revert					=	true;
								}
							}
							$listMatch				=	array();
							$indexDummy =	0;
							
							foreach ( $listMatchLeague as $value ){
									
								if($revert){
									for( ; $indexDummy < count($listMatchDummy)  ; $indexDummy++ ){
										if( strtotime($listMatchDummy[$indexDummy]['MatchDateTime']) >= strtotime($value['MatchDateTime']) ){
												
											$MatchDate		=	$listMatchDummy[$indexDummy]['MatchDateTime'];
											if(date('G',strtotime($MatchDate))>=6){
												$tmpDateMatch		=		date("Y-m-d", strtotime($MatchDate));
											}else{
												$tmpDateMatch		=		date("Y-m-d", strtotime($MatchDate .' - 1 days'));
											}
											if($onlyDate==''){
												$onlyDate			=		$tmpDateMatch;
											}else if($onlyDate!=$tmpDateMatch){
												break;
											}
											array_unshift($listMatch,$this->setting_match_dummy($listMatchDummy[$indexDummy]));
										}else{
											break;
										}
									}
								}else{
									for( ; $indexDummy < count($listMatchDummy)  ; $indexDummy++ ){
										if( strtotime($listMatchDummy[$indexDummy]['MatchDateTime']) < strtotime($value['MatchDateTime']) ){
												
											$MatchDate		=	$listMatchDummy[$indexDummy]['MatchDateTime'];
											if(date('G',strtotime($MatchDate))>=6){
												$tmpDateMatch		=		date("Y-m-d", strtotime($MatchDate));
											}else{
												$tmpDateMatch		=		date("Y-m-d", strtotime($MatchDate .' - 1 days'));
											}
											if($onlyDate==''){
												$onlyDate			=		$tmpDateMatch;
											}else if($onlyDate!=$tmpDateMatch){
												break;
											}
											$listMatch[] = $this->setting_match_dummy($listMatchDummy[$indexDummy]);
										}else{
											break;
										}
									}
								}
									
								$MatchDate		=	$value['MatchDateTime'];
								if(date('G',strtotime($MatchDate))>=6){
									$tmpDateMatch		=		date("Y-m-d", strtotime($MatchDate));
								}else{
									$tmpDateMatch		=		date("Y-m-d", strtotime($MatchDate .' - 1 days'));
								}
								if($onlyDate==''){
									$onlyDate			=		$tmpDateMatch;
								}else if($onlyDate!=$tmpDateMatch){
									break;
								}

								if($revert){
									array_unshift($listMatch,$this->setting_match_real($value));
								}else{
									$listMatch[]	=	$this->setting_match_real($value);
								}
							}
							for( ; $indexDummy < count($listMatchDummy)  ; $indexDummy++ ){
							
								$MatchDate		=	$listMatchDummy[$indexDummy]['MatchDateTime'];
								if(date('G',strtotime($MatchDate))>=6){
									$tmpDateMatch		=		date("Y-m-d", strtotime($MatchDate));
								}else{
									$tmpDateMatch		=		date("Y-m-d", strtotime($MatchDate .' - 1 days'));
								}
								if($onlyDate==''){
									$onlyDate			=		$tmpDateMatch;
								}else if($onlyDate!=$tmpDateMatch){
									break;
								}
								if($revert){
									array_unshift($listMatch,$this->setting_match_dummy($listMatchDummy[$indexDummy]));
								}else{
									$listMatch[] = $this->setting_match_dummy($listMatchDummy[$indexDummy]);
								}
							}
							$result['Today']	=	$onlyDate;
						}else if($_REQUEST['date'] == 'all'){ //all in one
							$listMatchLeague 		= 	$this->match_model->get_by_listLeagueID($dataLeague,'','','asc');
							$listMatchDummy			=	$this->matchcomp_model->loadListMatchDummy($id,0,'','','asc');

							$listMatch				=	array();
						
							$indexDummy =	0;
							foreach ( $listMatchLeague as $value ){
								for( ; $indexDummy < count($listMatchDummy)  ; $indexDummy++ ){
									if( strtotime($listMatchDummy[$indexDummy]['MatchDateTime']) < strtotime($value['MatchDateTime']) ){
										$listMatch[]	=	$this->setting_match_dummy($listMatchDummy[$indexDummy]);
									}else{
										break;
									}
								}

								$listMatch[]	=	$this->setting_match_real($value);
							}
							for( ; $indexDummy < count($listMatchDummy)  ; $indexDummy++ ){
								$listMatch[] = $this->setting_match_dummy($listMatchDummy[$indexDummy]);
							}
						}else{
							$fromDate			=	date("Y-m-d", strtotime($_REQUEST['date'] . "06:00:00"));
							$toDate				=	date("Y-m-d", strtotime($_REQUEST['date'] . "06:00:00 +1 day"));
							
							$listMatchLeague 		= 	$this->match_model->get_by_listLeagueID($dataLeague,$fromDate,$toDate,'asc');
							$listMatchDummy			=	$this->matchcomp_model->loadListMatchDummy($id,0,$fromDate,$toDate,'asc');
							$listMatch				=	array();
						
							$indexDummy =	0;
							foreach ( $listMatchLeague as $value ){
								for( ; $indexDummy < count($listMatchDummy)  ; $indexDummy++ ){
									if( strtotime($listMatchDummy[$indexDummy]['MatchDateTime']) < strtotime($value['MatchDateTime']) ){
										$listMatch[]	=	$this->setting_match_dummy($listMatchDummy[$indexDummy]);
									}else{
										break;
									}
								}

								$listMatch[]	=	$this->setting_match_real($value);
							}
							for( ; $indexDummy < count($listMatchDummy)  ; $indexDummy++ ){
								$listMatch[] = $this->setting_match_dummy($listMatchDummy[$indexDummy]);
							}
						}
						if(!$listMatch){
							$this->mem_lib->set($namecache, -1, MEMCACHE_COMPRESSED, $expire_match);
						}else{
							$this->mem_lib->set($namecache, $listMatch, MEMCACHE_COMPRESSED, $expire_match);
						}
					}
					
					if($listMatch == -1){
						$listMatch = array();
					}
					$result['program']	=	$listMatch;
					//$result['listCompLeague'] = $listCompLeague;
				}else{ // all by round
					// List Of Round
					$listRound			=	$this->mem_lib->get('Comp-Program-ListRound-' . $cupname);
					if((!$listRound) || ($_REQUEST['remove_cache'] == 1)){
						$listRound				=	$this->roundmatch_model->loadListRound($id);
						if(!$listRound){
							$this->mem_lib->set('Comp-Program-ListRound-' . $cupname, -1, MEMCACHE_COMPRESSED, $expire);
						}else{
							$this->mem_lib->set('Comp-Program-ListRound-' . $cupname, $listRound, MEMCACHE_COMPRESSED, $expire);
						}
					}else if($listRound == -1){
						$listRound = array();
					}
					
					$indexRound				=	array();
					foreach ( $listRound as $key => $value ){
						$indexRound[$value['id']] = intval($key);
					}
					
					$arrLeagueIDAll			=	array();
					$arrLeagueID			=	array();
					$arrLeagueIDRound		=	array();
					
					foreach ( $listCompLeague as $value ){
						$arrLeagueIDAll[]		=	intval($value['league_id']);
						$round_id			=	intval($value['round_id']);
						if( $round_id > -1){
							if(!isset($arrLeagueIDRound[$round_id])){
								$arrLeagueIDRound[$round_id]		=	array();
							}
							$arrLeagueIDRound[$round_id][]		=	intval($value['league_id']);
						}else{
							$arrLeagueID[]					=	intval($value['league_id']);
						}
					}
					$result['arrLeagueIDAll'] 	= 	$arrLeagueIDAll;
					$result['arrLeagueID'] 		= 	$arrLeagueID;
					$result['indexRound']		=	$indexRound;
					$result['arrLeagueIDRound'] = 	$arrLeagueIDRound;
					
					$namecache = 'Comp-Program-' . $cupname;
					$listMatch = $this->mem_lib->get($namecache);
					
					if ( (!$listMatch) || ($_REQUEST['remove_cache'] == 1) ){
						$listMatchLeague 		= 	$this->match_model->get_by_listLeagueID($arrLeagueIDAll,'','','asc');
						$listMatchDummy			=	$this->matchcomp_model->loadListMatchDummy($id,0,'','','asc');
						
						$listMatch				=	array();
						
						$indexDummy =	0;
						foreach ( $listMatchLeague as $value ){
							for( ; $indexDummy < count($listMatchDummy)  ; $indexDummy++ ){
								if(	( strtotime($listMatchDummy[$indexDummy]['MatchDateTime']) < strtotime($value['MatchDateTime']) ) ){
									if($listMatchDummy['round_id'] == -1){
										$listMatch[]	=	$this->setting_match_dummy($listMatchDummy[$indexDummy]);
									}else{
										$tmpIdxRound	=	$indexRound[$listMatchDummy[$indexDummy]['round_id']];
										if(!isset($listRound[$tmpIdxRound]['Program'])){
											$listRound[$tmpIdxRound]['Program'] = array();
										}
										$listRound[$tmpIdxRound]['Program'][] = $this->setting_match_dummy($listMatchDummy[$indexDummy]);
									}
								}else{
									break;
								}
							}

							$tmpMatch	=	$this->setting_match_real($value);
							if(in_array($tmpMatch['KPLeagueID'], $arrLeagueID)){
								$listMatch[] = $tmpMatch;
							}
							
							foreach ( $listRound as $key => $value ){
								$tmp_round_id = $value['id'];
								if(!isset($listRound[$key]['Program'])){
									$listRound[$key]['Program'] = array();
								}
								if(in_array($tmpMatch['KPLeagueID'], $arrLeagueIDRound[$tmp_round_id])){
									$listRound[$key]['Program'][] = $tmpMatch;
								}
							}
						}
						for( ; $indexDummy < count($listMatchDummy)  ; $indexDummy++ ){
							if($listMatchDummy['round_id'] == -1){
								$listMatch[]	=	$this->setting_match_dummy($listMatchDummy[$indexDummy]);
							}else{
								$tmpIdxRound	=	$indexRound[$listMatchDummy[$indexDummy]['round_id']];
								if(!isset($listRound[$tmpIdxRound]['Program'])){
									$listRound[$tmpIdxRound]['Program'] = array();
								}
								$listRound[$tmpIdxRound]['Program'][] = $this->setting_match_dummy($listMatchDummy[$indexDummy]);
							}
						}
						$this->mem_lib->set($namecache, $listMatch, MEMCACHE_COMPRESSED, $expire_match);
						$this->mem_lib->set('Comp-Program-ListRound-' . $cupname, $listRound, MEMCACHE_COMPRESSED, $expire);
					}
					$result['program']			=	$listMatch;
					$result['programByRound']	=	$listRound;
				}
			}
		}
		
		// Print a data
		if($_REQUEST['type']=='json'){
			header('Content-Type: application/json');
			echo json_encode($result);
		}else{
			var_dump($result);
		}
	}
	
	function StatData($cupname = '') {
		$result			=		array();
		$expire			=		3600;
		$expire_stat	=		900;
		
		if(strlen($cupname) > 0){
			$result 	= 	$this->mem_lib->get('Comp-Info-'.$cupname);
			if ((!$result)||($_REQUEST['remove_cache'] == 1)){
				$result		=	$this->comp_model->loadByName($cupname);
				if (!$result){
					$result		=		-1;
				}
				$this->mem_lib->set('Comp-Info-'.$cupname, $result, MEMCACHE_COMPRESSED, $expire);
			}
			if($result != -1){
				$stat_data 				= 	$this->mem_lib->get('Comp-StatInfo-'.$cupname);
				if( (!$stat_data) || ($_REQUEST['remove_cache'] == 1) ){
					$stat_data				=	array(
						'Played' => 0,
						'Goal' => 0,
						'TeamGF' => array( 'id' => -1, 'goal' => 0 ),
						'TeamGA' => array( 'id' => -1, 'goal' => 0 )
					);
					$team_data				=	array();
					$id						=	$result['id'];
					$listCompLeague			=	$this->mem_lib->get('Comp-Program-ListLeague-' . $cupname . '-All');
					if((!$listCompLeague) || ($_REQUEST['remove_cache'] == 1)){
						$listCompLeague			=	$this->matchcomp_model->get_by_comp($id,1,true);
						if(!$listCompLeague){
							$this->mem_lib->set('Comp-Program-ListLeague-' . $cupname . '-All', -1, MEMCACHE_COMPRESSED, $expire);
						}else{
							$this->mem_lib->set('Comp-Program-ListLeague-' . $cupname . '-All', $listCompLeague, MEMCACHE_COMPRESSED, $expire);
						}
						$result['cache_listLeague'] = false;
					}else{
						if($listCompLeague==-1){
							$listCompLeague = array();
						}
						$result['cache_listLeague'] = true;
					}
						
					$arrLeagueIDAll			=	array();
						
					foreach ( $listCompLeague as $value ){
						$arrLeagueIDAll[]		=	intval($value['league_id']);
					}

					$listMatchLeague 		= 	$this->match_model->get_by_listLeagueID($arrLeagueIDAll,'','','asc');
					$listMatchDummy			=	$this->matchcomp_model->loadListMatchDummy($id,0,'','','asc');

					foreach ( $listMatchLeague as $value ){
						if($value['MatchStatus']=='Fin'){
							$stat_data['Played']++;
							if(strlen($value['ETScore'])>1){
								$tmp_goal = explode("-", $value['ETScore']);
								
								//Team1KPID
								if(!isset($team_data[$value['Team1KPID']])){
									$team_data[$value['Team1KPID']] = array( 'GF' => 0,'GA' => 0 );
								}
								$team_data[$value['Team1KPID']]['GF'] = $team_data[$value['Team1KPID']]['GF'] + intval($tmp_goal[0]);
								$team_data[$value['Team1KPID']]['GA'] = $team_data[$value['Team1KPID']]['GA'] + intval($tmp_goal[1]);
								
								//Team2KPID
								if(!isset($team_data[$value['Team2KPID']])){
									$team_data[$value['Team2KPID']] = array( 'GF' => 0,'GA' => 0 );
								}
								$team_data[$value['Team2KPID']]['GF'] = $team_data[$value['Team2KPID']]['GF'] + intval($tmp_goal[1]);
								$team_data[$value['Team2KPID']]['GA'] = $team_data[$value['Team2KPID']]['GA'] + intval($tmp_goal[0]);
								
								$stat_data['Goal'] = $stat_data['Goal'] + intval($tmp_goal[0]) + intval($tmp_goal[1]);
							}else{
								//Team1KPID
								if(!isset($team_data[$value['Team1KPID']])){
									$team_data[$value['Team1KPID']] = array( 'GF' => 0,'GA' => 0 );
								}
								$team_data[$value['Team1KPID']]['GF'] = $team_data[$value['Team1KPID']]['GF'] + intval($value['Team1FTScore']);
								$team_data[$value['Team1KPID']]['GA'] = $team_data[$value['Team1KPID']]['GA'] + intval($value['Team2FTScore']);
								
								//Team2KPID
								if(!isset($team_data[$value['Team2KPID']])){
									$team_data[$value['Team2KPID']] = array( 'GF' => 0,'GA' => 0 );
								}
								$team_data[$value['Team2KPID']]['GF'] = $team_data[$value['Team2KPID']]['GF'] + intval($value['Team2FTScore']);
								$team_data[$value['Team2KPID']]['GA'] = $team_data[$value['Team2KPID']]['GA'] + intval($value['Team1FTScore']);
								
								$stat_data['Goal'] = $stat_data['Goal'] + intval($value['Team1FTScore']) + intval($value['Team2FTScore']);
							}
						}
					}
					
					foreach ( $listMatchDummy as $value ){
						if($value['MatchStatus']=='Fin'){
							$stat_data['Played']++;
							$stat_data['Goal'] = $stat_data['Goal'] + intval($value['Team1FTScore']) + intval($value['Team2FTScore']);								
						}
					}
					
					$this->mem_lib->set('Comp-StatInfo-'.$cupname, $stat_data, MEMCACHE_COMPRESSED, $expire_stat);
				}
				foreach($team_data as $key => $value){
					//TeamGF
					if($stat_data['TeamGF']['id'] == -1){
						$stat_data['TeamGF']['id'] = intval($key);
						$stat_data['TeamGF']['goal'] = $value['GF'];
					}elseif($stat_data['TeamGF']['goal'] < $value['GF']){
						$stat_data['TeamGF']['id'] = intval($key);
						$stat_data['TeamGF']['goal'] = $value['GF'];
					}
					//TeamGA
					if($stat_data['TeamGA']['id'] == -1){
						$stat_data['TeamGA']['id'] = intval($key);
						$stat_data['TeamGA']['goal'] = $value['GA'];
					}elseif($stat_data['TeamGA']['goal'] < $value['GA']){
						$stat_data['TeamGA']['id'] = intval($key);
						$stat_data['TeamGA']['goal'] = $value['GA'];
					}
				}
				
				if($stat_data['TeamGF']['id'] != -1){
					$teamDataTmp		=		$this->team_model->loadByID(intval($stat_data['TeamGF']['id']));
					$stat_data['TeamGF']['NameTH']			=	$teamDataTmp['NameTH'];
					$stat_data['TeamGF']['NameTHShort']		=	$teamDataTmp['NameTHShort'];
					$stat_data['TeamGF']['NameEN']			=	$teamDataTmp['NameEN'];
					$stat_data['TeamGF']['url']				=	'http://football.kapook.com/team-' . strtolower(str_replace(" ","-",$teamDataTmp['NameEN']));
					
					$Logo 						= 		str_replace(' ','-',$teamDataTmp['NameEN']).'.png';
					$Logo_MC					=		$this->mem_lib->get('Football2014-Team-Logo-' . $Logo);
					if($Logo_MC==true){
						$stat_data['TeamGF']['Logo'] 		= 		'http://football.kapook.com/uploads/logo/' . $Logo;
					}else{
						$stat_data['TeamGF']['Logo'] 		= 		'http://football.kapook.com/uploads/logo/default.png';
					}
				}
				
				if($stat_data['TeamGA']['id'] != -1){
					$teamDataTmp		=		$this->team_model->loadByID(intval($stat_data['TeamGA']['id']));
					$stat_data['TeamGA']['NameTH']			=	$teamDataTmp['NameTH'];
					$stat_data['TeamGA']['NameTHShort']		=	$teamDataTmp['NameTHShort'];
					$stat_data['TeamGA']['NameEN']			=	$teamDataTmp['NameEN'];
					$stat_data['TeamGA']['url']				=	'http://football.kapook.com/team-' . strtolower(str_replace(" ","-",$teamDataTmp['NameEN']));
					
					$Logo 						= 		str_replace(' ','-',$teamDataTmp['NameEN']).'.png';
					$Logo_MC					=		$this->mem_lib->get('Football2014-Team-Logo-' . $Logo);
					if($Logo_MC==true){
						$stat_data['TeamGA']['Logo'] 		= 		'http://football.kapook.com/uploads/logo/' . $Logo;
					}else{
						$stat_data['TeamGA']['Logo'] 		= 		'http://football.kapook.com/uploads/logo/default.png';
					}
				}
				
				$result['stat_zone']		=	$stat_data;
			}
		}
		
		// Print a data
		if($_REQUEST['type']=='json'){
			header('Content-Type: application/json');
			echo json_encode($result);
		}else{
			var_dump($result);
		}
	}
	
	function Table($cupname = '') {
	
		$result			=		array();
		$expire			=		3600;
		$expire_match 	= 		60;
		
		if(strlen($cupname) > 0){
			$result 	= 	$this->mem_lib->get('Comp-Info-'.$cupname);
			if (!$result){
				$result		=	$this->comp_model->loadByName($cupname);
				if (!$result){
					$result		=		-1;
				}
				$this->mem_lib->set('Comp-Info-'.$cupname, $result, MEMCACHE_COMPRESSED, $expire);
			}
			
			if ($result != -1){
				$id		=		$result['id'];
				$listTable 	= 	$this->mem_lib->get('Comp-Table-'.$cupname);
				if ((!$listTable)||($_REQUEST['remove_cache'] == 1)){
					/* Table Old Style */
					$listTable		=		$this->table_model->loadListTable($id,-1);
					foreach($listTable as $i => $tmpData){
						if($tmpData['type']==1){
							$dataListMatch		=		$this->match_model->get_by_listLeagueAndMatch($tmpData['league_list'],$tmpData['match_list']);
							$TableData			=		array();
							$dataListMatchID	=		array();

							foreach($dataListMatch as $tmpMatch){
								// Team 1
								$teamid_1	=	$tmpMatch['Team1KPID'];
								if(!isset($TableData[$tmpMatch['Team1KPID']])){
									
									$dataTeam	=	$this->team_model->loadByID(intval($teamid_1));

									if(!empty($dataTeam['NameTH'])){
										$strTeam	=	$dataTeam['NameTH'];
									}else if(!empty($dataTeam['NameTHShort'])){
										$strTeam	=	$dataTeam['NameTHShort'];
									}else{
										$strTeam	=	$dataTeam['NameEN'];
									}
									
									$Logo 						= 		str_replace(' ','-',$dataTeam['NameEN']).'.png';
									$Logo_MC					=		$this->mem_lib->get('Football2014-Team-Logo-' . $Logo);
									if($Logo_MC==true){
										$pathlogo 				= 		'http://football.kapook.com/uploads/logo/' . $Logo;
									}else{
										$pathlogo 				= 		'http://football.kapook.com/uploads/logo/default.png';
									}
									
									$TableData[$teamid_1]['name']			=		$strTeam;
									$TableData[$teamid_1]['NameTHShort']	=		$dataTeam['NameTHShort'];
									$TableData[$teamid_1]['NameEN']			=		$dataTeam['NameEN'];
									$TableData[$teamid_1]['url']			=		'http://football.kapook.com/team-' . strtolower(str_replace(" ","-",$dataTeam['NameEN']));
									$TableData[$teamid_1]['logo']			=		$pathlogo;
									$TableData[$teamid_1]['sp_point']		=		0;
									
									$TableData[$teamid_1]['win']			=		0;
									$TableData[$teamid_1]['draw']			=		0;
									$TableData[$teamid_1]['lose']			=		0;
									$TableData[$teamid_1]['point']			=		0;
									$TableData[$teamid_1]['goal_for']		=		0;
									$TableData[$teamid_1]['goal_against']	=		0;
									$TableData[$teamid_1]['goal_dif']		=		0;
									
								}
								// Team 2
								$teamid_2	=	$tmpMatch['Team2KPID'];
								if(!isset($TableData[$tmpMatch['Team2KPID']])){
									
									$dataTeam	=	$this->team_model->loadByID(intval($teamid_2));
									
									if(!empty($dataTeam['NameTH'])){
										$strTeam	=	$dataTeam['NameTH'];
									}else if(!empty($dataTeam['NameTHShort'])){
										$strTeam	=	$dataTeam['NameTHShort'];
									}else{
										$strTeam	=	$dataTeam['NameEN'];
									}
									
									$Logo 						= 		str_replace(' ','-',$dataTeam['NameEN']).'.png';
									$Logo_MC					=		$this->mem_lib->get('Football2014-Team-Logo-' . $Logo);
									if($Logo_MC==true){
										$pathlogo 				= 		'http://football.kapook.com/uploads/logo/' . $Logo;
									}else{
										$pathlogo 				= 		'http://football.kapook.com/uploads/logo/default.png';
									}
									
									$TableData[$teamid_2]['name']			=		$strTeam;
									$TableData[$teamid_2]['NameTHShort']	=		$dataTeam['NameTHShort'];
									$TableData[$teamid_2]['NameEN']			=		$dataTeam['NameEN'];
									$TableData[$teamid_2]['url']			=		'http://football.kapook.com/team-' . strtolower(str_replace(" ","-",$dataTeam['NameEN']));
									$TableData[$teamid_2]['logo']			=		$pathlogo;
									$TableData[$teamid_2]['sp_point']		=		0;
									
									$TableData[$teamid_2]['win']			=		0;
									$TableData[$teamid_2]['draw']			=		0;
									$TableData[$teamid_2]['lose']			=		0;
									$TableData[$teamid_2]['point']			=		0;
									$TableData[$teamid_2]['goal_for']		=		0;
									$TableData[$teamid_2]['goal_against']	=		0;
									$TableData[$teamid_2]['goal_dif']		=		0;
									
								}
								if($tmpMatch['MatchStatus']=='Fin'){
									//Team 1 > Team 2
									if($tmpMatch['Team1FTScore'] > $tmpMatch['Team2FTScore']){
										$TableData[$teamid_1]['win']++;
										$TableData[$teamid_1]['point']	=	$TableData[$teamid_1]['point']+3;
										
										$TableData[$teamid_2]['lose']++;
									}
									//Team 1 < Team 2
									else if($tmpMatch['Team1FTScore'] < $tmpMatch['Team2FTScore']){
										$TableData[$teamid_1]['lose']++;
										
										$TableData[$teamid_2]['win']++;
										$TableData[$teamid_2]['point']	=	$TableData[$teamid_2]['point']+3;
									}
									//Team 1 < Team 2
									else{
										$TableData[$teamid_1]['draw']++;
										$TableData[$teamid_1]['point']++;
										
										$TableData[$teamid_2]['draw']++;
										$TableData[$teamid_2]['point']++;
									}
									
									$TableData[$teamid_1]['goal_for']		=		$TableData[$teamid_1]['goal_for']		+		$tmpMatch['Team1FTScore'];
									$TableData[$teamid_1]['goal_against']	=		$TableData[$teamid_1]['goal_against']	+		$tmpMatch['Team2FTScore'];
									$TableData[$teamid_1]['goal_dif']		=		$TableData[$teamid_1]['goal_dif']		+		($tmpMatch['Team1FTScore']-$tmpMatch['Team2FTScore']);
										
									$TableData[$teamid_2]['goal_for']		=		$TableData[$teamid_2]['goal_for']		+		$tmpMatch['Team2FTScore'];
									$TableData[$teamid_2]['goal_against']	=		$TableData[$teamid_2]['goal_against']	+		$tmpMatch['Team1FTScore'];
									$TableData[$teamid_2]['goal_dif']		=		$TableData[$teamid_2]['goal_dif']		+		($tmpMatch['Team2FTScore']-$tmpMatch['Team1FTScore']);
									
								}
							}
							
							foreach($tmpData['team_list'] as $tmpTeamAndLeague){
								$tmpTeamID = $tmpTeamAndLeague['team_id'];
								$tmpLeagueID = $tmpTeamAndLeague['league_id'];
								
								$tmpListMatchByTeam = $this->match_model->get_by_LeagueAndTeam($tmpLeagueID,$tmpTeamID);
								
								foreach($tmpListMatchByTeam as $tmpMatch){
								
									if(!isset($TableData[$tmpTeamID])){

										$dataTeam	=	$this->team_model->loadByID(intval($tmpTeamID));

										if(!empty($dataTeam['NameTH'])){
											$strTeam	=	$dataTeam['NameTH'];
										}else if(!empty($dataTeam['NameTHShort'])){
											$strTeam	=	$dataTeam['NameTHShort'];
										}else{
											$strTeam	=	$dataTeam['NameEN'];
										}
											
										$Logo 						= 		str_replace(' ','-',$dataTeam['NameEN']).'.png';
										$Logo_MC					=		$this->mem_lib->get('Football2014-Team-Logo-' . $Logo);
										if($Logo_MC==true){
											$pathlogo 				= 		'http://football.kapook.com/uploads/logo/' . $Logo;
										}else{
											$pathlogo 				= 		'http://football.kapook.com/uploads/logo/default.png';
										}
											
										$TableData[$tmpTeamID]['name']			=		$strTeam;
										$TableData[$tmpTeamID]['NameTHShort']	=		$dataTeam['NameTHShort'];
										$TableData[$tmpTeamID]['NameEN']		=		$dataTeam['NameEN'];
										$TableData[$tmpTeamID]['url']			=		'http://football.kapook.com/team-' . strtolower(str_replace(" ","-",$dataTeam['NameEN']));
										$TableData[$tmpTeamID]['logo']			=		$pathlogo;
										$TableData[$tmpTeamID]['sp_point']		=		0;
											
										$TableData[$tmpTeamID]['win']			=		0;
										$TableData[$tmpTeamID]['draw']			=		0;
										$TableData[$tmpTeamID]['lose']			=		0;
										$TableData[$tmpTeamID]['point']			=		0;
										$TableData[$tmpTeamID]['goal_for']		=		0;
										$TableData[$tmpTeamID]['goal_against']	=		0;
										$TableData[$tmpTeamID]['goal_dif']		=		0;
									}
								
									if (in_array($tmpMatch["id"], $dataListMatchID)){
										continue;
									}
								
									if($tmpMatch['MatchStatus']=='Fin'){
										if($tmpMatch['Team2KPID'] == $tmpTeamID){
											$tmpScore = $tmpMatch['Team1FTScore'];
											$tmpMatch['Team1FTScore'] = $tmpMatch['Team2FTScore'];
											$tmpMatch['Team2FTScore'] = $tmpScore;
										}

										//Team 1 > Team 2
										if($tmpMatch['Team1FTScore'] > $tmpMatch['Team2FTScore']){
											$TableData[$tmpTeamID]['win']++;
											$TableData[$tmpTeamID]['point']	=	$TableData[$tmpTeamID]['point']+3;
										}
										//Team 1 < Team 2
										else if($tmpMatch['Team1FTScore'] < $tmpMatch['Team2FTScore']){
											$TableData[$tmpTeamID]['lose']++;
										}
										//Team 1 < Team 2
										else{
											$TableData[$tmpTeamID]['draw']++;
											$TableData[$tmpTeamID]['point']++;
										}
										
										$TableData[$tmpTeamID]['goal_for']		=		$TableData[$tmpTeamID]['goal_for']		+		$tmpMatch['Team1FTScore'];
										$TableData[$tmpTeamID]['goal_against']	=		$TableData[$tmpTeamID]['goal_against']	+		$tmpMatch['Team2FTScore'];
										$TableData[$tmpTeamID]['goal_dif']		=		$TableData[$tmpTeamID]['goal_dif']		+		($tmpMatch['Team1FTScore']-$tmpMatch['Team2FTScore']);
										
									}
								}
							}
							$listTable[$i]['TeamData']				=		$TableData;
						}else if($tmpData['type']==2){
							foreach($listTable[$i]['TeamData'] as $j => $tmpTeam){
								
								if(strlen($tmpTeam['logo'])<=0){
									$listTable[$i]['TeamData'][$j]['logo']		=		'http://football.kapook.com/uploads/logo/default.png';
								}
								$listTable[$i]['TeamData'][$j]['goal_dif']		=		$listTable[$i]['TeamData'][$j]['goal_for'] - $listTable[$i]['TeamData'][$j]['goal_against'];
								$listTable[$i]['TeamData'][$j]['point']			=		($listTable[$i]['TeamData'][$j]['win']*3) + $listTable[$i]['TeamData'][$j]['draw'];
								$listTable[$i]['TeamData'][$j]['index']			=		$j;
							}
						}else if($tmpData['type']==3){
							//NOT DO ANYTHING
						}
						
						if(($tmpData['type']==1)||(($tmpData['type']==2)&&($tmpData['type_order']==1))){
							$leaderboard		=		$listTable[$i]['TeamData'];
							usort($leaderboard,array("Api", "compare_point"));
							$listTable[$i]['TeamData']		=		$leaderboard;
						}
						
					}
					$this->mem_lib->set('Comp-Table-'.$cupname, $listTable, MEMCACHE_COMPRESSED, $expire);
					$result['table']		=	$listTable;
					$result['table_cache'] 	= 	false;
				}else{
					$result['table']	=	$listTable;
					$result['table_cache'] = 	true;
				}
				
				$listRound 	= 	$this->mem_lib->get('Comp-TableRound-'.$cupname);
				if ((!$listRound)||($_REQUEST['remove_cache'] == 1)){
					/* Table Round Style */
					$listRound		=		$this->roundtable_model->loadListRound($id);
					foreach($listRound as $iRound => $dataRound){
						$listRoundTable		=		$this->table_model->loadListTable($id,$dataRound['id']);
						foreach($listRoundTable as $i => $tmpData){
							if($tmpData['type']==1){
								$dataListMatch		=		$this->match_model->get_by_listLeagueAndMatch($tmpData['league_list'],$tmpData['match_list']);
								$TableData			=		array();
								$dataListMatchID	=		array();

								foreach($dataListMatch as $tmpMatch){
									// Team 1
									$teamid_1	=	$tmpMatch['Team1KPID'];
									if(!isset($TableData[$tmpMatch['Team1KPID']])){
										
										$dataTeam	=	$this->team_model->loadByID(intval($teamid_1));

										if(!empty($dataTeam['NameTH'])){
											$strTeam	=	$dataTeam['NameTH'];
										}else if(!empty($dataTeam['NameTHShort'])){
											$strTeam	=	$dataTeam['NameTHShort'];
										}else{
											$strTeam	=	$dataTeam['NameEN'];
										}
										
										$Logo 						= 		str_replace(' ','-',$dataTeam['NameEN']).'.png';
										$Logo_MC					=		$this->mem_lib->get('Football2014-Team-Logo-' . $Logo);
										if($Logo_MC==true){
											$pathlogo 				= 		'http://football.kapook.com/uploads/logo/' . $Logo;
										}else{
											$pathlogo 				= 		'http://football.kapook.com/uploads/logo/default.png';
										}
										
										$TableData[$teamid_1]['name']			=		$strTeam;
										$TableData[$teamid_1]['NameEN']			=		$dataTeam['NameEN'];
										$TableData[$teamid_1]['NameTHShort']	=		$dataTeam['NameTHShort'];
										$TableData[$teamid_1]['url']			=		'http://football.kapook.com/team-' . strtolower(str_replace(" ","-",$dataTeam['NameEN']));
										$TableData[$teamid_1]['logo']			=		$pathlogo;
										$TableData[$teamid_1]['sp_point']		=		0;
										
										$TableData[$teamid_1]['win']			=		0;
										$TableData[$teamid_1]['draw']			=		0;
										$TableData[$teamid_1]['lose']			=		0;
										$TableData[$teamid_1]['point']			=		0;
										$TableData[$teamid_1]['goal_for']		=		0;
										$TableData[$teamid_1]['goal_against']	=		0;
										$TableData[$teamid_1]['goal_dif']		=		0;
										
									}
									// Team 2
									$teamid_2	=	$tmpMatch['Team2KPID'];
									if(!isset($TableData[$tmpMatch['Team2KPID']])){
										
										$dataTeam	=	$this->team_model->loadByID(intval($teamid_2));
										
										if(!empty($dataTeam['NameTH'])){
											$strTeam	=	$dataTeam['NameTH'];
										}else if(!empty($dataTeam['NameTHShort'])){
											$strTeam	=	$dataTeam['NameTHShort'];
										}else{
											$strTeam	=	$dataTeam['NameEN'];
										}
										
										$Logo 						= 		str_replace(' ','-',$dataTeam['NameEN']).'.png';
										$Logo_MC					=		$this->mem_lib->get('Football2014-Team-Logo-' . $Logo);
										if($Logo_MC==true){
											$pathlogo 				= 		'http://football.kapook.com/uploads/logo/' . $Logo;
										}else{
											$pathlogo 				= 		'http://football.kapook.com/uploads/logo/default.png';
										}
										
										$TableData[$teamid_2]['name']			=		$strTeam;
										$TableData[$teamid_2]['NameTHShort']	=		$dataTeam['NameTHShort'];
										$TableData[$teamid_2]['NameEN']			=		$dataTeam['NaameEN'];
										$TableData[$teamid_2]['url']			=		'http://football.kapook.com/team-' . strtolower(str_replace(" ","-",$dataTeam['NameEN']));
										$TableData[$teamid_2]['logo']			=		$pathlogo;
										$TableData[$teamid_2]['sp_point']		=		0;
										
										$TableData[$teamid_2]['win']			=		0;
										$TableData[$teamid_2]['draw']			=		0;
										$TableData[$teamid_2]['lose']			=		0;
										$TableData[$teamid_2]['point']			=		0;
										$TableData[$teamid_2]['goal_for']		=		0;
										$TableData[$teamid_2]['goal_against']	=		0;
										$TableData[$teamid_2]['goal_dif']		=		0;
										
									}
									if($tmpMatch['MatchStatus']=='Fin'){
										//Team 1 > Team 2
										if($tmpMatch['Team1FTScore'] > $tmpMatch['Team2FTScore']){
											$TableData[$teamid_1]['win']++;
											$TableData[$teamid_1]['point']	=	$TableData[$teamid_1]['point']+3;
											
											$TableData[$teamid_2]['lose']++;
										}
										//Team 1 < Team 2
										else if($tmpMatch['Team1FTScore'] < $tmpMatch['Team2FTScore']){
											$TableData[$teamid_1]['lose']++;
											
											$TableData[$teamid_2]['win']++;
											$TableData[$teamid_2]['point']	=	$TableData[$teamid_2]['point']+3;
										}
										//Team 1 < Team 2
										else{
											$TableData[$teamid_1]['draw']++;
											$TableData[$teamid_1]['point']++;
											
											$TableData[$teamid_2]['draw']++;
											$TableData[$teamid_2]['point']++;
										}
										
										$TableData[$teamid_1]['goal_for']		=		$TableData[$teamid_1]['goal_for']		+		$tmpMatch['Team1FTScore'];
										$TableData[$teamid_1]['goal_against']	=		$TableData[$teamid_1]['goal_against']	+		$tmpMatch['Team2FTScore'];
										$TableData[$teamid_1]['goal_dif']		=		$TableData[$teamid_1]['goal_dif']		+		($tmpMatch['Team1FTScore']-$tmpMatch['Team2FTScore']);
											
										$TableData[$teamid_2]['goal_for']		=		$TableData[$teamid_2]['goal_for']		+		$tmpMatch['Team2FTScore'];
										$TableData[$teamid_2]['goal_against']	=		$TableData[$teamid_2]['goal_against']	+		$tmpMatch['Team1FTScore'];
										$TableData[$teamid_2]['goal_dif']		=		$TableData[$teamid_2]['goal_dif']		+		($tmpMatch['Team2FTScore']-$tmpMatch['Team1FTScore']);
										
									}
								}
								
								foreach($tmpData['team_list'] as $tmpTeamAndLeague){
									$tmpTeamID = $tmpTeamAndLeague['team_id'];
									$tmpLeagueID = $tmpTeamAndLeague['league_id'];
									
									$tmpListMatchByTeam = $this->match_model->get_by_LeagueAndTeam($tmpLeagueID,$tmpTeamID);
									
									foreach($tmpListMatchByTeam as $tmpMatch){
									
										if(!isset($TableData[$tmpTeamID])){

											$dataTeam	=	$this->team_model->loadByID(intval($tmpTeamID));

											if(!empty($dataTeam['NameTH'])){
												$strTeam	=	$dataTeam['NameTH'];
											}else if(!empty($dataTeam['NameTHShort'])){
												$strTeam	=	$dataTeam['NameTHShort'];
											}else{
												$strTeam	=	$dataTeam['NameEN'];
											}
												
											$Logo 						= 		str_replace(' ','-',$dataTeam['NameEN']).'.png';
											$Logo_MC					=		$this->mem_lib->get('Football2014-Team-Logo-' . $Logo);
											if($Logo_MC==true){
												$pathlogo 				= 		'http://football.kapook.com/uploads/logo/' . $Logo;
											}else{
												$pathlogo 				= 		'http://football.kapook.com/uploads/logo/default.png';
											}
												
											$TableData[$tmpTeamID]['name']			=		$strTeam;
											$TableData[$tmpTeamID]['NameTHShort']	=		$dataTeam['NameTHShort'];
											$TableData[$tmpTeamID]['NameEN']		=		$dataTeam['NaameEN'];
											$TableData[$tmpTeamID]['url']			=		'http://football.kapook.com/team-' . strtolower(str_replace(" ","-",$dataTeam['NameEN']));
											$TableData[$tmpTeamID]['logo']			=		$pathlogo;
											$TableData[$tmpTeamID]['sp_point']		=		0;
												
											$TableData[$tmpTeamID]['win']			=		0;
											$TableData[$tmpTeamID]['draw']			=		0;
											$TableData[$tmpTeamID]['lose']			=		0;
											$TableData[$tmpTeamID]['point']			=		0;
											$TableData[$tmpTeamID]['goal_for']		=		0;
											$TableData[$tmpTeamID]['goal_against']	=		0;
											$TableData[$tmpTeamID]['goal_dif']		=		0;
										}
									
										if (in_array($tmpMatch["id"], $dataListMatchID)){
											continue;
										}
									
										if($tmpMatch['MatchStatus']=='Fin'){
											if($tmpMatch['Team2KPID'] == $tmpTeamID){
												$tmpScore = $tmpMatch['Team1FTScore'];
												$tmpMatch['Team1FTScore'] = $tmpMatch['Team2FTScore'];
												$tmpMatch['Team2FTScore'] = $tmpScore;
											}

											//Team 1 > Team 2
											if($tmpMatch['Team1FTScore'] > $tmpMatch['Team2FTScore']){
												$TableData[$tmpTeamID]['win']++;
												$TableData[$tmpTeamID]['point']	=	$TableData[$tmpTeamID]['point']+3;
											}
											//Team 1 < Team 2
											else if($tmpMatch['Team1FTScore'] < $tmpMatch['Team2FTScore']){
												$TableData[$tmpTeamID]['lose']++;
											}
											//Team 1 < Team 2
											else{
												$TableData[$tmpTeamID]['draw']++;
												$TableData[$tmpTeamID]['point']++;
											}
											
											$TableData[$tmpTeamID]['goal_for']		=		$TableData[$tmpTeamID]['goal_for']		+		$tmpMatch['Team1FTScore'];
											$TableData[$tmpTeamID]['goal_against']	=		$TableData[$tmpTeamID]['goal_against']	+		$tmpMatch['Team2FTScore'];
											$TableData[$tmpTeamID]['goal_dif']		=		$TableData[$tmpTeamID]['goal_dif']		+		($tmpMatch['Team1FTScore']-$tmpMatch['Team2FTScore']);
											
										}
									}
								}
								$listRoundTable[$i]['TeamData']				=		$TableData;
							}else if($tmpData['type']==2){
								foreach($listRoundTable[$i]['TeamData'] as $j => $tmpTeam){
									
									if(strlen($tmpTeam['logo'])<=0){
										$listRoundTable[$i]['TeamData'][$j]['logo']		=		'http://football.kapook.com/uploads/logo/default.png';
									}
									$listRoundTable[$i]['TeamData'][$j]['goal_dif']		=		$listRoundTable[$i]['TeamData'][$j]['goal_for'] - $listRoundTable[$i]['TeamData'][$j]['goal_against'];
									$listRoundTable[$i]['TeamData'][$j]['point']		=		($listRoundTable[$i]['TeamData'][$j]['win']*3) + $listRoundTable[$i]['TeamData'][$j]['draw'];
									$listRoundTable[$i]['TeamData'][$j]['index']		=		$j;
								}
							}else if($tmpData['type']==3){
								//NOT DO ANYTHING
							}
							
							if(($tmpData['type']==1)||(($tmpData['type']==2)&&($tmpData['type_order']==1))){
								$leaderboard		=		$listRoundTable[$i]['TeamData'];
								usort($leaderboard,array("Api", "compare_point"));
								$listRoundTable[$i]['TeamData']		=		$leaderboard;
							}
						}
						$listRound[$iRound]['table']		=		$listRoundTable;
					}
					$this->mem_lib->set('Comp-TableRound-'.$cupname, $listRound, MEMCACHE_COMPRESSED, $expire);
					$result['table_round']		=	$listRound;
					$result['table_round_cache'] 	= 	false;
				}else{
					$result['table_round']	=	$listRound;
					$result['table_round_cache'] = 	true;
				}
			}
		}
		
		// Print a data
		if($_REQUEST['type']=='json'){
			header('Content-Type: application/json');
			echo json_encode($result);
		}else{
			var_dump($result);
		}
		
    }
	
	function News($cupname = '',$size = 12,$page = 1) {
	
		$result			=		array();
		$expire			=		3600;
		$expire_match 	= 		60;
		
		if(strlen($cupname) > 0){
			$result 	= 	$this->mem_lib->get('Comp-Info-'.$cupname);
			if ((!$result)||($_REQUEST['remove_cache'] == 1)){
				$result		=	$this->comp_model->loadByName($cupname);
				if (!$result){
					$result		=		-1;
				}
				$this->mem_lib->set('Comp-Info-'.$cupname, $result, MEMCACHE_COMPRESSED, $expire);
			}
			if($result != -1){
				$id		=		$result['id'];
				$news 	= 	$this->mem_lib->get('Comp-News-'. $size .'-'. $page .'-'.$cupname);
				if (!$news){
					$news	=	$this->news_model->getnews(
						$result['kapookfootball_News_Country'],
						$result['kapookfootball_News_CountryOtherKeyword'],
						$result['kapookfootball_News_CountryOtherKeyword2'],
						$result['kapookfootball_News_CountryOtherKeyword3'],
						intval($size),
						intval($page)
					);
					$this->mem_lib->set('Comp-News-'. $size .'-'. $page .'-'.$cupname, MEMCACHE_COMPRESSED, $expire);
				}
				$result['news']	=	$news;
			}
		}
		
		// Print a data
		if($_REQUEST['type']=='json'){
			header('Content-Type: application/json');
			echo json_encode($result);
		}else{
			var_dump($result);
		}
		
    }
	
	function Scorer($cupname = '') {
	
		$result			=		array();
		$expire			=		3600;
		$expire_match 	= 		60;
		
		if(strlen($cupname) > 0){
			$result 	= 	$this->mem_lib->get('Comp-Info-'.$cupname);
			if ((!$result)||($_REQUEST['remove_cache'] == 1)){
				$result		=	$this->comp_model->loadByName($cupname);
				$this->mem_lib->set('Comp-Info-'.$cupname, $result, MEMCACHE_COMPRESSED, $expire);
			}
			if($result != -1){
				$id		=		$result['id'];
				$dataScorer 	= 		$this->mem_lib->get('Comp-scorer-'.$cupname);
				if((!$dataScorer) || ($_REQUEST['remove_cache'] == 1)){
					$dataScorer	=	$this->scorer_model->loadByCompID($id,10);
					foreach($dataScorer as $key => $tmpData){
						$tmpTeam		=		$this->team_model->loadByID($tmpData['team_id']);
						
						$dataScorer[$key]['TeamKPID'] = $tmpData['team_id'];
						
						$Logo 			= 		str_replace(' ','-',$tmpTeam['NameEN']).'.png';
						$Logo_MC		=		$this->mem_lib->get('Football2014-Team-Logo-' . $Logo);
						if($Logo_MC==true){
							$dataScorer[$key]['TeamLogo'] 		= 		'http://football.kapook.com/uploads/logo/' . $Logo;
						}else{
							$dataScorer[$key]['TeamLogo'] 		= 		'http://football.kapook.com/uploads/logo/default.png';
						}
						
						if(!empty($tmpTeam['NameTH'])){
							$strTeam	=	$tmpTeam['NameTH'];
						}else if(!empty($tmpTeam['NameTHShort'])){
							$strTeam	=	$tmpTeam['NameTHShort'];
						}else{
							$strTeam	=	$tmpTeam['NameEN'];
						}
						$dataScorer[$key]['TeamName']			=		$strTeam;
						$dataScorer[$key]['TeamNameShort']		=		$tmpTeam['NameTHShort'];
						
						$dataScorer[$key]['TeamURL']			=		'http://football.kapook.com/team-' . strtolower(str_replace(" ","-",$tmpTeam['NameEN']));
						
						$tmpPlayer		=		$this->player_model->loadByID($tmpData['player_id']);
						
						$dataScorer[$key]['PlayerName']			=		$tmpPlayer['name'];
						
						if(strlen($tmpPlayer['img'])){
							$dataScorer[$key]['PlayerImg']		=		$tmpPlayer['img'];
							$dataScorer[$key]['Picture']		=		$tmpPlayer['img'];
						}else{
							$dataScorer[$key]['PlayerImg']		=		'http://football.kapook.com/uploads/scorer/noface.jpg';
							$dataScorer[$key]['Picture']		=		'http://football.kapook.com/uploads/scorer/noface.jpg';
						}
						$dataScorer[$key]['Goal']				=		$dataScorer[$key]['goal'];
						$dataScorer[$key]['NameTH']				=		$tmpPlayer['name'];
						
					}
					$this->mem_lib->set('Comp-scorer-'.$cupname, $dataScorer, MEMCACHE_COMPRESSED, $expire);
				}
				$result['scorer']	=	$dataScorer;
			}
		}
		
		// Print a data
		if($_REQUEST['type']=='json'){
			header('Content-Type: application/json');
			echo json_encode($result);
		}else{
			var_dump($result);
		}
		
    }
	
	function market() {
		
		if(intval($_REQUEST['page']) > 0){
			$page = intval($_REQUEST['page']);
		}else{
			$page = 1;
		}
		
		if(intval($_REQUEST['size']) > 0){
			$size = intval($_REQUEST['size']);
		}else{
			$size = 10;
		}
		
		$data_market = $this->market_model->loadSortByTime(10,$page);
		foreach($data_market['list'] as $key => $item){
			// Player
			$tmpPlayer		=	$this->player_model->loadByID(intval($item['PlayerID']));
			$data_market['list'][$key]['PlayerData'] = $tmpPlayer;
			//var_dump($tmpPlayer);
			// Team Out
			$tmpTeamOut		=	$this->team_model->loadByID(intval($item['FromTeamKPID']));
			$Logo 			= 	str_replace(' ','-',$tmpTeamOut['NameEN']).'.png';
			$Logo_MC		=	$this->mem_lib->get('Football2014-Team-Logo-' . $Logo);
			if($Logo_MC==true){
				$tmpTeamOut['Logo'] 	= 		'http://football.kapook.com/uploads/logo/' . $Logo;
			}else{
				$tmpTeamOut['Logo'] 	= 		'http://football.kapook.com/uploads/logo/default.png';
			}
			$data_market['list'][$key]['TeamOutData'] = $tmpTeamOut;
			// Team In
			$tmpTeamIn		=	$this->team_model->loadByID(intval($item['ToTeamKPID']));
			$Logo 			= 	str_replace(' ','-',$tmpTeamIn['NameEN']).'.png';
			$Logo_MC		=	$this->mem_lib->get('Football2014-Team-Logo-' . $Logo);
			if($Logo_MC==true){
				$tmpTeamIn['Logo'] 	= 		'http://football.kapook.com/uploads/logo/' . $Logo;
			}else{
				$tmpTeamIn['Logo'] 	= 		'http://football.kapook.com/uploads/logo/default.png';
			}
			$data_market['list'][$key]['TeamInData'] = $tmpTeamIn;
		}
		// Print a data
		if($_REQUEST['type']=='json'){
			header('Content-Type: application/json');
			echo json_encode($data_market);
		}else{
			var_dump($data_market);
		}
	}
}
