<?php

class upload_lib
{
    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct()
    {
    }
	
	function uploadImage($file,$filename,$path){

		if($file['error'] != 0){
			return array(-1);
		}
					
		if(	($file['type'] != 'image/gif')
		&& 	($file['type'] != 'image/jpeg')
		&& 	($file['type'] != 'image/jpg')
		&& 	($file['type'] != 'image/pjpeg')
		&& 	($file['type'] != 'image/x-png')
		&& 	($file['type'] != 'image/png')
		){
			return array(-2);
		}

		// gif
		if($file['type'] == 'image/gif'){
			$extension	=	'gif';
		}
						
		// jpeg
		if(	($file['type'] == 'image/jpeg')
		||	($file['type'] == 'image/jpg')
		||	($file['type'] == 'image/pjpeg')
		){
			$extension	=	'jpg';
		}
						
		// png
		if(	($file['type'] == 'image/x-png')
		||	($file['type'] == 'image/png'))
		{
			$extension	=	'png';
		}
		
		$tmp_name = $file["tmp_name"];
		
		// Check directory for upload
		if(!is_dir($path)){
			if(!mkdir($path,0777,true)){
				return array(-3);
			}
		}
		// loop upload
		if(!move_uploaded_file($tmp_name, $path . $filename . '.' . $extension )){
			return array(-4);
		}
		return array( 1 , $filename . '.' . $extension );
	}
}