<?php
if (!defined('MINIZONE')) exit;
if (!class_exists('Memcache')) die('Class Memcache Not Found');

Class Memcache_Lib extends Memcache
{
var $minizone;
var $config;

///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
function __construct($name='default')
{
$this->minizone = minizone::getzone();
$this->config = $this->minizone->config('memcache', $name);
for ($i = 0, $m = sizeof($this->config); $i < $m; $i++) {
parent::addserver($this->config[$i]['host'], $this->config[$i]['port']);
}
}

}
/* End of file couchbase_Lib.php */
/* Location: ./system/library/couchbase_Lib.php */
