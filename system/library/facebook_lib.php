<?php
include_once dirname(__FILE__).'/social/facebook.php';
class Facebook_lib extends Facebook {

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct() {
        /* -- ========= Set : Minizone ========= -- */
        $this->minizone = minizone::getzone();

        /* -- Load : Config (social) -- */
        $this->config = $this->minizone->config('social');
        
        /* -- Load : Library (Facebook) -- */
        $this->minizone->library('social/facebook');
        parent::__construct(array('appId' => $this->config['facebook']['id'], 'secret' => $this->config['facebook']['secret']));
    }
    
    function getLoginLink($options=array()) {
        $params['scope'] = 'email,user_birthday,publish_actions';
        if(!empty($options['callbackURL'])) $params['redirect_uri'] = $options['callbackURL'];
        return parent::getLoginUrl($params);
    }
    
    function checkUserDenied() {
        if (isset($_GET['error_reason']) && $_GET['error_reason'] == 'user_denied') {
            return TRUE;
        }
    }
    
    function checkUser() {
        $this->facebook_lib = $this->minizone->library('facebook_lib');
        $fb_id = $this->facebook_lib->getUser();
        $fb_profile = $this->facebook_lib->api('/me');
        $fb_profile['token'] = $this->facebook_lib->getAccessToken();
        $this->fbuser_model = $this->minizone->model('fbuser_model');
        if($this->fbuser_model->exists($fb_id)) {
            $this->fbuser_model->update($fb_profile);
            return true;
        }
        $this->fbuser_model->add($fb_profile); 
        return true;
    }

    ///////////////////////////////////////////////// Post (Facebook) /////////////////////////////////////////////////
    function postToWall($data) {
        /* -- Condition : True -- */
        if (defined('FACEBOOK') && FACEBOOK == 1 && defined('TOKEN')) {
            $data['access_token'] = TOKEN;
            try {
                $return = $this->facebook->api('/me/feed', 'POST', $data);
            } catch (FacebookApiException $e) {
//                header('Location: '.$this->facebook->getLoginUrl(array('scope'=>'email, user_birthday, publish_actions')));
                return FALSE;
            }
            /* -- Return -- */
            return $return['id'];
        }

        /* -- Condition : False -- */ else {
            
        }
    }

}

/* End of file facebook_lib.php */
/* Location: ./application/libraries/facebook_lib.php */