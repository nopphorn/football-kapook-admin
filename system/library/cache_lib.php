<?php

class Cache_lib
{
    var $_cacheDir;
    var $_file;
    var $_path;
    var $log = array();

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct()
    {
        $this->_cacheDir = 'cache';
    }

    ///////////////////////////////////////////////// Get /////////////////////////////////////////////////
    function get($id, $path='www/default', $life=0)
    {
        $this->_setFileName($id, $path);
        clearstatcache();
        
        if (file_exists($this->_file)) {
            if (($life<=0) || (@filemtime($this->_file)+$life>time())) {	
                $fp = @fopen($this->_file, 'rb');
//                @flock($fp, LOCK_SH);
                
                if ($fp) {
                    $length = @filesize($this->_file);
                    $mqr = get_magic_quotes_runtime();
                    //set_magic_quotes_runtime(0);
                    
                    if ($length)
                        $data = @fread($fp, $length);
                    else
                        $data = '';
                    //set_magic_quotes_runtime($mqr);
                    
//                    @flock($fp, LOCK_UN);
                    @fclose($fp);
                    
                    $this->log['get']++;
                    return $data;
                }
            }
        }
        
        /* -- Return -- */
        return FALSE;
    }

    ///////////////////////////////////////////////// Save /////////////////////////////////////////////////
    function save($data, $id='', $path='')
    {
        if (!empty($id)) 
            $this->_setFileName($id, $path);
        
        $try = 1;
        while ($try<=2) {	
            if ($try==1) {
                $dir = explode('/',$this->_path);
                $cdir = $this->_cacheDir;
                
                for ($i=0;$i<count($dir);$i++) {
                    $cdir.=$dir[$i].'/';
                    if (!is_dir($cdir)) 
                        mkdir($cdir, 0777);
                }

                $try = 2;
            }   
            else 
                return FALSE;  

            $fp = @fopen($this->_file, "wb");
            
            if (@$fp) {
                //@flock($fp, LOCK_EX);
                $len = strlen($data);
                @fwrite($fp, $data, $len);
                //@flock($fp, LOCK_UN);
                @fclose($fp);
                
                $this->log['save']++;
                return TRUE;
            }
        }
        
        /* -- Return -- */
        return FALSE;
    }

    ///////////////////////////////////////////////// Delete /////////////////////////////////////////////////
    function delete($id='', $path='www/default')
    {
        $this->_setFileName($id, $path);
        
        /* -- Return -- */
        return $this->_unlink($this->_file);
    }

    ///////////////////////////////////////////////// setFileName /////////////////////////////////////////////////
    function _setFileName($id, $path)
    {
        $this->_path = $path;
        $this->_file = $this->_cacheDir.$path.'/cache_'.$id;
    }

    ///////////////////////////////////////////////// Clean /////////////////////////////////////////////////
    function clean($path='')
    {
        /* -- Return -- */
        return $this->_cleanDir($this->_cacheDir.$path.'/');
    }

    ///////////////////////////////////////////////// Unlink /////////////////////////////////////////////////
    function _unlink($file)
    {
        if (file_exists($file)) {
            if (!@unlink($file)) 
                return FALSE;
        }
        
        /* -- Return -- */
        return TRUE;        
    }

    ///////////////////////////////////////////////// cleanDir /////////////////////////////////////////////////
    function _cleanDir($dir)
    {
        $motif = 'cache_';
        
        if (!($dh = opendir($dir))) {
            return;
        }
        
        $result = TRUE;
        while ($file = readdir($dh)) {
            if (($file != '.') && ($file != '..')) {
                $file2 = $dir.$file;
                
                if (substr($file, 0, strlen($motif))==$motif) {
                    if (is_file($file2)) {			
                        if (strpos($file2, $motif, 0))
                            $result = ($result and ($this->_unlink($file2)));
                    //break;
                    }
                }
            }
        }
        
        /* -- Return -- */
        return FALSE;
    }

}
/* End of file cache_lib.php */
/* Location: ./system/library/cache_lib.php */