<?php

class zone_lib 
{
    var $minizone;

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct()
    {
        $this->minizone = minizone::getzone();
        $this->cache_lib = $this->minizone->library('cache_lib');
    }
    
    ///////////////////////////////////////////////// Get : Breadcrumb (By ID) /////////////////////////////////////////////////
    function get_breadcrumb_byID($zone_id)
    {
        $breadcrumb[1] = array('link'=>BASE_HREF, 'name'=>'หน้าแรก');
        
        /* -- Set : Cache zone -- */
        $cache_zone = "zone_{$zone_id}";
        $data_zone = $this->cache_lib->get($cache_zone, '/zone');
        $data_zone = unserialize($data_zone);
        
        if ($data_zone['isChild'] > 0) {
            $breadcrumb[2] = array('link'=>BASE_HREF.$data_zone['zoneMainURL'], 'name'=>$data_zone['zoneMain']);
            $breadcrumb[3] = array('link'=>BASE_HREF.$data_zone['url'], 'name'=>$data_zone['zonename']);
        } else {
            $breadcrumb[2] = array('link'=>BASE_HREF.$data_zone['url'], 'name'=>$data_zone['zonename']);
        }

        /* -- Return -- */
        return $breadcrumb;
    }
    
    ///////////////////////////////////////////////// Get : Breadcrumb /////////////////////////////////////////////////
    function get_breadcrumb_byURL($urlMain='', $urlSub='')
    {
        $breadcrumb[1] = array('link'=>BASE_HREF, 'name'=>'หน้าแรก');
        
        if ($urlMain=='' && $urlSub=='') {
            /* -- Return -- */
            return $breadcrumb;
        }
        
        list ($zone_id) = $this->get_zoneid($urlMain, $urlSub);
        
        /* -- Set : Cache zone -- */
        $cache_zone = "zone_{$zone_id}";
        $data_zone = $this->cache_lib->get($cache_zone, '/zone');
        $data_zone = unserialize($data_zone);
        
        if ($data_zone['isChild'] > 0) {
            $breadcrumb[2] = array('link'=>BASE_HREF.$data_zone['zoneMainURL'], 'name'=>$data_zone['zoneMain']);
            $breadcrumb[3] = array('link'=>BASE_HREF.$data_zone['url'], 'name'=>$data_zone['zonename']);
        } else {
            $breadcrumb[2] = array('link'=>BASE_HREF.$data_zone['url'], 'name'=>$data_zone['zonename']);
        }

        /* -- Return -- */
        return $breadcrumb;
    }
    
    ///////////////////////////////////////////////// Get : Zone ID /////////////////////////////////////////////////
    function get_zoneid($urlMain, $urlSub='')
    {
        /* -- Set : Portal & Zone -- */
        $cache_zoneURL = unserialize($this->cache_lib->get('zone_url', '/zone'));
        
        /* -- Condition : Have URL Sub -- */
        if ($urlSub != '') {
            $data_zoneURL = $cache_zoneURL[$urlMain]['subzone'][$urlMain.'/'.$urlSub];
        }
        
        /* -- Condition : No URL Sub -- */
        else {
            $data_zoneURL = $cache_zoneURL[$urlMain];
        }
        
        /* -- Return -- */
        return array($data_zoneURL['zoneid'], $data_zoneURL['subzone']);
    }
        
}
/* End of file zone_lib.php */
/* Location: ./library/zone_lib.php */