<?php

class getIP_lib 
{
	
    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct()
    { 
        header ('Content-type: text/html; charset=utf-8');
    }

    ///////////////////////////////////////////////// Get : User IP /////////////////////////////////////////////////
    function getUserIP() 
    {
        if (isset($_SERVER)) {
            if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                return $_SERVER['HTTP_X_FORWARDED_FOR'];

            if (isset($_SERVER['HTTP_CLIENT_IP']))
                return $_SERVER['HTTP_CLIENT_IP'];

            return $_SERVER['REMOTE_ADDR'];
        }

        if (getenv('HTTP_X_FORWARDED_FOR'))
            return getenv('HTTP_X_FORWARDED_FOR');

        if (getenv('HTTP_CLIENT_IP'))
            return getenv('HTTP_CLIENT_IP');

        /* -- Return -- */
        return getenv('REMOTE_ADDR');
    }

}
/* End of file getIP_lib.php */
/* Location: ./system/library/getIP_lib.php */