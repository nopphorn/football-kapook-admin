<?php

class pager_lib
{
    
    ///////////////////////////////////////////////// Get : Data /////////////////////////////////////////////////
    function get_data($limit, $total_data, $link, $page, $option=null, $ajax=false) 
    {
        /* -- Set : Page -- */
        if ($page<1) $page=1;
        
        /* -- Set : Total Page -- */
        $total_page = ceil($total_data/$limit);
        if ($total_page==1) return FALSE;
        
        /* -- Set : Option (Default) -- */
        if (!$option) {
            $option['prev'] = 'prev';
            $option['next'] = '&raquo;';
            $option['pipe'] = '...';
            $option['disabled'] = 'disabled';
            $option['active'] = 'active';
        }
        
        /* -- Set : Prev -- */
        if ($page >= 1) {
            $prev_page = $page-1;
            
            if ($ajax) {
                if ($page == 1)
                    $data_pager[0] = array('label'=>"{$option['prev']}", 'link'=>"javascript:void(0);", 'class'=>"class='{$option['disabled']}'");
                else
                    $data_pager[0] = array('label'=>"{$option['prev']}", 'link'=>"javascript:{$link}({$prev_page});");
            } else {
                if ($page == 1)
                    $data_pager[0] = array('label'=>"{$option['prev']}", 'link'=>"javascript:void(0);", 'class'=>"class='{$option['disabled']}'");
                else
                    $data_pager[0] = array('label'=>"{$option['prev']}", 'link'=>$link.$prev_page);
            }
        }
       
       /* -- Set : Next -- */
        if ($page<=$total_page && $total_page>1) {
            $next_page = $page+1;
            
            if ($ajax) {
                if ($page == $total_page)
                    $arr_next = array('label'=>"{$option['next']}", 'link'=>"javascript:void(0);", 'class'=>"class='{$option['disabled']}'");
                else
                    $arr_next = array('label'=>"{$option['next']}", 'link'=>"javascript:{$link}({$next_page});");
            } else {
                if ($page == $total_page)
                    $arr_next = array('label'=>"{$option['next']}", 'link'=>"javascript:void(0);", 'class'=>"class='{$option['disabled']}'");
                else
                    $arr_next = array('label'=>"{$option['next']}", 'link'=>$link.$next_page);
            }
        }
        
        /* -- Set : Initial Space -- */
        $dotted = 0;
        $dotspace = 3;
        
        /* -- Calculate : Space -- */
        $dotend = $total_page - $dotspace;
        $curdotend = $page - $dotspace;
        $curdotstart = $page + $dotspace;
        
        /* -- Set : Each page label -- */
        for ($i=1; $i<=$total_page; $i++) {
            
            if (($i >= $dotspace && $i <= $curdotend) || ($i >= $curdotstart && $i < $dotend)) {
                if (!$dotted) $data_pager[] = array('label'=>"{$option['pipe']}", 'link'=>"javascript:void(0);", 'class'=>"class='{$option['disabled']}'");
                $dotted = 1;
                continue;
            }
           
            $dotted = 0;
            
            if ($i != $page) {
                if ($ajax) {
                    $data_pager[] = array('label'=>$i, 'link'=>"javascript:{$link}({$i});");
                } else {
                    $data_pager[] = array('label'=>$i, 'link'=>$link.$i);
                }
            } else {
                $data_pager[] = array('label'=>$i, 'link'=>"javascript:void(0);", 'class'=>"class='{$option['active']}'");
            }
        }
        
        /* -- Combine -- */
        $data_pager[] = $arr_next;
        
//        $arr[0] = array('label'=>'&laquo;', 'link'=>"javascript:void(0);", 'class'=>'class="disabled"'); // prev
//        $arr[1] = array('label'=>1, 'link'=>$link, 'class'=>'class="active"');
//        $arr[2] = array('label'=>2, 'link'=>"javascript:void(0);");
//        $arr[3] = array('label'=>'&raquo;', 'link'=>$link); // next
        
        /* -- Return -- */
        return $data_pager;
    }
    
    ///////////////////////////////////////////////// page_css /////////////////////////////////////////////////
    function page_css($rpp, $count, $href, $page=1,$jax = false) 
    {
        if ($page<1) $page=1;
        $pages = ceil($count / $rpp);
        $pager = "";
        
       if($page>1) {
           if($jax){
               $page1 = "<a href=\"#\" onclick=\"".$href."('".($page-1)."');return false;\" class=\"pre_next_bt\"><img src=\"images/previous_bt.gif\" alt=\"\" /></a>";
           }else{
               $page1 = "<div class=\"back\" ><a href=\"{$href}".($page-1)."\" >&laquo;&nbsp; Back</a></div>";
           }
       }
       if($page<$pages&&$pages>1) {
           if($jax){
               $page2 = "<a href=\"#\" onclick=\"".$href."('".($page+1)."');return false;\" class=\"page1\">Next</a>";
           }else{
               $page2 = "<div class=\"next\" ><a href=\"{$href}".($page+1)."\" > Next&nbsp;&raquo;</a></div>";
           }
       }
        if ($count) {
            $pagerarr = array();
            $dotted = 0;
            $dotspace = 3;
            $dotend = $pages - $dotspace;
            $curdotend = $page - $dotspace;
            $curdotstart = $page + $dotspace;
            for ($i = 1; $i <= $pages; $i++) {
               if (($i >= $dotspace && $i <= $curdotend) || ($i >= $curdotstart && $i < $dotend)) {
                   if (!$dotted) $pagerarr[] = "<li class='pipe'> | </li>";
                   $dotted = 1;
                   continue;
               }
                $dotted = 0;
                $start = $i * $rpp + 1;
                $end = $start + $rpp - 1;
                if ($end > $count) $end = $count;
                $text = "$start&nbsp;-&nbsp;$end";
                if ($i != $page) {
                    if($jax){
                        $pagerarr[] = "<li><a href=\"#\" onclick=\"".$href."('$i');return false;\" class=\"page1\">$i</a></li>";
                    }else{
                        $pagerarr[] = "<li><a href=\"{$href}$i\">$i</a></li>";
                    }
                }else{
                    $pagerarr[] = "<li><a class=\"here\">$i</a></li>";
                }
            }
            
            $pagerstr = join(" ", $pagerarr);
            if ($page > 1 ) {
                $to = $rpp * $page;
                $t =  $page * 2;
                if($to > $count ) {
                    $to = $count;
                }
                $t  = (($page-1) * $rpp) + 1;
            }
            else {
                $to = $rpp;
                $t = $page;
                if($count < $to ) $to = $count;
            }
            $show = "<span align=\"left\">Showing <b> $t </b>to <b>$to</b> of <b>$count</b>  </span>";
            $pagertop = "<div id=\"navi\"><ul>$page1 $pagerstr $page2</ul><div class=\"clear-div\"></div></div>";
        }
        else {
            $pagertop = "<div id=\"navi\"><ul>$page1 $pager $page2</ul><div class=\"clear-div\"></div></div>";
        }
        
        $start = ($page-1) * $rpp;
        
        /* -- Return -- */
        return array($pagertop, "LIMIT $start,$rpp",$show);
    }
    
    ///////////////////////////////////////////////// page_cal /////////////////////////////////////////////////
    function page_cal($limit, $total, $page=1) 
    {
        if ($page < 1) $page = 1;
        
        $total_page = ceil($total/$limit);
        $start = ($page-1)*$limit;
        
        /* -- Return -- */
        return array($total_page, " LIMIT $start, $limit");
    }
    
}
/* End of file pager_lib.php */
/* Location: ./system/library/pager_lib.php */